<?php

use App\Models\Spot_Shipment_Category;
use App\Repositories\Spot_Shipment_CategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_CategoryRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_CategoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_CategoryRepository
     */
    protected $spotShipmentCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentCategoryRepo = App::make(Spot_Shipment_CategoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Category()
    {
        $spotShipmentCategory = $this->fakeSpot_Shipment_CategoryData();
        $createdSpot_Shipment_Category = $this->spotShipmentCategoryRepo->create($spotShipmentCategory);
        $createdSpot_Shipment_Category = $createdSpot_Shipment_Category->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Category);
        $this->assertNotNull($createdSpot_Shipment_Category['id'], 'Created Spot_Shipment_Category must have id specified');
        $this->assertNotNull(Spot_Shipment_Category::find($createdSpot_Shipment_Category['id']), 'Spot_Shipment_Category with given id must be in DB');
        $this->assertModelData($spotShipmentCategory, $createdSpot_Shipment_Category);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Category()
    {
        $spotShipmentCategory = $this->makeSpot_Shipment_Category();
        $dbSpot_Shipment_Category = $this->spotShipmentCategoryRepo->find($spotShipmentCategory->id);
        $dbSpot_Shipment_Category = $dbSpot_Shipment_Category->toArray();
        $this->assertModelData($spotShipmentCategory->toArray(), $dbSpot_Shipment_Category);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Category()
    {
        $spotShipmentCategory = $this->makeSpot_Shipment_Category();
        $fakeSpot_Shipment_Category = $this->fakeSpot_Shipment_CategoryData();
        $updatedSpot_Shipment_Category = $this->spotShipmentCategoryRepo->update($fakeSpot_Shipment_Category, $spotShipmentCategory->id);
        $this->assertModelData($fakeSpot_Shipment_Category, $updatedSpot_Shipment_Category->toArray());
        $dbSpot_Shipment_Category = $this->spotShipmentCategoryRepo->find($spotShipmentCategory->id);
        $this->assertModelData($fakeSpot_Shipment_Category, $dbSpot_Shipment_Category->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Category()
    {
        $spotShipmentCategory = $this->makeSpot_Shipment_Category();
        $resp = $this->spotShipmentCategoryRepo->delete($spotShipmentCategory->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Category::find($spotShipmentCategory->id), 'Spot_Shipment_Category should not exist in DB');
    }
}
