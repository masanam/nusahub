<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_LocationApiTest extends TestCase
{
    use MakeSpot_Container_LocationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Container_Location()
    {
        $spotContainerLocation = $this->fakeSpot_Container_LocationData();
        $this->json('POST', '/api/v1/spotContainerLocations', $spotContainerLocation);

        $this->assertApiResponse($spotContainerLocation);
    }

    /**
     * @test
     */
    public function testReadSpot_Container_Location()
    {
        $spotContainerLocation = $this->makeSpot_Container_Location();
        $this->json('GET', '/api/v1/spotContainerLocations/'.$spotContainerLocation->id);

        $this->assertApiResponse($spotContainerLocation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Container_Location()
    {
        $spotContainerLocation = $this->makeSpot_Container_Location();
        $editedSpot_Container_Location = $this->fakeSpot_Container_LocationData();

        $this->json('PUT', '/api/v1/spotContainerLocations/'.$spotContainerLocation->id, $editedSpot_Container_Location);

        $this->assertApiResponse($editedSpot_Container_Location);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Container_Location()
    {
        $spotContainerLocation = $this->makeSpot_Container_Location();
        $this->json('DELETE', '/api/v1/spotContainerLocations/'.$spotContainerLocation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotContainerLocations/'.$spotContainerLocation->id);

        $this->assertResponseStatus(404);
    }
}
