<?php

use App\Models\Spot_Shipment_Treasury;
use App\Repositories\Spot_Shipment_TreasuryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_TreasuryRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_TreasuryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_TreasuryRepository
     */
    protected $spotShipmentTreasuryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentTreasuryRepo = App::make(Spot_Shipment_TreasuryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Treasury()
    {
        $spotShipmentTreasury = $this->fakeSpot_Shipment_TreasuryData();
        $createdSpot_Shipment_Treasury = $this->spotShipmentTreasuryRepo->create($spotShipmentTreasury);
        $createdSpot_Shipment_Treasury = $createdSpot_Shipment_Treasury->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Treasury);
        $this->assertNotNull($createdSpot_Shipment_Treasury['id'], 'Created Spot_Shipment_Treasury must have id specified');
        $this->assertNotNull(Spot_Shipment_Treasury::find($createdSpot_Shipment_Treasury['id']), 'Spot_Shipment_Treasury with given id must be in DB');
        $this->assertModelData($spotShipmentTreasury, $createdSpot_Shipment_Treasury);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Treasury()
    {
        $spotShipmentTreasury = $this->makeSpot_Shipment_Treasury();
        $dbSpot_Shipment_Treasury = $this->spotShipmentTreasuryRepo->find($spotShipmentTreasury->id);
        $dbSpot_Shipment_Treasury = $dbSpot_Shipment_Treasury->toArray();
        $this->assertModelData($spotShipmentTreasury->toArray(), $dbSpot_Shipment_Treasury);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Treasury()
    {
        $spotShipmentTreasury = $this->makeSpot_Shipment_Treasury();
        $fakeSpot_Shipment_Treasury = $this->fakeSpot_Shipment_TreasuryData();
        $updatedSpot_Shipment_Treasury = $this->spotShipmentTreasuryRepo->update($fakeSpot_Shipment_Treasury, $spotShipmentTreasury->id);
        $this->assertModelData($fakeSpot_Shipment_Treasury, $updatedSpot_Shipment_Treasury->toArray());
        $dbSpot_Shipment_Treasury = $this->spotShipmentTreasuryRepo->find($spotShipmentTreasury->id);
        $this->assertModelData($fakeSpot_Shipment_Treasury, $dbSpot_Shipment_Treasury->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Treasury()
    {
        $spotShipmentTreasury = $this->makeSpot_Shipment_Treasury();
        $resp = $this->spotShipmentTreasuryRepo->delete($spotShipmentTreasury->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Treasury::find($spotShipmentTreasury->id), 'Spot_Shipment_Treasury should not exist in DB');
    }
}
