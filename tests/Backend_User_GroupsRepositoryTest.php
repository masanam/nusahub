<?php

use App\Models\Backend_User_Groups;
use App\Repositories\Backend_User_GroupsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Backend_User_GroupsRepositoryTest extends TestCase
{
    use MakeBackend_User_GroupsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Backend_User_GroupsRepository
     */
    protected $backendUserGroupsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->backendUserGroupsRepo = App::make(Backend_User_GroupsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBackend_User_Groups()
    {
        $backendUserGroups = $this->fakeBackend_User_GroupsData();
        $createdBackend_User_Groups = $this->backendUserGroupsRepo->create($backendUserGroups);
        $createdBackend_User_Groups = $createdBackend_User_Groups->toArray();
        $this->assertArrayHasKey('id', $createdBackend_User_Groups);
        $this->assertNotNull($createdBackend_User_Groups['id'], 'Created Backend_User_Groups must have id specified');
        $this->assertNotNull(Backend_User_Groups::find($createdBackend_User_Groups['id']), 'Backend_User_Groups with given id must be in DB');
        $this->assertModelData($backendUserGroups, $createdBackend_User_Groups);
    }

    /**
     * @test read
     */
    public function testReadBackend_User_Groups()
    {
        $backendUserGroups = $this->makeBackend_User_Groups();
        $dbBackend_User_Groups = $this->backendUserGroupsRepo->find($backendUserGroups->id);
        $dbBackend_User_Groups = $dbBackend_User_Groups->toArray();
        $this->assertModelData($backendUserGroups->toArray(), $dbBackend_User_Groups);
    }

    /**
     * @test update
     */
    public function testUpdateBackend_User_Groups()
    {
        $backendUserGroups = $this->makeBackend_User_Groups();
        $fakeBackend_User_Groups = $this->fakeBackend_User_GroupsData();
        $updatedBackend_User_Groups = $this->backendUserGroupsRepo->update($fakeBackend_User_Groups, $backendUserGroups->id);
        $this->assertModelData($fakeBackend_User_Groups, $updatedBackend_User_Groups->toArray());
        $dbBackend_User_Groups = $this->backendUserGroupsRepo->find($backendUserGroups->id);
        $this->assertModelData($fakeBackend_User_Groups, $dbBackend_User_Groups->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBackend_User_Groups()
    {
        $backendUserGroups = $this->makeBackend_User_Groups();
        $resp = $this->backendUserGroupsRepo->delete($backendUserGroups->id);
        $this->assertTrue($resp);
        $this->assertNull(Backend_User_Groups::find($backendUserGroups->id), 'Backend_User_Groups should not exist in DB');
    }
}
