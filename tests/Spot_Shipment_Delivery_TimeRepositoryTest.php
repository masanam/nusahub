<?php

use App\Models\Spot_Shipment_Delivery_Time;
use App\Repositories\Spot_Shipment_Delivery_TimeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_Delivery_TimeRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_Delivery_TimeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_Delivery_TimeRepository
     */
    protected $spotShipmentDeliveryTimeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentDeliveryTimeRepo = App::make(Spot_Shipment_Delivery_TimeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Delivery_Time()
    {
        $spotShipmentDeliveryTime = $this->fakeSpot_Shipment_Delivery_TimeData();
        $createdSpot_Shipment_Delivery_Time = $this->spotShipmentDeliveryTimeRepo->create($spotShipmentDeliveryTime);
        $createdSpot_Shipment_Delivery_Time = $createdSpot_Shipment_Delivery_Time->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Delivery_Time);
        $this->assertNotNull($createdSpot_Shipment_Delivery_Time['id'], 'Created Spot_Shipment_Delivery_Time must have id specified');
        $this->assertNotNull(Spot_Shipment_Delivery_Time::find($createdSpot_Shipment_Delivery_Time['id']), 'Spot_Shipment_Delivery_Time with given id must be in DB');
        $this->assertModelData($spotShipmentDeliveryTime, $createdSpot_Shipment_Delivery_Time);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Delivery_Time()
    {
        $spotShipmentDeliveryTime = $this->makeSpot_Shipment_Delivery_Time();
        $dbSpot_Shipment_Delivery_Time = $this->spotShipmentDeliveryTimeRepo->find($spotShipmentDeliveryTime->id);
        $dbSpot_Shipment_Delivery_Time = $dbSpot_Shipment_Delivery_Time->toArray();
        $this->assertModelData($spotShipmentDeliveryTime->toArray(), $dbSpot_Shipment_Delivery_Time);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Delivery_Time()
    {
        $spotShipmentDeliveryTime = $this->makeSpot_Shipment_Delivery_Time();
        $fakeSpot_Shipment_Delivery_Time = $this->fakeSpot_Shipment_Delivery_TimeData();
        $updatedSpot_Shipment_Delivery_Time = $this->spotShipmentDeliveryTimeRepo->update($fakeSpot_Shipment_Delivery_Time, $spotShipmentDeliveryTime->id);
        $this->assertModelData($fakeSpot_Shipment_Delivery_Time, $updatedSpot_Shipment_Delivery_Time->toArray());
        $dbSpot_Shipment_Delivery_Time = $this->spotShipmentDeliveryTimeRepo->find($spotShipmentDeliveryTime->id);
        $this->assertModelData($fakeSpot_Shipment_Delivery_Time, $dbSpot_Shipment_Delivery_Time->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Delivery_Time()
    {
        $spotShipmentDeliveryTime = $this->makeSpot_Shipment_Delivery_Time();
        $resp = $this->spotShipmentDeliveryTimeRepo->delete($spotShipmentDeliveryTime->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Delivery_Time::find($spotShipmentDeliveryTime->id), 'Spot_Shipment_Delivery_Time should not exist in DB');
    }
}
