<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_OrderApiTest extends TestCase
{
    use MakeSpot_Shipment_OrderTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Order()
    {
        $spotShipmentOrder = $this->fakeSpot_Shipment_OrderData();
        $this->json('POST', '/api/v1/spotShipmentOrders', $spotShipmentOrder);

        $this->assertApiResponse($spotShipmentOrder);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Order()
    {
        $spotShipmentOrder = $this->makeSpot_Shipment_Order();
        $this->json('GET', '/api/v1/spotShipmentOrders/'.$spotShipmentOrder->id);

        $this->assertApiResponse($spotShipmentOrder->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Order()
    {
        $spotShipmentOrder = $this->makeSpot_Shipment_Order();
        $editedSpot_Shipment_Order = $this->fakeSpot_Shipment_OrderData();

        $this->json('PUT', '/api/v1/spotShipmentOrders/'.$spotShipmentOrder->id, $editedSpot_Shipment_Order);

        $this->assertApiResponse($editedSpot_Shipment_Order);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Order()
    {
        $spotShipmentOrder = $this->makeSpot_Shipment_Order();
        $this->json('DELETE', '/api/v1/spotShipmentOrders/'.$spotShipmentOrder->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentOrders/'.$spotShipmentOrder->id);

        $this->assertResponseStatus(404);
    }
}
