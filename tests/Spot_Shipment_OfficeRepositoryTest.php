<?php

use App\Models\Spot_Shipment_Office;
use App\Repositories\Spot_Shipment_OfficeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_OfficeRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_OfficeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_OfficeRepository
     */
    protected $spotShipmentOfficeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentOfficeRepo = App::make(Spot_Shipment_OfficeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Office()
    {
        $spotShipmentOffice = $this->fakeSpot_Shipment_OfficeData();
        $createdSpot_Shipment_Office = $this->spotShipmentOfficeRepo->create($spotShipmentOffice);
        $createdSpot_Shipment_Office = $createdSpot_Shipment_Office->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Office);
        $this->assertNotNull($createdSpot_Shipment_Office['id'], 'Created Spot_Shipment_Office must have id specified');
        $this->assertNotNull(Spot_Shipment_Office::find($createdSpot_Shipment_Office['id']), 'Spot_Shipment_Office with given id must be in DB');
        $this->assertModelData($spotShipmentOffice, $createdSpot_Shipment_Office);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Office()
    {
        $spotShipmentOffice = $this->makeSpot_Shipment_Office();
        $dbSpot_Shipment_Office = $this->spotShipmentOfficeRepo->find($spotShipmentOffice->id);
        $dbSpot_Shipment_Office = $dbSpot_Shipment_Office->toArray();
        $this->assertModelData($spotShipmentOffice->toArray(), $dbSpot_Shipment_Office);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Office()
    {
        $spotShipmentOffice = $this->makeSpot_Shipment_Office();
        $fakeSpot_Shipment_Office = $this->fakeSpot_Shipment_OfficeData();
        $updatedSpot_Shipment_Office = $this->spotShipmentOfficeRepo->update($fakeSpot_Shipment_Office, $spotShipmentOffice->id);
        $this->assertModelData($fakeSpot_Shipment_Office, $updatedSpot_Shipment_Office->toArray());
        $dbSpot_Shipment_Office = $this->spotShipmentOfficeRepo->find($spotShipmentOffice->id);
        $this->assertModelData($fakeSpot_Shipment_Office, $dbSpot_Shipment_Office->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Office()
    {
        $spotShipmentOffice = $this->makeSpot_Shipment_Office();
        $resp = $this->spotShipmentOfficeRepo->delete($spotShipmentOffice->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Office::find($spotShipmentOffice->id), 'Spot_Shipment_Office should not exist in DB');
    }
}
