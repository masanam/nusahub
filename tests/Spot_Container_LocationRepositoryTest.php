<?php

use App\Models\Spot_Container_Location;
use App\Repositories\Spot_Container_LocationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_LocationRepositoryTest extends TestCase
{
    use MakeSpot_Container_LocationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Container_LocationRepository
     */
    protected $spotContainerLocationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotContainerLocationRepo = App::make(Spot_Container_LocationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Container_Location()
    {
        $spotContainerLocation = $this->fakeSpot_Container_LocationData();
        $createdSpot_Container_Location = $this->spotContainerLocationRepo->create($spotContainerLocation);
        $createdSpot_Container_Location = $createdSpot_Container_Location->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Container_Location);
        $this->assertNotNull($createdSpot_Container_Location['id'], 'Created Spot_Container_Location must have id specified');
        $this->assertNotNull(Spot_Container_Location::find($createdSpot_Container_Location['id']), 'Spot_Container_Location with given id must be in DB');
        $this->assertModelData($spotContainerLocation, $createdSpot_Container_Location);
    }

    /**
     * @test read
     */
    public function testReadSpot_Container_Location()
    {
        $spotContainerLocation = $this->makeSpot_Container_Location();
        $dbSpot_Container_Location = $this->spotContainerLocationRepo->find($spotContainerLocation->id);
        $dbSpot_Container_Location = $dbSpot_Container_Location->toArray();
        $this->assertModelData($spotContainerLocation->toArray(), $dbSpot_Container_Location);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Container_Location()
    {
        $spotContainerLocation = $this->makeSpot_Container_Location();
        $fakeSpot_Container_Location = $this->fakeSpot_Container_LocationData();
        $updatedSpot_Container_Location = $this->spotContainerLocationRepo->update($fakeSpot_Container_Location, $spotContainerLocation->id);
        $this->assertModelData($fakeSpot_Container_Location, $updatedSpot_Container_Location->toArray());
        $dbSpot_Container_Location = $this->spotContainerLocationRepo->find($spotContainerLocation->id);
        $this->assertModelData($fakeSpot_Container_Location, $dbSpot_Container_Location->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Container_Location()
    {
        $spotContainerLocation = $this->makeSpot_Container_Location();
        $resp = $this->spotContainerLocationRepo->delete($spotContainerLocation->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Container_Location::find($spotContainerLocation->id), 'Spot_Container_Location should not exist in DB');
    }
}
