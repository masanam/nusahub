<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_PackagingApiTest extends TestCase
{
    use MakeSpot_Shipment_PackagingTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Packaging()
    {
        $spotShipmentPackaging = $this->fakeSpot_Shipment_PackagingData();
        $this->json('POST', '/api/v1/spotShipmentPackagings', $spotShipmentPackaging);

        $this->assertApiResponse($spotShipmentPackaging);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Packaging()
    {
        $spotShipmentPackaging = $this->makeSpot_Shipment_Packaging();
        $this->json('GET', '/api/v1/spotShipmentPackagings/'.$spotShipmentPackaging->id);

        $this->assertApiResponse($spotShipmentPackaging->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Packaging()
    {
        $spotShipmentPackaging = $this->makeSpot_Shipment_Packaging();
        $editedSpot_Shipment_Packaging = $this->fakeSpot_Shipment_PackagingData();

        $this->json('PUT', '/api/v1/spotShipmentPackagings/'.$spotShipmentPackaging->id, $editedSpot_Shipment_Packaging);

        $this->assertApiResponse($editedSpot_Shipment_Packaging);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Packaging()
    {
        $spotShipmentPackaging = $this->makeSpot_Shipment_Packaging();
        $this->json('DELETE', '/api/v1/spotShipmentPackagings/'.$spotShipmentPackaging->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentPackagings/'.$spotShipmentPackaging->id);

        $this->assertResponseStatus(404);
    }
}
