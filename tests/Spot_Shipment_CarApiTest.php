<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_CarApiTest extends TestCase
{
    use MakeSpot_Shipment_CarTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Car()
    {
        $spotShipmentCar = $this->fakeSpot_Shipment_CarData();
        $this->json('POST', '/api/v1/spotShipmentCars', $spotShipmentCar);

        $this->assertApiResponse($spotShipmentCar);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Car()
    {
        $spotShipmentCar = $this->makeSpot_Shipment_Car();
        $this->json('GET', '/api/v1/spotShipmentCars/'.$spotShipmentCar->id);

        $this->assertApiResponse($spotShipmentCar->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Car()
    {
        $spotShipmentCar = $this->makeSpot_Shipment_Car();
        $editedSpot_Shipment_Car = $this->fakeSpot_Shipment_CarData();

        $this->json('PUT', '/api/v1/spotShipmentCars/'.$spotShipmentCar->id, $editedSpot_Shipment_Car);

        $this->assertApiResponse($editedSpot_Shipment_Car);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Car()
    {
        $spotShipmentCar = $this->makeSpot_Shipment_Car();
        $this->json('DELETE', '/api/v1/spotShipmentCars/'.$spotShipmentCar->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentCars/'.$spotShipmentCar->id);

        $this->assertResponseStatus(404);
    }
}
