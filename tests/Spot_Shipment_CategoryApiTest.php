<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_CategoryApiTest extends TestCase
{
    use MakeSpot_Shipment_CategoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Category()
    {
        $spotShipmentCategory = $this->fakeSpot_Shipment_CategoryData();
        $this->json('POST', '/api/v1/spotShipmentCategories', $spotShipmentCategory);

        $this->assertApiResponse($spotShipmentCategory);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Category()
    {
        $spotShipmentCategory = $this->makeSpot_Shipment_Category();
        $this->json('GET', '/api/v1/spotShipmentCategories/'.$spotShipmentCategory->id);

        $this->assertApiResponse($spotShipmentCategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Category()
    {
        $spotShipmentCategory = $this->makeSpot_Shipment_Category();
        $editedSpot_Shipment_Category = $this->fakeSpot_Shipment_CategoryData();

        $this->json('PUT', '/api/v1/spotShipmentCategories/'.$spotShipmentCategory->id, $editedSpot_Shipment_Category);

        $this->assertApiResponse($editedSpot_Shipment_Category);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Category()
    {
        $spotShipmentCategory = $this->makeSpot_Shipment_Category();
        $this->json('DELETE', '/api/v1/spotShipmentCategories/'.$spotShipmentCategory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentCategories/'.$spotShipmentCategory->id);

        $this->assertResponseStatus(404);
    }
}
