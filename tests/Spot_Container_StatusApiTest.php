<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_StatusApiTest extends TestCase
{
    use MakeSpot_Container_StatusTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Container_Status()
    {
        $spotContainerStatus = $this->fakeSpot_Container_StatusData();
        $this->json('POST', '/api/v1/spotContainerStatuses', $spotContainerStatus);

        $this->assertApiResponse($spotContainerStatus);
    }

    /**
     * @test
     */
    public function testReadSpot_Container_Status()
    {
        $spotContainerStatus = $this->makeSpot_Container_Status();
        $this->json('GET', '/api/v1/spotContainerStatuses/'.$spotContainerStatus->id);

        $this->assertApiResponse($spotContainerStatus->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Container_Status()
    {
        $spotContainerStatus = $this->makeSpot_Container_Status();
        $editedSpot_Container_Status = $this->fakeSpot_Container_StatusData();

        $this->json('PUT', '/api/v1/spotContainerStatuses/'.$spotContainerStatus->id, $editedSpot_Container_Status);

        $this->assertApiResponse($editedSpot_Container_Status);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Container_Status()
    {
        $spotContainerStatus = $this->makeSpot_Container_Status();
        $this->json('DELETE', '/api/v1/spotContainerStatuses/'.$spotContainerStatus->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotContainerStatuses/'.$spotContainerStatus->id);

        $this->assertResponseStatus(404);
    }
}
