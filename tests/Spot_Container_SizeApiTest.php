<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_SizeApiTest extends TestCase
{
    use MakeSpot_Container_SizeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Container_Size()
    {
        $spotContainerSize = $this->fakeSpot_Container_SizeData();
        $this->json('POST', '/api/v1/spotContainerSizes', $spotContainerSize);

        $this->assertApiResponse($spotContainerSize);
    }

    /**
     * @test
     */
    public function testReadSpot_Container_Size()
    {
        $spotContainerSize = $this->makeSpot_Container_Size();
        $this->json('GET', '/api/v1/spotContainerSizes/'.$spotContainerSize->id);

        $this->assertApiResponse($spotContainerSize->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Container_Size()
    {
        $spotContainerSize = $this->makeSpot_Container_Size();
        $editedSpot_Container_Size = $this->fakeSpot_Container_SizeData();

        $this->json('PUT', '/api/v1/spotContainerSizes/'.$spotContainerSize->id, $editedSpot_Container_Size);

        $this->assertApiResponse($editedSpot_Container_Size);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Container_Size()
    {
        $spotContainerSize = $this->makeSpot_Container_Size();
        $this->json('DELETE', '/api/v1/spotContainerSizes/'.$spotContainerSize->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotContainerSizes/'.$spotContainerSize->id);

        $this->assertResponseStatus(404);
    }
}
