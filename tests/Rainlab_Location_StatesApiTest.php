<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Rainlab_Location_StatesApiTest extends TestCase
{
    use MakeRainlab_Location_StatesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRainlab_Location_States()
    {
        $rainlabLocationStates = $this->fakeRainlab_Location_StatesData();
        $this->json('POST', '/api/v1/rainlabLocationStates', $rainlabLocationStates);

        $this->assertApiResponse($rainlabLocationStates);
    }

    /**
     * @test
     */
    public function testReadRainlab_Location_States()
    {
        $rainlabLocationStates = $this->makeRainlab_Location_States();
        $this->json('GET', '/api/v1/rainlabLocationStates/'.$rainlabLocationStates->id);

        $this->assertApiResponse($rainlabLocationStates->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRainlab_Location_States()
    {
        $rainlabLocationStates = $this->makeRainlab_Location_States();
        $editedRainlab_Location_States = $this->fakeRainlab_Location_StatesData();

        $this->json('PUT', '/api/v1/rainlabLocationStates/'.$rainlabLocationStates->id, $editedRainlab_Location_States);

        $this->assertApiResponse($editedRainlab_Location_States);
    }

    /**
     * @test
     */
    public function testDeleteRainlab_Location_States()
    {
        $rainlabLocationStates = $this->makeRainlab_Location_States();
        $this->json('DELETE', '/api/v1/rainlabLocationStates/'.$rainlabLocationStates->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/rainlabLocationStates/'.$rainlabLocationStates->id);

        $this->assertResponseStatus(404);
    }
}
