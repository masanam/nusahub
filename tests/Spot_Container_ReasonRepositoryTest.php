<?php

use App\Models\Spot_Container_Reason;
use App\Repositories\Spot_Container_ReasonRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_ReasonRepositoryTest extends TestCase
{
    use MakeSpot_Container_ReasonTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Container_ReasonRepository
     */
    protected $spotContainerReasonRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotContainerReasonRepo = App::make(Spot_Container_ReasonRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Container_Reason()
    {
        $spotContainerReason = $this->fakeSpot_Container_ReasonData();
        $createdSpot_Container_Reason = $this->spotContainerReasonRepo->create($spotContainerReason);
        $createdSpot_Container_Reason = $createdSpot_Container_Reason->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Container_Reason);
        $this->assertNotNull($createdSpot_Container_Reason['id'], 'Created Spot_Container_Reason must have id specified');
        $this->assertNotNull(Spot_Container_Reason::find($createdSpot_Container_Reason['id']), 'Spot_Container_Reason with given id must be in DB');
        $this->assertModelData($spotContainerReason, $createdSpot_Container_Reason);
    }

    /**
     * @test read
     */
    public function testReadSpot_Container_Reason()
    {
        $spotContainerReason = $this->makeSpot_Container_Reason();
        $dbSpot_Container_Reason = $this->spotContainerReasonRepo->find($spotContainerReason->id);
        $dbSpot_Container_Reason = $dbSpot_Container_Reason->toArray();
        $this->assertModelData($spotContainerReason->toArray(), $dbSpot_Container_Reason);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Container_Reason()
    {
        $spotContainerReason = $this->makeSpot_Container_Reason();
        $fakeSpot_Container_Reason = $this->fakeSpot_Container_ReasonData();
        $updatedSpot_Container_Reason = $this->spotContainerReasonRepo->update($fakeSpot_Container_Reason, $spotContainerReason->id);
        $this->assertModelData($fakeSpot_Container_Reason, $updatedSpot_Container_Reason->toArray());
        $dbSpot_Container_Reason = $this->spotContainerReasonRepo->find($spotContainerReason->id);
        $this->assertModelData($fakeSpot_Container_Reason, $dbSpot_Container_Reason->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Container_Reason()
    {
        $spotContainerReason = $this->makeSpot_Container_Reason();
        $resp = $this->spotContainerReasonRepo->delete($spotContainerReason->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Container_Reason::find($spotContainerReason->id), 'Spot_Container_Reason should not exist in DB');
    }
}
