<?php

use App\Models\Spot_Container_Size;
use App\Repositories\Spot_Container_SizeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_SizeRepositoryTest extends TestCase
{
    use MakeSpot_Container_SizeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Container_SizeRepository
     */
    protected $spotContainerSizeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotContainerSizeRepo = App::make(Spot_Container_SizeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Container_Size()
    {
        $spotContainerSize = $this->fakeSpot_Container_SizeData();
        $createdSpot_Container_Size = $this->spotContainerSizeRepo->create($spotContainerSize);
        $createdSpot_Container_Size = $createdSpot_Container_Size->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Container_Size);
        $this->assertNotNull($createdSpot_Container_Size['id'], 'Created Spot_Container_Size must have id specified');
        $this->assertNotNull(Spot_Container_Size::find($createdSpot_Container_Size['id']), 'Spot_Container_Size with given id must be in DB');
        $this->assertModelData($spotContainerSize, $createdSpot_Container_Size);
    }

    /**
     * @test read
     */
    public function testReadSpot_Container_Size()
    {
        $spotContainerSize = $this->makeSpot_Container_Size();
        $dbSpot_Container_Size = $this->spotContainerSizeRepo->find($spotContainerSize->id);
        $dbSpot_Container_Size = $dbSpot_Container_Size->toArray();
        $this->assertModelData($spotContainerSize->toArray(), $dbSpot_Container_Size);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Container_Size()
    {
        $spotContainerSize = $this->makeSpot_Container_Size();
        $fakeSpot_Container_Size = $this->fakeSpot_Container_SizeData();
        $updatedSpot_Container_Size = $this->spotContainerSizeRepo->update($fakeSpot_Container_Size, $spotContainerSize->id);
        $this->assertModelData($fakeSpot_Container_Size, $updatedSpot_Container_Size->toArray());
        $dbSpot_Container_Size = $this->spotContainerSizeRepo->find($spotContainerSize->id);
        $this->assertModelData($fakeSpot_Container_Size, $dbSpot_Container_Size->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Container_Size()
    {
        $spotContainerSize = $this->makeSpot_Container_Size();
        $resp = $this->spotContainerSizeRepo->delete($spotContainerSize->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Container_Size::find($spotContainerSize->id), 'Spot_Container_Size should not exist in DB');
    }
}
