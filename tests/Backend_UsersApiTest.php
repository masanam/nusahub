<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Backend_UsersApiTest extends TestCase
{
    use MakeBackend_UsersTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBackend_Users()
    {
        $backendUsers = $this->fakeBackend_UsersData();
        $this->json('POST', '/api/v1/backendUsers', $backendUsers);

        $this->assertApiResponse($backendUsers);
    }

    /**
     * @test
     */
    public function testReadBackend_Users()
    {
        $backendUsers = $this->makeBackend_Users();
        $this->json('GET', '/api/v1/backendUsers/'.$backendUsers->id);

        $this->assertApiResponse($backendUsers->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBackend_Users()
    {
        $backendUsers = $this->makeBackend_Users();
        $editedBackend_Users = $this->fakeBackend_UsersData();

        $this->json('PUT', '/api/v1/backendUsers/'.$backendUsers->id, $editedBackend_Users);

        $this->assertApiResponse($editedBackend_Users);
    }

    /**
     * @test
     */
    public function testDeleteBackend_Users()
    {
        $backendUsers = $this->makeBackend_Users();
        $this->json('DELETE', '/api/v1/backendUsers/'.$backendUsers->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/backendUsers/'.$backendUsers->id);

        $this->assertResponseStatus(404);
    }
}
