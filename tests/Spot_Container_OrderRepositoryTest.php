<?php

use App\Models\Spot_Container_Order;
use App\Repositories\Spot_Container_OrderRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_OrderRepositoryTest extends TestCase
{
    use MakeSpot_Container_OrderTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Container_OrderRepository
     */
    protected $spotContainerOrderRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotContainerOrderRepo = App::make(Spot_Container_OrderRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Container_Order()
    {
        $spotContainerOrder = $this->fakeSpot_Container_OrderData();
        $createdSpot_Container_Order = $this->spotContainerOrderRepo->create($spotContainerOrder);
        $createdSpot_Container_Order = $createdSpot_Container_Order->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Container_Order);
        $this->assertNotNull($createdSpot_Container_Order['id'], 'Created Spot_Container_Order must have id specified');
        $this->assertNotNull(Spot_Container_Order::find($createdSpot_Container_Order['id']), 'Spot_Container_Order with given id must be in DB');
        $this->assertModelData($spotContainerOrder, $createdSpot_Container_Order);
    }

    /**
     * @test read
     */
    public function testReadSpot_Container_Order()
    {
        $spotContainerOrder = $this->makeSpot_Container_Order();
        $dbSpot_Container_Order = $this->spotContainerOrderRepo->find($spotContainerOrder->id);
        $dbSpot_Container_Order = $dbSpot_Container_Order->toArray();
        $this->assertModelData($spotContainerOrder->toArray(), $dbSpot_Container_Order);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Container_Order()
    {
        $spotContainerOrder = $this->makeSpot_Container_Order();
        $fakeSpot_Container_Order = $this->fakeSpot_Container_OrderData();
        $updatedSpot_Container_Order = $this->spotContainerOrderRepo->update($fakeSpot_Container_Order, $spotContainerOrder->id);
        $this->assertModelData($fakeSpot_Container_Order, $updatedSpot_Container_Order->toArray());
        $dbSpot_Container_Order = $this->spotContainerOrderRepo->find($spotContainerOrder->id);
        $this->assertModelData($fakeSpot_Container_Order, $dbSpot_Container_Order->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Container_Order()
    {
        $spotContainerOrder = $this->makeSpot_Container_Order();
        $resp = $this->spotContainerOrderRepo->delete($spotContainerOrder->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Container_Order::find($spotContainerOrder->id), 'Spot_Container_Order should not exist in DB');
    }
}
