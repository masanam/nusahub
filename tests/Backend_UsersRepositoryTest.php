<?php

use App\Models\Backend_Users;
use App\Repositories\Backend_UsersRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Backend_UsersRepositoryTest extends TestCase
{
    use MakeBackend_UsersTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Backend_UsersRepository
     */
    protected $backendUsersRepo;

    public function setUp()
    {
        parent::setUp();
        $this->backendUsersRepo = App::make(Backend_UsersRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBackend_Users()
    {
        $backendUsers = $this->fakeBackend_UsersData();
        $createdBackend_Users = $this->backendUsersRepo->create($backendUsers);
        $createdBackend_Users = $createdBackend_Users->toArray();
        $this->assertArrayHasKey('id', $createdBackend_Users);
        $this->assertNotNull($createdBackend_Users['id'], 'Created Backend_Users must have id specified');
        $this->assertNotNull(Backend_Users::find($createdBackend_Users['id']), 'Backend_Users with given id must be in DB');
        $this->assertModelData($backendUsers, $createdBackend_Users);
    }

    /**
     * @test read
     */
    public function testReadBackend_Users()
    {
        $backendUsers = $this->makeBackend_Users();
        $dbBackend_Users = $this->backendUsersRepo->find($backendUsers->id);
        $dbBackend_Users = $dbBackend_Users->toArray();
        $this->assertModelData($backendUsers->toArray(), $dbBackend_Users);
    }

    /**
     * @test update
     */
    public function testUpdateBackend_Users()
    {
        $backendUsers = $this->makeBackend_Users();
        $fakeBackend_Users = $this->fakeBackend_UsersData();
        $updatedBackend_Users = $this->backendUsersRepo->update($fakeBackend_Users, $backendUsers->id);
        $this->assertModelData($fakeBackend_Users, $updatedBackend_Users->toArray());
        $dbBackend_Users = $this->backendUsersRepo->find($backendUsers->id);
        $this->assertModelData($fakeBackend_Users, $dbBackend_Users->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBackend_Users()
    {
        $backendUsers = $this->makeBackend_Users();
        $resp = $this->backendUsersRepo->delete($backendUsers->id);
        $this->assertTrue($resp);
        $this->assertNull(Backend_Users::find($backendUsers->id), 'Backend_Users should not exist in DB');
    }
}
