<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Rainlab_Location_CountriesApiTest extends TestCase
{
    use MakeRainlab_Location_CountriesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRainlab_Location_Countries()
    {
        $rainlabLocationCountries = $this->fakeRainlab_Location_CountriesData();
        $this->json('POST', '/api/v1/rainlabLocationCountries', $rainlabLocationCountries);

        $this->assertApiResponse($rainlabLocationCountries);
    }

    /**
     * @test
     */
    public function testReadRainlab_Location_Countries()
    {
        $rainlabLocationCountries = $this->makeRainlab_Location_Countries();
        $this->json('GET', '/api/v1/rainlabLocationCountries/'.$rainlabLocationCountries->id);

        $this->assertApiResponse($rainlabLocationCountries->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRainlab_Location_Countries()
    {
        $rainlabLocationCountries = $this->makeRainlab_Location_Countries();
        $editedRainlab_Location_Countries = $this->fakeRainlab_Location_CountriesData();

        $this->json('PUT', '/api/v1/rainlabLocationCountries/'.$rainlabLocationCountries->id, $editedRainlab_Location_Countries);

        $this->assertApiResponse($editedRainlab_Location_Countries);
    }

    /**
     * @test
     */
    public function testDeleteRainlab_Location_Countries()
    {
        $rainlabLocationCountries = $this->makeRainlab_Location_Countries();
        $this->json('DELETE', '/api/v1/rainlabLocationCountries/'.$rainlabLocationCountries->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/rainlabLocationCountries/'.$rainlabLocationCountries->id);

        $this->assertResponseStatus(404);
    }
}
