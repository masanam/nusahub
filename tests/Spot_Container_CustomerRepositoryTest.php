<?php

use App\Models\Spot_Container_Customer;
use App\Repositories\Spot_Container_CustomerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_CustomerRepositoryTest extends TestCase
{
    use MakeSpot_Container_CustomerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Container_CustomerRepository
     */
    protected $spotContainerCustomerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotContainerCustomerRepo = App::make(Spot_Container_CustomerRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Container_Customer()
    {
        $spotContainerCustomer = $this->fakeSpot_Container_CustomerData();
        $createdSpot_Container_Customer = $this->spotContainerCustomerRepo->create($spotContainerCustomer);
        $createdSpot_Container_Customer = $createdSpot_Container_Customer->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Container_Customer);
        $this->assertNotNull($createdSpot_Container_Customer['id'], 'Created Spot_Container_Customer must have id specified');
        $this->assertNotNull(Spot_Container_Customer::find($createdSpot_Container_Customer['id']), 'Spot_Container_Customer with given id must be in DB');
        $this->assertModelData($spotContainerCustomer, $createdSpot_Container_Customer);
    }

    /**
     * @test read
     */
    public function testReadSpot_Container_Customer()
    {
        $spotContainerCustomer = $this->makeSpot_Container_Customer();
        $dbSpot_Container_Customer = $this->spotContainerCustomerRepo->find($spotContainerCustomer->id);
        $dbSpot_Container_Customer = $dbSpot_Container_Customer->toArray();
        $this->assertModelData($spotContainerCustomer->toArray(), $dbSpot_Container_Customer);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Container_Customer()
    {
        $spotContainerCustomer = $this->makeSpot_Container_Customer();
        $fakeSpot_Container_Customer = $this->fakeSpot_Container_CustomerData();
        $updatedSpot_Container_Customer = $this->spotContainerCustomerRepo->update($fakeSpot_Container_Customer, $spotContainerCustomer->id);
        $this->assertModelData($fakeSpot_Container_Customer, $updatedSpot_Container_Customer->toArray());
        $dbSpot_Container_Customer = $this->spotContainerCustomerRepo->find($spotContainerCustomer->id);
        $this->assertModelData($fakeSpot_Container_Customer, $dbSpot_Container_Customer->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Container_Customer()
    {
        $spotContainerCustomer = $this->makeSpot_Container_Customer();
        $resp = $this->spotContainerCustomerRepo->delete($spotContainerCustomer->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Container_Customer::find($spotContainerCustomer->id), 'Spot_Container_Customer should not exist in DB');
    }
}
