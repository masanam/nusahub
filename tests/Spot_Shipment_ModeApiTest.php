<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_ModeApiTest extends TestCase
{
    use MakeSpot_Shipment_ModeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Mode()
    {
        $spotShipmentMode = $this->fakeSpot_Shipment_ModeData();
        $this->json('POST', '/api/v1/spotShipmentModes', $spotShipmentMode);

        $this->assertApiResponse($spotShipmentMode);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Mode()
    {
        $spotShipmentMode = $this->makeSpot_Shipment_Mode();
        $this->json('GET', '/api/v1/spotShipmentModes/'.$spotShipmentMode->id);

        $this->assertApiResponse($spotShipmentMode->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Mode()
    {
        $spotShipmentMode = $this->makeSpot_Shipment_Mode();
        $editedSpot_Shipment_Mode = $this->fakeSpot_Shipment_ModeData();

        $this->json('PUT', '/api/v1/spotShipmentModes/'.$spotShipmentMode->id, $editedSpot_Shipment_Mode);

        $this->assertApiResponse($editedSpot_Shipment_Mode);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Mode()
    {
        $spotShipmentMode = $this->makeSpot_Shipment_Mode();
        $this->json('DELETE', '/api/v1/spotShipmentModes/'.$spotShipmentMode->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentModes/'.$spotShipmentMode->id);

        $this->assertResponseStatus(404);
    }
}
