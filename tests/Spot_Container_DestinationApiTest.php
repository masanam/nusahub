<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_DestinationApiTest extends TestCase
{
    use MakeSpot_Container_DestinationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Container_Destination()
    {
        $spotContainerDestination = $this->fakeSpot_Container_DestinationData();
        $this->json('POST', '/api/v1/spotContainerDestinations', $spotContainerDestination);

        $this->assertApiResponse($spotContainerDestination);
    }

    /**
     * @test
     */
    public function testReadSpot_Container_Destination()
    {
        $spotContainerDestination = $this->makeSpot_Container_Destination();
        $this->json('GET', '/api/v1/spotContainerDestinations/'.$spotContainerDestination->id);

        $this->assertApiResponse($spotContainerDestination->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Container_Destination()
    {
        $spotContainerDestination = $this->makeSpot_Container_Destination();
        $editedSpot_Container_Destination = $this->fakeSpot_Container_DestinationData();

        $this->json('PUT', '/api/v1/spotContainerDestinations/'.$spotContainerDestination->id, $editedSpot_Container_Destination);

        $this->assertApiResponse($editedSpot_Container_Destination);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Container_Destination()
    {
        $spotContainerDestination = $this->makeSpot_Container_Destination();
        $this->json('DELETE', '/api/v1/spotContainerDestinations/'.$spotContainerDestination->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotContainerDestinations/'.$spotContainerDestination->id);

        $this->assertResponseStatus(404);
    }
}
