<?php

use App\Models\Spot_Shipment_Packaging;
use App\Repositories\Spot_Shipment_PackagingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_PackagingRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_PackagingTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_PackagingRepository
     */
    protected $spotShipmentPackagingRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentPackagingRepo = App::make(Spot_Shipment_PackagingRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Packaging()
    {
        $spotShipmentPackaging = $this->fakeSpot_Shipment_PackagingData();
        $createdSpot_Shipment_Packaging = $this->spotShipmentPackagingRepo->create($spotShipmentPackaging);
        $createdSpot_Shipment_Packaging = $createdSpot_Shipment_Packaging->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Packaging);
        $this->assertNotNull($createdSpot_Shipment_Packaging['id'], 'Created Spot_Shipment_Packaging must have id specified');
        $this->assertNotNull(Spot_Shipment_Packaging::find($createdSpot_Shipment_Packaging['id']), 'Spot_Shipment_Packaging with given id must be in DB');
        $this->assertModelData($spotShipmentPackaging, $createdSpot_Shipment_Packaging);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Packaging()
    {
        $spotShipmentPackaging = $this->makeSpot_Shipment_Packaging();
        $dbSpot_Shipment_Packaging = $this->spotShipmentPackagingRepo->find($spotShipmentPackaging->id);
        $dbSpot_Shipment_Packaging = $dbSpot_Shipment_Packaging->toArray();
        $this->assertModelData($spotShipmentPackaging->toArray(), $dbSpot_Shipment_Packaging);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Packaging()
    {
        $spotShipmentPackaging = $this->makeSpot_Shipment_Packaging();
        $fakeSpot_Shipment_Packaging = $this->fakeSpot_Shipment_PackagingData();
        $updatedSpot_Shipment_Packaging = $this->spotShipmentPackagingRepo->update($fakeSpot_Shipment_Packaging, $spotShipmentPackaging->id);
        $this->assertModelData($fakeSpot_Shipment_Packaging, $updatedSpot_Shipment_Packaging->toArray());
        $dbSpot_Shipment_Packaging = $this->spotShipmentPackagingRepo->find($spotShipmentPackaging->id);
        $this->assertModelData($fakeSpot_Shipment_Packaging, $dbSpot_Shipment_Packaging->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Packaging()
    {
        $spotShipmentPackaging = $this->makeSpot_Shipment_Packaging();
        $resp = $this->spotShipmentPackagingRepo->delete($spotShipmentPackaging->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Packaging::find($spotShipmentPackaging->id), 'Spot_Shipment_Packaging should not exist in DB');
    }
}
