<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Backend_User_RolesApiTest extends TestCase
{
    use MakeBackend_User_RolesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBackend_User_Roles()
    {
        $backendUserRoles = $this->fakeBackend_User_RolesData();
        $this->json('POST', '/api/v1/backendUserRoles', $backendUserRoles);

        $this->assertApiResponse($backendUserRoles);
    }

    /**
     * @test
     */
    public function testReadBackend_User_Roles()
    {
        $backendUserRoles = $this->makeBackend_User_Roles();
        $this->json('GET', '/api/v1/backendUserRoles/'.$backendUserRoles->id);

        $this->assertApiResponse($backendUserRoles->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBackend_User_Roles()
    {
        $backendUserRoles = $this->makeBackend_User_Roles();
        $editedBackend_User_Roles = $this->fakeBackend_User_RolesData();

        $this->json('PUT', '/api/v1/backendUserRoles/'.$backendUserRoles->id, $editedBackend_User_Roles);

        $this->assertApiResponse($editedBackend_User_Roles);
    }

    /**
     * @test
     */
    public function testDeleteBackend_User_Roles()
    {
        $backendUserRoles = $this->makeBackend_User_Roles();
        $this->json('DELETE', '/api/v1/backendUserRoles/'.$backendUserRoles->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/backendUserRoles/'.$backendUserRoles->id);

        $this->assertResponseStatus(404);
    }
}
