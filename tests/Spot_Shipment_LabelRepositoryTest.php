<?php

use App\Models\Spot_Shipment_Label;
use App\Repositories\Spot_Shipment_LabelRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_LabelRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_LabelTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_LabelRepository
     */
    protected $spotShipmentLabelRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentLabelRepo = App::make(Spot_Shipment_LabelRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Label()
    {
        $spotShipmentLabel = $this->fakeSpot_Shipment_LabelData();
        $createdSpot_Shipment_Label = $this->spotShipmentLabelRepo->create($spotShipmentLabel);
        $createdSpot_Shipment_Label = $createdSpot_Shipment_Label->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Label);
        $this->assertNotNull($createdSpot_Shipment_Label['id'], 'Created Spot_Shipment_Label must have id specified');
        $this->assertNotNull(Spot_Shipment_Label::find($createdSpot_Shipment_Label['id']), 'Spot_Shipment_Label with given id must be in DB');
        $this->assertModelData($spotShipmentLabel, $createdSpot_Shipment_Label);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Label()
    {
        $spotShipmentLabel = $this->makeSpot_Shipment_Label();
        $dbSpot_Shipment_Label = $this->spotShipmentLabelRepo->find($spotShipmentLabel->id);
        $dbSpot_Shipment_Label = $dbSpot_Shipment_Label->toArray();
        $this->assertModelData($spotShipmentLabel->toArray(), $dbSpot_Shipment_Label);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Label()
    {
        $spotShipmentLabel = $this->makeSpot_Shipment_Label();
        $fakeSpot_Shipment_Label = $this->fakeSpot_Shipment_LabelData();
        $updatedSpot_Shipment_Label = $this->spotShipmentLabelRepo->update($fakeSpot_Shipment_Label, $spotShipmentLabel->id);
        $this->assertModelData($fakeSpot_Shipment_Label, $updatedSpot_Shipment_Label->toArray());
        $dbSpot_Shipment_Label = $this->spotShipmentLabelRepo->find($spotShipmentLabel->id);
        $this->assertModelData($fakeSpot_Shipment_Label, $dbSpot_Shipment_Label->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Label()
    {
        $spotShipmentLabel = $this->makeSpot_Shipment_Label();
        $resp = $this->spotShipmentLabelRepo->delete($spotShipmentLabel->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Label::find($spotShipmentLabel->id), 'Spot_Shipment_Label should not exist in DB');
    }
}
