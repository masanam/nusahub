<?php

use App\Models\Spot_Shipment_Payment;
use App\Repositories\Spot_Shipment_PaymentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_PaymentRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_PaymentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_PaymentRepository
     */
    protected $spotShipmentPaymentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentPaymentRepo = App::make(Spot_Shipment_PaymentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Payment()
    {
        $spotShipmentPayment = $this->fakeSpot_Shipment_PaymentData();
        $createdSpot_Shipment_Payment = $this->spotShipmentPaymentRepo->create($spotShipmentPayment);
        $createdSpot_Shipment_Payment = $createdSpot_Shipment_Payment->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Payment);
        $this->assertNotNull($createdSpot_Shipment_Payment['id'], 'Created Spot_Shipment_Payment must have id specified');
        $this->assertNotNull(Spot_Shipment_Payment::find($createdSpot_Shipment_Payment['id']), 'Spot_Shipment_Payment with given id must be in DB');
        $this->assertModelData($spotShipmentPayment, $createdSpot_Shipment_Payment);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Payment()
    {
        $spotShipmentPayment = $this->makeSpot_Shipment_Payment();
        $dbSpot_Shipment_Payment = $this->spotShipmentPaymentRepo->find($spotShipmentPayment->id);
        $dbSpot_Shipment_Payment = $dbSpot_Shipment_Payment->toArray();
        $this->assertModelData($spotShipmentPayment->toArray(), $dbSpot_Shipment_Payment);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Payment()
    {
        $spotShipmentPayment = $this->makeSpot_Shipment_Payment();
        $fakeSpot_Shipment_Payment = $this->fakeSpot_Shipment_PaymentData();
        $updatedSpot_Shipment_Payment = $this->spotShipmentPaymentRepo->update($fakeSpot_Shipment_Payment, $spotShipmentPayment->id);
        $this->assertModelData($fakeSpot_Shipment_Payment, $updatedSpot_Shipment_Payment->toArray());
        $dbSpot_Shipment_Payment = $this->spotShipmentPaymentRepo->find($spotShipmentPayment->id);
        $this->assertModelData($fakeSpot_Shipment_Payment, $dbSpot_Shipment_Payment->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Payment()
    {
        $spotShipmentPayment = $this->makeSpot_Shipment_Payment();
        $resp = $this->spotShipmentPaymentRepo->delete($spotShipmentPayment->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Payment::find($spotShipmentPayment->id), 'Spot_Shipment_Payment should not exist in DB');
    }
}
