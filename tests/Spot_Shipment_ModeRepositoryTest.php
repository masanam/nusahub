<?php

use App\Models\Spot_Shipment_Mode;
use App\Repositories\Spot_Shipment_ModeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_ModeRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_ModeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_ModeRepository
     */
    protected $spotShipmentModeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentModeRepo = App::make(Spot_Shipment_ModeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Mode()
    {
        $spotShipmentMode = $this->fakeSpot_Shipment_ModeData();
        $createdSpot_Shipment_Mode = $this->spotShipmentModeRepo->create($spotShipmentMode);
        $createdSpot_Shipment_Mode = $createdSpot_Shipment_Mode->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Mode);
        $this->assertNotNull($createdSpot_Shipment_Mode['id'], 'Created Spot_Shipment_Mode must have id specified');
        $this->assertNotNull(Spot_Shipment_Mode::find($createdSpot_Shipment_Mode['id']), 'Spot_Shipment_Mode with given id must be in DB');
        $this->assertModelData($spotShipmentMode, $createdSpot_Shipment_Mode);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Mode()
    {
        $spotShipmentMode = $this->makeSpot_Shipment_Mode();
        $dbSpot_Shipment_Mode = $this->spotShipmentModeRepo->find($spotShipmentMode->id);
        $dbSpot_Shipment_Mode = $dbSpot_Shipment_Mode->toArray();
        $this->assertModelData($spotShipmentMode->toArray(), $dbSpot_Shipment_Mode);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Mode()
    {
        $spotShipmentMode = $this->makeSpot_Shipment_Mode();
        $fakeSpot_Shipment_Mode = $this->fakeSpot_Shipment_ModeData();
        $updatedSpot_Shipment_Mode = $this->spotShipmentModeRepo->update($fakeSpot_Shipment_Mode, $spotShipmentMode->id);
        $this->assertModelData($fakeSpot_Shipment_Mode, $updatedSpot_Shipment_Mode->toArray());
        $dbSpot_Shipment_Mode = $this->spotShipmentModeRepo->find($spotShipmentMode->id);
        $this->assertModelData($fakeSpot_Shipment_Mode, $dbSpot_Shipment_Mode->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Mode()
    {
        $spotShipmentMode = $this->makeSpot_Shipment_Mode();
        $resp = $this->spotShipmentModeRepo->delete($spotShipmentMode->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Mode::find($spotShipmentMode->id), 'Spot_Shipment_Mode should not exist in DB');
    }
}
