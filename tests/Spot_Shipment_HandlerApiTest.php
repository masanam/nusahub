<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_HandlerApiTest extends TestCase
{
    use MakeSpot_Shipment_HandlerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Handler()
    {
        $spotShipmentHandler = $this->fakeSpot_Shipment_HandlerData();
        $this->json('POST', '/api/v1/spotShipmentHandlers', $spotShipmentHandler);

        $this->assertApiResponse($spotShipmentHandler);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Handler()
    {
        $spotShipmentHandler = $this->makeSpot_Shipment_Handler();
        $this->json('GET', '/api/v1/spotShipmentHandlers/'.$spotShipmentHandler->id);

        $this->assertApiResponse($spotShipmentHandler->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Handler()
    {
        $spotShipmentHandler = $this->makeSpot_Shipment_Handler();
        $editedSpot_Shipment_Handler = $this->fakeSpot_Shipment_HandlerData();

        $this->json('PUT', '/api/v1/spotShipmentHandlers/'.$spotShipmentHandler->id, $editedSpot_Shipment_Handler);

        $this->assertApiResponse($editedSpot_Shipment_Handler);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Handler()
    {
        $spotShipmentHandler = $this->makeSpot_Shipment_Handler();
        $this->json('DELETE', '/api/v1/spotShipmentHandlers/'.$spotShipmentHandler->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentHandlers/'.$spotShipmentHandler->id);

        $this->assertResponseStatus(404);
    }
}
