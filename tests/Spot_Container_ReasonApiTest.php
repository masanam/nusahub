<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_ReasonApiTest extends TestCase
{
    use MakeSpot_Container_ReasonTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Container_Reason()
    {
        $spotContainerReason = $this->fakeSpot_Container_ReasonData();
        $this->json('POST', '/api/v1/spotContainerReasons', $spotContainerReason);

        $this->assertApiResponse($spotContainerReason);
    }

    /**
     * @test
     */
    public function testReadSpot_Container_Reason()
    {
        $spotContainerReason = $this->makeSpot_Container_Reason();
        $this->json('GET', '/api/v1/spotContainerReasons/'.$spotContainerReason->id);

        $this->assertApiResponse($spotContainerReason->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Container_Reason()
    {
        $spotContainerReason = $this->makeSpot_Container_Reason();
        $editedSpot_Container_Reason = $this->fakeSpot_Container_ReasonData();

        $this->json('PUT', '/api/v1/spotContainerReasons/'.$spotContainerReason->id, $editedSpot_Container_Reason);

        $this->assertApiResponse($editedSpot_Container_Reason);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Container_Reason()
    {
        $spotContainerReason = $this->makeSpot_Container_Reason();
        $this->json('DELETE', '/api/v1/spotContainerReasons/'.$spotContainerReason->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotContainerReasons/'.$spotContainerReason->id);

        $this->assertResponseStatus(404);
    }
}
