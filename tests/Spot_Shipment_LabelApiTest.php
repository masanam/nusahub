<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_LabelApiTest extends TestCase
{
    use MakeSpot_Shipment_LabelTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Label()
    {
        $spotShipmentLabel = $this->fakeSpot_Shipment_LabelData();
        $this->json('POST', '/api/v1/spotShipmentLabels', $spotShipmentLabel);

        $this->assertApiResponse($spotShipmentLabel);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Label()
    {
        $spotShipmentLabel = $this->makeSpot_Shipment_Label();
        $this->json('GET', '/api/v1/spotShipmentLabels/'.$spotShipmentLabel->id);

        $this->assertApiResponse($spotShipmentLabel->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Label()
    {
        $spotShipmentLabel = $this->makeSpot_Shipment_Label();
        $editedSpot_Shipment_Label = $this->fakeSpot_Shipment_LabelData();

        $this->json('PUT', '/api/v1/spotShipmentLabels/'.$spotShipmentLabel->id, $editedSpot_Shipment_Label);

        $this->assertApiResponse($editedSpot_Shipment_Label);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Label()
    {
        $spotShipmentLabel = $this->makeSpot_Shipment_Label();
        $this->json('DELETE', '/api/v1/spotShipmentLabels/'.$spotShipmentLabel->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentLabels/'.$spotShipmentLabel->id);

        $this->assertResponseStatus(404);
    }
}
