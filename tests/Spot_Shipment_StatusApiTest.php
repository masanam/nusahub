<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_StatusApiTest extends TestCase
{
    use MakeSpot_Shipment_StatusTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Status()
    {
        $spotShipmentStatus = $this->fakeSpot_Shipment_StatusData();
        $this->json('POST', '/api/v1/spotShipmentStatuses', $spotShipmentStatus);

        $this->assertApiResponse($spotShipmentStatus);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Status()
    {
        $spotShipmentStatus = $this->makeSpot_Shipment_Status();
        $this->json('GET', '/api/v1/spotShipmentStatuses/'.$spotShipmentStatus->id);

        $this->assertApiResponse($spotShipmentStatus->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Status()
    {
        $spotShipmentStatus = $this->makeSpot_Shipment_Status();
        $editedSpot_Shipment_Status = $this->fakeSpot_Shipment_StatusData();

        $this->json('PUT', '/api/v1/spotShipmentStatuses/'.$spotShipmentStatus->id, $editedSpot_Shipment_Status);

        $this->assertApiResponse($editedSpot_Shipment_Status);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Status()
    {
        $spotShipmentStatus = $this->makeSpot_Shipment_Status();
        $this->json('DELETE', '/api/v1/spotShipmentStatuses/'.$spotShipmentStatus->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentStatuses/'.$spotShipmentStatus->id);

        $this->assertResponseStatus(404);
    }
}
