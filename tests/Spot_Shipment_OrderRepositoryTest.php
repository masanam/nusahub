<?php

use App\Models\Spot_Shipment_Order;
use App\Repositories\Spot_Shipment_OrderRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_OrderRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_OrderTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_OrderRepository
     */
    protected $spotShipmentOrderRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentOrderRepo = App::make(Spot_Shipment_OrderRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Order()
    {
        $spotShipmentOrder = $this->fakeSpot_Shipment_OrderData();
        $createdSpot_Shipment_Order = $this->spotShipmentOrderRepo->create($spotShipmentOrder);
        $createdSpot_Shipment_Order = $createdSpot_Shipment_Order->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Order);
        $this->assertNotNull($createdSpot_Shipment_Order['id'], 'Created Spot_Shipment_Order must have id specified');
        $this->assertNotNull(Spot_Shipment_Order::find($createdSpot_Shipment_Order['id']), 'Spot_Shipment_Order with given id must be in DB');
        $this->assertModelData($spotShipmentOrder, $createdSpot_Shipment_Order);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Order()
    {
        $spotShipmentOrder = $this->makeSpot_Shipment_Order();
        $dbSpot_Shipment_Order = $this->spotShipmentOrderRepo->find($spotShipmentOrder->id);
        $dbSpot_Shipment_Order = $dbSpot_Shipment_Order->toArray();
        $this->assertModelData($spotShipmentOrder->toArray(), $dbSpot_Shipment_Order);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Order()
    {
        $spotShipmentOrder = $this->makeSpot_Shipment_Order();
        $fakeSpot_Shipment_Order = $this->fakeSpot_Shipment_OrderData();
        $updatedSpot_Shipment_Order = $this->spotShipmentOrderRepo->update($fakeSpot_Shipment_Order, $spotShipmentOrder->id);
        $this->assertModelData($fakeSpot_Shipment_Order, $updatedSpot_Shipment_Order->toArray());
        $dbSpot_Shipment_Order = $this->spotShipmentOrderRepo->find($spotShipmentOrder->id);
        $this->assertModelData($fakeSpot_Shipment_Order, $dbSpot_Shipment_Order->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Order()
    {
        $spotShipmentOrder = $this->makeSpot_Shipment_Order();
        $resp = $this->spotShipmentOrderRepo->delete($spotShipmentOrder->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Order::find($spotShipmentOrder->id), 'Spot_Shipment_Order should not exist in DB');
    }
}
