<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_AreaApiTest extends TestCase
{
    use MakeSpot_Shipment_AreaTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Area()
    {
        $spotShipmentArea = $this->fakeSpot_Shipment_AreaData();
        $this->json('POST', '/api/v1/spotShipmentAreas', $spotShipmentArea);

        $this->assertApiResponse($spotShipmentArea);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Area()
    {
        $spotShipmentArea = $this->makeSpot_Shipment_Area();
        $this->json('GET', '/api/v1/spotShipmentAreas/'.$spotShipmentArea->id);

        $this->assertApiResponse($spotShipmentArea->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Area()
    {
        $spotShipmentArea = $this->makeSpot_Shipment_Area();
        $editedSpot_Shipment_Area = $this->fakeSpot_Shipment_AreaData();

        $this->json('PUT', '/api/v1/spotShipmentAreas/'.$spotShipmentArea->id, $editedSpot_Shipment_Area);

        $this->assertApiResponse($editedSpot_Shipment_Area);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Area()
    {
        $spotShipmentArea = $this->makeSpot_Shipment_Area();
        $this->json('DELETE', '/api/v1/spotShipmentAreas/'.$spotShipmentArea->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentAreas/'.$spotShipmentArea->id);

        $this->assertResponseStatus(404);
    }
}
