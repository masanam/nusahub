<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Responsiv_Currency_CurrenciesApiTest extends TestCase
{
    use MakeResponsiv_Currency_CurrenciesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateResponsiv_Currency_Currencies()
    {
        $responsivCurrencyCurrencies = $this->fakeResponsiv_Currency_CurrenciesData();
        $this->json('POST', '/api/v1/responsivCurrencyCurrencies', $responsivCurrencyCurrencies);

        $this->assertApiResponse($responsivCurrencyCurrencies);
    }

    /**
     * @test
     */
    public function testReadResponsiv_Currency_Currencies()
    {
        $responsivCurrencyCurrencies = $this->makeResponsiv_Currency_Currencies();
        $this->json('GET', '/api/v1/responsivCurrencyCurrencies/'.$responsivCurrencyCurrencies->id);

        $this->assertApiResponse($responsivCurrencyCurrencies->toArray());
    }

    /**
     * @test
     */
    public function testUpdateResponsiv_Currency_Currencies()
    {
        $responsivCurrencyCurrencies = $this->makeResponsiv_Currency_Currencies();
        $editedResponsiv_Currency_Currencies = $this->fakeResponsiv_Currency_CurrenciesData();

        $this->json('PUT', '/api/v1/responsivCurrencyCurrencies/'.$responsivCurrencyCurrencies->id, $editedResponsiv_Currency_Currencies);

        $this->assertApiResponse($editedResponsiv_Currency_Currencies);
    }

    /**
     * @test
     */
    public function testDeleteResponsiv_Currency_Currencies()
    {
        $responsivCurrencyCurrencies = $this->makeResponsiv_Currency_Currencies();
        $this->json('DELETE', '/api/v1/responsivCurrencyCurrencies/'.$responsivCurrencyCurrencies->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/responsivCurrencyCurrencies/'.$responsivCurrencyCurrencies->id);

        $this->assertResponseStatus(404);
    }
}
