<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_PaymentApiTest extends TestCase
{
    use MakeSpot_Shipment_PaymentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Payment()
    {
        $spotShipmentPayment = $this->fakeSpot_Shipment_PaymentData();
        $this->json('POST', '/api/v1/spotShipmentPayments', $spotShipmentPayment);

        $this->assertApiResponse($spotShipmentPayment);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Payment()
    {
        $spotShipmentPayment = $this->makeSpot_Shipment_Payment();
        $this->json('GET', '/api/v1/spotShipmentPayments/'.$spotShipmentPayment->id);

        $this->assertApiResponse($spotShipmentPayment->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Payment()
    {
        $spotShipmentPayment = $this->makeSpot_Shipment_Payment();
        $editedSpot_Shipment_Payment = $this->fakeSpot_Shipment_PaymentData();

        $this->json('PUT', '/api/v1/spotShipmentPayments/'.$spotShipmentPayment->id, $editedSpot_Shipment_Payment);

        $this->assertApiResponse($editedSpot_Shipment_Payment);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Payment()
    {
        $spotShipmentPayment = $this->makeSpot_Shipment_Payment();
        $this->json('DELETE', '/api/v1/spotShipmentPayments/'.$spotShipmentPayment->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentPayments/'.$spotShipmentPayment->id);

        $this->assertResponseStatus(404);
    }
}
