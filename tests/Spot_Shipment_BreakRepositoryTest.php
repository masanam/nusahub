<?php

use App\Models\Spot_Shipment_Break;
use App\Repositories\Spot_Shipment_BreakRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_BreakRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_BreakTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_BreakRepository
     */
    protected $spotShipmentBreakRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentBreakRepo = App::make(Spot_Shipment_BreakRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Break()
    {
        $spotShipmentBreak = $this->fakeSpot_Shipment_BreakData();
        $createdSpot_Shipment_Break = $this->spotShipmentBreakRepo->create($spotShipmentBreak);
        $createdSpot_Shipment_Break = $createdSpot_Shipment_Break->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Break);
        $this->assertNotNull($createdSpot_Shipment_Break['id'], 'Created Spot_Shipment_Break must have id specified');
        $this->assertNotNull(Spot_Shipment_Break::find($createdSpot_Shipment_Break['id']), 'Spot_Shipment_Break with given id must be in DB');
        $this->assertModelData($spotShipmentBreak, $createdSpot_Shipment_Break);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Break()
    {
        $spotShipmentBreak = $this->makeSpot_Shipment_Break();
        $dbSpot_Shipment_Break = $this->spotShipmentBreakRepo->find($spotShipmentBreak->id);
        $dbSpot_Shipment_Break = $dbSpot_Shipment_Break->toArray();
        $this->assertModelData($spotShipmentBreak->toArray(), $dbSpot_Shipment_Break);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Break()
    {
        $spotShipmentBreak = $this->makeSpot_Shipment_Break();
        $fakeSpot_Shipment_Break = $this->fakeSpot_Shipment_BreakData();
        $updatedSpot_Shipment_Break = $this->spotShipmentBreakRepo->update($fakeSpot_Shipment_Break, $spotShipmentBreak->id);
        $this->assertModelData($fakeSpot_Shipment_Break, $updatedSpot_Shipment_Break->toArray());
        $dbSpot_Shipment_Break = $this->spotShipmentBreakRepo->find($spotShipmentBreak->id);
        $this->assertModelData($fakeSpot_Shipment_Break, $dbSpot_Shipment_Break->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Break()
    {
        $spotShipmentBreak = $this->makeSpot_Shipment_Break();
        $resp = $this->spotShipmentBreakRepo->delete($spotShipmentBreak->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Break::find($spotShipmentBreak->id), 'Spot_Shipment_Break should not exist in DB');
    }
}
