<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class User_GroupsApiTest extends TestCase
{
    use MakeUser_GroupsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateUser_Groups()
    {
        $userGroups = $this->fakeUser_GroupsData();
        $this->json('POST', '/api/v1/userGroups', $userGroups);

        $this->assertApiResponse($userGroups);
    }

    /**
     * @test
     */
    public function testReadUser_Groups()
    {
        $userGroups = $this->makeUser_Groups();
        $this->json('GET', '/api/v1/userGroups/'.$userGroups->id);

        $this->assertApiResponse($userGroups->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUser_Groups()
    {
        $userGroups = $this->makeUser_Groups();
        $editedUser_Groups = $this->fakeUser_GroupsData();

        $this->json('PUT', '/api/v1/userGroups/'.$userGroups->id, $editedUser_Groups);

        $this->assertApiResponse($editedUser_Groups);
    }

    /**
     * @test
     */
    public function testDeleteUser_Groups()
    {
        $userGroups = $this->makeUser_Groups();
        $this->json('DELETE', '/api/v1/userGroups/'.$userGroups->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/userGroups/'.$userGroups->id);

        $this->assertResponseStatus(404);
    }
}
