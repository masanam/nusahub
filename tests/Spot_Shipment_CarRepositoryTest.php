<?php

use App\Models\Spot_Shipment_Car;
use App\Repositories\Spot_Shipment_CarRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_CarRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_CarTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_CarRepository
     */
    protected $spotShipmentCarRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentCarRepo = App::make(Spot_Shipment_CarRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Car()
    {
        $spotShipmentCar = $this->fakeSpot_Shipment_CarData();
        $createdSpot_Shipment_Car = $this->spotShipmentCarRepo->create($spotShipmentCar);
        $createdSpot_Shipment_Car = $createdSpot_Shipment_Car->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Car);
        $this->assertNotNull($createdSpot_Shipment_Car['id'], 'Created Spot_Shipment_Car must have id specified');
        $this->assertNotNull(Spot_Shipment_Car::find($createdSpot_Shipment_Car['id']), 'Spot_Shipment_Car with given id must be in DB');
        $this->assertModelData($spotShipmentCar, $createdSpot_Shipment_Car);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Car()
    {
        $spotShipmentCar = $this->makeSpot_Shipment_Car();
        $dbSpot_Shipment_Car = $this->spotShipmentCarRepo->find($spotShipmentCar->id);
        $dbSpot_Shipment_Car = $dbSpot_Shipment_Car->toArray();
        $this->assertModelData($spotShipmentCar->toArray(), $dbSpot_Shipment_Car);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Car()
    {
        $spotShipmentCar = $this->makeSpot_Shipment_Car();
        $fakeSpot_Shipment_Car = $this->fakeSpot_Shipment_CarData();
        $updatedSpot_Shipment_Car = $this->spotShipmentCarRepo->update($fakeSpot_Shipment_Car, $spotShipmentCar->id);
        $this->assertModelData($fakeSpot_Shipment_Car, $updatedSpot_Shipment_Car->toArray());
        $dbSpot_Shipment_Car = $this->spotShipmentCarRepo->find($spotShipmentCar->id);
        $this->assertModelData($fakeSpot_Shipment_Car, $dbSpot_Shipment_Car->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Car()
    {
        $spotShipmentCar = $this->makeSpot_Shipment_Car();
        $resp = $this->spotShipmentCarRepo->delete($spotShipmentCar->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Car::find($spotShipmentCar->id), 'Spot_Shipment_Car should not exist in DB');
    }
}
