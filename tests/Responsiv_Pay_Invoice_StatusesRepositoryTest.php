<?php

use App\Models\Responsiv_Pay_Invoice_Statuses;
use App\Repositories\Responsiv_Pay_Invoice_StatusesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Responsiv_Pay_Invoice_StatusesRepositoryTest extends TestCase
{
    use MakeResponsiv_Pay_Invoice_StatusesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Responsiv_Pay_Invoice_StatusesRepository
     */
    protected $responsivPayInvoiceStatusesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->responsivPayInvoiceStatusesRepo = App::make(Responsiv_Pay_Invoice_StatusesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateResponsiv_Pay_Invoice_Statuses()
    {
        $responsivPayInvoiceStatuses = $this->fakeResponsiv_Pay_Invoice_StatusesData();
        $createdResponsiv_Pay_Invoice_Statuses = $this->responsivPayInvoiceStatusesRepo->create($responsivPayInvoiceStatuses);
        $createdResponsiv_Pay_Invoice_Statuses = $createdResponsiv_Pay_Invoice_Statuses->toArray();
        $this->assertArrayHasKey('id', $createdResponsiv_Pay_Invoice_Statuses);
        $this->assertNotNull($createdResponsiv_Pay_Invoice_Statuses['id'], 'Created Responsiv_Pay_Invoice_Statuses must have id specified');
        $this->assertNotNull(Responsiv_Pay_Invoice_Statuses::find($createdResponsiv_Pay_Invoice_Statuses['id']), 'Responsiv_Pay_Invoice_Statuses with given id must be in DB');
        $this->assertModelData($responsivPayInvoiceStatuses, $createdResponsiv_Pay_Invoice_Statuses);
    }

    /**
     * @test read
     */
    public function testReadResponsiv_Pay_Invoice_Statuses()
    {
        $responsivPayInvoiceStatuses = $this->makeResponsiv_Pay_Invoice_Statuses();
        $dbResponsiv_Pay_Invoice_Statuses = $this->responsivPayInvoiceStatusesRepo->find($responsivPayInvoiceStatuses->id);
        $dbResponsiv_Pay_Invoice_Statuses = $dbResponsiv_Pay_Invoice_Statuses->toArray();
        $this->assertModelData($responsivPayInvoiceStatuses->toArray(), $dbResponsiv_Pay_Invoice_Statuses);
    }

    /**
     * @test update
     */
    public function testUpdateResponsiv_Pay_Invoice_Statuses()
    {
        $responsivPayInvoiceStatuses = $this->makeResponsiv_Pay_Invoice_Statuses();
        $fakeResponsiv_Pay_Invoice_Statuses = $this->fakeResponsiv_Pay_Invoice_StatusesData();
        $updatedResponsiv_Pay_Invoice_Statuses = $this->responsivPayInvoiceStatusesRepo->update($fakeResponsiv_Pay_Invoice_Statuses, $responsivPayInvoiceStatuses->id);
        $this->assertModelData($fakeResponsiv_Pay_Invoice_Statuses, $updatedResponsiv_Pay_Invoice_Statuses->toArray());
        $dbResponsiv_Pay_Invoice_Statuses = $this->responsivPayInvoiceStatusesRepo->find($responsivPayInvoiceStatuses->id);
        $this->assertModelData($fakeResponsiv_Pay_Invoice_Statuses, $dbResponsiv_Pay_Invoice_Statuses->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteResponsiv_Pay_Invoice_Statuses()
    {
        $responsivPayInvoiceStatuses = $this->makeResponsiv_Pay_Invoice_Statuses();
        $resp = $this->responsivPayInvoiceStatusesRepo->delete($responsivPayInvoiceStatuses->id);
        $this->assertTrue($resp);
        $this->assertNull(Responsiv_Pay_Invoice_Statuses::find($responsivPayInvoiceStatuses->id), 'Responsiv_Pay_Invoice_Statuses should not exist in DB');
    }
}
