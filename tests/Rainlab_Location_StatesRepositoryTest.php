<?php

use App\Models\Rainlab_Location_States;
use App\Repositories\Rainlab_Location_StatesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Rainlab_Location_StatesRepositoryTest extends TestCase
{
    use MakeRainlab_Location_StatesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Rainlab_Location_StatesRepository
     */
    protected $rainlabLocationStatesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->rainlabLocationStatesRepo = App::make(Rainlab_Location_StatesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRainlab_Location_States()
    {
        $rainlabLocationStates = $this->fakeRainlab_Location_StatesData();
        $createdRainlab_Location_States = $this->rainlabLocationStatesRepo->create($rainlabLocationStates);
        $createdRainlab_Location_States = $createdRainlab_Location_States->toArray();
        $this->assertArrayHasKey('id', $createdRainlab_Location_States);
        $this->assertNotNull($createdRainlab_Location_States['id'], 'Created Rainlab_Location_States must have id specified');
        $this->assertNotNull(Rainlab_Location_States::find($createdRainlab_Location_States['id']), 'Rainlab_Location_States with given id must be in DB');
        $this->assertModelData($rainlabLocationStates, $createdRainlab_Location_States);
    }

    /**
     * @test read
     */
    public function testReadRainlab_Location_States()
    {
        $rainlabLocationStates = $this->makeRainlab_Location_States();
        $dbRainlab_Location_States = $this->rainlabLocationStatesRepo->find($rainlabLocationStates->id);
        $dbRainlab_Location_States = $dbRainlab_Location_States->toArray();
        $this->assertModelData($rainlabLocationStates->toArray(), $dbRainlab_Location_States);
    }

    /**
     * @test update
     */
    public function testUpdateRainlab_Location_States()
    {
        $rainlabLocationStates = $this->makeRainlab_Location_States();
        $fakeRainlab_Location_States = $this->fakeRainlab_Location_StatesData();
        $updatedRainlab_Location_States = $this->rainlabLocationStatesRepo->update($fakeRainlab_Location_States, $rainlabLocationStates->id);
        $this->assertModelData($fakeRainlab_Location_States, $updatedRainlab_Location_States->toArray());
        $dbRainlab_Location_States = $this->rainlabLocationStatesRepo->find($rainlabLocationStates->id);
        $this->assertModelData($fakeRainlab_Location_States, $dbRainlab_Location_States->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRainlab_Location_States()
    {
        $rainlabLocationStates = $this->makeRainlab_Location_States();
        $resp = $this->rainlabLocationStatesRepo->delete($rainlabLocationStates->id);
        $this->assertTrue($resp);
        $this->assertNull(Rainlab_Location_States::find($rainlabLocationStates->id), 'Rainlab_Location_States should not exist in DB');
    }
}
