<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_OrderApiTest extends TestCase
{
    use MakeSpot_Container_OrderTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Container_Order()
    {
        $spotContainerOrder = $this->fakeSpot_Container_OrderData();
        $this->json('POST', '/api/v1/spotContainerOrders', $spotContainerOrder);

        $this->assertApiResponse($spotContainerOrder);
    }

    /**
     * @test
     */
    public function testReadSpot_Container_Order()
    {
        $spotContainerOrder = $this->makeSpot_Container_Order();
        $this->json('GET', '/api/v1/spotContainerOrders/'.$spotContainerOrder->id);

        $this->assertApiResponse($spotContainerOrder->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Container_Order()
    {
        $spotContainerOrder = $this->makeSpot_Container_Order();
        $editedSpot_Container_Order = $this->fakeSpot_Container_OrderData();

        $this->json('PUT', '/api/v1/spotContainerOrders/'.$spotContainerOrder->id, $editedSpot_Container_Order);

        $this->assertApiResponse($editedSpot_Container_Order);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Container_Order()
    {
        $spotContainerOrder = $this->makeSpot_Container_Order();
        $this->json('DELETE', '/api/v1/spotContainerOrders/'.$spotContainerOrder->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotContainerOrders/'.$spotContainerOrder->id);

        $this->assertResponseStatus(404);
    }
}
