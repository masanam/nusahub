<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Responsiv_Pay_Invoice_StatusesApiTest extends TestCase
{
    use MakeResponsiv_Pay_Invoice_StatusesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateResponsiv_Pay_Invoice_Statuses()
    {
        $responsivPayInvoiceStatuses = $this->fakeResponsiv_Pay_Invoice_StatusesData();
        $this->json('POST', '/api/v1/responsivPayInvoiceStatuses', $responsivPayInvoiceStatuses);

        $this->assertApiResponse($responsivPayInvoiceStatuses);
    }

    /**
     * @test
     */
    public function testReadResponsiv_Pay_Invoice_Statuses()
    {
        $responsivPayInvoiceStatuses = $this->makeResponsiv_Pay_Invoice_Statuses();
        $this->json('GET', '/api/v1/responsivPayInvoiceStatuses/'.$responsivPayInvoiceStatuses->id);

        $this->assertApiResponse($responsivPayInvoiceStatuses->toArray());
    }

    /**
     * @test
     */
    public function testUpdateResponsiv_Pay_Invoice_Statuses()
    {
        $responsivPayInvoiceStatuses = $this->makeResponsiv_Pay_Invoice_Statuses();
        $editedResponsiv_Pay_Invoice_Statuses = $this->fakeResponsiv_Pay_Invoice_StatusesData();

        $this->json('PUT', '/api/v1/responsivPayInvoiceStatuses/'.$responsivPayInvoiceStatuses->id, $editedResponsiv_Pay_Invoice_Statuses);

        $this->assertApiResponse($editedResponsiv_Pay_Invoice_Statuses);
    }

    /**
     * @test
     */
    public function testDeleteResponsiv_Pay_Invoice_Statuses()
    {
        $responsivPayInvoiceStatuses = $this->makeResponsiv_Pay_Invoice_Statuses();
        $this->json('DELETE', '/api/v1/responsivPayInvoiceStatuses/'.$responsivPayInvoiceStatuses->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/responsivPayInvoiceStatuses/'.$responsivPayInvoiceStatuses->id);

        $this->assertResponseStatus(404);
    }
}
