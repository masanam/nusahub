<?php

use App\Models\User_Groups;
use App\Repositories\User_GroupsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class User_GroupsRepositoryTest extends TestCase
{
    use MakeUser_GroupsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var User_GroupsRepository
     */
    protected $userGroupsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->userGroupsRepo = App::make(User_GroupsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateUser_Groups()
    {
        $userGroups = $this->fakeUser_GroupsData();
        $createdUser_Groups = $this->userGroupsRepo->create($userGroups);
        $createdUser_Groups = $createdUser_Groups->toArray();
        $this->assertArrayHasKey('id', $createdUser_Groups);
        $this->assertNotNull($createdUser_Groups['id'], 'Created User_Groups must have id specified');
        $this->assertNotNull(User_Groups::find($createdUser_Groups['id']), 'User_Groups with given id must be in DB');
        $this->assertModelData($userGroups, $createdUser_Groups);
    }

    /**
     * @test read
     */
    public function testReadUser_Groups()
    {
        $userGroups = $this->makeUser_Groups();
        $dbUser_Groups = $this->userGroupsRepo->find($userGroups->id);
        $dbUser_Groups = $dbUser_Groups->toArray();
        $this->assertModelData($userGroups->toArray(), $dbUser_Groups);
    }

    /**
     * @test update
     */
    public function testUpdateUser_Groups()
    {
        $userGroups = $this->makeUser_Groups();
        $fakeUser_Groups = $this->fakeUser_GroupsData();
        $updatedUser_Groups = $this->userGroupsRepo->update($fakeUser_Groups, $userGroups->id);
        $this->assertModelData($fakeUser_Groups, $updatedUser_Groups->toArray());
        $dbUser_Groups = $this->userGroupsRepo->find($userGroups->id);
        $this->assertModelData($fakeUser_Groups, $dbUser_Groups->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteUser_Groups()
    {
        $userGroups = $this->makeUser_Groups();
        $resp = $this->userGroupsRepo->delete($userGroups->id);
        $this->assertTrue($resp);
        $this->assertNull(User_Groups::find($userGroups->id), 'User_Groups should not exist in DB');
    }
}
