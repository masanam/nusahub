<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Backend_User_GroupsApiTest extends TestCase
{
    use MakeBackend_User_GroupsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBackend_User_Groups()
    {
        $backendUserGroups = $this->fakeBackend_User_GroupsData();
        $this->json('POST', '/api/v1/backendUserGroups', $backendUserGroups);

        $this->assertApiResponse($backendUserGroups);
    }

    /**
     * @test
     */
    public function testReadBackend_User_Groups()
    {
        $backendUserGroups = $this->makeBackend_User_Groups();
        $this->json('GET', '/api/v1/backendUserGroups/'.$backendUserGroups->id);

        $this->assertApiResponse($backendUserGroups->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBackend_User_Groups()
    {
        $backendUserGroups = $this->makeBackend_User_Groups();
        $editedBackend_User_Groups = $this->fakeBackend_User_GroupsData();

        $this->json('PUT', '/api/v1/backendUserGroups/'.$backendUserGroups->id, $editedBackend_User_Groups);

        $this->assertApiResponse($editedBackend_User_Groups);
    }

    /**
     * @test
     */
    public function testDeleteBackend_User_Groups()
    {
        $backendUserGroups = $this->makeBackend_User_Groups();
        $this->json('DELETE', '/api/v1/backendUserGroups/'.$backendUserGroups->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/backendUserGroups/'.$backendUserGroups->id);

        $this->assertResponseStatus(404);
    }
}
