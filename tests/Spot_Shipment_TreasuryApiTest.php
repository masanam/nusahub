<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_TreasuryApiTest extends TestCase
{
    use MakeSpot_Shipment_TreasuryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Treasury()
    {
        $spotShipmentTreasury = $this->fakeSpot_Shipment_TreasuryData();
        $this->json('POST', '/api/v1/spotShipmentTreasuries', $spotShipmentTreasury);

        $this->assertApiResponse($spotShipmentTreasury);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Treasury()
    {
        $spotShipmentTreasury = $this->makeSpot_Shipment_Treasury();
        $this->json('GET', '/api/v1/spotShipmentTreasuries/'.$spotShipmentTreasury->id);

        $this->assertApiResponse($spotShipmentTreasury->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Treasury()
    {
        $spotShipmentTreasury = $this->makeSpot_Shipment_Treasury();
        $editedSpot_Shipment_Treasury = $this->fakeSpot_Shipment_TreasuryData();

        $this->json('PUT', '/api/v1/spotShipmentTreasuries/'.$spotShipmentTreasury->id, $editedSpot_Shipment_Treasury);

        $this->assertApiResponse($editedSpot_Shipment_Treasury);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Treasury()
    {
        $spotShipmentTreasury = $this->makeSpot_Shipment_Treasury();
        $this->json('DELETE', '/api/v1/spotShipmentTreasuries/'.$spotShipmentTreasury->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentTreasuries/'.$spotShipmentTreasury->id);

        $this->assertResponseStatus(404);
    }
}
