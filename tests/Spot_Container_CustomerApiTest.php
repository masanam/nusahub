<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_CustomerApiTest extends TestCase
{
    use MakeSpot_Container_CustomerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Container_Customer()
    {
        $spotContainerCustomer = $this->fakeSpot_Container_CustomerData();
        $this->json('POST', '/api/v1/spotContainerCustomers', $spotContainerCustomer);

        $this->assertApiResponse($spotContainerCustomer);
    }

    /**
     * @test
     */
    public function testReadSpot_Container_Customer()
    {
        $spotContainerCustomer = $this->makeSpot_Container_Customer();
        $this->json('GET', '/api/v1/spotContainerCustomers/'.$spotContainerCustomer->id);

        $this->assertApiResponse($spotContainerCustomer->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Container_Customer()
    {
        $spotContainerCustomer = $this->makeSpot_Container_Customer();
        $editedSpot_Container_Customer = $this->fakeSpot_Container_CustomerData();

        $this->json('PUT', '/api/v1/spotContainerCustomers/'.$spotContainerCustomer->id, $editedSpot_Container_Customer);

        $this->assertApiResponse($editedSpot_Container_Customer);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Container_Customer()
    {
        $spotContainerCustomer = $this->makeSpot_Container_Customer();
        $this->json('DELETE', '/api/v1/spotContainerCustomers/'.$spotContainerCustomer->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotContainerCustomers/'.$spotContainerCustomer->id);

        $this->assertResponseStatus(404);
    }
}
