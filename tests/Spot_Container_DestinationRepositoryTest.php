<?php

use App\Models\Spot_Container_Destination;
use App\Repositories\Spot_Container_DestinationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_DestinationRepositoryTest extends TestCase
{
    use MakeSpot_Container_DestinationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Container_DestinationRepository
     */
    protected $spotContainerDestinationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotContainerDestinationRepo = App::make(Spot_Container_DestinationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Container_Destination()
    {
        $spotContainerDestination = $this->fakeSpot_Container_DestinationData();
        $createdSpot_Container_Destination = $this->spotContainerDestinationRepo->create($spotContainerDestination);
        $createdSpot_Container_Destination = $createdSpot_Container_Destination->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Container_Destination);
        $this->assertNotNull($createdSpot_Container_Destination['id'], 'Created Spot_Container_Destination must have id specified');
        $this->assertNotNull(Spot_Container_Destination::find($createdSpot_Container_Destination['id']), 'Spot_Container_Destination with given id must be in DB');
        $this->assertModelData($spotContainerDestination, $createdSpot_Container_Destination);
    }

    /**
     * @test read
     */
    public function testReadSpot_Container_Destination()
    {
        $spotContainerDestination = $this->makeSpot_Container_Destination();
        $dbSpot_Container_Destination = $this->spotContainerDestinationRepo->find($spotContainerDestination->id);
        $dbSpot_Container_Destination = $dbSpot_Container_Destination->toArray();
        $this->assertModelData($spotContainerDestination->toArray(), $dbSpot_Container_Destination);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Container_Destination()
    {
        $spotContainerDestination = $this->makeSpot_Container_Destination();
        $fakeSpot_Container_Destination = $this->fakeSpot_Container_DestinationData();
        $updatedSpot_Container_Destination = $this->spotContainerDestinationRepo->update($fakeSpot_Container_Destination, $spotContainerDestination->id);
        $this->assertModelData($fakeSpot_Container_Destination, $updatedSpot_Container_Destination->toArray());
        $dbSpot_Container_Destination = $this->spotContainerDestinationRepo->find($spotContainerDestination->id);
        $this->assertModelData($fakeSpot_Container_Destination, $dbSpot_Container_Destination->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Container_Destination()
    {
        $spotContainerDestination = $this->makeSpot_Container_Destination();
        $resp = $this->spotContainerDestinationRepo->delete($spotContainerDestination->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Container_Destination::find($spotContainerDestination->id), 'Spot_Container_Destination should not exist in DB');
    }
}
