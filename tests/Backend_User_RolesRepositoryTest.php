<?php

use App\Models\Backend_User_Roles;
use App\Repositories\Backend_User_RolesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Backend_User_RolesRepositoryTest extends TestCase
{
    use MakeBackend_User_RolesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Backend_User_RolesRepository
     */
    protected $backendUserRolesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->backendUserRolesRepo = App::make(Backend_User_RolesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBackend_User_Roles()
    {
        $backendUserRoles = $this->fakeBackend_User_RolesData();
        $createdBackend_User_Roles = $this->backendUserRolesRepo->create($backendUserRoles);
        $createdBackend_User_Roles = $createdBackend_User_Roles->toArray();
        $this->assertArrayHasKey('id', $createdBackend_User_Roles);
        $this->assertNotNull($createdBackend_User_Roles['id'], 'Created Backend_User_Roles must have id specified');
        $this->assertNotNull(Backend_User_Roles::find($createdBackend_User_Roles['id']), 'Backend_User_Roles with given id must be in DB');
        $this->assertModelData($backendUserRoles, $createdBackend_User_Roles);
    }

    /**
     * @test read
     */
    public function testReadBackend_User_Roles()
    {
        $backendUserRoles = $this->makeBackend_User_Roles();
        $dbBackend_User_Roles = $this->backendUserRolesRepo->find($backendUserRoles->id);
        $dbBackend_User_Roles = $dbBackend_User_Roles->toArray();
        $this->assertModelData($backendUserRoles->toArray(), $dbBackend_User_Roles);
    }

    /**
     * @test update
     */
    public function testUpdateBackend_User_Roles()
    {
        $backendUserRoles = $this->makeBackend_User_Roles();
        $fakeBackend_User_Roles = $this->fakeBackend_User_RolesData();
        $updatedBackend_User_Roles = $this->backendUserRolesRepo->update($fakeBackend_User_Roles, $backendUserRoles->id);
        $this->assertModelData($fakeBackend_User_Roles, $updatedBackend_User_Roles->toArray());
        $dbBackend_User_Roles = $this->backendUserRolesRepo->find($backendUserRoles->id);
        $this->assertModelData($fakeBackend_User_Roles, $dbBackend_User_Roles->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBackend_User_Roles()
    {
        $backendUserRoles = $this->makeBackend_User_Roles();
        $resp = $this->backendUserRolesRepo->delete($backendUserRoles->id);
        $this->assertTrue($resp);
        $this->assertNull(Backend_User_Roles::find($backendUserRoles->id), 'Backend_User_Roles should not exist in DB');
    }
}
