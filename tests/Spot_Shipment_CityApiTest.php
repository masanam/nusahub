<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_CityApiTest extends TestCase
{
    use MakeSpot_Shipment_CityTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_City()
    {
        $spotShipmentCity = $this->fakeSpot_Shipment_CityData();
        $this->json('POST', '/api/v1/spotShipmentCities', $spotShipmentCity);

        $this->assertApiResponse($spotShipmentCity);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_City()
    {
        $spotShipmentCity = $this->makeSpot_Shipment_City();
        $this->json('GET', '/api/v1/spotShipmentCities/'.$spotShipmentCity->id);

        $this->assertApiResponse($spotShipmentCity->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_City()
    {
        $spotShipmentCity = $this->makeSpot_Shipment_City();
        $editedSpot_Shipment_City = $this->fakeSpot_Shipment_CityData();

        $this->json('PUT', '/api/v1/spotShipmentCities/'.$spotShipmentCity->id, $editedSpot_Shipment_City);

        $this->assertApiResponse($editedSpot_Shipment_City);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_City()
    {
        $spotShipmentCity = $this->makeSpot_Shipment_City();
        $this->json('DELETE', '/api/v1/spotShipmentCities/'.$spotShipmentCity->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentCities/'.$spotShipmentCity->id);

        $this->assertResponseStatus(404);
    }
}
