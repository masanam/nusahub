<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_CourierApiTest extends TestCase
{
    use MakeSpot_Shipment_CourierTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Courier()
    {
        $spotShipmentCourier = $this->fakeSpot_Shipment_CourierData();
        $this->json('POST', '/api/v1/spotShipmentCouriers', $spotShipmentCourier);

        $this->assertApiResponse($spotShipmentCourier);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Courier()
    {
        $spotShipmentCourier = $this->makeSpot_Shipment_Courier();
        $this->json('GET', '/api/v1/spotShipmentCouriers/'.$spotShipmentCourier->id);

        $this->assertApiResponse($spotShipmentCourier->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Courier()
    {
        $spotShipmentCourier = $this->makeSpot_Shipment_Courier();
        $editedSpot_Shipment_Courier = $this->fakeSpot_Shipment_CourierData();

        $this->json('PUT', '/api/v1/spotShipmentCouriers/'.$spotShipmentCourier->id, $editedSpot_Shipment_Courier);

        $this->assertApiResponse($editedSpot_Shipment_Courier);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Courier()
    {
        $spotShipmentCourier = $this->makeSpot_Shipment_Courier();
        $this->json('DELETE', '/api/v1/spotShipmentCouriers/'.$spotShipmentCourier->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentCouriers/'.$spotShipmentCourier->id);

        $this->assertResponseStatus(404);
    }
}
