<?php

use App\Models\Spot_Container_Status;
use App\Repositories\Spot_Container_StatusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Container_StatusRepositoryTest extends TestCase
{
    use MakeSpot_Container_StatusTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Container_StatusRepository
     */
    protected $spotContainerStatusRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotContainerStatusRepo = App::make(Spot_Container_StatusRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Container_Status()
    {
        $spotContainerStatus = $this->fakeSpot_Container_StatusData();
        $createdSpot_Container_Status = $this->spotContainerStatusRepo->create($spotContainerStatus);
        $createdSpot_Container_Status = $createdSpot_Container_Status->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Container_Status);
        $this->assertNotNull($createdSpot_Container_Status['id'], 'Created Spot_Container_Status must have id specified');
        $this->assertNotNull(Spot_Container_Status::find($createdSpot_Container_Status['id']), 'Spot_Container_Status with given id must be in DB');
        $this->assertModelData($spotContainerStatus, $createdSpot_Container_Status);
    }

    /**
     * @test read
     */
    public function testReadSpot_Container_Status()
    {
        $spotContainerStatus = $this->makeSpot_Container_Status();
        $dbSpot_Container_Status = $this->spotContainerStatusRepo->find($spotContainerStatus->id);
        $dbSpot_Container_Status = $dbSpot_Container_Status->toArray();
        $this->assertModelData($spotContainerStatus->toArray(), $dbSpot_Container_Status);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Container_Status()
    {
        $spotContainerStatus = $this->makeSpot_Container_Status();
        $fakeSpot_Container_Status = $this->fakeSpot_Container_StatusData();
        $updatedSpot_Container_Status = $this->spotContainerStatusRepo->update($fakeSpot_Container_Status, $spotContainerStatus->id);
        $this->assertModelData($fakeSpot_Container_Status, $updatedSpot_Container_Status->toArray());
        $dbSpot_Container_Status = $this->spotContainerStatusRepo->find($spotContainerStatus->id);
        $this->assertModelData($fakeSpot_Container_Status, $dbSpot_Container_Status->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Container_Status()
    {
        $spotContainerStatus = $this->makeSpot_Container_Status();
        $resp = $this->spotContainerStatusRepo->delete($spotContainerStatus->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Container_Status::find($spotContainerStatus->id), 'Spot_Container_Status should not exist in DB');
    }
}
