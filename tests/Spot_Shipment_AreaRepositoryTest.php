<?php

use App\Models\Spot_Shipment_Area;
use App\Repositories\Spot_Shipment_AreaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_AreaRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_AreaTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_AreaRepository
     */
    protected $spotShipmentAreaRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentAreaRepo = App::make(Spot_Shipment_AreaRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Area()
    {
        $spotShipmentArea = $this->fakeSpot_Shipment_AreaData();
        $createdSpot_Shipment_Area = $this->spotShipmentAreaRepo->create($spotShipmentArea);
        $createdSpot_Shipment_Area = $createdSpot_Shipment_Area->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Area);
        $this->assertNotNull($createdSpot_Shipment_Area['id'], 'Created Spot_Shipment_Area must have id specified');
        $this->assertNotNull(Spot_Shipment_Area::find($createdSpot_Shipment_Area['id']), 'Spot_Shipment_Area with given id must be in DB');
        $this->assertModelData($spotShipmentArea, $createdSpot_Shipment_Area);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Area()
    {
        $spotShipmentArea = $this->makeSpot_Shipment_Area();
        $dbSpot_Shipment_Area = $this->spotShipmentAreaRepo->find($spotShipmentArea->id);
        $dbSpot_Shipment_Area = $dbSpot_Shipment_Area->toArray();
        $this->assertModelData($spotShipmentArea->toArray(), $dbSpot_Shipment_Area);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Area()
    {
        $spotShipmentArea = $this->makeSpot_Shipment_Area();
        $fakeSpot_Shipment_Area = $this->fakeSpot_Shipment_AreaData();
        $updatedSpot_Shipment_Area = $this->spotShipmentAreaRepo->update($fakeSpot_Shipment_Area, $spotShipmentArea->id);
        $this->assertModelData($fakeSpot_Shipment_Area, $updatedSpot_Shipment_Area->toArray());
        $dbSpot_Shipment_Area = $this->spotShipmentAreaRepo->find($spotShipmentArea->id);
        $this->assertModelData($fakeSpot_Shipment_Area, $dbSpot_Shipment_Area->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Area()
    {
        $spotShipmentArea = $this->makeSpot_Shipment_Area();
        $resp = $this->spotShipmentAreaRepo->delete($spotShipmentArea->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Area::find($spotShipmentArea->id), 'Spot_Shipment_Area should not exist in DB');
    }
}
