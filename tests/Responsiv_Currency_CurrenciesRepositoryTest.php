<?php

use App\Models\Responsiv_Currency_Currencies;
use App\Repositories\Responsiv_Currency_CurrenciesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Responsiv_Currency_CurrenciesRepositoryTest extends TestCase
{
    use MakeResponsiv_Currency_CurrenciesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Responsiv_Currency_CurrenciesRepository
     */
    protected $responsivCurrencyCurrenciesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->responsivCurrencyCurrenciesRepo = App::make(Responsiv_Currency_CurrenciesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateResponsiv_Currency_Currencies()
    {
        $responsivCurrencyCurrencies = $this->fakeResponsiv_Currency_CurrenciesData();
        $createdResponsiv_Currency_Currencies = $this->responsivCurrencyCurrenciesRepo->create($responsivCurrencyCurrencies);
        $createdResponsiv_Currency_Currencies = $createdResponsiv_Currency_Currencies->toArray();
        $this->assertArrayHasKey('id', $createdResponsiv_Currency_Currencies);
        $this->assertNotNull($createdResponsiv_Currency_Currencies['id'], 'Created Responsiv_Currency_Currencies must have id specified');
        $this->assertNotNull(Responsiv_Currency_Currencies::find($createdResponsiv_Currency_Currencies['id']), 'Responsiv_Currency_Currencies with given id must be in DB');
        $this->assertModelData($responsivCurrencyCurrencies, $createdResponsiv_Currency_Currencies);
    }

    /**
     * @test read
     */
    public function testReadResponsiv_Currency_Currencies()
    {
        $responsivCurrencyCurrencies = $this->makeResponsiv_Currency_Currencies();
        $dbResponsiv_Currency_Currencies = $this->responsivCurrencyCurrenciesRepo->find($responsivCurrencyCurrencies->id);
        $dbResponsiv_Currency_Currencies = $dbResponsiv_Currency_Currencies->toArray();
        $this->assertModelData($responsivCurrencyCurrencies->toArray(), $dbResponsiv_Currency_Currencies);
    }

    /**
     * @test update
     */
    public function testUpdateResponsiv_Currency_Currencies()
    {
        $responsivCurrencyCurrencies = $this->makeResponsiv_Currency_Currencies();
        $fakeResponsiv_Currency_Currencies = $this->fakeResponsiv_Currency_CurrenciesData();
        $updatedResponsiv_Currency_Currencies = $this->responsivCurrencyCurrenciesRepo->update($fakeResponsiv_Currency_Currencies, $responsivCurrencyCurrencies->id);
        $this->assertModelData($fakeResponsiv_Currency_Currencies, $updatedResponsiv_Currency_Currencies->toArray());
        $dbResponsiv_Currency_Currencies = $this->responsivCurrencyCurrenciesRepo->find($responsivCurrencyCurrencies->id);
        $this->assertModelData($fakeResponsiv_Currency_Currencies, $dbResponsiv_Currency_Currencies->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteResponsiv_Currency_Currencies()
    {
        $responsivCurrencyCurrencies = $this->makeResponsiv_Currency_Currencies();
        $resp = $this->responsivCurrencyCurrenciesRepo->delete($responsivCurrencyCurrencies->id);
        $this->assertTrue($resp);
        $this->assertNull(Responsiv_Currency_Currencies::find($responsivCurrencyCurrencies->id), 'Responsiv_Currency_Currencies should not exist in DB');
    }
}
