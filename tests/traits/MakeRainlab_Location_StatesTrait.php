<?php

use Faker\Factory as Faker;
use App\Models\Rainlab_Location_States;
use App\Repositories\Rainlab_Location_StatesRepository;

trait MakeRainlab_Location_StatesTrait
{
    /**
     * Create fake instance of Rainlab_Location_States and save it in database
     *
     * @param array $rainlabLocationStatesFields
     * @return Rainlab_Location_States
     */
    public function makeRainlab_Location_States($rainlabLocationStatesFields = [])
    {
        /** @var Rainlab_Location_StatesRepository $rainlabLocationStatesRepo */
        $rainlabLocationStatesRepo = App::make(Rainlab_Location_StatesRepository::class);
        $theme = $this->fakeRainlab_Location_StatesData($rainlabLocationStatesFields);
        return $rainlabLocationStatesRepo->create($theme);
    }

    /**
     * Get fake instance of Rainlab_Location_States
     *
     * @param array $rainlabLocationStatesFields
     * @return Rainlab_Location_States
     */
    public function fakeRainlab_Location_States($rainlabLocationStatesFields = [])
    {
        return new Rainlab_Location_States($this->fakeRainlab_Location_StatesData($rainlabLocationStatesFields));
    }

    /**
     * Get fake data of Rainlab_Location_States
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRainlab_Location_StatesData($rainlabLocationStatesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'country_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'code' => $fake->word
        ], $rainlabLocationStatesFields);
    }
}
