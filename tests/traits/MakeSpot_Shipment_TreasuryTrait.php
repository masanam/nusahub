<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Treasury;
use App\Repositories\Spot_Shipment_TreasuryRepository;

trait MakeSpot_Shipment_TreasuryTrait
{
    /**
     * Create fake instance of Spot_Shipment_Treasury and save it in database
     *
     * @param array $spotShipmentTreasuryFields
     * @return Spot_Shipment_Treasury
     */
    public function makeSpot_Shipment_Treasury($spotShipmentTreasuryFields = [])
    {
        /** @var Spot_Shipment_TreasuryRepository $spotShipmentTreasuryRepo */
        $spotShipmentTreasuryRepo = App::make(Spot_Shipment_TreasuryRepository::class);
        $theme = $this->fakeSpot_Shipment_TreasuryData($spotShipmentTreasuryFields);
        return $spotShipmentTreasuryRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Treasury
     *
     * @param array $spotShipmentTreasuryFields
     * @return Spot_Shipment_Treasury
     */
    public function fakeSpot_Shipment_Treasury($spotShipmentTreasuryFields = [])
    {
        return new Spot_Shipment_Treasury($this->fakeSpot_Shipment_TreasuryData($spotShipmentTreasuryFields));
    }

    /**
     * Get fake data of Spot_Shipment_Treasury
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_TreasuryData($spotShipmentTreasuryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'default' => $fake->word,
            'office_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'balance' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $spotShipmentTreasuryFields);
    }
}
