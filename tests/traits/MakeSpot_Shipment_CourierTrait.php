<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Courier;
use App\Repositories\Spot_Shipment_CourierRepository;

trait MakeSpot_Shipment_CourierTrait
{
    /**
     * Create fake instance of Spot_Shipment_Courier and save it in database
     *
     * @param array $spotShipmentCourierFields
     * @return Spot_Shipment_Courier
     */
    public function makeSpot_Shipment_Courier($spotShipmentCourierFields = [])
    {
        /** @var Spot_Shipment_CourierRepository $spotShipmentCourierRepo */
        $spotShipmentCourierRepo = App::make(Spot_Shipment_CourierRepository::class);
        $theme = $this->fakeSpot_Shipment_CourierData($spotShipmentCourierFields);
        return $spotShipmentCourierRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Courier
     *
     * @param array $spotShipmentCourierFields
     * @return Spot_Shipment_Courier
     */
    public function fakeSpot_Shipment_Courier($spotShipmentCourierFields = [])
    {
        return new Spot_Shipment_Courier($this->fakeSpot_Shipment_CourierData($spotShipmentCourierFields));
    }

    /**
     * Get fake data of Spot_Shipment_Courier
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_CourierData($spotShipmentCourierFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'address' => $fake->word,
            'phone' => $fake->word
        ], $spotShipmentCourierFields);
    }
}
