<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Packaging;
use App\Repositories\Spot_Shipment_PackagingRepository;

trait MakeSpot_Shipment_PackagingTrait
{
    /**
     * Create fake instance of Spot_Shipment_Packaging and save it in database
     *
     * @param array $spotShipmentPackagingFields
     * @return Spot_Shipment_Packaging
     */
    public function makeSpot_Shipment_Packaging($spotShipmentPackagingFields = [])
    {
        /** @var Spot_Shipment_PackagingRepository $spotShipmentPackagingRepo */
        $spotShipmentPackagingRepo = App::make(Spot_Shipment_PackagingRepository::class);
        $theme = $this->fakeSpot_Shipment_PackagingData($spotShipmentPackagingFields);
        return $spotShipmentPackagingRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Packaging
     *
     * @param array $spotShipmentPackagingFields
     * @return Spot_Shipment_Packaging
     */
    public function fakeSpot_Shipment_Packaging($spotShipmentPackagingFields = [])
    {
        return new Spot_Shipment_Packaging($this->fakeSpot_Shipment_PackagingData($spotShipmentPackagingFields));
    }

    /**
     * Get fake data of Spot_Shipment_Packaging
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_PackagingData($spotShipmentPackagingFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text
        ], $spotShipmentPackagingFields);
    }
}
