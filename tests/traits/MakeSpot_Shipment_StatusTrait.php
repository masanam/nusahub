<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Status;
use App\Repositories\Spot_Shipment_StatusRepository;

trait MakeSpot_Shipment_StatusTrait
{
    /**
     * Create fake instance of Spot_Shipment_Status and save it in database
     *
     * @param array $spotShipmentStatusFields
     * @return Spot_Shipment_Status
     */
    public function makeSpot_Shipment_Status($spotShipmentStatusFields = [])
    {
        /** @var Spot_Shipment_StatusRepository $spotShipmentStatusRepo */
        $spotShipmentStatusRepo = App::make(Spot_Shipment_StatusRepository::class);
        $theme = $this->fakeSpot_Shipment_StatusData($spotShipmentStatusFields);
        return $spotShipmentStatusRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Status
     *
     * @param array $spotShipmentStatusFields
     * @return Spot_Shipment_Status
     */
    public function fakeSpot_Shipment_Status($spotShipmentStatusFields = [])
    {
        return new Spot_Shipment_Status($this->fakeSpot_Shipment_StatusData($spotShipmentStatusFields));
    }

    /**
     * Get fake data of Spot_Shipment_Status
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_StatusData($spotShipmentStatusFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text,
            'color' => $fake->word,
            'equal' => $fake->randomDigitNotNull
        ], $spotShipmentStatusFields);
    }
}
