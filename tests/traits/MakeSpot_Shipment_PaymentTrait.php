<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Payment;
use App\Repositories\Spot_Shipment_PaymentRepository;

trait MakeSpot_Shipment_PaymentTrait
{
    /**
     * Create fake instance of Spot_Shipment_Payment and save it in database
     *
     * @param array $spotShipmentPaymentFields
     * @return Spot_Shipment_Payment
     */
    public function makeSpot_Shipment_Payment($spotShipmentPaymentFields = [])
    {
        /** @var Spot_Shipment_PaymentRepository $spotShipmentPaymentRepo */
        $spotShipmentPaymentRepo = App::make(Spot_Shipment_PaymentRepository::class);
        $theme = $this->fakeSpot_Shipment_PaymentData($spotShipmentPaymentFields);
        return $spotShipmentPaymentRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Payment
     *
     * @param array $spotShipmentPaymentFields
     * @return Spot_Shipment_Payment
     */
    public function fakeSpot_Shipment_Payment($spotShipmentPaymentFields = [])
    {
        return new Spot_Shipment_Payment($this->fakeSpot_Shipment_PaymentData($spotShipmentPaymentFields));
    }

    /**
     * Get fake data of Spot_Shipment_Payment
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_PaymentData($spotShipmentPaymentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'type' => $fake->randomDigitNotNull,
            'for_id' => $fake->randomDigitNotNull,
            'movement' => $fake->randomDigitNotNull,
            'other' => $fake->word,
            'description' => $fake->text,
            'amount' => $fake->word,
            'status' => $fake->randomDigitNotNull,
            'date' => $fake->word,
            'item_id' => $fake->randomDigitNotNull,
            'payment_type' => $fake->word,
            'payment_with' => $fake->randomDigitNotNull,
            'payment_method' => $fake->word,
            'treasury_id' => $fake->randomDigitNotNull,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $spotShipmentPaymentFields);
    }
}
