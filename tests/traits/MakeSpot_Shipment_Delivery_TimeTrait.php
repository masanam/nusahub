<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Delivery_Time;
use App\Repositories\Spot_Shipment_Delivery_TimeRepository;

trait MakeSpot_Shipment_Delivery_TimeTrait
{
    /**
     * Create fake instance of Spot_Shipment_Delivery_Time and save it in database
     *
     * @param array $spotShipmentDeliveryTimeFields
     * @return Spot_Shipment_Delivery_Time
     */
    public function makeSpot_Shipment_Delivery_Time($spotShipmentDeliveryTimeFields = [])
    {
        /** @var Spot_Shipment_Delivery_TimeRepository $spotShipmentDeliveryTimeRepo */
        $spotShipmentDeliveryTimeRepo = App::make(Spot_Shipment_Delivery_TimeRepository::class);
        $theme = $this->fakeSpot_Shipment_Delivery_TimeData($spotShipmentDeliveryTimeFields);
        return $spotShipmentDeliveryTimeRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Delivery_Time
     *
     * @param array $spotShipmentDeliveryTimeFields
     * @return Spot_Shipment_Delivery_Time
     */
    public function fakeSpot_Shipment_Delivery_Time($spotShipmentDeliveryTimeFields = [])
    {
        return new Spot_Shipment_Delivery_Time($this->fakeSpot_Shipment_Delivery_TimeData($spotShipmentDeliveryTimeFields));
    }

    /**
     * Get fake data of Spot_Shipment_Delivery_Time
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_Delivery_TimeData($spotShipmentDeliveryTimeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'count' => $fake->randomDigitNotNull
        ], $spotShipmentDeliveryTimeFields);
    }
}
