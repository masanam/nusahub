<?php

use Faker\Factory as Faker;
use App\Models\Spot_Container_Size;
use App\Repositories\Spot_Container_SizeRepository;

trait MakeSpot_Container_SizeTrait
{
    /**
     * Create fake instance of Spot_Container_Size and save it in database
     *
     * @param array $spotContainerSizeFields
     * @return Spot_Container_Size
     */
    public function makeSpot_Container_Size($spotContainerSizeFields = [])
    {
        /** @var Spot_Container_SizeRepository $spotContainerSizeRepo */
        $spotContainerSizeRepo = App::make(Spot_Container_SizeRepository::class);
        $theme = $this->fakeSpot_Container_SizeData($spotContainerSizeFields);
        return $spotContainerSizeRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Container_Size
     *
     * @param array $spotContainerSizeFields
     * @return Spot_Container_Size
     */
    public function fakeSpot_Container_Size($spotContainerSizeFields = [])
    {
        return new Spot_Container_Size($this->fakeSpot_Container_SizeData($spotContainerSizeFields));
    }

    /**
     * Get fake data of Spot_Container_Size
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Container_SizeData($spotContainerSizeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'size_desc' => $fake->text
        ], $spotContainerSizeFields);
    }
}
