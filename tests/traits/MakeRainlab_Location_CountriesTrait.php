<?php

use Faker\Factory as Faker;
use App\Models\Rainlab_Location_Countries;
use App\Repositories\Rainlab_Location_CountriesRepository;

trait MakeRainlab_Location_CountriesTrait
{
    /**
     * Create fake instance of Rainlab_Location_Countries and save it in database
     *
     * @param array $rainlabLocationCountriesFields
     * @return Rainlab_Location_Countries
     */
    public function makeRainlab_Location_Countries($rainlabLocationCountriesFields = [])
    {
        /** @var Rainlab_Location_CountriesRepository $rainlabLocationCountriesRepo */
        $rainlabLocationCountriesRepo = App::make(Rainlab_Location_CountriesRepository::class);
        $theme = $this->fakeRainlab_Location_CountriesData($rainlabLocationCountriesFields);
        return $rainlabLocationCountriesRepo->create($theme);
    }

    /**
     * Get fake instance of Rainlab_Location_Countries
     *
     * @param array $rainlabLocationCountriesFields
     * @return Rainlab_Location_Countries
     */
    public function fakeRainlab_Location_Countries($rainlabLocationCountriesFields = [])
    {
        return new Rainlab_Location_Countries($this->fakeRainlab_Location_CountriesData($rainlabLocationCountriesFields));
    }

    /**
     * Get fake data of Rainlab_Location_Countries
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRainlab_Location_CountriesData($rainlabLocationCountriesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'is_enabled' => $fake->word,
            'name' => $fake->word,
            'code' => $fake->word,
            'is_pinned' => $fake->word
        ], $rainlabLocationCountriesFields);
    }
}
