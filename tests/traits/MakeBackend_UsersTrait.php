<?php

use Faker\Factory as Faker;
use App\Models\Backend_Users;
use App\Repositories\Backend_UsersRepository;

trait MakeBackend_UsersTrait
{
    /**
     * Create fake instance of Backend_Users and save it in database
     *
     * @param array $backendUsersFields
     * @return Backend_Users
     */
    public function makeBackend_Users($backendUsersFields = [])
    {
        /** @var Backend_UsersRepository $backendUsersRepo */
        $backendUsersRepo = App::make(Backend_UsersRepository::class);
        $theme = $this->fakeBackend_UsersData($backendUsersFields);
        return $backendUsersRepo->create($theme);
    }

    /**
     * Get fake instance of Backend_Users
     *
     * @param array $backendUsersFields
     * @return Backend_Users
     */
    public function fakeBackend_Users($backendUsersFields = [])
    {
        return new Backend_Users($this->fakeBackend_UsersData($backendUsersFields));
    }

    /**
     * Get fake data of Backend_Users
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBackend_UsersData($backendUsersFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'first_name' => $fake->word,
            'last_name' => $fake->word,
            'login' => $fake->word,
            'email' => $fake->word,
            'password' => $fake->word,
            'activation_code' => $fake->word,
            'persist_code' => $fake->word,
            'reset_password_code' => $fake->word,
            'permissions' => $fake->text,
            'is_activated' => $fake->word,
            'role_id' => $fake->randomDigitNotNull,
            'activated_at' => $fake->date('Y-m-d H:i:s'),
            'last_login' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'is_superuser' => $fake->word
        ], $backendUsersFields);
    }
}
