<?php

use Faker\Factory as Faker;
use App\Models\Spot_Container_Reason;
use App\Repositories\Spot_Container_ReasonRepository;

trait MakeSpot_Container_ReasonTrait
{
    /**
     * Create fake instance of Spot_Container_Reason and save it in database
     *
     * @param array $spotContainerReasonFields
     * @return Spot_Container_Reason
     */
    public function makeSpot_Container_Reason($spotContainerReasonFields = [])
    {
        /** @var Spot_Container_ReasonRepository $spotContainerReasonRepo */
        $spotContainerReasonRepo = App::make(Spot_Container_ReasonRepository::class);
        $theme = $this->fakeSpot_Container_ReasonData($spotContainerReasonFields);
        return $spotContainerReasonRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Container_Reason
     *
     * @param array $spotContainerReasonFields
     * @return Spot_Container_Reason
     */
    public function fakeSpot_Container_Reason($spotContainerReasonFields = [])
    {
        return new Spot_Container_Reason($this->fakeSpot_Container_ReasonData($spotContainerReasonFields));
    }

    /**
     * Get fake data of Spot_Container_Reason
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Container_ReasonData($spotContainerReasonFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'reason_desc' => $fake->text
        ], $spotContainerReasonFields);
    }
}
