<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Label;
use App\Repositories\Spot_Shipment_LabelRepository;

trait MakeSpot_Shipment_LabelTrait
{
    /**
     * Create fake instance of Spot_Shipment_Label and save it in database
     *
     * @param array $spotShipmentLabelFields
     * @return Spot_Shipment_Label
     */
    public function makeSpot_Shipment_Label($spotShipmentLabelFields = [])
    {
        /** @var Spot_Shipment_LabelRepository $spotShipmentLabelRepo */
        $spotShipmentLabelRepo = App::make(Spot_Shipment_LabelRepository::class);
        $theme = $this->fakeSpot_Shipment_LabelData($spotShipmentLabelFields);
        return $spotShipmentLabelRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Label
     *
     * @param array $spotShipmentLabelFields
     * @return Spot_Shipment_Label
     */
    public function fakeSpot_Shipment_Label($spotShipmentLabelFields = [])
    {
        return new Spot_Shipment_Label($this->fakeSpot_Shipment_LabelData($spotShipmentLabelFields));
    }

    /**
     * Get fake data of Spot_Shipment_Label
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_LabelData($spotShipmentLabelFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'label_desc' => $fake->text
        ], $spotShipmentLabelFields);
    }
}
