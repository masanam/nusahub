<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Break;
use App\Repositories\Spot_Shipment_BreakRepository;

trait MakeSpot_Shipment_BreakTrait
{
    /**
     * Create fake instance of Spot_Shipment_Break and save it in database
     *
     * @param array $spotShipmentBreakFields
     * @return Spot_Shipment_Break
     */
    public function makeSpot_Shipment_Break($spotShipmentBreakFields = [])
    {
        /** @var Spot_Shipment_BreakRepository $spotShipmentBreakRepo */
        $spotShipmentBreakRepo = App::make(Spot_Shipment_BreakRepository::class);
        $theme = $this->fakeSpot_Shipment_BreakData($spotShipmentBreakFields);
        return $spotShipmentBreakRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Break
     *
     * @param array $spotShipmentBreakFields
     * @return Spot_Shipment_Break
     */
    public function fakeSpot_Shipment_Break($spotShipmentBreakFields = [])
    {
        return new Spot_Shipment_Break($this->fakeSpot_Shipment_BreakData($spotShipmentBreakFields));
    }

    /**
     * Get fake data of Spot_Shipment_Break
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_BreakData($spotShipmentBreakFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'break_desc' => $fake->text
        ], $spotShipmentBreakFields);
    }
}
