<?php

use Faker\Factory as Faker;
use App\Models\Responsiv_Pay_Invoice_Statuses;
use App\Repositories\Responsiv_Pay_Invoice_StatusesRepository;

trait MakeResponsiv_Pay_Invoice_StatusesTrait
{
    /**
     * Create fake instance of Responsiv_Pay_Invoice_Statuses and save it in database
     *
     * @param array $responsivPayInvoiceStatusesFields
     * @return Responsiv_Pay_Invoice_Statuses
     */
    public function makeResponsiv_Pay_Invoice_Statuses($responsivPayInvoiceStatusesFields = [])
    {
        /** @var Responsiv_Pay_Invoice_StatusesRepository $responsivPayInvoiceStatusesRepo */
        $responsivPayInvoiceStatusesRepo = App::make(Responsiv_Pay_Invoice_StatusesRepository::class);
        $theme = $this->fakeResponsiv_Pay_Invoice_StatusesData($responsivPayInvoiceStatusesFields);
        return $responsivPayInvoiceStatusesRepo->create($theme);
    }

    /**
     * Get fake instance of Responsiv_Pay_Invoice_Statuses
     *
     * @param array $responsivPayInvoiceStatusesFields
     * @return Responsiv_Pay_Invoice_Statuses
     */
    public function fakeResponsiv_Pay_Invoice_Statuses($responsivPayInvoiceStatusesFields = [])
    {
        return new Responsiv_Pay_Invoice_Statuses($this->fakeResponsiv_Pay_Invoice_StatusesData($responsivPayInvoiceStatusesFields));
    }

    /**
     * Get fake data of Responsiv_Pay_Invoice_Statuses
     *
     * @param array $postFields
     * @return array
     */
    public function fakeResponsiv_Pay_Invoice_StatusesData($responsivPayInvoiceStatusesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'code' => $fake->word,
            'is_enabled' => $fake->word,
            'notify_user' => $fake->word,
            'notify_template' => $fake->word
        ], $responsivPayInvoiceStatusesFields);
    }
}
