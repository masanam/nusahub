<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Mode;
use App\Repositories\Spot_Shipment_ModeRepository;

trait MakeSpot_Shipment_ModeTrait
{
    /**
     * Create fake instance of Spot_Shipment_Mode and save it in database
     *
     * @param array $spotShipmentModeFields
     * @return Spot_Shipment_Mode
     */
    public function makeSpot_Shipment_Mode($spotShipmentModeFields = [])
    {
        /** @var Spot_Shipment_ModeRepository $spotShipmentModeRepo */
        $spotShipmentModeRepo = App::make(Spot_Shipment_ModeRepository::class);
        $theme = $this->fakeSpot_Shipment_ModeData($spotShipmentModeFields);
        return $spotShipmentModeRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Mode
     *
     * @param array $spotShipmentModeFields
     * @return Spot_Shipment_Mode
     */
    public function fakeSpot_Shipment_Mode($spotShipmentModeFields = [])
    {
        return new Spot_Shipment_Mode($this->fakeSpot_Shipment_ModeData($spotShipmentModeFields));
    }

    /**
     * Get fake data of Spot_Shipment_Mode
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_ModeData($spotShipmentModeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text
        ], $spotShipmentModeFields);
    }
}
