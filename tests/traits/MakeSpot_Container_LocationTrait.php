<?php

use Faker\Factory as Faker;
use App\Models\Spot_Container_Location;
use App\Repositories\Spot_Container_LocationRepository;

trait MakeSpot_Container_LocationTrait
{
    /**
     * Create fake instance of Spot_Container_Location and save it in database
     *
     * @param array $spotContainerLocationFields
     * @return Spot_Container_Location
     */
    public function makeSpot_Container_Location($spotContainerLocationFields = [])
    {
        /** @var Spot_Container_LocationRepository $spotContainerLocationRepo */
        $spotContainerLocationRepo = App::make(Spot_Container_LocationRepository::class);
        $theme = $this->fakeSpot_Container_LocationData($spotContainerLocationFields);
        return $spotContainerLocationRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Container_Location
     *
     * @param array $spotContainerLocationFields
     * @return Spot_Container_Location
     */
    public function fakeSpot_Container_Location($spotContainerLocationFields = [])
    {
        return new Spot_Container_Location($this->fakeSpot_Container_LocationData($spotContainerLocationFields));
    }

    /**
     * Get fake data of Spot_Container_Location
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Container_LocationData($spotContainerLocationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'location_desc' => $fake->text
        ], $spotContainerLocationFields);
    }
}
