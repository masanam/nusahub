<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Handler;
use App\Repositories\Spot_Shipment_HandlerRepository;

trait MakeSpot_Shipment_HandlerTrait
{
    /**
     * Create fake instance of Spot_Shipment_Handler and save it in database
     *
     * @param array $spotShipmentHandlerFields
     * @return Spot_Shipment_Handler
     */
    public function makeSpot_Shipment_Handler($spotShipmentHandlerFields = [])
    {
        /** @var Spot_Shipment_HandlerRepository $spotShipmentHandlerRepo */
        $spotShipmentHandlerRepo = App::make(Spot_Shipment_HandlerRepository::class);
        $theme = $this->fakeSpot_Shipment_HandlerData($spotShipmentHandlerFields);
        return $spotShipmentHandlerRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Handler
     *
     * @param array $spotShipmentHandlerFields
     * @return Spot_Shipment_Handler
     */
    public function fakeSpot_Shipment_Handler($spotShipmentHandlerFields = [])
    {
        return new Spot_Shipment_Handler($this->fakeSpot_Shipment_HandlerData($spotShipmentHandlerFields));
    }

    /**
     * Get fake data of Spot_Shipment_Handler
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_HandlerData($spotShipmentHandlerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'handler_desc' => $fake->text
        ], $spotShipmentHandlerFields);
    }
}
