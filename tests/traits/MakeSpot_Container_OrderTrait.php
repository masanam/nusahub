<?php

use Faker\Factory as Faker;
use App\Models\Spot_Container_Order;
use App\Repositories\Spot_Container_OrderRepository;

trait MakeSpot_Container_OrderTrait
{
    /**
     * Create fake instance of Spot_Container_Order and save it in database
     *
     * @param array $spotContainerOrderFields
     * @return Spot_Container_Order
     */
    public function makeSpot_Container_Order($spotContainerOrderFields = [])
    {
        /** @var Spot_Container_OrderRepository $spotContainerOrderRepo */
        $spotContainerOrderRepo = App::make(Spot_Container_OrderRepository::class);
        $theme = $this->fakeSpot_Container_OrderData($spotContainerOrderFields);
        return $spotContainerOrderRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Container_Order
     *
     * @param array $spotContainerOrderFields
     * @return Spot_Container_Order
     */
    public function fakeSpot_Container_Order($spotContainerOrderFields = [])
    {
        return new Spot_Container_Order($this->fakeSpot_Container_OrderData($spotContainerOrderFields));
    }

    /**
     * Get fake data of Spot_Container_Order
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Container_OrderData($spotContainerOrderFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'customer_id' => $fake->randomDigitNotNull,
            'customer_address_id' => $fake->randomDigitNotNull,
            'number' => $fake->word,
            'bol' => $fake->word,
            'location_type' => $fake->randomDigitNotNull,
            'company_id' => $fake->randomDigitNotNull,
            'destination' => $fake->randomDigitNotNull,
            'reason_for_arrive' => $fake->randomDigitNotNull,
            't1' => $fake->word,
            'status_id' => $fake->randomDigitNotNull,
            'cc' => $fake->randomDigitNotNull,
            'eta_port' => $fake->date('Y-m-d H:i:s'),
            'eta_lgg' => $fake->date('Y-m-d H:i:s'),
            'loading_date' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'inspected_port' => $fake->randomDigitNotNull,
            'dp' => $fake->word,
            'requested' => $fake->randomDigitNotNull,
            'barcode' => $fake->randomDigitNotNull,
            'custom_clearance' => $fake->randomDigitNotNull,
            'fiscal_representation' => $fake->randomDigitNotNull,
            'payment_term' => $fake->randomDigitNotNull,
            'price_kg' => $fake->randomDigitNotNull,
            'storage_fee' => $fake->word,
            'cost_24' => $fake->randomDigitNotNull,
            'cost_48' => $fake->randomDigitNotNull
        ], $spotContainerOrderFields);
    }
}
