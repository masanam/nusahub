<?php

use Faker\Factory as Faker;
use App\Models\Spot_Container_Customer;
use App\Repositories\Spot_Container_CustomerRepository;

trait MakeSpot_Container_CustomerTrait
{
    /**
     * Create fake instance of Spot_Container_Customer and save it in database
     *
     * @param array $spotContainerCustomerFields
     * @return Spot_Container_Customer
     */
    public function makeSpot_Container_Customer($spotContainerCustomerFields = [])
    {
        /** @var Spot_Container_CustomerRepository $spotContainerCustomerRepo */
        $spotContainerCustomerRepo = App::make(Spot_Container_CustomerRepository::class);
        $theme = $this->fakeSpot_Container_CustomerData($spotContainerCustomerFields);
        return $spotContainerCustomerRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Container_Customer
     *
     * @param array $spotContainerCustomerFields
     * @return Spot_Container_Customer
     */
    public function fakeSpot_Container_Customer($spotContainerCustomerFields = [])
    {
        return new Spot_Container_Customer($this->fakeSpot_Container_CustomerData($spotContainerCustomerFields));
    }

    /**
     * Get fake data of Spot_Container_Customer
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Container_CustomerData($spotContainerCustomerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'username' => $fake->word,
            'password' => $fake->word,
            'display_name' => $fake->word,
            'email' => $fake->word,
            'contact_email' => $fake->word,
            'vat_number' => $fake->word,
            'budget' => $fake->randomDigitNotNull,
            'street' => $fake->text,
            'country' => $fake->randomDigitNotNull,
            'city' => $fake->randomDigitNotNull,
            'postcode' => $fake->word,
            'number' => $fake->word,
            'box' => $fake->word,
            'language' => $fake->word,
            'clearance' => $fake->randomDigitNotNull,
            'payment_term' => $fake->randomDigitNotNull,
            'fiscal_representation' => $fake->randomDigitNotNull,
            'storage_fee' => $fake->randomDigitNotNull,
            'cost_24' => $fake->randomDigitNotNull,
            'cost_48' => $fake->randomDigitNotNull,
            'kg_price' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $spotContainerCustomerFields);
    }
}
