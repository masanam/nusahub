<?php

use Faker\Factory as Faker;
use App\Models\Spot_Container_Status;
use App\Repositories\Spot_Container_StatusRepository;

trait MakeSpot_Container_StatusTrait
{
    /**
     * Create fake instance of Spot_Container_Status and save it in database
     *
     * @param array $spotContainerStatusFields
     * @return Spot_Container_Status
     */
    public function makeSpot_Container_Status($spotContainerStatusFields = [])
    {
        /** @var Spot_Container_StatusRepository $spotContainerStatusRepo */
        $spotContainerStatusRepo = App::make(Spot_Container_StatusRepository::class);
        $theme = $this->fakeSpot_Container_StatusData($spotContainerStatusFields);
        return $spotContainerStatusRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Container_Status
     *
     * @param array $spotContainerStatusFields
     * @return Spot_Container_Status
     */
    public function fakeSpot_Container_Status($spotContainerStatusFields = [])
    {
        return new Spot_Container_Status($this->fakeSpot_Container_StatusData($spotContainerStatusFields));
    }

    /**
     * Get fake data of Spot_Container_Status
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Container_StatusData($spotContainerStatusFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'color' => $fake->word,
            'description' => $fake->text,
            'equal' => $fake->randomDigitNotNull
        ], $spotContainerStatusFields);
    }
}
