<?php

use Faker\Factory as Faker;
use App\Models\Spot_Container_Destination;
use App\Repositories\Spot_Container_DestinationRepository;

trait MakeSpot_Container_DestinationTrait
{
    /**
     * Create fake instance of Spot_Container_Destination and save it in database
     *
     * @param array $spotContainerDestinationFields
     * @return Spot_Container_Destination
     */
    public function makeSpot_Container_Destination($spotContainerDestinationFields = [])
    {
        /** @var Spot_Container_DestinationRepository $spotContainerDestinationRepo */
        $spotContainerDestinationRepo = App::make(Spot_Container_DestinationRepository::class);
        $theme = $this->fakeSpot_Container_DestinationData($spotContainerDestinationFields);
        return $spotContainerDestinationRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Container_Destination
     *
     * @param array $spotContainerDestinationFields
     * @return Spot_Container_Destination
     */
    public function fakeSpot_Container_Destination($spotContainerDestinationFields = [])
    {
        return new Spot_Container_Destination($this->fakeSpot_Container_DestinationData($spotContainerDestinationFields));
    }

    /**
     * Get fake data of Spot_Container_Destination
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Container_DestinationData($spotContainerDestinationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'desc' => $fake->text
        ], $spotContainerDestinationFields);
    }
}
