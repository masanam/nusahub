<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Office;
use App\Repositories\Spot_Shipment_OfficeRepository;

trait MakeSpot_Shipment_OfficeTrait
{
    /**
     * Create fake instance of Spot_Shipment_Office and save it in database
     *
     * @param array $spotShipmentOfficeFields
     * @return Spot_Shipment_Office
     */
    public function makeSpot_Shipment_Office($spotShipmentOfficeFields = [])
    {
        /** @var Spot_Shipment_OfficeRepository $spotShipmentOfficeRepo */
        $spotShipmentOfficeRepo = App::make(Spot_Shipment_OfficeRepository::class);
        $theme = $this->fakeSpot_Shipment_OfficeData($spotShipmentOfficeFields);
        return $spotShipmentOfficeRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Office
     *
     * @param array $spotShipmentOfficeFields
     * @return Spot_Shipment_Office
     */
    public function fakeSpot_Shipment_Office($spotShipmentOfficeFields = [])
    {
        return new Spot_Shipment_Office($this->fakeSpot_Shipment_OfficeData($spotShipmentOfficeFields));
    }

    /**
     * Get fake data of Spot_Shipment_Office
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_OfficeData($spotShipmentOfficeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'code' => $fake->word,
            'street' => $fake->text,
            'county' => $fake->word,
            'city' => $fake->word,
            'state' => $fake->word,
            'zipcode' => $fake->word,
            'country' => $fake->word,
            'lat' => $fake->word,
            'lng' => $fake->word,
            'url' => $fake->text,
            'phone' => $fake->word,
            'mobile' => $fake->word,
            'mobile_2' => $fake->word,
            'mobile_3' => $fake->word,
            'manger_id' => $fake->randomDigitNotNull,
            'responsible_id' => $fake->randomDigitNotNull
        ], $spotShipmentOfficeFields);
    }
}
