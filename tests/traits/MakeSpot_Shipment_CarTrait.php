<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Car;
use App\Repositories\Spot_Shipment_CarRepository;

trait MakeSpot_Shipment_CarTrait
{
    /**
     * Create fake instance of Spot_Shipment_Car and save it in database
     *
     * @param array $spotShipmentCarFields
     * @return Spot_Shipment_Car
     */
    public function makeSpot_Shipment_Car($spotShipmentCarFields = [])
    {
        /** @var Spot_Shipment_CarRepository $spotShipmentCarRepo */
        $spotShipmentCarRepo = App::make(Spot_Shipment_CarRepository::class);
        $theme = $this->fakeSpot_Shipment_CarData($spotShipmentCarFields);
        return $spotShipmentCarRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Car
     *
     * @param array $spotShipmentCarFields
     * @return Spot_Shipment_Car
     */
    public function fakeSpot_Shipment_Car($spotShipmentCarFields = [])
    {
        return new Spot_Shipment_Car($this->fakeSpot_Shipment_CarData($spotShipmentCarFields));
    }

    /**
     * Get fake data of Spot_Shipment_Car
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_CarData($spotShipmentCarFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'license_plate' => $fake->word,
            'name' => $fake->word,
            'destination_id' => $fake->randomDigitNotNull,
            'transport_id' => $fake->randomDigitNotNull,
            'transport_date' => $fake->word,
            'enable' => $fake->word,
            'driver_id' => $fake->randomDigitNotNull,
            'responsible_id' => $fake->randomDigitNotNull,
            'description' => $fake->text
        ], $spotShipmentCarFields);
    }
}
