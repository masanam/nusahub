<?php

use Faker\Factory as Faker;
use App\Models\User_Groups;
use App\Repositories\User_GroupsRepository;

trait MakeUser_GroupsTrait
{
    /**
     * Create fake instance of User_Groups and save it in database
     *
     * @param array $userGroupsFields
     * @return User_Groups
     */
    public function makeUser_Groups($userGroupsFields = [])
    {
        /** @var User_GroupsRepository $userGroupsRepo */
        $userGroupsRepo = App::make(User_GroupsRepository::class);
        $theme = $this->fakeUser_GroupsData($userGroupsFields);
        return $userGroupsRepo->create($theme);
    }

    /**
     * Get fake instance of User_Groups
     *
     * @param array $userGroupsFields
     * @return User_Groups
     */
    public function fakeUser_Groups($userGroupsFields = [])
    {
        return new User_Groups($this->fakeUser_GroupsData($userGroupsFields));
    }

    /**
     * Get fake data of User_Groups
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUser_GroupsData($userGroupsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'code' => $fake->word,
            'description' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $userGroupsFields);
    }
}
