<?php

use Faker\Factory as Faker;
use App\Models\Backend_User_Groups;
use App\Repositories\Backend_User_GroupsRepository;

trait MakeBackend_User_GroupsTrait
{
    /**
     * Create fake instance of Backend_User_Groups and save it in database
     *
     * @param array $backendUserGroupsFields
     * @return Backend_User_Groups
     */
    public function makeBackend_User_Groups($backendUserGroupsFields = [])
    {
        /** @var Backend_User_GroupsRepository $backendUserGroupsRepo */
        $backendUserGroupsRepo = App::make(Backend_User_GroupsRepository::class);
        $theme = $this->fakeBackend_User_GroupsData($backendUserGroupsFields);
        return $backendUserGroupsRepo->create($theme);
    }

    /**
     * Get fake instance of Backend_User_Groups
     *
     * @param array $backendUserGroupsFields
     * @return Backend_User_Groups
     */
    public function fakeBackend_User_Groups($backendUserGroupsFields = [])
    {
        return new Backend_User_Groups($this->fakeBackend_User_GroupsData($backendUserGroupsFields));
    }

    /**
     * Get fake data of Backend_User_Groups
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBackend_User_GroupsData($backendUserGroupsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'code' => $fake->word,
            'description' => $fake->text,
            'is_new_user_default' => $fake->word
        ], $backendUserGroupsFields);
    }
}
