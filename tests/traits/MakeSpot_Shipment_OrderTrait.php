<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Order;
use App\Repositories\Spot_Shipment_OrderRepository;

trait MakeSpot_Shipment_OrderTrait
{
    /**
     * Create fake instance of Spot_Shipment_Order and save it in database
     *
     * @param array $spotShipmentOrderFields
     * @return Spot_Shipment_Order
     */
    public function makeSpot_Shipment_Order($spotShipmentOrderFields = [])
    {
        /** @var Spot_Shipment_OrderRepository $spotShipmentOrderRepo */
        $spotShipmentOrderRepo = App::make(Spot_Shipment_OrderRepository::class);
        $theme = $this->fakeSpot_Shipment_OrderData($spotShipmentOrderFields);
        return $spotShipmentOrderRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Order
     *
     * @param array $spotShipmentOrderFields
     * @return Spot_Shipment_Order
     */
    public function fakeSpot_Shipment_Order($spotShipmentOrderFields = [])
    {
        return new Spot_Shipment_Order($this->fakeSpot_Shipment_OrderData($spotShipmentOrderFields));
    }

    /**
     * Get fake data of Spot_Shipment_Order
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_OrderData($spotShipmentOrderFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'office_id' => $fake->randomDigitNotNull,
            'number' => $fake->randomDigitNotNull,
            'type' => $fake->word,
            'sender_id' => $fake->randomDigitNotNull,
            'sender_address_id' => $fake->randomDigitNotNull,
            'receiver_id' => $fake->randomDigitNotNull,
            'receiver_address_id' => $fake->randomDigitNotNull,
            'sender_name' => $fake->word,
            'sender_mobile' => $fake->word,
            'sender_city' => $fake->randomDigitNotNull,
            'sender_sector' => $fake->randomDigitNotNull,
            'sender_addr' => $fake->word,
            'receiver_name' => $fake->word,
            'receiver_mobile' => $fake->word,
            'receiver_city' => $fake->randomDigitNotNull,
            'receiver_sector' => $fake->randomDigitNotNull,
            'receiver_addr' => $fake->word,
            'packaging_id' => $fake->randomDigitNotNull,
            'ship_date' => $fake->word,
            'receive_date' => $fake->word,
            'courier_id' => $fake->randomDigitNotNull,
            'delivery_time_id' => $fake->randomDigitNotNull,
            'delivery_date' => $fake->word,
            'mode_id' => $fake->randomDigitNotNull,
            'status_id' => $fake->randomDigitNotNull,
            'tax' => $fake->randomDigitNotNull,
            'insurance' => $fake->randomDigitNotNull,
            'currency_id' => $fake->randomDigitNotNull,
            'payment_type' => $fake->word,
            'customs_value' => $fake->word,
            'courier_fee' => $fake->word,
            'package_fee' => $fake->word,
            'return_package_fee' => $fake->word,
            'return_courier_fee' => $fake->word,
            'return_defray' => $fake->word,
            'manifest_id' => $fake->randomDigitNotNull,
            'channel' => $fake->word,
            'assigned_id' => $fake->randomDigitNotNull,
            'requested' => $fake->randomDigitNotNull,
            'barcode' => $fake->word,
            'esign' => $fake->text,
            'postponed' => $fake->word,
            'delivered_by' => $fake->randomDigitNotNull,
            'delivered_responsiable' => $fake->randomDigitNotNull,
            'received_by' => $fake->word,
            'airWayBill' => $fake->word,
            'location' => $fake->word,
            'cc' => $fake->word,
            'transfer_jost' => $fake->word,
            'label_id' => $fake->randomDigitNotNull,
            'handler_id' => $fake->randomDigitNotNull,
            'breakdown_id' => $fake->randomDigitNotNull,
            'preAlert_received' => $fake->date('Y-m-d H:i:s'),
            'releasedNote_received' => $fake->date('Y-m-d H:i:s'),
            'remarks' => $fake->text,
            'released_note' => $fake->text,
            'custom_clearance' => $fake->randomDigitNotNull,
            'fiscal_representation' => $fake->randomDigitNotNull,
            'payment_term' => $fake->randomDigitNotNull,
            'price_kg' => $fake->randomDigitNotNull,
            'storage_fee' => $fake->word,
            'cost_24' => $fake->randomDigitNotNull,
            'cost_48' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $spotShipmentOrderFields);
    }
}
