<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Category;
use App\Repositories\Spot_Shipment_CategoryRepository;

trait MakeSpot_Shipment_CategoryTrait
{
    /**
     * Create fake instance of Spot_Shipment_Category and save it in database
     *
     * @param array $spotShipmentCategoryFields
     * @return Spot_Shipment_Category
     */
    public function makeSpot_Shipment_Category($spotShipmentCategoryFields = [])
    {
        /** @var Spot_Shipment_CategoryRepository $spotShipmentCategoryRepo */
        $spotShipmentCategoryRepo = App::make(Spot_Shipment_CategoryRepository::class);
        $theme = $this->fakeSpot_Shipment_CategoryData($spotShipmentCategoryFields);
        return $spotShipmentCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Category
     *
     * @param array $spotShipmentCategoryFields
     * @return Spot_Shipment_Category
     */
    public function fakeSpot_Shipment_Category($spotShipmentCategoryFields = [])
    {
        return new Spot_Shipment_Category($this->fakeSpot_Shipment_CategoryData($spotShipmentCategoryFields));
    }

    /**
     * Get fake data of Spot_Shipment_Category
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_CategoryData($spotShipmentCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text
        ], $spotShipmentCategoryFields);
    }
}
