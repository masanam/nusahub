<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_Area;
use App\Repositories\Spot_Shipment_AreaRepository;

trait MakeSpot_Shipment_AreaTrait
{
    /**
     * Create fake instance of Spot_Shipment_Area and save it in database
     *
     * @param array $spotShipmentAreaFields
     * @return Spot_Shipment_Area
     */
    public function makeSpot_Shipment_Area($spotShipmentAreaFields = [])
    {
        /** @var Spot_Shipment_AreaRepository $spotShipmentAreaRepo */
        $spotShipmentAreaRepo = App::make(Spot_Shipment_AreaRepository::class);
        $theme = $this->fakeSpot_Shipment_AreaData($spotShipmentAreaFields);
        return $spotShipmentAreaRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_Area
     *
     * @param array $spotShipmentAreaFields
     * @return Spot_Shipment_Area
     */
    public function fakeSpot_Shipment_Area($spotShipmentAreaFields = [])
    {
        return new Spot_Shipment_Area($this->fakeSpot_Shipment_AreaData($spotShipmentAreaFields));
    }

    /**
     * Get fake data of Spot_Shipment_Area
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_AreaData($spotShipmentAreaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'city_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $spotShipmentAreaFields);
    }
}
