<?php

use Faker\Factory as Faker;
use App\Models\Backend_User_Roles;
use App\Repositories\Backend_User_RolesRepository;

trait MakeBackend_User_RolesTrait
{
    /**
     * Create fake instance of Backend_User_Roles and save it in database
     *
     * @param array $backendUserRolesFields
     * @return Backend_User_Roles
     */
    public function makeBackend_User_Roles($backendUserRolesFields = [])
    {
        /** @var Backend_User_RolesRepository $backendUserRolesRepo */
        $backendUserRolesRepo = App::make(Backend_User_RolesRepository::class);
        $theme = $this->fakeBackend_User_RolesData($backendUserRolesFields);
        return $backendUserRolesRepo->create($theme);
    }

    /**
     * Get fake instance of Backend_User_Roles
     *
     * @param array $backendUserRolesFields
     * @return Backend_User_Roles
     */
    public function fakeBackend_User_Roles($backendUserRolesFields = [])
    {
        return new Backend_User_Roles($this->fakeBackend_User_RolesData($backendUserRolesFields));
    }

    /**
     * Get fake data of Backend_User_Roles
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBackend_User_RolesData($backendUserRolesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'code' => $fake->word,
            'description' => $fake->text,
            'permissions' => $fake->text,
            'is_system' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $backendUserRolesFields);
    }
}
