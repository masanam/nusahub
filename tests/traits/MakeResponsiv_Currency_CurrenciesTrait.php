<?php

use Faker\Factory as Faker;
use App\Models\Responsiv_Currency_Currencies;
use App\Repositories\Responsiv_Currency_CurrenciesRepository;

trait MakeResponsiv_Currency_CurrenciesTrait
{
    /**
     * Create fake instance of Responsiv_Currency_Currencies and save it in database
     *
     * @param array $responsivCurrencyCurrenciesFields
     * @return Responsiv_Currency_Currencies
     */
    public function makeResponsiv_Currency_Currencies($responsivCurrencyCurrenciesFields = [])
    {
        /** @var Responsiv_Currency_CurrenciesRepository $responsivCurrencyCurrenciesRepo */
        $responsivCurrencyCurrenciesRepo = App::make(Responsiv_Currency_CurrenciesRepository::class);
        $theme = $this->fakeResponsiv_Currency_CurrenciesData($responsivCurrencyCurrenciesFields);
        return $responsivCurrencyCurrenciesRepo->create($theme);
    }

    /**
     * Get fake instance of Responsiv_Currency_Currencies
     *
     * @param array $responsivCurrencyCurrenciesFields
     * @return Responsiv_Currency_Currencies
     */
    public function fakeResponsiv_Currency_Currencies($responsivCurrencyCurrenciesFields = [])
    {
        return new Responsiv_Currency_Currencies($this->fakeResponsiv_Currency_CurrenciesData($responsivCurrencyCurrenciesFields));
    }

    /**
     * Get fake data of Responsiv_Currency_Currencies
     *
     * @param array $postFields
     * @return array
     */
    public function fakeResponsiv_Currency_CurrenciesData($responsivCurrencyCurrenciesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'currency_code' => $fake->word,
            'currency_symbol' => $fake->word,
            'decimal_point' => $fake->word,
            'thousand_separator' => $fake->word,
            'place_symbol_before' => $fake->word,
            'with_space' => $fake->word,
            'is_enabled' => $fake->word,
            'is_primary' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'initbiz_money_fraction_digits' => $fake->randomDigitNotNull
        ], $responsivCurrencyCurrenciesFields);
    }
}
