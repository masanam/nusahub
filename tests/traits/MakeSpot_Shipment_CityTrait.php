<?php

use Faker\Factory as Faker;
use App\Models\Spot_Shipment_City;
use App\Repositories\Spot_Shipment_CityRepository;

trait MakeSpot_Shipment_CityTrait
{
    /**
     * Create fake instance of Spot_Shipment_City and save it in database
     *
     * @param array $spotShipmentCityFields
     * @return Spot_Shipment_City
     */
    public function makeSpot_Shipment_City($spotShipmentCityFields = [])
    {
        /** @var Spot_Shipment_CityRepository $spotShipmentCityRepo */
        $spotShipmentCityRepo = App::make(Spot_Shipment_CityRepository::class);
        $theme = $this->fakeSpot_Shipment_CityData($spotShipmentCityFields);
        return $spotShipmentCityRepo->create($theme);
    }

    /**
     * Get fake instance of Spot_Shipment_City
     *
     * @param array $spotShipmentCityFields
     * @return Spot_Shipment_City
     */
    public function fakeSpot_Shipment_City($spotShipmentCityFields = [])
    {
        return new Spot_Shipment_City($this->fakeSpot_Shipment_CityData($spotShipmentCityFields));
    }

    /**
     * Get fake data of Spot_Shipment_City
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSpot_Shipment_CityData($spotShipmentCityFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'country_id' => $fake->randomDigitNotNull,
            'state_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $spotShipmentCityFields);
    }
}
