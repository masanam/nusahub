<?php

use App\Models\Spot_Shipment_City;
use App\Repositories\Spot_Shipment_CityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_CityRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_CityTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_CityRepository
     */
    protected $spotShipmentCityRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentCityRepo = App::make(Spot_Shipment_CityRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_City()
    {
        $spotShipmentCity = $this->fakeSpot_Shipment_CityData();
        $createdSpot_Shipment_City = $this->spotShipmentCityRepo->create($spotShipmentCity);
        $createdSpot_Shipment_City = $createdSpot_Shipment_City->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_City);
        $this->assertNotNull($createdSpot_Shipment_City['id'], 'Created Spot_Shipment_City must have id specified');
        $this->assertNotNull(Spot_Shipment_City::find($createdSpot_Shipment_City['id']), 'Spot_Shipment_City with given id must be in DB');
        $this->assertModelData($spotShipmentCity, $createdSpot_Shipment_City);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_City()
    {
        $spotShipmentCity = $this->makeSpot_Shipment_City();
        $dbSpot_Shipment_City = $this->spotShipmentCityRepo->find($spotShipmentCity->id);
        $dbSpot_Shipment_City = $dbSpot_Shipment_City->toArray();
        $this->assertModelData($spotShipmentCity->toArray(), $dbSpot_Shipment_City);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_City()
    {
        $spotShipmentCity = $this->makeSpot_Shipment_City();
        $fakeSpot_Shipment_City = $this->fakeSpot_Shipment_CityData();
        $updatedSpot_Shipment_City = $this->spotShipmentCityRepo->update($fakeSpot_Shipment_City, $spotShipmentCity->id);
        $this->assertModelData($fakeSpot_Shipment_City, $updatedSpot_Shipment_City->toArray());
        $dbSpot_Shipment_City = $this->spotShipmentCityRepo->find($spotShipmentCity->id);
        $this->assertModelData($fakeSpot_Shipment_City, $dbSpot_Shipment_City->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_City()
    {
        $spotShipmentCity = $this->makeSpot_Shipment_City();
        $resp = $this->spotShipmentCityRepo->delete($spotShipmentCity->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_City::find($spotShipmentCity->id), 'Spot_Shipment_City should not exist in DB');
    }
}
