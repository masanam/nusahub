<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_BreakApiTest extends TestCase
{
    use MakeSpot_Shipment_BreakTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Break()
    {
        $spotShipmentBreak = $this->fakeSpot_Shipment_BreakData();
        $this->json('POST', '/api/v1/spotShipmentBreaks', $spotShipmentBreak);

        $this->assertApiResponse($spotShipmentBreak);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Break()
    {
        $spotShipmentBreak = $this->makeSpot_Shipment_Break();
        $this->json('GET', '/api/v1/spotShipmentBreaks/'.$spotShipmentBreak->id);

        $this->assertApiResponse($spotShipmentBreak->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Break()
    {
        $spotShipmentBreak = $this->makeSpot_Shipment_Break();
        $editedSpot_Shipment_Break = $this->fakeSpot_Shipment_BreakData();

        $this->json('PUT', '/api/v1/spotShipmentBreaks/'.$spotShipmentBreak->id, $editedSpot_Shipment_Break);

        $this->assertApiResponse($editedSpot_Shipment_Break);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Break()
    {
        $spotShipmentBreak = $this->makeSpot_Shipment_Break();
        $this->json('DELETE', '/api/v1/spotShipmentBreaks/'.$spotShipmentBreak->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentBreaks/'.$spotShipmentBreak->id);

        $this->assertResponseStatus(404);
    }
}
