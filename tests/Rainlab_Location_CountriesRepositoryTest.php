<?php

use App\Models\Rainlab_Location_Countries;
use App\Repositories\Rainlab_Location_CountriesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Rainlab_Location_CountriesRepositoryTest extends TestCase
{
    use MakeRainlab_Location_CountriesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Rainlab_Location_CountriesRepository
     */
    protected $rainlabLocationCountriesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->rainlabLocationCountriesRepo = App::make(Rainlab_Location_CountriesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRainlab_Location_Countries()
    {
        $rainlabLocationCountries = $this->fakeRainlab_Location_CountriesData();
        $createdRainlab_Location_Countries = $this->rainlabLocationCountriesRepo->create($rainlabLocationCountries);
        $createdRainlab_Location_Countries = $createdRainlab_Location_Countries->toArray();
        $this->assertArrayHasKey('id', $createdRainlab_Location_Countries);
        $this->assertNotNull($createdRainlab_Location_Countries['id'], 'Created Rainlab_Location_Countries must have id specified');
        $this->assertNotNull(Rainlab_Location_Countries::find($createdRainlab_Location_Countries['id']), 'Rainlab_Location_Countries with given id must be in DB');
        $this->assertModelData($rainlabLocationCountries, $createdRainlab_Location_Countries);
    }

    /**
     * @test read
     */
    public function testReadRainlab_Location_Countries()
    {
        $rainlabLocationCountries = $this->makeRainlab_Location_Countries();
        $dbRainlab_Location_Countries = $this->rainlabLocationCountriesRepo->find($rainlabLocationCountries->id);
        $dbRainlab_Location_Countries = $dbRainlab_Location_Countries->toArray();
        $this->assertModelData($rainlabLocationCountries->toArray(), $dbRainlab_Location_Countries);
    }

    /**
     * @test update
     */
    public function testUpdateRainlab_Location_Countries()
    {
        $rainlabLocationCountries = $this->makeRainlab_Location_Countries();
        $fakeRainlab_Location_Countries = $this->fakeRainlab_Location_CountriesData();
        $updatedRainlab_Location_Countries = $this->rainlabLocationCountriesRepo->update($fakeRainlab_Location_Countries, $rainlabLocationCountries->id);
        $this->assertModelData($fakeRainlab_Location_Countries, $updatedRainlab_Location_Countries->toArray());
        $dbRainlab_Location_Countries = $this->rainlabLocationCountriesRepo->find($rainlabLocationCountries->id);
        $this->assertModelData($fakeRainlab_Location_Countries, $dbRainlab_Location_Countries->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRainlab_Location_Countries()
    {
        $rainlabLocationCountries = $this->makeRainlab_Location_Countries();
        $resp = $this->rainlabLocationCountriesRepo->delete($rainlabLocationCountries->id);
        $this->assertTrue($resp);
        $this->assertNull(Rainlab_Location_Countries::find($rainlabLocationCountries->id), 'Rainlab_Location_Countries should not exist in DB');
    }
}
