<?php

use App\Models\Spot_Shipment_Courier;
use App\Repositories\Spot_Shipment_CourierRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_CourierRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_CourierTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_CourierRepository
     */
    protected $spotShipmentCourierRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentCourierRepo = App::make(Spot_Shipment_CourierRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Courier()
    {
        $spotShipmentCourier = $this->fakeSpot_Shipment_CourierData();
        $createdSpot_Shipment_Courier = $this->spotShipmentCourierRepo->create($spotShipmentCourier);
        $createdSpot_Shipment_Courier = $createdSpot_Shipment_Courier->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Courier);
        $this->assertNotNull($createdSpot_Shipment_Courier['id'], 'Created Spot_Shipment_Courier must have id specified');
        $this->assertNotNull(Spot_Shipment_Courier::find($createdSpot_Shipment_Courier['id']), 'Spot_Shipment_Courier with given id must be in DB');
        $this->assertModelData($spotShipmentCourier, $createdSpot_Shipment_Courier);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Courier()
    {
        $spotShipmentCourier = $this->makeSpot_Shipment_Courier();
        $dbSpot_Shipment_Courier = $this->spotShipmentCourierRepo->find($spotShipmentCourier->id);
        $dbSpot_Shipment_Courier = $dbSpot_Shipment_Courier->toArray();
        $this->assertModelData($spotShipmentCourier->toArray(), $dbSpot_Shipment_Courier);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Courier()
    {
        $spotShipmentCourier = $this->makeSpot_Shipment_Courier();
        $fakeSpot_Shipment_Courier = $this->fakeSpot_Shipment_CourierData();
        $updatedSpot_Shipment_Courier = $this->spotShipmentCourierRepo->update($fakeSpot_Shipment_Courier, $spotShipmentCourier->id);
        $this->assertModelData($fakeSpot_Shipment_Courier, $updatedSpot_Shipment_Courier->toArray());
        $dbSpot_Shipment_Courier = $this->spotShipmentCourierRepo->find($spotShipmentCourier->id);
        $this->assertModelData($fakeSpot_Shipment_Courier, $dbSpot_Shipment_Courier->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Courier()
    {
        $spotShipmentCourier = $this->makeSpot_Shipment_Courier();
        $resp = $this->spotShipmentCourierRepo->delete($spotShipmentCourier->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Courier::find($spotShipmentCourier->id), 'Spot_Shipment_Courier should not exist in DB');
    }
}
