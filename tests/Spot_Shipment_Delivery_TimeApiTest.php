<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_Delivery_TimeApiTest extends TestCase
{
    use MakeSpot_Shipment_Delivery_TimeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Delivery_Time()
    {
        $spotShipmentDeliveryTime = $this->fakeSpot_Shipment_Delivery_TimeData();
        $this->json('POST', '/api/v1/spotShipmentDeliveryTimes', $spotShipmentDeliveryTime);

        $this->assertApiResponse($spotShipmentDeliveryTime);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Delivery_Time()
    {
        $spotShipmentDeliveryTime = $this->makeSpot_Shipment_Delivery_Time();
        $this->json('GET', '/api/v1/spotShipmentDeliveryTimes/'.$spotShipmentDeliveryTime->id);

        $this->assertApiResponse($spotShipmentDeliveryTime->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Delivery_Time()
    {
        $spotShipmentDeliveryTime = $this->makeSpot_Shipment_Delivery_Time();
        $editedSpot_Shipment_Delivery_Time = $this->fakeSpot_Shipment_Delivery_TimeData();

        $this->json('PUT', '/api/v1/spotShipmentDeliveryTimes/'.$spotShipmentDeliveryTime->id, $editedSpot_Shipment_Delivery_Time);

        $this->assertApiResponse($editedSpot_Shipment_Delivery_Time);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Delivery_Time()
    {
        $spotShipmentDeliveryTime = $this->makeSpot_Shipment_Delivery_Time();
        $this->json('DELETE', '/api/v1/spotShipmentDeliveryTimes/'.$spotShipmentDeliveryTime->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentDeliveryTimes/'.$spotShipmentDeliveryTime->id);

        $this->assertResponseStatus(404);
    }
}
