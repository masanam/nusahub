<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_OfficeApiTest extends TestCase
{
    use MakeSpot_Shipment_OfficeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSpot_Shipment_Office()
    {
        $spotShipmentOffice = $this->fakeSpot_Shipment_OfficeData();
        $this->json('POST', '/api/v1/spotShipmentOffices', $spotShipmentOffice);

        $this->assertApiResponse($spotShipmentOffice);
    }

    /**
     * @test
     */
    public function testReadSpot_Shipment_Office()
    {
        $spotShipmentOffice = $this->makeSpot_Shipment_Office();
        $this->json('GET', '/api/v1/spotShipmentOffices/'.$spotShipmentOffice->id);

        $this->assertApiResponse($spotShipmentOffice->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSpot_Shipment_Office()
    {
        $spotShipmentOffice = $this->makeSpot_Shipment_Office();
        $editedSpot_Shipment_Office = $this->fakeSpot_Shipment_OfficeData();

        $this->json('PUT', '/api/v1/spotShipmentOffices/'.$spotShipmentOffice->id, $editedSpot_Shipment_Office);

        $this->assertApiResponse($editedSpot_Shipment_Office);
    }

    /**
     * @test
     */
    public function testDeleteSpot_Shipment_Office()
    {
        $spotShipmentOffice = $this->makeSpot_Shipment_Office();
        $this->json('DELETE', '/api/v1/spotShipmentOffices/'.$spotShipmentOffice->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/spotShipmentOffices/'.$spotShipmentOffice->id);

        $this->assertResponseStatus(404);
    }
}
