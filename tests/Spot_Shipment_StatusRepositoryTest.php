<?php

use App\Models\Spot_Shipment_Status;
use App\Repositories\Spot_Shipment_StatusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_StatusRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_StatusTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_StatusRepository
     */
    protected $spotShipmentStatusRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentStatusRepo = App::make(Spot_Shipment_StatusRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Status()
    {
        $spotShipmentStatus = $this->fakeSpot_Shipment_StatusData();
        $createdSpot_Shipment_Status = $this->spotShipmentStatusRepo->create($spotShipmentStatus);
        $createdSpot_Shipment_Status = $createdSpot_Shipment_Status->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Status);
        $this->assertNotNull($createdSpot_Shipment_Status['id'], 'Created Spot_Shipment_Status must have id specified');
        $this->assertNotNull(Spot_Shipment_Status::find($createdSpot_Shipment_Status['id']), 'Spot_Shipment_Status with given id must be in DB');
        $this->assertModelData($spotShipmentStatus, $createdSpot_Shipment_Status);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Status()
    {
        $spotShipmentStatus = $this->makeSpot_Shipment_Status();
        $dbSpot_Shipment_Status = $this->spotShipmentStatusRepo->find($spotShipmentStatus->id);
        $dbSpot_Shipment_Status = $dbSpot_Shipment_Status->toArray();
        $this->assertModelData($spotShipmentStatus->toArray(), $dbSpot_Shipment_Status);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Status()
    {
        $spotShipmentStatus = $this->makeSpot_Shipment_Status();
        $fakeSpot_Shipment_Status = $this->fakeSpot_Shipment_StatusData();
        $updatedSpot_Shipment_Status = $this->spotShipmentStatusRepo->update($fakeSpot_Shipment_Status, $spotShipmentStatus->id);
        $this->assertModelData($fakeSpot_Shipment_Status, $updatedSpot_Shipment_Status->toArray());
        $dbSpot_Shipment_Status = $this->spotShipmentStatusRepo->find($spotShipmentStatus->id);
        $this->assertModelData($fakeSpot_Shipment_Status, $dbSpot_Shipment_Status->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Status()
    {
        $spotShipmentStatus = $this->makeSpot_Shipment_Status();
        $resp = $this->spotShipmentStatusRepo->delete($spotShipmentStatus->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Status::find($spotShipmentStatus->id), 'Spot_Shipment_Status should not exist in DB');
    }
}
