<?php

use App\Models\Spot_Shipment_Handler;
use App\Repositories\Spot_Shipment_HandlerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Spot_Shipment_HandlerRepositoryTest extends TestCase
{
    use MakeSpot_Shipment_HandlerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Spot_Shipment_HandlerRepository
     */
    protected $spotShipmentHandlerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->spotShipmentHandlerRepo = App::make(Spot_Shipment_HandlerRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSpot_Shipment_Handler()
    {
        $spotShipmentHandler = $this->fakeSpot_Shipment_HandlerData();
        $createdSpot_Shipment_Handler = $this->spotShipmentHandlerRepo->create($spotShipmentHandler);
        $createdSpot_Shipment_Handler = $createdSpot_Shipment_Handler->toArray();
        $this->assertArrayHasKey('id', $createdSpot_Shipment_Handler);
        $this->assertNotNull($createdSpot_Shipment_Handler['id'], 'Created Spot_Shipment_Handler must have id specified');
        $this->assertNotNull(Spot_Shipment_Handler::find($createdSpot_Shipment_Handler['id']), 'Spot_Shipment_Handler with given id must be in DB');
        $this->assertModelData($spotShipmentHandler, $createdSpot_Shipment_Handler);
    }

    /**
     * @test read
     */
    public function testReadSpot_Shipment_Handler()
    {
        $spotShipmentHandler = $this->makeSpot_Shipment_Handler();
        $dbSpot_Shipment_Handler = $this->spotShipmentHandlerRepo->find($spotShipmentHandler->id);
        $dbSpot_Shipment_Handler = $dbSpot_Shipment_Handler->toArray();
        $this->assertModelData($spotShipmentHandler->toArray(), $dbSpot_Shipment_Handler);
    }

    /**
     * @test update
     */
    public function testUpdateSpot_Shipment_Handler()
    {
        $spotShipmentHandler = $this->makeSpot_Shipment_Handler();
        $fakeSpot_Shipment_Handler = $this->fakeSpot_Shipment_HandlerData();
        $updatedSpot_Shipment_Handler = $this->spotShipmentHandlerRepo->update($fakeSpot_Shipment_Handler, $spotShipmentHandler->id);
        $this->assertModelData($fakeSpot_Shipment_Handler, $updatedSpot_Shipment_Handler->toArray());
        $dbSpot_Shipment_Handler = $this->spotShipmentHandlerRepo->find($spotShipmentHandler->id);
        $this->assertModelData($fakeSpot_Shipment_Handler, $dbSpot_Shipment_Handler->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSpot_Shipment_Handler()
    {
        $spotShipmentHandler = $this->makeSpot_Shipment_Handler();
        $resp = $this->spotShipmentHandlerRepo->delete($spotShipmentHandler->id);
        $this->assertTrue($resp);
        $this->assertNull(Spot_Shipment_Handler::find($spotShipmentHandler->id), 'Spot_Shipment_Handler should not exist in DB');
    }
}
