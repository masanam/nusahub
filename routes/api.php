<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });


    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'API\AuthAPIController@logout');

    });

/** post data */
Route::post('/saveContact','API\PublicAPIController@saveContact');

Route::post('/saveCareerRecruitment','API\PublicAPIController@saveCareerRecruitment');

Route::post('/saveCareerRecruitment2','API\PublicAPIController@saveCareerRecruitment2');

Route::post('/saveInvestor','API\PublicAPIController@saveInvestor');

Route::post('/login', 'API\AuthAPIController@login');

Route::post('/signup', 'API\AuthAPIController@signup');



Route::resource('users', 'Admin\UsersAPIController');

Route::resource('spot__container__customers', 'Admin\Spot_Container_CustomerAPIController');

Route::resource('spot__container__orders', 'Admin\Spot_Container_OrderAPIController');

Route::resource('spot__shipment__orders', 'Admin\Spot_Shipment_OrderAPIController');

Route::resource('spot__shipment__payments', 'Admin\Spot_Shipment_PaymentAPIController');

Route::resource('backend__user__roles', 'Admin\Backend_User_RolesAPIController');

Route::resource('backend__users', 'Admin\Backend_UsersAPIController');

Route::resource('backend__user__groups', 'Admin\Backend_User_GroupsAPIController');

Route::resource('spot__shipment__orders', 'Admin\Spot_Shipment_OrderAPIController');

Route::resource('spot__container__statuses', 'Admin\Spot_Container_StatusAPIController');

Route::resource('spot__container__locations', 'Admin\Spot_Container_LocationAPIController');

Route::resource('spot__container__reasons', 'Admin\Spot_Container_ReasonAPIController');

Route::resource('spot__container__sizes', 'Admin\Spot_Container_SizeAPIController');

Route::resource('spot__shipment__cars', 'Admin\Spot_Shipment_CarAPIController');

Route::resource('spot__shipment__categories', 'Admin\Spot_Shipment_CategoryAPIController');

Route::resource('spot__shipment__couriers', 'Admin\Spot_Shipment_CourierAPIController');

Route::resource('spot__shipment__labels', 'Admin\Spot_Shipment_LabelAPIController');

Route::resource('spot__shipment__delivery__times', 'Admin\Spot_Shipment_Delivery_TimeAPIController');

Route::resource('spot__shipment__handlers', 'Admin\Spot_Shipment_HandlerAPIController');

Route::resource('spot__shipment__modes', 'Admin\Spot_Shipment_ModeAPIController');

Route::resource('spot__shipment__offices', 'Admin\Spot_Shipment_OfficeAPIController');

Route::resource('spot__shipment__packagings', 'Admin\Spot_Shipment_PackagingAPIController');

Route::resource('spot__shipment__statuses', 'Admin\Spot_Shipment_StatusAPIController');

Route::resource('spot__shipment__treasuries', 'Admin\Spot_Shipment_TreasuryAPIController');

Route::resource('spot__shipment__breaks', 'Admin\Spot_Shipment_BreakAPIController');

Route::resource('spot__container__destinations', 'Admin\Spot_Container_DestinationAPIController');

Route::resource('rainlab__location__countries', 'Admin\Rainlab_Location_CountriesAPIController');

Route::resource('rainlab__location__states', 'Admin\Rainlab_Location_StatesAPIController');

Route::resource('spot__shipment__cities', 'Admin\Spot_Shipment_CityAPIController');

Route::resource('spot__shipment__areas', 'Admin\Spot_Shipment_AreaAPIController');

Route::resource('user__groups', 'Admin\User_GroupsAPIController');

Route::resource('responsiv__currency__currencies', 'Admin\Responsiv_Currency_CurrenciesAPIController');

Route::resource('responsiv__pay__invoice__statuses', 'Admin\Responsiv_Pay_Invoice_StatusesAPIController');