<?php

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

use League\Glide\ServerFactory;
use League\Glide\Responses\LaravelResponseFactory;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     // if(Auth::check()) {
//     //     return redirect(route('home'));
//     // }
//     return view('welcome');
// });

Route::redirect('/', '/login');

// Route::get('/', function () {
//     View::addExtension('html','php');
//     return View::make('index');
// });

Route::get('sendemail', function () {

    $data = array(
        'name' => "Learning Laravel",
    );

    Mail::send('emails.welcome', $data, function ($message) {

        $message->from('masanam@yahoo.com', 'Learning Laravel');

        $message->to('masanam@gmail.com')->subject('Learning Laravel test email');

    });

    return "Your email has been sent successfully";

});


Route::fallback(function () {
    return view('index');
});

Route::get('/img/{path}', function(Filesystem $filesystem, $path) {
    $server = ServerFactory::create([
        'response' => new LaravelResponseFactory(app('request')),
        // 'source' => $filesystem->getDriver(),
        // 'cache' => $filesystem->getDriver(),
        'source' => Storage::disk("public")->getDriver(),
        'cache' => Storage::disk("public")->getDriver(),
        'cache_path_prefix' => '.cache',
        'base_url' => 'img',
    ]);

    return $server->getImageResponse($path, request()->all());
})->where('path', '.*');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin/users', ['as'=> 'admin.users.index', 'uses' => 'Admin\UsersController@index']);
Route::post('admin/users', ['as'=> 'admin.users.store', 'uses' => 'Admin\UsersController@store']);
Route::get('admin/users/create', ['as'=> 'admin.users.create', 'uses' => 'Admin\UsersController@create']);
Route::put('admin/users/{users}', ['as'=> 'admin.users.update', 'uses' => 'Admin\UsersController@update']);
Route::patch('admin/users/{users}', ['as'=> 'admin.users.update', 'uses' => 'Admin\UsersController@update']);
Route::delete('admin/users/{users}', ['as'=> 'admin.users.destroy', 'uses' => 'Admin\UsersController@destroy']);
Route::get('admin/users/{users}', ['as'=> 'admin.users.show', 'uses' => 'Admin\UsersController@show']);
Route::get('admin/users/{users}/edit', ['as'=> 'admin.users.edit', 'uses' => 'Admin\UsersController@edit']);
// Route::post('importUsers', 'Admin\UsersController@import');

Route::get('admin/spotContainerCustomers', ['as'=> 'admin.spotContainerCustomers.index', 'uses' => 'Admin\Spot_Container_CustomerController@index']);
Route::post('admin/spotContainerCustomers', ['as'=> 'admin.spotContainerCustomers.store', 'uses' => 'Admin\Spot_Container_CustomerController@store']);
Route::get('admin/spotContainerCustomers/create', ['as'=> 'admin.spotContainerCustomers.create', 'uses' => 'Admin\Spot_Container_CustomerController@create']);
Route::put('admin/spotContainerCustomers/{spotContainerCustomers}', ['as'=> 'admin.spotContainerCustomers.update', 'uses' => 'Admin\Spot_Container_CustomerController@update']);
Route::patch('admin/spotContainerCustomers/{spotContainerCustomers}', ['as'=> 'admin.spotContainerCustomers.update', 'uses' => 'Admin\Spot_Container_CustomerController@update']);
Route::delete('admin/spotContainerCustomers/{spotContainerCustomers}', ['as'=> 'admin.spotContainerCustomers.destroy', 'uses' => 'Admin\Spot_Container_CustomerController@destroy']);
Route::get('admin/spotContainerCustomers/{spotContainerCustomers}', ['as'=> 'admin.spotContainerCustomers.show', 'uses' => 'Admin\Spot_Container_CustomerController@show']);
Route::get('admin/spotContainerCustomers/{spotContainerCustomers}/edit', ['as'=> 'admin.spotContainerCustomers.edit', 'uses' => 'Admin\Spot_Container_CustomerController@edit']);
// Route::post('importSpot_Container_Customer', 'Admin\Spot_Container_CustomerController@import');

Route::get('admin/spotContainerOrders', ['as'=> 'admin.spotContainerOrders.index', 'uses' => 'Admin\Spot_Container_OrderController@index']);
Route::post('admin/spotContainerOrders', ['as'=> 'admin.spotContainerOrders.store', 'uses' => 'Admin\Spot_Container_OrderController@store']);
Route::get('admin/spotContainerOrders/create', ['as'=> 'admin.spotContainerOrders.create', 'uses' => 'Admin\Spot_Container_OrderController@create']);
Route::put('admin/spotContainerOrders/{spotContainerOrders}', ['as'=> 'admin.spotContainerOrders.update', 'uses' => 'Admin\Spot_Container_OrderController@update']);
Route::patch('admin/spotContainerOrders/{spotContainerOrders}', ['as'=> 'admin.spotContainerOrders.update', 'uses' => 'Admin\Spot_Container_OrderController@update']);
Route::delete('admin/spotContainerOrders/{spotContainerOrders}', ['as'=> 'admin.spotContainerOrders.destroy', 'uses' => 'Admin\Spot_Container_OrderController@destroy']);
Route::get('admin/spotContainerOrders/{spotContainerOrders}', ['as'=> 'admin.spotContainerOrders.show', 'uses' => 'Admin\Spot_Container_OrderController@show']);
Route::get('admin/spotContainerOrders/{spotContainerOrders}/edit', ['as'=> 'admin.spotContainerOrders.edit', 'uses' => 'Admin\Spot_Container_OrderController@edit']);
// Route::post('importSpot_Container_Order', 'Admin\Spot_Container_OrderController@import');

Route::get('admin/spotShipmentOrders', ['as'=> 'admin.spotShipmentOrders.index', 'uses' => 'Admin\Spot_Shipment_OrderController@index']);
Route::post('admin/spotShipmentOrders', ['as'=> 'admin.spotShipmentOrders.store', 'uses' => 'Admin\Spot_Shipment_OrderController@store']);
Route::get('admin/spotShipmentOrders/create', ['as'=> 'admin.spotShipmentOrders.create', 'uses' => 'Admin\Spot_Shipment_OrderController@create']);
Route::put('admin/spotShipmentOrders/{spotShipmentOrders}', ['as'=> 'admin.spotShipmentOrders.update', 'uses' => 'Admin\Spot_Shipment_OrderController@update']);
Route::patch('admin/spotShipmentOrders/{spotShipmentOrders}', ['as'=> 'admin.spotShipmentOrders.update', 'uses' => 'Admin\Spot_Shipment_OrderController@update']);
Route::delete('admin/spotShipmentOrders/{spotShipmentOrders}', ['as'=> 'admin.spotShipmentOrders.destroy', 'uses' => 'Admin\Spot_Shipment_OrderController@destroy']);
Route::get('admin/spotShipmentOrders/{spotShipmentOrders}', ['as'=> 'admin.spotShipmentOrders.show', 'uses' => 'Admin\Spot_Shipment_OrderController@show']);
Route::get('admin/spotShipmentOrders/{spotShipmentOrders}/edit', ['as'=> 'admin.spotShipmentOrders.edit', 'uses' => 'Admin\Spot_Shipment_OrderController@edit']);
// Route::post('importSpot_Shipment_Order', 'Admin\Spot_Shipment_OrderController@import');

Route::get('admin/spotShipmentPayments', ['as'=> 'admin.spotShipmentPayments.index', 'uses' => 'Admin\Spot_Shipment_PaymentController@index']);
Route::post('admin/spotShipmentPayments', ['as'=> 'admin.spotShipmentPayments.store', 'uses' => 'Admin\Spot_Shipment_PaymentController@store']);
Route::get('admin/spotShipmentPayments/create', ['as'=> 'admin.spotShipmentPayments.create', 'uses' => 'Admin\Spot_Shipment_PaymentController@create']);
Route::put('admin/spotShipmentPayments/{spotShipmentPayments}', ['as'=> 'admin.spotShipmentPayments.update', 'uses' => 'Admin\Spot_Shipment_PaymentController@update']);
Route::patch('admin/spotShipmentPayments/{spotShipmentPayments}', ['as'=> 'admin.spotShipmentPayments.update', 'uses' => 'Admin\Spot_Shipment_PaymentController@update']);
Route::delete('admin/spotShipmentPayments/{spotShipmentPayments}', ['as'=> 'admin.spotShipmentPayments.destroy', 'uses' => 'Admin\Spot_Shipment_PaymentController@destroy']);
Route::get('admin/spotShipmentPayments/{spotShipmentPayments}', ['as'=> 'admin.spotShipmentPayments.show', 'uses' => 'Admin\Spot_Shipment_PaymentController@show']);
Route::get('admin/spotShipmentPayments/{spotShipmentPayments}/edit', ['as'=> 'admin.spotShipmentPayments.edit', 'uses' => 'Admin\Spot_Shipment_PaymentController@edit']);
// Route::post('importSpot_Shipment_Payment', 'Admin\Spot_Shipment_PaymentController@import');

Route::get('admin/backendUserRoles', ['as'=> 'admin.backendUserRoles.index', 'uses' => 'Admin\Backend_User_RolesController@index']);
Route::post('admin/backendUserRoles', ['as'=> 'admin.backendUserRoles.store', 'uses' => 'Admin\Backend_User_RolesController@store']);
Route::get('admin/backendUserRoles/create', ['as'=> 'admin.backendUserRoles.create', 'uses' => 'Admin\Backend_User_RolesController@create']);
Route::put('admin/backendUserRoles/{backendUserRoles}', ['as'=> 'admin.backendUserRoles.update', 'uses' => 'Admin\Backend_User_RolesController@update']);
Route::patch('admin/backendUserRoles/{backendUserRoles}', ['as'=> 'admin.backendUserRoles.update', 'uses' => 'Admin\Backend_User_RolesController@update']);
Route::delete('admin/backendUserRoles/{backendUserRoles}', ['as'=> 'admin.backendUserRoles.destroy', 'uses' => 'Admin\Backend_User_RolesController@destroy']);
Route::get('admin/backendUserRoles/{backendUserRoles}', ['as'=> 'admin.backendUserRoles.show', 'uses' => 'Admin\Backend_User_RolesController@show']);
Route::get('admin/backendUserRoles/{backendUserRoles}/edit', ['as'=> 'admin.backendUserRoles.edit', 'uses' => 'Admin\Backend_User_RolesController@edit']);
// Route::post('importBackend_User_Roles', 'Admin\Backend_User_RolesController@import');

Route::get('admin/backendUsers', ['as'=> 'admin.backendUsers.index', 'uses' => 'Admin\Backend_UsersController@index']);
Route::post('admin/backendUsers', ['as'=> 'admin.backendUsers.store', 'uses' => 'Admin\Backend_UsersController@store']);
Route::get('admin/backendUsers/create', ['as'=> 'admin.backendUsers.create', 'uses' => 'Admin\Backend_UsersController@create']);
Route::put('admin/backendUsers/{backendUsers}', ['as'=> 'admin.backendUsers.update', 'uses' => 'Admin\Backend_UsersController@update']);
Route::patch('admin/backendUsers/{backendUsers}', ['as'=> 'admin.backendUsers.update', 'uses' => 'Admin\Backend_UsersController@update']);
Route::delete('admin/backendUsers/{backendUsers}', ['as'=> 'admin.backendUsers.destroy', 'uses' => 'Admin\Backend_UsersController@destroy']);
Route::get('admin/backendUsers/{backendUsers}', ['as'=> 'admin.backendUsers.show', 'uses' => 'Admin\Backend_UsersController@show']);
Route::get('admin/backendUsers/{backendUsers}/edit', ['as'=> 'admin.backendUsers.edit', 'uses' => 'Admin\Backend_UsersController@edit']);
// Route::post('importBackend_Users', 'Admin\Backend_UsersController@import');

Route::get('admin/backendUserGroups', ['as'=> 'admin.backendUserGroups.index', 'uses' => 'Admin\Backend_User_GroupsController@index']);
Route::post('admin/backendUserGroups', ['as'=> 'admin.backendUserGroups.store', 'uses' => 'Admin\Backend_User_GroupsController@store']);
Route::get('admin/backendUserGroups/create', ['as'=> 'admin.backendUserGroups.create', 'uses' => 'Admin\Backend_User_GroupsController@create']);
Route::put('admin/backendUserGroups/{backendUserGroups}', ['as'=> 'admin.backendUserGroups.update', 'uses' => 'Admin\Backend_User_GroupsController@update']);
Route::patch('admin/backendUserGroups/{backendUserGroups}', ['as'=> 'admin.backendUserGroups.update', 'uses' => 'Admin\Backend_User_GroupsController@update']);
Route::delete('admin/backendUserGroups/{backendUserGroups}', ['as'=> 'admin.backendUserGroups.destroy', 'uses' => 'Admin\Backend_User_GroupsController@destroy']);
Route::get('admin/backendUserGroups/{backendUserGroups}', ['as'=> 'admin.backendUserGroups.show', 'uses' => 'Admin\Backend_User_GroupsController@show']);
Route::get('admin/backendUserGroups/{backendUserGroups}/edit', ['as'=> 'admin.backendUserGroups.edit', 'uses' => 'Admin\Backend_User_GroupsController@edit']);
// Route::post('importBackend_User_Groups', 'Admin\Backend_User_GroupsController@import');


Route::get('admin/spotContainerStatuses', ['as'=> 'admin.spotContainerStatuses.index', 'uses' => 'Admin\Spot_Container_StatusController@index']);
Route::post('admin/spotContainerStatuses', ['as'=> 'admin.spotContainerStatuses.store', 'uses' => 'Admin\Spot_Container_StatusController@store']);
Route::get('admin/spotContainerStatuses/create', ['as'=> 'admin.spotContainerStatuses.create', 'uses' => 'Admin\Spot_Container_StatusController@create']);
Route::put('admin/spotContainerStatuses/{spotContainerStatuses}', ['as'=> 'admin.spotContainerStatuses.update', 'uses' => 'Admin\Spot_Container_StatusController@update']);
Route::patch('admin/spotContainerStatuses/{spotContainerStatuses}', ['as'=> 'admin.spotContainerStatuses.update', 'uses' => 'Admin\Spot_Container_StatusController@update']);
Route::delete('admin/spotContainerStatuses/{spotContainerStatuses}', ['as'=> 'admin.spotContainerStatuses.destroy', 'uses' => 'Admin\Spot_Container_StatusController@destroy']);
Route::get('admin/spotContainerStatuses/{spotContainerStatuses}', ['as'=> 'admin.spotContainerStatuses.show', 'uses' => 'Admin\Spot_Container_StatusController@show']);
Route::get('admin/spotContainerStatuses/{spotContainerStatuses}/edit', ['as'=> 'admin.spotContainerStatuses.edit', 'uses' => 'Admin\Spot_Container_StatusController@edit']);
// Route::post('importSpot_Container_Status', 'Admin\Spot_Container_StatusController@import');

Route::get('admin/spotContainerLocations', ['as'=> 'admin.spotContainerLocations.index', 'uses' => 'Admin\Spot_Container_LocationController@index']);
Route::post('admin/spotContainerLocations', ['as'=> 'admin.spotContainerLocations.store', 'uses' => 'Admin\Spot_Container_LocationController@store']);
Route::get('admin/spotContainerLocations/create', ['as'=> 'admin.spotContainerLocations.create', 'uses' => 'Admin\Spot_Container_LocationController@create']);
Route::put('admin/spotContainerLocations/{spotContainerLocations}', ['as'=> 'admin.spotContainerLocations.update', 'uses' => 'Admin\Spot_Container_LocationController@update']);
Route::patch('admin/spotContainerLocations/{spotContainerLocations}', ['as'=> 'admin.spotContainerLocations.update', 'uses' => 'Admin\Spot_Container_LocationController@update']);
Route::delete('admin/spotContainerLocations/{spotContainerLocations}', ['as'=> 'admin.spotContainerLocations.destroy', 'uses' => 'Admin\Spot_Container_LocationController@destroy']);
Route::get('admin/spotContainerLocations/{spotContainerLocations}', ['as'=> 'admin.spotContainerLocations.show', 'uses' => 'Admin\Spot_Container_LocationController@show']);
Route::get('admin/spotContainerLocations/{spotContainerLocations}/edit', ['as'=> 'admin.spotContainerLocations.edit', 'uses' => 'Admin\Spot_Container_LocationController@edit']);
// Route::post('importSpot_Container_Location', 'Admin\Spot_Container_LocationController@import');

Route::get('admin/spotContainerReasons', ['as'=> 'admin.spotContainerReasons.index', 'uses' => 'Admin\Spot_Container_ReasonController@index']);
Route::post('admin/spotContainerReasons', ['as'=> 'admin.spotContainerReasons.store', 'uses' => 'Admin\Spot_Container_ReasonController@store']);
Route::get('admin/spotContainerReasons/create', ['as'=> 'admin.spotContainerReasons.create', 'uses' => 'Admin\Spot_Container_ReasonController@create']);
Route::put('admin/spotContainerReasons/{spotContainerReasons}', ['as'=> 'admin.spotContainerReasons.update', 'uses' => 'Admin\Spot_Container_ReasonController@update']);
Route::patch('admin/spotContainerReasons/{spotContainerReasons}', ['as'=> 'admin.spotContainerReasons.update', 'uses' => 'Admin\Spot_Container_ReasonController@update']);
Route::delete('admin/spotContainerReasons/{spotContainerReasons}', ['as'=> 'admin.spotContainerReasons.destroy', 'uses' => 'Admin\Spot_Container_ReasonController@destroy']);
Route::get('admin/spotContainerReasons/{spotContainerReasons}', ['as'=> 'admin.spotContainerReasons.show', 'uses' => 'Admin\Spot_Container_ReasonController@show']);
Route::get('admin/spotContainerReasons/{spotContainerReasons}/edit', ['as'=> 'admin.spotContainerReasons.edit', 'uses' => 'Admin\Spot_Container_ReasonController@edit']);
// Route::post('importSpot_Container_Reason', 'Admin\Spot_Container_ReasonController@import');

Route::get('admin/spotContainerSizes', ['as'=> 'admin.spotContainerSizes.index', 'uses' => 'Admin\Spot_Container_SizeController@index']);
Route::post('admin/spotContainerSizes', ['as'=> 'admin.spotContainerSizes.store', 'uses' => 'Admin\Spot_Container_SizeController@store']);
Route::get('admin/spotContainerSizes/create', ['as'=> 'admin.spotContainerSizes.create', 'uses' => 'Admin\Spot_Container_SizeController@create']);
Route::put('admin/spotContainerSizes/{spotContainerSizes}', ['as'=> 'admin.spotContainerSizes.update', 'uses' => 'Admin\Spot_Container_SizeController@update']);
Route::patch('admin/spotContainerSizes/{spotContainerSizes}', ['as'=> 'admin.spotContainerSizes.update', 'uses' => 'Admin\Spot_Container_SizeController@update']);
Route::delete('admin/spotContainerSizes/{spotContainerSizes}', ['as'=> 'admin.spotContainerSizes.destroy', 'uses' => 'Admin\Spot_Container_SizeController@destroy']);
Route::get('admin/spotContainerSizes/{spotContainerSizes}', ['as'=> 'admin.spotContainerSizes.show', 'uses' => 'Admin\Spot_Container_SizeController@show']);
Route::get('admin/spotContainerSizes/{spotContainerSizes}/edit', ['as'=> 'admin.spotContainerSizes.edit', 'uses' => 'Admin\Spot_Container_SizeController@edit']);
// Route::post('importSpot_Container_Size', 'Admin\Spot_Container_SizeController@import');

Route::get('admin/spotShipmentCars', ['as'=> 'admin.spotShipmentCars.index', 'uses' => 'Admin\Spot_Shipment_CarController@index']);
Route::post('admin/spotShipmentCars', ['as'=> 'admin.spotShipmentCars.store', 'uses' => 'Admin\Spot_Shipment_CarController@store']);
Route::get('admin/spotShipmentCars/create', ['as'=> 'admin.spotShipmentCars.create', 'uses' => 'Admin\Spot_Shipment_CarController@create']);
Route::put('admin/spotShipmentCars/{spotShipmentCars}', ['as'=> 'admin.spotShipmentCars.update', 'uses' => 'Admin\Spot_Shipment_CarController@update']);
Route::patch('admin/spotShipmentCars/{spotShipmentCars}', ['as'=> 'admin.spotShipmentCars.update', 'uses' => 'Admin\Spot_Shipment_CarController@update']);
Route::delete('admin/spotShipmentCars/{spotShipmentCars}', ['as'=> 'admin.spotShipmentCars.destroy', 'uses' => 'Admin\Spot_Shipment_CarController@destroy']);
Route::get('admin/spotShipmentCars/{spotShipmentCars}', ['as'=> 'admin.spotShipmentCars.show', 'uses' => 'Admin\Spot_Shipment_CarController@show']);
Route::get('admin/spotShipmentCars/{spotShipmentCars}/edit', ['as'=> 'admin.spotShipmentCars.edit', 'uses' => 'Admin\Spot_Shipment_CarController@edit']);
// Route::post('importSpot_Shipment_Car', 'Admin\Spot_Shipment_CarController@import');

Route::get('admin/spotShipmentCategories', ['as'=> 'admin.spotShipmentCategories.index', 'uses' => 'Admin\Spot_Shipment_CategoryController@index']);
Route::post('admin/spotShipmentCategories', ['as'=> 'admin.spotShipmentCategories.store', 'uses' => 'Admin\Spot_Shipment_CategoryController@store']);
Route::get('admin/spotShipmentCategories/create', ['as'=> 'admin.spotShipmentCategories.create', 'uses' => 'Admin\Spot_Shipment_CategoryController@create']);
Route::put('admin/spotShipmentCategories/{spotShipmentCategories}', ['as'=> 'admin.spotShipmentCategories.update', 'uses' => 'Admin\Spot_Shipment_CategoryController@update']);
Route::patch('admin/spotShipmentCategories/{spotShipmentCategories}', ['as'=> 'admin.spotShipmentCategories.update', 'uses' => 'Admin\Spot_Shipment_CategoryController@update']);
Route::delete('admin/spotShipmentCategories/{spotShipmentCategories}', ['as'=> 'admin.spotShipmentCategories.destroy', 'uses' => 'Admin\Spot_Shipment_CategoryController@destroy']);
Route::get('admin/spotShipmentCategories/{spotShipmentCategories}', ['as'=> 'admin.spotShipmentCategories.show', 'uses' => 'Admin\Spot_Shipment_CategoryController@show']);
Route::get('admin/spotShipmentCategories/{spotShipmentCategories}/edit', ['as'=> 'admin.spotShipmentCategories.edit', 'uses' => 'Admin\Spot_Shipment_CategoryController@edit']);
// Route::post('importSpot_Shipment_Category', 'Admin\Spot_Shipment_CategoryController@import');

Route::get('admin/spotShipmentCouriers', ['as'=> 'admin.spotShipmentCouriers.index', 'uses' => 'Admin\Spot_Shipment_CourierController@index']);
Route::post('admin/spotShipmentCouriers', ['as'=> 'admin.spotShipmentCouriers.store', 'uses' => 'Admin\Spot_Shipment_CourierController@store']);
Route::get('admin/spotShipmentCouriers/create', ['as'=> 'admin.spotShipmentCouriers.create', 'uses' => 'Admin\Spot_Shipment_CourierController@create']);
Route::put('admin/spotShipmentCouriers/{spotShipmentCouriers}', ['as'=> 'admin.spotShipmentCouriers.update', 'uses' => 'Admin\Spot_Shipment_CourierController@update']);
Route::patch('admin/spotShipmentCouriers/{spotShipmentCouriers}', ['as'=> 'admin.spotShipmentCouriers.update', 'uses' => 'Admin\Spot_Shipment_CourierController@update']);
Route::delete('admin/spotShipmentCouriers/{spotShipmentCouriers}', ['as'=> 'admin.spotShipmentCouriers.destroy', 'uses' => 'Admin\Spot_Shipment_CourierController@destroy']);
Route::get('admin/spotShipmentCouriers/{spotShipmentCouriers}', ['as'=> 'admin.spotShipmentCouriers.show', 'uses' => 'Admin\Spot_Shipment_CourierController@show']);
Route::get('admin/spotShipmentCouriers/{spotShipmentCouriers}/edit', ['as'=> 'admin.spotShipmentCouriers.edit', 'uses' => 'Admin\Spot_Shipment_CourierController@edit']);
// Route::post('importSpot_Shipment_Courier', 'Admin\Spot_Shipment_CourierController@import');

Route::get('admin/spotShipmentLabels', ['as'=> 'admin.spotShipmentLabels.index', 'uses' => 'Admin\Spot_Shipment_LabelController@index']);
Route::post('admin/spotShipmentLabels', ['as'=> 'admin.spotShipmentLabels.store', 'uses' => 'Admin\Spot_Shipment_LabelController@store']);
Route::get('admin/spotShipmentLabels/create', ['as'=> 'admin.spotShipmentLabels.create', 'uses' => 'Admin\Spot_Shipment_LabelController@create']);
Route::put('admin/spotShipmentLabels/{spotShipmentLabels}', ['as'=> 'admin.spotShipmentLabels.update', 'uses' => 'Admin\Spot_Shipment_LabelController@update']);
Route::patch('admin/spotShipmentLabels/{spotShipmentLabels}', ['as'=> 'admin.spotShipmentLabels.update', 'uses' => 'Admin\Spot_Shipment_LabelController@update']);
Route::delete('admin/spotShipmentLabels/{spotShipmentLabels}', ['as'=> 'admin.spotShipmentLabels.destroy', 'uses' => 'Admin\Spot_Shipment_LabelController@destroy']);
Route::get('admin/spotShipmentLabels/{spotShipmentLabels}', ['as'=> 'admin.spotShipmentLabels.show', 'uses' => 'Admin\Spot_Shipment_LabelController@show']);
Route::get('admin/spotShipmentLabels/{spotShipmentLabels}/edit', ['as'=> 'admin.spotShipmentLabels.edit', 'uses' => 'Admin\Spot_Shipment_LabelController@edit']);
// Route::post('importSpot_Shipment_Label', 'Admin\Spot_Shipment_LabelController@import');

Route::get('admin/spotShipmentDeliveryTimes', ['as'=> 'admin.spotShipmentDeliveryTimes.index', 'uses' => 'Admin\Spot_Shipment_Delivery_TimeController@index']);
Route::post('admin/spotShipmentDeliveryTimes', ['as'=> 'admin.spotShipmentDeliveryTimes.store', 'uses' => 'Admin\Spot_Shipment_Delivery_TimeController@store']);
Route::get('admin/spotShipmentDeliveryTimes/create', ['as'=> 'admin.spotShipmentDeliveryTimes.create', 'uses' => 'Admin\Spot_Shipment_Delivery_TimeController@create']);
Route::put('admin/spotShipmentDeliveryTimes/{spotShipmentDeliveryTimes}', ['as'=> 'admin.spotShipmentDeliveryTimes.update', 'uses' => 'Admin\Spot_Shipment_Delivery_TimeController@update']);
Route::patch('admin/spotShipmentDeliveryTimes/{spotShipmentDeliveryTimes}', ['as'=> 'admin.spotShipmentDeliveryTimes.update', 'uses' => 'Admin\Spot_Shipment_Delivery_TimeController@update']);
Route::delete('admin/spotShipmentDeliveryTimes/{spotShipmentDeliveryTimes}', ['as'=> 'admin.spotShipmentDeliveryTimes.destroy', 'uses' => 'Admin\Spot_Shipment_Delivery_TimeController@destroy']);
Route::get('admin/spotShipmentDeliveryTimes/{spotShipmentDeliveryTimes}', ['as'=> 'admin.spotShipmentDeliveryTimes.show', 'uses' => 'Admin\Spot_Shipment_Delivery_TimeController@show']);
Route::get('admin/spotShipmentDeliveryTimes/{spotShipmentDeliveryTimes}/edit', ['as'=> 'admin.spotShipmentDeliveryTimes.edit', 'uses' => 'Admin\Spot_Shipment_Delivery_TimeController@edit']);
// Route::post('importSpot_Shipment_Delivery_Time', 'Admin\Spot_Shipment_Delivery_TimeController@import');

Route::get('admin/spotShipmentHandlers', ['as'=> 'admin.spotShipmentHandlers.index', 'uses' => 'Admin\Spot_Shipment_HandlerController@index']);
Route::post('admin/spotShipmentHandlers', ['as'=> 'admin.spotShipmentHandlers.store', 'uses' => 'Admin\Spot_Shipment_HandlerController@store']);
Route::get('admin/spotShipmentHandlers/create', ['as'=> 'admin.spotShipmentHandlers.create', 'uses' => 'Admin\Spot_Shipment_HandlerController@create']);
Route::put('admin/spotShipmentHandlers/{spotShipmentHandlers}', ['as'=> 'admin.spotShipmentHandlers.update', 'uses' => 'Admin\Spot_Shipment_HandlerController@update']);
Route::patch('admin/spotShipmentHandlers/{spotShipmentHandlers}', ['as'=> 'admin.spotShipmentHandlers.update', 'uses' => 'Admin\Spot_Shipment_HandlerController@update']);
Route::delete('admin/spotShipmentHandlers/{spotShipmentHandlers}', ['as'=> 'admin.spotShipmentHandlers.destroy', 'uses' => 'Admin\Spot_Shipment_HandlerController@destroy']);
Route::get('admin/spotShipmentHandlers/{spotShipmentHandlers}', ['as'=> 'admin.spotShipmentHandlers.show', 'uses' => 'Admin\Spot_Shipment_HandlerController@show']);
Route::get('admin/spotShipmentHandlers/{spotShipmentHandlers}/edit', ['as'=> 'admin.spotShipmentHandlers.edit', 'uses' => 'Admin\Spot_Shipment_HandlerController@edit']);
// Route::post('importSpot_Shipment_Handler', 'Admin\Spot_Shipment_HandlerController@import');

Route::get('admin/spotShipmentModes', ['as'=> 'admin.spotShipmentModes.index', 'uses' => 'Admin\Spot_Shipment_ModeController@index']);
Route::post('admin/spotShipmentModes', ['as'=> 'admin.spotShipmentModes.store', 'uses' => 'Admin\Spot_Shipment_ModeController@store']);
Route::get('admin/spotShipmentModes/create', ['as'=> 'admin.spotShipmentModes.create', 'uses' => 'Admin\Spot_Shipment_ModeController@create']);
Route::put('admin/spotShipmentModes/{spotShipmentModes}', ['as'=> 'admin.spotShipmentModes.update', 'uses' => 'Admin\Spot_Shipment_ModeController@update']);
Route::patch('admin/spotShipmentModes/{spotShipmentModes}', ['as'=> 'admin.spotShipmentModes.update', 'uses' => 'Admin\Spot_Shipment_ModeController@update']);
Route::delete('admin/spotShipmentModes/{spotShipmentModes}', ['as'=> 'admin.spotShipmentModes.destroy', 'uses' => 'Admin\Spot_Shipment_ModeController@destroy']);
Route::get('admin/spotShipmentModes/{spotShipmentModes}', ['as'=> 'admin.spotShipmentModes.show', 'uses' => 'Admin\Spot_Shipment_ModeController@show']);
Route::get('admin/spotShipmentModes/{spotShipmentModes}/edit', ['as'=> 'admin.spotShipmentModes.edit', 'uses' => 'Admin\Spot_Shipment_ModeController@edit']);
// Route::post('importSpot_Shipment_Mode', 'Admin\Spot_Shipment_ModeController@import');

Route::get('admin/spotShipmentOffices', ['as'=> 'admin.spotShipmentOffices.index', 'uses' => 'Admin\Spot_Shipment_OfficeController@index']);
Route::post('admin/spotShipmentOffices', ['as'=> 'admin.spotShipmentOffices.store', 'uses' => 'Admin\Spot_Shipment_OfficeController@store']);
Route::get('admin/spotShipmentOffices/create', ['as'=> 'admin.spotShipmentOffices.create', 'uses' => 'Admin\Spot_Shipment_OfficeController@create']);
Route::put('admin/spotShipmentOffices/{spotShipmentOffices}', ['as'=> 'admin.spotShipmentOffices.update', 'uses' => 'Admin\Spot_Shipment_OfficeController@update']);
Route::patch('admin/spotShipmentOffices/{spotShipmentOffices}', ['as'=> 'admin.spotShipmentOffices.update', 'uses' => 'Admin\Spot_Shipment_OfficeController@update']);
Route::delete('admin/spotShipmentOffices/{spotShipmentOffices}', ['as'=> 'admin.spotShipmentOffices.destroy', 'uses' => 'Admin\Spot_Shipment_OfficeController@destroy']);
Route::get('admin/spotShipmentOffices/{spotShipmentOffices}', ['as'=> 'admin.spotShipmentOffices.show', 'uses' => 'Admin\Spot_Shipment_OfficeController@show']);
Route::get('admin/spotShipmentOffices/{spotShipmentOffices}/edit', ['as'=> 'admin.spotShipmentOffices.edit', 'uses' => 'Admin\Spot_Shipment_OfficeController@edit']);
// Route::post('importSpot_Shipment_Office', 'Admin\Spot_Shipment_OfficeController@import');

Route::get('admin/spotShipmentPackagings', ['as'=> 'admin.spotShipmentPackagings.index', 'uses' => 'Admin\Spot_Shipment_PackagingController@index']);
Route::post('admin/spotShipmentPackagings', ['as'=> 'admin.spotShipmentPackagings.store', 'uses' => 'Admin\Spot_Shipment_PackagingController@store']);
Route::get('admin/spotShipmentPackagings/create', ['as'=> 'admin.spotShipmentPackagings.create', 'uses' => 'Admin\Spot_Shipment_PackagingController@create']);
Route::put('admin/spotShipmentPackagings/{spotShipmentPackagings}', ['as'=> 'admin.spotShipmentPackagings.update', 'uses' => 'Admin\Spot_Shipment_PackagingController@update']);
Route::patch('admin/spotShipmentPackagings/{spotShipmentPackagings}', ['as'=> 'admin.spotShipmentPackagings.update', 'uses' => 'Admin\Spot_Shipment_PackagingController@update']);
Route::delete('admin/spotShipmentPackagings/{spotShipmentPackagings}', ['as'=> 'admin.spotShipmentPackagings.destroy', 'uses' => 'Admin\Spot_Shipment_PackagingController@destroy']);
Route::get('admin/spotShipmentPackagings/{spotShipmentPackagings}', ['as'=> 'admin.spotShipmentPackagings.show', 'uses' => 'Admin\Spot_Shipment_PackagingController@show']);
Route::get('admin/spotShipmentPackagings/{spotShipmentPackagings}/edit', ['as'=> 'admin.spotShipmentPackagings.edit', 'uses' => 'Admin\Spot_Shipment_PackagingController@edit']);
// Route::post('importSpot_Shipment_Packaging', 'Admin\Spot_Shipment_PackagingController@import');

Route::get('admin/spotShipmentStatuses', ['as'=> 'admin.spotShipmentStatuses.index', 'uses' => 'Admin\Spot_Shipment_StatusController@index']);
Route::post('admin/spotShipmentStatuses', ['as'=> 'admin.spotShipmentStatuses.store', 'uses' => 'Admin\Spot_Shipment_StatusController@store']);
Route::get('admin/spotShipmentStatuses/create', ['as'=> 'admin.spotShipmentStatuses.create', 'uses' => 'Admin\Spot_Shipment_StatusController@create']);
Route::put('admin/spotShipmentStatuses/{spotShipmentStatuses}', ['as'=> 'admin.spotShipmentStatuses.update', 'uses' => 'Admin\Spot_Shipment_StatusController@update']);
Route::patch('admin/spotShipmentStatuses/{spotShipmentStatuses}', ['as'=> 'admin.spotShipmentStatuses.update', 'uses' => 'Admin\Spot_Shipment_StatusController@update']);
Route::delete('admin/spotShipmentStatuses/{spotShipmentStatuses}', ['as'=> 'admin.spotShipmentStatuses.destroy', 'uses' => 'Admin\Spot_Shipment_StatusController@destroy']);
Route::get('admin/spotShipmentStatuses/{spotShipmentStatuses}', ['as'=> 'admin.spotShipmentStatuses.show', 'uses' => 'Admin\Spot_Shipment_StatusController@show']);
Route::get('admin/spotShipmentStatuses/{spotShipmentStatuses}/edit', ['as'=> 'admin.spotShipmentStatuses.edit', 'uses' => 'Admin\Spot_Shipment_StatusController@edit']);
// Route::post('importSpot_Shipment_Status', 'Admin\Spot_Shipment_StatusController@import');

Route::get('admin/spotShipmentTreasuries', ['as'=> 'admin.spotShipmentTreasuries.index', 'uses' => 'Admin\Spot_Shipment_TreasuryController@index']);
Route::post('admin/spotShipmentTreasuries', ['as'=> 'admin.spotShipmentTreasuries.store', 'uses' => 'Admin\Spot_Shipment_TreasuryController@store']);
Route::get('admin/spotShipmentTreasuries/create', ['as'=> 'admin.spotShipmentTreasuries.create', 'uses' => 'Admin\Spot_Shipment_TreasuryController@create']);
Route::put('admin/spotShipmentTreasuries/{spotShipmentTreasuries}', ['as'=> 'admin.spotShipmentTreasuries.update', 'uses' => 'Admin\Spot_Shipment_TreasuryController@update']);
Route::patch('admin/spotShipmentTreasuries/{spotShipmentTreasuries}', ['as'=> 'admin.spotShipmentTreasuries.update', 'uses' => 'Admin\Spot_Shipment_TreasuryController@update']);
Route::delete('admin/spotShipmentTreasuries/{spotShipmentTreasuries}', ['as'=> 'admin.spotShipmentTreasuries.destroy', 'uses' => 'Admin\Spot_Shipment_TreasuryController@destroy']);
Route::get('admin/spotShipmentTreasuries/{spotShipmentTreasuries}', ['as'=> 'admin.spotShipmentTreasuries.show', 'uses' => 'Admin\Spot_Shipment_TreasuryController@show']);
Route::get('admin/spotShipmentTreasuries/{spotShipmentTreasuries}/edit', ['as'=> 'admin.spotShipmentTreasuries.edit', 'uses' => 'Admin\Spot_Shipment_TreasuryController@edit']);
// Route::post('importSpot_Shipment_Treasury', 'Admin\Spot_Shipment_TreasuryController@import');

Route::get('admin/spotShipmentBreaks', ['as'=> 'admin.spotShipmentBreaks.index', 'uses' => 'Admin\Spot_Shipment_BreakController@index']);
Route::post('admin/spotShipmentBreaks', ['as'=> 'admin.spotShipmentBreaks.store', 'uses' => 'Admin\Spot_Shipment_BreakController@store']);
Route::get('admin/spotShipmentBreaks/create', ['as'=> 'admin.spotShipmentBreaks.create', 'uses' => 'Admin\Spot_Shipment_BreakController@create']);
Route::put('admin/spotShipmentBreaks/{spotShipmentBreaks}', ['as'=> 'admin.spotShipmentBreaks.update', 'uses' => 'Admin\Spot_Shipment_BreakController@update']);
Route::patch('admin/spotShipmentBreaks/{spotShipmentBreaks}', ['as'=> 'admin.spotShipmentBreaks.update', 'uses' => 'Admin\Spot_Shipment_BreakController@update']);
Route::delete('admin/spotShipmentBreaks/{spotShipmentBreaks}', ['as'=> 'admin.spotShipmentBreaks.destroy', 'uses' => 'Admin\Spot_Shipment_BreakController@destroy']);
Route::get('admin/spotShipmentBreaks/{spotShipmentBreaks}', ['as'=> 'admin.spotShipmentBreaks.show', 'uses' => 'Admin\Spot_Shipment_BreakController@show']);
Route::get('admin/spotShipmentBreaks/{spotShipmentBreaks}/edit', ['as'=> 'admin.spotShipmentBreaks.edit', 'uses' => 'Admin\Spot_Shipment_BreakController@edit']);
// Route::post('importSpot_Shipment_Break', 'Admin\Spot_Shipment_BreakController@import');

Route::get('admin/spotContainerDestinations', ['as'=> 'admin.spotContainerDestinations.index', 'uses' => 'Admin\Spot_Container_DestinationController@index']);
Route::post('admin/spotContainerDestinations', ['as'=> 'admin.spotContainerDestinations.store', 'uses' => 'Admin\Spot_Container_DestinationController@store']);
Route::get('admin/spotContainerDestinations/create', ['as'=> 'admin.spotContainerDestinations.create', 'uses' => 'Admin\Spot_Container_DestinationController@create']);
Route::put('admin/spotContainerDestinations/{spotContainerDestinations}', ['as'=> 'admin.spotContainerDestinations.update', 'uses' => 'Admin\Spot_Container_DestinationController@update']);
Route::patch('admin/spotContainerDestinations/{spotContainerDestinations}', ['as'=> 'admin.spotContainerDestinations.update', 'uses' => 'Admin\Spot_Container_DestinationController@update']);
Route::delete('admin/spotContainerDestinations/{spotContainerDestinations}', ['as'=> 'admin.spotContainerDestinations.destroy', 'uses' => 'Admin\Spot_Container_DestinationController@destroy']);
Route::get('admin/spotContainerDestinations/{spotContainerDestinations}', ['as'=> 'admin.spotContainerDestinations.show', 'uses' => 'Admin\Spot_Container_DestinationController@show']);
Route::get('admin/spotContainerDestinations/{spotContainerDestinations}/edit', ['as'=> 'admin.spotContainerDestinations.edit', 'uses' => 'Admin\Spot_Container_DestinationController@edit']);
// Route::post('importSpot_Container_Destination', 'Admin\Spot_Container_DestinationController@import');

Route::get('admin/rainlabLocationCountries', ['as'=> 'admin.rainlabLocationCountries.index', 'uses' => 'Admin\Rainlab_Location_CountriesController@index']);
Route::post('admin/rainlabLocationCountries', ['as'=> 'admin.rainlabLocationCountries.store', 'uses' => 'Admin\Rainlab_Location_CountriesController@store']);
Route::get('admin/rainlabLocationCountries/create', ['as'=> 'admin.rainlabLocationCountries.create', 'uses' => 'Admin\Rainlab_Location_CountriesController@create']);
Route::put('admin/rainlabLocationCountries/{rainlabLocationCountries}', ['as'=> 'admin.rainlabLocationCountries.update', 'uses' => 'Admin\Rainlab_Location_CountriesController@update']);
Route::patch('admin/rainlabLocationCountries/{rainlabLocationCountries}', ['as'=> 'admin.rainlabLocationCountries.update', 'uses' => 'Admin\Rainlab_Location_CountriesController@update']);
Route::delete('admin/rainlabLocationCountries/{rainlabLocationCountries}', ['as'=> 'admin.rainlabLocationCountries.destroy', 'uses' => 'Admin\Rainlab_Location_CountriesController@destroy']);
Route::get('admin/rainlabLocationCountries/{rainlabLocationCountries}', ['as'=> 'admin.rainlabLocationCountries.show', 'uses' => 'Admin\Rainlab_Location_CountriesController@show']);
Route::get('admin/rainlabLocationCountries/{rainlabLocationCountries}/edit', ['as'=> 'admin.rainlabLocationCountries.edit', 'uses' => 'Admin\Rainlab_Location_CountriesController@edit']);
// Route::post('importRainlab_Location_Countries', 'Admin\Rainlab_Location_CountriesController@import');

Route::get('admin/rainlabLocationStates', ['as'=> 'admin.rainlabLocationStates.index', 'uses' => 'Admin\Rainlab_Location_StatesController@index']);
Route::post('admin/rainlabLocationStates', ['as'=> 'admin.rainlabLocationStates.store', 'uses' => 'Admin\Rainlab_Location_StatesController@store']);
Route::get('admin/rainlabLocationStates/create', ['as'=> 'admin.rainlabLocationStates.create', 'uses' => 'Admin\Rainlab_Location_StatesController@create']);
Route::put('admin/rainlabLocationStates/{rainlabLocationStates}', ['as'=> 'admin.rainlabLocationStates.update', 'uses' => 'Admin\Rainlab_Location_StatesController@update']);
Route::patch('admin/rainlabLocationStates/{rainlabLocationStates}', ['as'=> 'admin.rainlabLocationStates.update', 'uses' => 'Admin\Rainlab_Location_StatesController@update']);
Route::delete('admin/rainlabLocationStates/{rainlabLocationStates}', ['as'=> 'admin.rainlabLocationStates.destroy', 'uses' => 'Admin\Rainlab_Location_StatesController@destroy']);
Route::get('admin/rainlabLocationStates/{rainlabLocationStates}', ['as'=> 'admin.rainlabLocationStates.show', 'uses' => 'Admin\Rainlab_Location_StatesController@show']);
Route::get('admin/rainlabLocationStates/{rainlabLocationStates}/edit', ['as'=> 'admin.rainlabLocationStates.edit', 'uses' => 'Admin\Rainlab_Location_StatesController@edit']);
// Route::post('importRainlab_Location_States', 'Admin\Rainlab_Location_StatesController@import');

Route::get('admin/spotShipmentCities', ['as'=> 'admin.spotShipmentCities.index', 'uses' => 'Admin\Spot_Shipment_CityController@index']);
Route::post('admin/spotShipmentCities', ['as'=> 'admin.spotShipmentCities.store', 'uses' => 'Admin\Spot_Shipment_CityController@store']);
Route::get('admin/spotShipmentCities/create', ['as'=> 'admin.spotShipmentCities.create', 'uses' => 'Admin\Spot_Shipment_CityController@create']);
Route::put('admin/spotShipmentCities/{spotShipmentCities}', ['as'=> 'admin.spotShipmentCities.update', 'uses' => 'Admin\Spot_Shipment_CityController@update']);
Route::patch('admin/spotShipmentCities/{spotShipmentCities}', ['as'=> 'admin.spotShipmentCities.update', 'uses' => 'Admin\Spot_Shipment_CityController@update']);
Route::delete('admin/spotShipmentCities/{spotShipmentCities}', ['as'=> 'admin.spotShipmentCities.destroy', 'uses' => 'Admin\Spot_Shipment_CityController@destroy']);
Route::get('admin/spotShipmentCities/{spotShipmentCities}', ['as'=> 'admin.spotShipmentCities.show', 'uses' => 'Admin\Spot_Shipment_CityController@show']);
Route::get('admin/spotShipmentCities/{spotShipmentCities}/edit', ['as'=> 'admin.spotShipmentCities.edit', 'uses' => 'Admin\Spot_Shipment_CityController@edit']);
// Route::post('importSpot_Shipment_City', 'Admin\Spot_Shipment_CityController@import');

Route::get('admin/spotShipmentAreas', ['as'=> 'admin.spotShipmentAreas.index', 'uses' => 'Admin\Spot_Shipment_AreaController@index']);
Route::post('admin/spotShipmentAreas', ['as'=> 'admin.spotShipmentAreas.store', 'uses' => 'Admin\Spot_Shipment_AreaController@store']);
Route::get('admin/spotShipmentAreas/create', ['as'=> 'admin.spotShipmentAreas.create', 'uses' => 'Admin\Spot_Shipment_AreaController@create']);
Route::put('admin/spotShipmentAreas/{spotShipmentAreas}', ['as'=> 'admin.spotShipmentAreas.update', 'uses' => 'Admin\Spot_Shipment_AreaController@update']);
Route::patch('admin/spotShipmentAreas/{spotShipmentAreas}', ['as'=> 'admin.spotShipmentAreas.update', 'uses' => 'Admin\Spot_Shipment_AreaController@update']);
Route::delete('admin/spotShipmentAreas/{spotShipmentAreas}', ['as'=> 'admin.spotShipmentAreas.destroy', 'uses' => 'Admin\Spot_Shipment_AreaController@destroy']);
Route::get('admin/spotShipmentAreas/{spotShipmentAreas}', ['as'=> 'admin.spotShipmentAreas.show', 'uses' => 'Admin\Spot_Shipment_AreaController@show']);
Route::get('admin/spotShipmentAreas/{spotShipmentAreas}/edit', ['as'=> 'admin.spotShipmentAreas.edit', 'uses' => 'Admin\Spot_Shipment_AreaController@edit']);
// Route::post('importSpot_Shipment_Area', 'Admin\Spot_Shipment_AreaController@import');

Route::get('admin/userGroups', ['as'=> 'admin.userGroups.index', 'uses' => 'Admin\User_GroupsController@index']);
Route::post('admin/userGroups', ['as'=> 'admin.userGroups.store', 'uses' => 'Admin\User_GroupsController@store']);
Route::get('admin/userGroups/create', ['as'=> 'admin.userGroups.create', 'uses' => 'Admin\User_GroupsController@create']);
Route::put('admin/userGroups/{userGroups}', ['as'=> 'admin.userGroups.update', 'uses' => 'Admin\User_GroupsController@update']);
Route::patch('admin/userGroups/{userGroups}', ['as'=> 'admin.userGroups.update', 'uses' => 'Admin\User_GroupsController@update']);
Route::delete('admin/userGroups/{userGroups}', ['as'=> 'admin.userGroups.destroy', 'uses' => 'Admin\User_GroupsController@destroy']);
Route::get('admin/userGroups/{userGroups}', ['as'=> 'admin.userGroups.show', 'uses' => 'Admin\User_GroupsController@show']);
Route::get('admin/userGroups/{userGroups}/edit', ['as'=> 'admin.userGroups.edit', 'uses' => 'Admin\User_GroupsController@edit']);
// Route::post('importUser_Groups', 'Admin\User_GroupsController@import');

Route::get('admin/responsivCurrencyCurrencies', ['as'=> 'admin.responsivCurrencyCurrencies.index', 'uses' => 'Admin\Responsiv_Currency_CurrenciesController@index']);
Route::post('admin/responsivCurrencyCurrencies', ['as'=> 'admin.responsivCurrencyCurrencies.store', 'uses' => 'Admin\Responsiv_Currency_CurrenciesController@store']);
Route::get('admin/responsivCurrencyCurrencies/create', ['as'=> 'admin.responsivCurrencyCurrencies.create', 'uses' => 'Admin\Responsiv_Currency_CurrenciesController@create']);
Route::put('admin/responsivCurrencyCurrencies/{responsivCurrencyCurrencies}', ['as'=> 'admin.responsivCurrencyCurrencies.update', 'uses' => 'Admin\Responsiv_Currency_CurrenciesController@update']);
Route::patch('admin/responsivCurrencyCurrencies/{responsivCurrencyCurrencies}', ['as'=> 'admin.responsivCurrencyCurrencies.update', 'uses' => 'Admin\Responsiv_Currency_CurrenciesController@update']);
Route::delete('admin/responsivCurrencyCurrencies/{responsivCurrencyCurrencies}', ['as'=> 'admin.responsivCurrencyCurrencies.destroy', 'uses' => 'Admin\Responsiv_Currency_CurrenciesController@destroy']);
Route::get('admin/responsivCurrencyCurrencies/{responsivCurrencyCurrencies}', ['as'=> 'admin.responsivCurrencyCurrencies.show', 'uses' => 'Admin\Responsiv_Currency_CurrenciesController@show']);
Route::get('admin/responsivCurrencyCurrencies/{responsivCurrencyCurrencies}/edit', ['as'=> 'admin.responsivCurrencyCurrencies.edit', 'uses' => 'Admin\Responsiv_Currency_CurrenciesController@edit']);
// Route::post('importResponsiv_Currency_Currencies', 'Admin\Responsiv_Currency_CurrenciesController@import');

Route::get('admin/responsivPayInvoiceStatuses', ['as'=> 'admin.responsivPayInvoiceStatuses.index', 'uses' => 'Admin\Responsiv_Pay_Invoice_StatusesController@index']);
Route::post('admin/responsivPayInvoiceStatuses', ['as'=> 'admin.responsivPayInvoiceStatuses.store', 'uses' => 'Admin\Responsiv_Pay_Invoice_StatusesController@store']);
Route::get('admin/responsivPayInvoiceStatuses/create', ['as'=> 'admin.responsivPayInvoiceStatuses.create', 'uses' => 'Admin\Responsiv_Pay_Invoice_StatusesController@create']);
Route::put('admin/responsivPayInvoiceStatuses/{responsivPayInvoiceStatuses}', ['as'=> 'admin.responsivPayInvoiceStatuses.update', 'uses' => 'Admin\Responsiv_Pay_Invoice_StatusesController@update']);
Route::patch('admin/responsivPayInvoiceStatuses/{responsivPayInvoiceStatuses}', ['as'=> 'admin.responsivPayInvoiceStatuses.update', 'uses' => 'Admin\Responsiv_Pay_Invoice_StatusesController@update']);
Route::delete('admin/responsivPayInvoiceStatuses/{responsivPayInvoiceStatuses}', ['as'=> 'admin.responsivPayInvoiceStatuses.destroy', 'uses' => 'Admin\Responsiv_Pay_Invoice_StatusesController@destroy']);
Route::get('admin/responsivPayInvoiceStatuses/{responsivPayInvoiceStatuses}', ['as'=> 'admin.responsivPayInvoiceStatuses.show', 'uses' => 'Admin\Responsiv_Pay_Invoice_StatusesController@show']);
Route::get('admin/responsivPayInvoiceStatuses/{responsivPayInvoiceStatuses}/edit', ['as'=> 'admin.responsivPayInvoiceStatuses.edit', 'uses' => 'Admin\Responsiv_Pay_Invoice_StatusesController@edit']);
// Route::post('importResponsiv_Pay_Invoice_Statuses', 'Admin\Responsiv_Pay_Invoice_StatusesController@import');