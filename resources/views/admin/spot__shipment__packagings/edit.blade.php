@extends('layouts.app')

@section('contents')
    {{-- <div class="content content-components"> --}}
    <div class="content">
        <div class="container">
            @include('dashforge-templates::common.errors')

            <h4 id="section1" class="mg-b-10">Spot  Shipment  Packaging</h4>

            <p class="mg-b-30">Please, fill all required fields before click save button.</p>

            <div data-label="Edit" class="df-example demo-forms spot__shipment__packagings-forms">
                {!! Form::model($spotShipmentPackaging, ['route' => ['admin.spotShipmentPackagings.update', $spotShipmentPackaging->id], 'method' => 'patch']) !!}
                    @include('admin.spot__shipment__packagings.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /.content -->
@endsection
