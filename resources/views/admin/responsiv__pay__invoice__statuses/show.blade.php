@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Responsiv  Pay  Invoice  Statuses
        </h1>--}}

        {{--@include('admin.responsiv__pay__invoice__statuses.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Responsiv  Pay  Invoice  Statuses</h4>

        <div data-label="Show" class="df-example demo-forms responsiv__pay__invoice__statuses-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.responsiv__pay__invoice__statuses.show_fields')
                        <a href="{!! route('admin.responsivPayInvoiceStatuses.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
