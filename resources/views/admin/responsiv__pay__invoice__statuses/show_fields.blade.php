<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $responsivPayInvoiceStatuses->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $responsivPayInvoiceStatuses->name !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $responsivPayInvoiceStatuses->code !!}</p>
</div>

<!-- Is Enabled Field -->
<div class="form-group">
    {!! Form::label('is_enabled', 'Is Enabled:') !!}
    <p>{!! $responsivPayInvoiceStatuses->is_enabled !!}</p>
</div>

<!-- Notify User Field -->
<div class="form-group">
    {!! Form::label('notify_user', 'Notify User:') !!}
    <p>{!! $responsivPayInvoiceStatuses->notify_user !!}</p>
</div>

<!-- Notify Template Field -->
<div class="form-group">
    {!! Form::label('notify_template', 'Notify Template:') !!}
    <p>{!! $responsivPayInvoiceStatuses->notify_template !!}</p>
</div>

