@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            User  Groups
        </h1>--}}

        {{--@include('admin.user__groups.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>User  Groups</h4>

        <div data-label="Show" class="df-example demo-forms user__groups-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.user__groups.show_fields')
                        <a href="{!! route('admin.userGroups.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
