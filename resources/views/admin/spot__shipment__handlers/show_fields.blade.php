<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotShipmentHandler->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $spotShipmentHandler->name !!}</p>
</div>

<!-- Handler Desc Field -->
<div class="form-group">
    {!! Form::label('handler_desc', 'Handler Desc:') !!}
    <p>{!! $spotShipmentHandler->handler_desc !!}</p>
</div>

