<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotContainerReason->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $spotContainerReason->name !!}</p>
</div>

<!-- Reason Desc Field -->
<div class="form-group">
    {!! Form::label('reason_desc', 'Reason Desc:') !!}
    <p>{!! $spotContainerReason->reason_desc !!}</p>
</div>

