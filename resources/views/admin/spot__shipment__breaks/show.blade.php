@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Spot  Shipment  Break
        </h1>--}}

        {{--@include('admin.spot__shipment__breaks.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Spot  Shipment  Break</h4>

        <div data-label="Show" class="df-example demo-forms spot__shipment__breaks-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__shipment__breaks.show_fields')
                        <a href="{!! route('admin.spotShipmentBreaks.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
