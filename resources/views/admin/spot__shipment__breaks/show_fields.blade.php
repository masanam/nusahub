<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotShipmentBreak->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $spotShipmentBreak->name !!}</p>
</div>

<!-- Break Desc Field -->
<div class="form-group">
    {!! Form::label('break_desc', 'Break Desc:') !!}
    <p>{!! $spotShipmentBreak->break_desc !!}</p>
</div>

