@extends('layouts.app')

@section('contents')
    {{-- <div class="content content-components"> --}}
    <div class="content">
        <div class="container">
            @include('dashforge-templates::common.errors')

            <h4 id="section1" class="mg-b-10">Rainlab  Location  States</h4>

            <p class="mg-b-30">Please, fill all required fields before click save button.</p>

            <div data-label="Edit" class="df-example demo-forms rainlab__location__states-forms">
                {!! Form::model($rainlabLocationStates, ['route' => ['admin.rainlabLocationStates.update', $rainlabLocationStates->id], 'method' => 'patch']) !!}
                    @include('admin.rainlab__location__states.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /.content -->
@endsection
