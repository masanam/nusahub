@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Rainlab  Location  States
        </h1>--}}

        {{--@include('admin.rainlab__location__states.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Rainlab  Location  States</h4>

        <div data-label="Show" class="df-example demo-forms rainlab__location__states-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.rainlab__location__states.show_fields')
                        <a href="{!! route('admin.rainlabLocationStates.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
