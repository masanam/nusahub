<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $rainlabLocationCountries->id !!}</p>
</div>

<!-- Is Enabled Field -->
<div class="form-group">
    {!! Form::label('is_enabled', 'Is Enabled:') !!}
    <p>{!! $rainlabLocationCountries->is_enabled !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $rainlabLocationCountries->name !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $rainlabLocationCountries->code !!}</p>
</div>

<!-- Is Pinned Field -->
<div class="form-group">
    {!! Form::label('is_pinned', 'Is Pinned:') !!}
    <p>{!! $rainlabLocationCountries->is_pinned !!}</p>
</div>

