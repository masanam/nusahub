@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Rainlab  Location  Countries
        </h1>--}}

        {{--@include('admin.rainlab__location__countries.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Rainlab  Location  Countries</h4>

        <div data-label="Show" class="df-example demo-forms rainlab__location__countries-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.rainlab__location__countries.show_fields')
                        <a href="{!! route('admin.rainlabLocationCountries.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
