<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotContainerLocation->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $spotContainerLocation->name !!}</p>
</div>

<!-- Location Desc Field -->
<div class="form-group">
    {!! Form::label('location_desc', 'Location Desc:') !!}
    <p>{!! $spotContainerLocation->location_desc !!}</p>
</div>

