@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Spot  Container  Location
        </h1>--}}

        {{--@include('admin.spot__container__locations.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Spot  Container  Location</h4>

        <div data-label="Show" class="df-example demo-forms spot__container__locations-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__container__locations.show_fields')
                        <a href="{!! route('admin.spotContainerLocations.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
