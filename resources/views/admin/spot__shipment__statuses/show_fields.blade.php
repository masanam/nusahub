<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotShipmentStatus->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $spotShipmentStatus->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $spotShipmentStatus->description !!}</p>
</div>

<!-- Color Field -->
<div class="form-group">
    {!! Form::label('color', 'Color:') !!}
    <p>{!! $spotShipmentStatus->color !!}</p>
</div>

<!-- Equal Field -->
<div class="form-group">
    {!! Form::label('equal', 'Equal:') !!}
    <p>{!! $spotShipmentStatus->equal !!}</p>
</div>

