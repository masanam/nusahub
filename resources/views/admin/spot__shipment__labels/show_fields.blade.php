<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotShipmentLabel->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $spotShipmentLabel->name !!}</p>
</div>

<!-- Label Desc Field -->
<div class="form-group">
    {!! Form::label('label_desc', 'Label Desc:') !!}
    <p>{!! $spotShipmentLabel->label_desc !!}</p>
</div>

