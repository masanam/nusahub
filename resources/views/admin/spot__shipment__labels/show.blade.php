@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Spot  Shipment  Label
        </h1>--}}

        {{--@include('admin.spot__shipment__labels.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Spot  Shipment  Label</h4>

        <div data-label="Show" class="df-example demo-forms spot__shipment__labels-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__shipment__labels.show_fields')
                        <a href="{!! route('admin.spotShipmentLabels.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
