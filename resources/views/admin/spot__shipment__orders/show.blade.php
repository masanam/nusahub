@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Shipment  Order
        </h1>--}}

        {{--@include('admin.spot__shipment__orders.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Shipment  Order</h4>

        <div data-label="Show" class="df-example demo-forms spot__shipment__orders-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__shipment__orders.show_fields')
                        <a href="{!! route('admin.spotShipmentOrders.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
