<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotShipmentOrder->id !!}</p>
</div>

<!-- Office Id Field -->
<div class="form-group">
    {!! Form::label('office_id', 'Office Id:') !!}
    <p>{!! $spotShipmentOrder->office_id !!}</p>
</div>

<!-- Number Field -->
<div class="form-group">
    {!! Form::label('number', 'Number:') !!}
    <p>{!! $spotShipmentOrder->number !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $spotShipmentOrder->type !!}</p>
</div>

<!-- Sender Id Field -->
<div class="form-group">
    {!! Form::label('sender_id', 'Sender Id:') !!}
    <p>{!! $spotShipmentOrder->sender_id !!}</p>
</div>

<!-- Sender Address Id Field -->
<div class="form-group">
    {!! Form::label('sender_address_id', 'Sender Address Id:') !!}
    <p>{!! $spotShipmentOrder->sender_address_id !!}</p>
</div>

<!-- Receiver Id Field -->
<div class="form-group">
    {!! Form::label('receiver_id', 'Receiver Id:') !!}
    <p>{!! $spotShipmentOrder->receiver_id !!}</p>
</div>

<!-- Receiver Address Id Field -->
<div class="form-group">
    {!! Form::label('receiver_address_id', 'Receiver Address Id:') !!}
    <p>{!! $spotShipmentOrder->receiver_address_id !!}</p>
</div>

<!-- Sender Name Field -->
<div class="form-group">
    {!! Form::label('sender_name', 'Sender Name:') !!}
    <p>{!! $spotShipmentOrder->sender_name !!}</p>
</div>

<!-- Sender Mobile Field -->
<div class="form-group">
    {!! Form::label('sender_mobile', 'Sender Mobile:') !!}
    <p>{!! $spotShipmentOrder->sender_mobile !!}</p>
</div>

<!-- Sender City Field -->
<div class="form-group">
    {!! Form::label('sender_city', 'Sender City:') !!}
    <p>{!! $spotShipmentOrder->sender_city !!}</p>
</div>

<!-- Sender Sector Field -->
<div class="form-group">
    {!! Form::label('sender_sector', 'Sender Sector:') !!}
    <p>{!! $spotShipmentOrder->sender_sector !!}</p>
</div>

<!-- Sender Addr Field -->
<div class="form-group">
    {!! Form::label('sender_addr', 'Sender Addr:') !!}
    <p>{!! $spotShipmentOrder->sender_addr !!}</p>
</div>

<!-- Receiver Name Field -->
<div class="form-group">
    {!! Form::label('receiver_name', 'Receiver Name:') !!}
    <p>{!! $spotShipmentOrder->receiver_name !!}</p>
</div>

<!-- Receiver Mobile Field -->
<div class="form-group">
    {!! Form::label('receiver_mobile', 'Receiver Mobile:') !!}
    <p>{!! $spotShipmentOrder->receiver_mobile !!}</p>
</div>

<!-- Receiver City Field -->
<div class="form-group">
    {!! Form::label('receiver_city', 'Receiver City:') !!}
    <p>{!! $spotShipmentOrder->receiver_city !!}</p>
</div>

<!-- Receiver Sector Field -->
<div class="form-group">
    {!! Form::label('receiver_sector', 'Receiver Sector:') !!}
    <p>{!! $spotShipmentOrder->receiver_sector !!}</p>
</div>

<!-- Receiver Addr Field -->
<div class="form-group">
    {!! Form::label('receiver_addr', 'Receiver Addr:') !!}
    <p>{!! $spotShipmentOrder->receiver_addr !!}</p>
</div>

<!-- Packaging Id Field -->
<div class="form-group">
    {!! Form::label('packaging_id', 'Packaging Id:') !!}
    <p>{!! $spotShipmentOrder->packaging_id !!}</p>
</div>

<!-- Ship Date Field -->
<div class="form-group">
    {!! Form::label('ship_date', 'Ship Date:') !!}
    <p>{!! $spotShipmentOrder->ship_date !!}</p>
</div>

<!-- Receive Date Field -->
<div class="form-group">
    {!! Form::label('receive_date', 'Receive Date:') !!}
    <p>{!! $spotShipmentOrder->receive_date !!}</p>
</div>

<!-- Courier Id Field -->
<div class="form-group">
    {!! Form::label('courier_id', 'Courier Id:') !!}
    <p>{!! $spotShipmentOrder->courier_id !!}</p>
</div>

<!-- Delivery Time Id Field -->
<div class="form-group">
    {!! Form::label('delivery_time_id', 'Delivery Time Id:') !!}
    <p>{!! $spotShipmentOrder->delivery_time_id !!}</p>
</div>

<!-- Delivery Date Field -->
<div class="form-group">
    {!! Form::label('delivery_date', 'Delivery Date:') !!}
    <p>{!! $spotShipmentOrder->delivery_date !!}</p>
</div>

<!-- Mode Id Field -->
<div class="form-group">
    {!! Form::label('mode_id', 'Mode Id:') !!}
    <p>{!! $spotShipmentOrder->mode_id !!}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{!! $spotShipmentOrder->status_id !!}</p>
</div>

<!-- Tax Field -->
<div class="form-group">
    {!! Form::label('tax', 'Tax:') !!}
    <p>{!! $spotShipmentOrder->tax !!}</p>
</div>

<!-- Insurance Field -->
<div class="form-group">
    {!! Form::label('insurance', 'Insurance:') !!}
    <p>{!! $spotShipmentOrder->insurance !!}</p>
</div>

<!-- Currency Id Field -->
<div class="form-group">
    {!! Form::label('currency_id', 'Currency Id:') !!}
    <p>{!! $spotShipmentOrder->currency_id !!}</p>
</div>

<!-- Payment Type Field -->
<div class="form-group">
    {!! Form::label('payment_type', 'Payment Type:') !!}
    <p>{!! $spotShipmentOrder->payment_type !!}</p>
</div>

<!-- Customs Value Field -->
<div class="form-group">
    {!! Form::label('customs_value', 'Customs Value:') !!}
    <p>{!! $spotShipmentOrder->customs_value !!}</p>
</div>

<!-- Courier Fee Field -->
<div class="form-group">
    {!! Form::label('courier_fee', 'Courier Fee:') !!}
    <p>{!! $spotShipmentOrder->courier_fee !!}</p>
</div>

<!-- Package Fee Field -->
<div class="form-group">
    {!! Form::label('package_fee', 'Package Fee:') !!}
    <p>{!! $spotShipmentOrder->package_fee !!}</p>
</div>

<!-- Return Package Fee Field -->
<div class="form-group">
    {!! Form::label('return_package_fee', 'Return Package Fee:') !!}
    <p>{!! $spotShipmentOrder->return_package_fee !!}</p>
</div>

<!-- Return Courier Fee Field -->
<div class="form-group">
    {!! Form::label('return_courier_fee', 'Return Courier Fee:') !!}
    <p>{!! $spotShipmentOrder->return_courier_fee !!}</p>
</div>

<!-- Return Defray Field -->
<div class="form-group">
    {!! Form::label('return_defray', 'Return Defray:') !!}
    <p>{!! $spotShipmentOrder->return_defray !!}</p>
</div>

<!-- Manifest Id Field -->
<div class="form-group">
    {!! Form::label('manifest_id', 'Manifest Id:') !!}
    <p>{!! $spotShipmentOrder->manifest_id !!}</p>
</div>

<!-- Channel Field -->
<div class="form-group">
    {!! Form::label('channel', 'Channel:') !!}
    <p>{!! $spotShipmentOrder->channel !!}</p>
</div>

<!-- Assigned Id Field -->
<div class="form-group">
    {!! Form::label('assigned_id', 'Assigned Id:') !!}
    <p>{!! $spotShipmentOrder->assigned_id !!}</p>
</div>

<!-- Requested Field -->
<div class="form-group">
    {!! Form::label('requested', 'Requested:') !!}
    <p>{!! $spotShipmentOrder->requested !!}</p>
</div>

<!-- Barcode Field -->
<div class="form-group">
    {!! Form::label('barcode', 'Barcode:') !!}
    <p>{!! $spotShipmentOrder->barcode !!}</p>
</div>

<!-- Esign Field -->
<div class="form-group">
    {!! Form::label('esign', 'Esign:') !!}
    <p>{!! $spotShipmentOrder->esign !!}</p>
</div>

<!-- Postponed Field -->
<div class="form-group">
    {!! Form::label('postponed', 'Postponed:') !!}
    <p>{!! $spotShipmentOrder->postponed !!}</p>
</div>

<!-- Delivered By Field -->
<div class="form-group">
    {!! Form::label('delivered_by', 'Delivered By:') !!}
    <p>{!! $spotShipmentOrder->delivered_by !!}</p>
</div>

<!-- Delivered Responsiable Field -->
<div class="form-group">
    {!! Form::label('delivered_responsiable', 'Delivered Responsiable:') !!}
    <p>{!! $spotShipmentOrder->delivered_responsiable !!}</p>
</div>

<!-- Received By Field -->
<div class="form-group">
    {!! Form::label('received_by', 'Received By:') !!}
    <p>{!! $spotShipmentOrder->received_by !!}</p>
</div>

<!-- Airwaybill Field -->
<div class="form-group">
    {!! Form::label('airWayBill', 'Airwaybill:') !!}
    <p>{!! $spotShipmentOrder->airWayBill !!}</p>
</div>

<!-- Location Field -->
<div class="form-group">
    {!! Form::label('location', 'Location:') !!}
    <p>{!! $spotShipmentOrder->location !!}</p>
</div>

<!-- Cc Field -->
<div class="form-group">
    {!! Form::label('cc', 'Cc:') !!}
    <p>{!! $spotShipmentOrder->cc !!}</p>
</div>

<!-- Transfer Jost Field -->
<div class="form-group">
    {!! Form::label('transfer_jost', 'Transfer Jost:') !!}
    <p>{!! $spotShipmentOrder->transfer_jost !!}</p>
</div>

<!-- Label Id Field -->
<div class="form-group">
    {!! Form::label('label_id', 'Label Id:') !!}
    <p>{!! $spotShipmentOrder->label_id !!}</p>
</div>

<!-- Handler Id Field -->
<div class="form-group">
    {!! Form::label('handler_id', 'Handler Id:') !!}
    <p>{!! $spotShipmentOrder->handler_id !!}</p>
</div>

<!-- Breakdown Id Field -->
<div class="form-group">
    {!! Form::label('breakdown_id', 'Breakdown Id:') !!}
    <p>{!! $spotShipmentOrder->breakdown_id !!}</p>
</div>

<!-- Prealert Received Field -->
<div class="form-group">
    {!! Form::label('preAlert_received', 'Prealert Received:') !!}
    <p>{!! $spotShipmentOrder->preAlert_received !!}</p>
</div>

<!-- Releasednote Received Field -->
<div class="form-group">
    {!! Form::label('releasedNote_received', 'Releasednote Received:') !!}
    <p>{!! $spotShipmentOrder->releasedNote_received !!}</p>
</div>

<!-- Remarks Field -->
<div class="form-group">
    {!! Form::label('remarks', 'Remarks:') !!}
    <p>{!! $spotShipmentOrder->remarks !!}</p>
</div>

<!-- Released Note Field -->
<div class="form-group">
    {!! Form::label('released_note', 'Released Note:') !!}
    <p>{!! $spotShipmentOrder->released_note !!}</p>
</div>

<!-- Custom Clearance Field -->
<div class="form-group">
    {!! Form::label('custom_clearance', 'Custom Clearance:') !!}
    <p>{!! $spotShipmentOrder->custom_clearance !!}</p>
</div>

<!-- Fiscal Representation Field -->
<div class="form-group">
    {!! Form::label('fiscal_representation', 'Fiscal Representation:') !!}
    <p>{!! $spotShipmentOrder->fiscal_representation !!}</p>
</div>

<!-- Payment Term Field -->
<div class="form-group">
    {!! Form::label('payment_term', 'Payment Term:') !!}
    <p>{!! $spotShipmentOrder->payment_term !!}</p>
</div>

<!-- Price Kg Field -->
<div class="form-group">
    {!! Form::label('price_kg', 'Price Kg:') !!}
    <p>{!! $spotShipmentOrder->price_kg !!}</p>
</div>

<!-- Storage Fee Field -->
<div class="form-group">
    {!! Form::label('storage_fee', 'Storage Fee:') !!}
    <p>{!! $spotShipmentOrder->storage_fee !!}</p>
</div>

<!-- Cost 24 Field -->
<div class="form-group">
    {!! Form::label('cost_24', 'Cost 24:') !!}
    <p>{!! $spotShipmentOrder->cost_24 !!}</p>
</div>

<!-- Cost 48 Field -->
<div class="form-group">
    {!! Form::label('cost_48', 'Cost 48:') !!}
    <p>{!! $spotShipmentOrder->cost_48 !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $spotShipmentOrder->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $spotShipmentOrder->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $spotShipmentOrder->deleted_at !!}</p>
</div>

