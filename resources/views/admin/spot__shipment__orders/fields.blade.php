<!-- Office Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('office_id', 'Office Id:') !!}
    {!! Form::number('office_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number', 'Number:') !!}
    {!! Form::number('number', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::number('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Sender Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sender_id', 'Sender Id:') !!}
    {!! Form::number('sender_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Sender Address Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sender_address_id', 'Sender Address Id:') !!}
    {!! Form::number('sender_address_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Receiver Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receiver_id', 'Receiver Id:') !!}
    {!! Form::number('receiver_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Receiver Address Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receiver_address_id', 'Receiver Address Id:') !!}
    {!! Form::number('receiver_address_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Sender Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sender_name', 'Sender Name:', ['class' => 'd-block']) !!}
    {!! Form::text('sender_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Sender Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sender_mobile', 'Sender Mobile:', ['class' => 'd-block']) !!}
    {!! Form::text('sender_mobile', null, ['class' => 'form-control']) !!}
</div>

<!-- Sender City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sender_city', 'Sender City:') !!}
    {!! Form::number('sender_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Sender Sector Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sender_sector', 'Sender Sector:') !!}
    {!! Form::number('sender_sector', null, ['class' => 'form-control']) !!}
</div>

<!-- Sender Addr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sender_addr', 'Sender Addr:', ['class' => 'd-block']) !!}
    {!! Form::text('sender_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Receiver Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receiver_name', 'Receiver Name:', ['class' => 'd-block']) !!}
    {!! Form::text('receiver_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Receiver Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receiver_mobile', 'Receiver Mobile:', ['class' => 'd-block']) !!}
    {!! Form::text('receiver_mobile', null, ['class' => 'form-control']) !!}
</div>

<!-- Receiver City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receiver_city', 'Receiver City:') !!}
    {!! Form::number('receiver_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Receiver Sector Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receiver_sector', 'Receiver Sector:') !!}
    {!! Form::number('receiver_sector', null, ['class' => 'form-control']) !!}
</div>

<!-- Receiver Addr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receiver_addr', 'Receiver Addr:', ['class' => 'd-block']) !!}
    {!! Form::text('receiver_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Packaging Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('packaging_id', 'Packaging Id:') !!}
    {!! Form::number('packaging_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Ship Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ship_date', 'Ship Date:') !!}
    {!! Form::date('ship_date', null, ['class' => 'form-control date']) !!}
</div>

<!-- Receive Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receive_date', 'Receive Date:') !!}
    {!! Form::date('receive_date', null, ['class' => 'form-control date']) !!}
</div>

<!-- Courier Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('courier_id', 'Courier Id:') !!}
    {!! Form::number('courier_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Time Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_time_id', 'Delivery Time Id:') !!}
    {!! Form::number('delivery_time_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_date', 'Delivery Date:') !!}
    {!! Form::date('delivery_date', null, ['class' => 'form-control date']) !!}
</div>

<!-- Mode Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mode_id', 'Mode Id:') !!}
    {!! Form::number('mode_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status_id', 'Status Id:') !!}
    {!! Form::number('status_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Tax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax', 'Tax:') !!}
    {!! Form::number('tax', null, ['class' => 'form-control']) !!}
</div>

<!-- Insurance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('insurance', 'Insurance:') !!}
    {!! Form::number('insurance', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_id', 'Currency Id:') !!}
    {!! Form::number('currency_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_type', 'Payment Type:') !!}
    {!! Form::number('payment_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Customs Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customs_value', 'Customs Value:') !!}
    {!! Form::number('customs_value', null, ['class' => 'form-control']) !!}
</div>

<!-- Courier Fee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('courier_fee', 'Courier Fee:') !!}
    {!! Form::number('courier_fee', null, ['class' => 'form-control']) !!}
</div>

<!-- Package Fee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('package_fee', 'Package Fee:') !!}
    {!! Form::number('package_fee', null, ['class' => 'form-control']) !!}
</div>

<!-- Return Package Fee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('return_package_fee', 'Return Package Fee:') !!}
    {!! Form::number('return_package_fee', null, ['class' => 'form-control']) !!}
</div>

<!-- Return Courier Fee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('return_courier_fee', 'Return Courier Fee:') !!}
    {!! Form::number('return_courier_fee', null, ['class' => 'form-control']) !!}
</div>

<!-- Return Defray Field -->
<div class="form-group col-sm-6">
    {!! Form::label('return_defray', 'Return Defray:') !!}
    {!! Form::number('return_defray', null, ['class' => 'form-control']) !!}
</div>

<!-- Manifest Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('manifest_id', 'Manifest Id:') !!}
    {!! Form::number('manifest_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Channel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('channel', 'Channel:', ['class' => 'd-block']) !!}
    {!! Form::text('channel', null, ['class' => 'form-control']) !!}
</div>

<!-- Assigned Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('assigned_id', 'Assigned Id:') !!}
    {!! Form::number('assigned_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Requested Field -->
<div class="form-group col-sm-6">
    {!! Form::label('requested', 'Requested:') !!}
    {!! Form::number('requested', null, ['class' => 'form-control']) !!}
</div>

<!-- Barcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('barcode', 'Barcode:', ['class' => 'd-block']) !!}
    {!! Form::text('barcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Esign Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('esign', 'Esign:', ['class' => 'd-block']) !!}
    {!! Form::textarea('esign', null, ['class' => 'form-control']) !!}
</div>

<div class="custom-control custom-checkbox">
  {!! Form::checkbox('postponed', '1', null, ['class' => 'custom-control-input', 'id' => 'postponed']) !!}
  <label class="custom-control-label" for="postponed">{!! 1 !!}</label>
</div>

<!-- Delivered By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivered_by', 'Delivered By:') !!}
    {!! Form::number('delivered_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivered Responsiable Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivered_responsiable', 'Delivered Responsiable:') !!}
    {!! Form::number('delivered_responsiable', null, ['class' => 'form-control']) !!}
</div>

<!-- Received By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('received_by', 'Received By:', ['class' => 'd-block']) !!}
    {!! Form::text('received_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Airwaybill Field -->
<div class="form-group col-sm-6">
    {!! Form::label('airWayBill', 'Airwaybill:', ['class' => 'd-block']) !!}
    {!! Form::text('airWayBill', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location', 'Location:', ['class' => 'd-block']) !!}
    {!! Form::text('location', null, ['class' => 'form-control']) !!}
</div>

<div class="custom-control custom-checkbox">
  {!! Form::checkbox('cc', '1', null, ['class' => 'custom-control-input', 'id' => 'cc']) !!}
  <label class="custom-control-label" for="cc">{!! 1 !!}</label>
</div>

<div class="custom-control custom-checkbox">
  {!! Form::checkbox('transfer_jost', '1', null, ['class' => 'custom-control-input', 'id' => 'transfer_jost']) !!}
  <label class="custom-control-label" for="transfer_jost">{!! 1 !!}</label>
</div>

<!-- Label Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('label_id', 'Label Id:') !!}
    {!! Form::number('label_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Handler Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('handler_id', 'Handler Id:') !!}
    {!! Form::number('handler_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Breakdown Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('breakdown_id', 'Breakdown Id:') !!}
    {!! Form::number('breakdown_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Prealert Received Field -->
<div class="form-group col-sm-6">
    {!! Form::label('preAlert_received', 'Prealert Received:') !!}
    {!! Form::date('preAlert_received', null, ['class' => 'form-control date']) !!}
</div>

<!-- Releasednote Received Field -->
<div class="form-group col-sm-6">
    {!! Form::label('releasedNote_received', 'Releasednote Received:') !!}
    {!! Form::date('releasedNote_received', null, ['class' => 'form-control date']) !!}
</div>

<!-- Remarks Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('remarks', 'Remarks:', ['class' => 'd-block']) !!}
    {!! Form::textarea('remarks', null, ['class' => 'form-control']) !!}
</div>

<!-- Released Note Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('released_note', 'Released Note:', ['class' => 'd-block']) !!}
    {!! Form::textarea('released_note', null, ['class' => 'form-control']) !!}
</div>

<!-- Custom Clearance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('custom_clearance', 'Custom Clearance:') !!}
    {!! Form::number('custom_clearance', null, ['class' => 'form-control']) !!}
</div>

<!-- Fiscal Representation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fiscal_representation', 'Fiscal Representation:') !!}
    {!! Form::number('fiscal_representation', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Term Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_term', 'Payment Term:') !!}
    {!! Form::number('payment_term', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Kg Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price_kg', 'Price Kg:') !!}
    {!! Form::number('price_kg', null, ['class' => 'form-control']) !!}
</div>

<div class="custom-control custom-checkbox">
  {!! Form::checkbox('storage_fee', '1', null, ['class' => 'custom-control-input', 'id' => 'storage_fee']) !!}
  <label class="custom-control-label" for="storage_fee">{!! 1 !!}</label>
</div>

<!-- Cost 24 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost_24', 'Cost 24:') !!}
    {!! Form::number('cost_24', null, ['class' => 'form-control']) !!}
</div>

<!-- Cost 48 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost_48', 'Cost 48:') !!}
    {!! Form::number('cost_48', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.spotShipmentOrders.index') !!}" class="btn btn-light">Cancel</a>
</div>

@section('scripts')
<!-- Relational Form table -->
<script>
    $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;

        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             {!! Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    {!! Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->
@endsection
