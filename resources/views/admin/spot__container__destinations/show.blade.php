@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Spot  Container  Destination
        </h1>--}}

        {{--@include('admin.spot__container__destinations.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Spot  Container  Destination</h4>

        <div data-label="Show" class="df-example demo-forms spot__container__destinations-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__container__destinations.show_fields')
                        <a href="{!! route('admin.spotContainerDestinations.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
