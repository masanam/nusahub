@extends('layouts.app')

@section('contents')
    {{-- <div class="content content-components"> --}}
    <div class="content">
        <div class="container">
            @include('dashforge-templates::common.errors')

            <h4 id="section1" class="mg-b-10">Backend  User  Groups</h4>

            <p class="mg-b-30">Please, fill all required fields before click save button.</p>

            <div data-label="Edit" class="df-example demo-forms backend__user__groups-forms">
                {!! Form::model($backendUserGroups, ['route' => ['admin.backendUserGroups.update', $backendUserGroups->id], 'method' => 'patch']) !!}
                    @include('admin.backend__user__groups.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /.content -->
@endsection
