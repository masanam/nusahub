<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotContainerOrder->id !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $spotContainerOrder->customer_id !!}</p>
</div>

<!-- Customer Address Id Field -->
<div class="form-group">
    {!! Form::label('customer_address_id', 'Customer Address Id:') !!}
    <p>{!! $spotContainerOrder->customer_address_id !!}</p>
</div>

<!-- Number Field -->
<div class="form-group">
    {!! Form::label('number', 'Number:') !!}
    <p>{!! $spotContainerOrder->number !!}</p>
</div>

<!-- Bol Field -->
<div class="form-group">
    {!! Form::label('bol', 'Bol:') !!}
    <p>{!! $spotContainerOrder->bol !!}</p>
</div>

<!-- Location Type Field -->
<div class="form-group">
    {!! Form::label('location_type', 'Location Type:') !!}
    <p>{!! $spotContainerOrder->location_type !!}</p>
</div>

<!-- Company Id Field -->
<div class="form-group">
    {!! Form::label('company_id', 'Company Id:') !!}
    <p>{!! $spotContainerOrder->company_id !!}</p>
</div>

<!-- Destination Field -->
<div class="form-group">
    {!! Form::label('destination', 'Destination:') !!}
    <p>{!! $spotContainerOrder->destination !!}</p>
</div>

<!-- Reason For Arrive Field -->
<div class="form-group">
    {!! Form::label('reason_for_arrive', 'Reason For Arrive:') !!}
    <p>{!! $spotContainerOrder->reason_for_arrive !!}</p>
</div>

<!-- T1 Field -->
<div class="form-group">
    {!! Form::label('t1', 'T1:') !!}
    <p>{!! $spotContainerOrder->t1 !!}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{!! $spotContainerOrder->status_id !!}</p>
</div>

<!-- Cc Field -->
<div class="form-group">
    {!! Form::label('cc', 'Cc:') !!}
    <p>{!! $spotContainerOrder->cc !!}</p>
</div>

<!-- Eta Port Field -->
<div class="form-group">
    {!! Form::label('eta_port', 'Eta Port:') !!}
    <p>{!! $spotContainerOrder->eta_port !!}</p>
</div>

<!-- Eta Lgg Field -->
<div class="form-group">
    {!! Form::label('eta_lgg', 'Eta Lgg:') !!}
    <p>{!! $spotContainerOrder->eta_lgg !!}</p>
</div>

<!-- Loading Date Field -->
<div class="form-group">
    {!! Form::label('loading_date', 'Loading Date:') !!}
    <p>{!! $spotContainerOrder->loading_date !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $spotContainerOrder->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $spotContainerOrder->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $spotContainerOrder->deleted_at !!}</p>
</div>

<!-- Inspected Port Field -->
<div class="form-group">
    {!! Form::label('inspected_port', 'Inspected Port:') !!}
    <p>{!! $spotContainerOrder->inspected_port !!}</p>
</div>

<!-- Dp Field -->
<div class="form-group">
    {!! Form::label('dp', 'Dp:') !!}
    <p>{!! $spotContainerOrder->dp !!}</p>
</div>

<!-- Requested Field -->
<div class="form-group">
    {!! Form::label('requested', 'Requested:') !!}
    <p>{!! $spotContainerOrder->requested !!}</p>
</div>

<!-- Barcode Field -->
<div class="form-group">
    {!! Form::label('barcode', 'Barcode:') !!}
    <p>{!! $spotContainerOrder->barcode !!}</p>
</div>

<!-- Custom Clearance Field -->
<div class="form-group">
    {!! Form::label('custom_clearance', 'Custom Clearance:') !!}
    <p>{!! $spotContainerOrder->custom_clearance !!}</p>
</div>

<!-- Fiscal Representation Field -->
<div class="form-group">
    {!! Form::label('fiscal_representation', 'Fiscal Representation:') !!}
    <p>{!! $spotContainerOrder->fiscal_representation !!}</p>
</div>

<!-- Payment Term Field -->
<div class="form-group">
    {!! Form::label('payment_term', 'Payment Term:') !!}
    <p>{!! $spotContainerOrder->payment_term !!}</p>
</div>

<!-- Price Kg Field -->
<div class="form-group">
    {!! Form::label('price_kg', 'Price Kg:') !!}
    <p>{!! $spotContainerOrder->price_kg !!}</p>
</div>

<!-- Storage Fee Field -->
<div class="form-group">
    {!! Form::label('storage_fee', 'Storage Fee:') !!}
    <p>{!! $spotContainerOrder->storage_fee !!}</p>
</div>

<!-- Cost 24 Field -->
<div class="form-group">
    {!! Form::label('cost_24', 'Cost 24:') !!}
    <p>{!! $spotContainerOrder->cost_24 !!}</p>
</div>

<!-- Cost 48 Field -->
<div class="form-group">
    {!! Form::label('cost_48', 'Cost 48:') !!}
    <p>{!! $spotContainerOrder->cost_48 !!}</p>
</div>

