<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Address Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_address_id', 'Customer Address Id:') !!}
    {!! Form::number('customer_address_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number', 'Number:', ['class' => 'd-block']) !!}
    {!! Form::text('number', null, ['class' => 'form-control']) !!}
</div>

<!-- Bol Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bol', 'Bol:', ['class' => 'd-block']) !!}
    {!! Form::text('bol', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_type', 'Location Type:') !!}
    {!! Form::number('location_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Company Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company_id', 'Company Id:') !!}
    {!! Form::number('company_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Destination Field -->
<div class="form-group col-sm-6">
    {!! Form::label('destination', 'Destination:') !!}
    {!! Form::number('destination', null, ['class' => 'form-control']) !!}
</div>

<!-- Reason For Arrive Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reason_for_arrive', 'Reason For Arrive:') !!}
    {!! Form::number('reason_for_arrive', null, ['class' => 'form-control']) !!}
</div>

<!-- T1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('t1', 'T1:', ['class' => 'd-block']) !!}
    {!! Form::text('t1', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status_id', 'Status Id:') !!}
    {!! Form::number('status_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Cc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cc', 'Cc:') !!}
    {!! Form::number('cc', null, ['class' => 'form-control']) !!}
</div>

<!-- Eta Port Field -->
<div class="form-group col-sm-6">
    {!! Form::label('eta_port', 'Eta Port:') !!}
    {!! Form::date('eta_port', null, ['class' => 'form-control date']) !!}
</div>

<!-- Eta Lgg Field -->
<div class="form-group col-sm-6">
    {!! Form::label('eta_lgg', 'Eta Lgg:') !!}
    {!! Form::date('eta_lgg', null, ['class' => 'form-control date']) !!}
</div>

<!-- Loading Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('loading_date', 'Loading Date:') !!}
    {!! Form::date('loading_date', null, ['class' => 'form-control date']) !!}
</div>

<!-- Inspected Port Field -->
<div class="form-group col-sm-6">
    {!! Form::label('inspected_port', 'Inspected Port:') !!}
    {!! Form::number('inspected_port', null, ['class' => 'form-control']) !!}
</div>

<!-- Dp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dp', 'Dp:', ['class' => 'd-block']) !!}
    {!! Form::text('dp', null, ['class' => 'form-control']) !!}
</div>

<!-- Requested Field -->
<div class="form-group col-sm-6">
    {!! Form::label('requested', 'Requested:') !!}
    {!! Form::number('requested', null, ['class' => 'form-control']) !!}
</div>

<!-- Barcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('barcode', 'Barcode:') !!}
    {!! Form::number('barcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Custom Clearance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('custom_clearance', 'Custom Clearance:') !!}
    {!! Form::number('custom_clearance', null, ['class' => 'form-control']) !!}
</div>

<!-- Fiscal Representation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fiscal_representation', 'Fiscal Representation:') !!}
    {!! Form::number('fiscal_representation', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Term Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_term', 'Payment Term:') !!}
    {!! Form::number('payment_term', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Kg Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price_kg', 'Price Kg:') !!}
    {!! Form::number('price_kg', null, ['class' => 'form-control']) !!}
</div>

<div class="custom-control custom-checkbox">
  {!! Form::checkbox('storage_fee', '1', null, ['class' => 'custom-control-input', 'id' => 'storage_fee']) !!}
  <label class="custom-control-label" for="storage_fee">{!! 1 !!}</label>
</div>

<!-- Cost 24 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost_24', 'Cost 24:') !!}
    {!! Form::number('cost_24', null, ['class' => 'form-control']) !!}
</div>

<!-- Cost 48 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost_48', 'Cost 48:') !!}
    {!! Form::number('cost_48', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.spotContainerOrders.index') !!}" class="btn btn-light">Cancel</a>
</div>

@section('scripts')
<!-- Relational Form table -->
<script>
    $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;

        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             {!! Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    {!! Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->
@endsection
