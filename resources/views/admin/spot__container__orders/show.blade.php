@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Container  Order
        </h1>--}}

        {{--@include('admin.spot__container__orders.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Container  Order</h4>

        <div data-label="Show" class="df-example demo-forms spot__container__orders-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__container__orders.show_fields')
                        <a href="{!! route('admin.spotContainerOrders.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
