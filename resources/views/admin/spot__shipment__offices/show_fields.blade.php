<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotShipmentOffice->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $spotShipmentOffice->name !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $spotShipmentOffice->code !!}</p>
</div>

<!-- Street Field -->
<div class="form-group">
    {!! Form::label('street', 'Street:') !!}
    <p>{!! $spotShipmentOffice->street !!}</p>
</div>

<!-- County Field -->
<div class="form-group">
    {!! Form::label('county', 'County:') !!}
    <p>{!! $spotShipmentOffice->county !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $spotShipmentOffice->city !!}</p>
</div>

<!-- State Field -->
<div class="form-group">
    {!! Form::label('state', 'State:') !!}
    <p>{!! $spotShipmentOffice->state !!}</p>
</div>

<!-- Zipcode Field -->
<div class="form-group">
    {!! Form::label('zipcode', 'Zipcode:') !!}
    <p>{!! $spotShipmentOffice->zipcode !!}</p>
</div>

<!-- Country Field -->
<div class="form-group">
    {!! Form::label('country', 'Country:') !!}
    <p>{!! $spotShipmentOffice->country !!}</p>
</div>

<!-- Lat Field -->
<div class="form-group">
    {!! Form::label('lat', 'Lat:') !!}
    <p>{!! $spotShipmentOffice->lat !!}</p>
</div>

<!-- Lng Field -->
<div class="form-group">
    {!! Form::label('lng', 'Lng:') !!}
    <p>{!! $spotShipmentOffice->lng !!}</p>
</div>

<!-- Url Field -->
<div class="form-group">
    {!! Form::label('url', 'Url:') !!}
    <p>{!! $spotShipmentOffice->url !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $spotShipmentOffice->phone !!}</p>
</div>

<!-- Mobile Field -->
<div class="form-group">
    {!! Form::label('mobile', 'Mobile:') !!}
    <p>{!! $spotShipmentOffice->mobile !!}</p>
</div>

<!-- Mobile 2 Field -->
<div class="form-group">
    {!! Form::label('mobile_2', 'Mobile 2:') !!}
    <p>{!! $spotShipmentOffice->mobile_2 !!}</p>
</div>

<!-- Mobile 3 Field -->
<div class="form-group">
    {!! Form::label('mobile_3', 'Mobile 3:') !!}
    <p>{!! $spotShipmentOffice->mobile_3 !!}</p>
</div>

<!-- Manger Id Field -->
<div class="form-group">
    {!! Form::label('manger_id', 'Manger Id:') !!}
    <p>{!! $spotShipmentOffice->manger_id !!}</p>
</div>

<!-- Responsible Id Field -->
<div class="form-group">
    {!! Form::label('responsible_id', 'Responsible Id:') !!}
    <p>{!! $spotShipmentOffice->responsible_id !!}</p>
</div>

