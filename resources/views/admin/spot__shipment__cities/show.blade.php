@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Spot  Shipment  City
        </h1>--}}

        {{--@include('admin.spot__shipment__cities.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Spot  Shipment  City</h4>

        <div data-label="Show" class="df-example demo-forms spot__shipment__cities-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__shipment__cities.show_fields')
                        <a href="{!! route('admin.spotShipmentCities.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
