@extends('layouts.app')

@section('contents')
    {{-- <div class="content content-components"> --}}
    <div class="content">
        <div class="container">
            @include('dashforge-templates::common.errors')

            <h4 id="section1" class="mg-b-10">Spot  Container  Status</h4>

            <p class="mg-b-30">Please, fill all required fields before click save button.</p>

            <div data-label="Edit" class="df-example demo-forms spot__container__statuses-forms">
                {!! Form::model($spotContainerStatus, ['route' => ['admin.spotContainerStatuses.update', $spotContainerStatus->id], 'method' => 'patch']) !!}
                    @include('admin.spot__container__statuses.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /.content -->
@endsection
