<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotContainerStatus->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $spotContainerStatus->name !!}</p>
</div>

<!-- Color Field -->
<div class="form-group">
    {!! Form::label('color', 'Color:') !!}
    <p>{!! $spotContainerStatus->color !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $spotContainerStatus->description !!}</p>
</div>

<!-- Equal Field -->
<div class="form-group">
    {!! Form::label('equal', 'Equal:') !!}
    <p>{!! $spotContainerStatus->equal !!}</p>
</div>

