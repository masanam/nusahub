@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Spot  Container  Status
        </h1>--}}

        {{--@include('admin.spot__container__statuses.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Spot  Container  Status</h4>

        <div data-label="Show" class="df-example demo-forms spot__container__statuses-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__container__statuses.show_fields')
                        <a href="{!! route('admin.spotContainerStatuses.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
