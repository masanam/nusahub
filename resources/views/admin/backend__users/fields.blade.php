<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:', ['class' => 'd-block']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:', ['class' => 'd-block']) !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Login Field -->
<div class="form-group col-sm-6">
    {!! Form::label('login', 'Login:', ['class' => 'd-block']) !!}
    {!! Form::text('login', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:', ['class' => 'd-block']) !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Activation Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('activation_code', 'Activation Code:', ['class' => 'd-block']) !!}
    {!! Form::text('activation_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Persist Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('persist_code', 'Persist Code:', ['class' => 'd-block']) !!}
    {!! Form::text('persist_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Reset Password Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reset_password_code', 'Reset Password Code:', ['class' => 'd-block']) !!}
    {!! Form::text('reset_password_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Permissions Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('permissions', 'Permissions:', ['class' => 'd-block']) !!}
    {!! Form::textarea('permissions', null, ['class' => 'form-control']) !!}
</div>

<div class="custom-control custom-checkbox">
  {!! Form::checkbox('is_activated', '1', null, ['class' => 'custom-control-input', 'id' => 'is_activated']) !!}
  <label class="custom-control-label" for="is_activated">{!! 1 !!}</label>
</div>

<!-- Role Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('role_id', 'Role Id:') !!}
    {!! Form::number('role_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Activated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('activated_at', 'Activated At:') !!}
    {!! Form::date('activated_at', null, ['class' => 'form-control date']) !!}
</div>

<!-- Last Login Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_login', 'Last Login:') !!}
    {!! Form::date('last_login', null, ['class' => 'form-control date']) !!}
</div>

<div class="custom-control custom-checkbox">
  {!! Form::checkbox('is_superuser', '1', null, ['class' => 'custom-control-input', 'id' => 'is_superuser']) !!}
  <label class="custom-control-label" for="is_superuser">{!! 1 !!}</label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.backendUsers.index') !!}" class="btn btn-light">Cancel</a>
</div>

@section('scripts')
<!-- Relational Form table -->
<script>
    $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;

        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             {!! Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    {!! Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->
@endsection
