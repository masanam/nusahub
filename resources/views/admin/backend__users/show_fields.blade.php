<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $backendUsers->id !!}</p>
</div>

<!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{!! $backendUsers->first_name !!}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    <p>{!! $backendUsers->last_name !!}</p>
</div>

<!-- Login Field -->
<div class="form-group">
    {!! Form::label('login', 'Login:') !!}
    <p>{!! $backendUsers->login !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $backendUsers->email !!}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{!! $backendUsers->password !!}</p>
</div>

<!-- Activation Code Field -->
<div class="form-group">
    {!! Form::label('activation_code', 'Activation Code:') !!}
    <p>{!! $backendUsers->activation_code !!}</p>
</div>

<!-- Persist Code Field -->
<div class="form-group">
    {!! Form::label('persist_code', 'Persist Code:') !!}
    <p>{!! $backendUsers->persist_code !!}</p>
</div>

<!-- Reset Password Code Field -->
<div class="form-group">
    {!! Form::label('reset_password_code', 'Reset Password Code:') !!}
    <p>{!! $backendUsers->reset_password_code !!}</p>
</div>

<!-- Permissions Field -->
<div class="form-group">
    {!! Form::label('permissions', 'Permissions:') !!}
    <p>{!! $backendUsers->permissions !!}</p>
</div>

<!-- Is Activated Field -->
<div class="form-group">
    {!! Form::label('is_activated', 'Is Activated:') !!}
    <p>{!! $backendUsers->is_activated !!}</p>
</div>

<!-- Role Id Field -->
<div class="form-group">
    {!! Form::label('role_id', 'Role Id:') !!}
    <p>{!! $backendUsers->role_id !!}</p>
</div>

<!-- Activated At Field -->
<div class="form-group">
    {!! Form::label('activated_at', 'Activated At:') !!}
    <p>{!! $backendUsers->activated_at !!}</p>
</div>

<!-- Last Login Field -->
<div class="form-group">
    {!! Form::label('last_login', 'Last Login:') !!}
    <p>{!! $backendUsers->last_login !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $backendUsers->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $backendUsers->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $backendUsers->deleted_at !!}</p>
</div>

<!-- Is Superuser Field -->
<div class="form-group">
    {!! Form::label('is_superuser', 'Is Superuser:') !!}
    <p>{!! $backendUsers->is_superuser !!}</p>
</div>

