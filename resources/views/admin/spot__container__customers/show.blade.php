@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Container  Customer
        </h1>--}}

        {{--@include('admin.spot__container__customers.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Container  Customer</h4>

        <div data-label="Show" class="df-example demo-forms spot__container__customers-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__container__customers.show_fields')
                        <a href="{!! route('admin.spotContainerCustomers.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
