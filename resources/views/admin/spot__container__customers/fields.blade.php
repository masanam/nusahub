<!-- Username Field -->
<div class="form-group col-sm-6">
    {!! Form::label('username', 'Username:', ['class' => 'd-block']) !!}
    {!! Form::text('username', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:', ['class' => 'd-block']) !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Display Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('display_name', 'Display Name:', ['class' => 'd-block']) !!}
    {!! Form::text('display_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Contact Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_email', 'Contact Email:', ['class' => 'd-block']) !!}
    {!! Form::text('contact_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Vat Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vat_number', 'Vat Number:', ['class' => 'd-block']) !!}
    {!! Form::text('vat_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Budget Field -->
<div class="form-group col-sm-6">
    {!! Form::label('budget', 'Budget:') !!}
    {!! Form::number('budget', null, ['class' => 'form-control']) !!}
</div>

<!-- Street Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('street', 'Street:', ['class' => 'd-block']) !!}
    {!! Form::textarea('street', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country', 'Country:') !!}
    {!! Form::number('country', null, ['class' => 'form-control']) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::number('city', null, ['class' => 'form-control']) !!}
</div>

<!-- Postcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('postcode', 'Postcode:', ['class' => 'd-block']) !!}
    {!! Form::text('postcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number', 'Number:', ['class' => 'd-block']) !!}
    {!! Form::text('number', null, ['class' => 'form-control']) !!}
</div>

<!-- Box Field -->
<div class="form-group col-sm-6">
    {!! Form::label('box', 'Box:', ['class' => 'd-block']) !!}
    {!! Form::text('box', null, ['class' => 'form-control']) !!}
</div>

<!-- Language Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language', 'Language:', ['class' => 'd-block']) !!}
    {!! Form::text('language', null, ['class' => 'form-control']) !!}
</div>

<!-- Clearance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('clearance', 'Clearance:') !!}
    {!! Form::number('clearance', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Term Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_term', 'Payment Term:') !!}
    {!! Form::number('payment_term', null, ['class' => 'form-control']) !!}
</div>

<!-- Fiscal Representation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fiscal_representation', 'Fiscal Representation:') !!}
    {!! Form::number('fiscal_representation', null, ['class' => 'form-control']) !!}
</div>

<!-- Storage Fee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('storage_fee', 'Storage Fee:') !!}
    {!! Form::number('storage_fee', null, ['class' => 'form-control']) !!}
</div>

<!-- Cost 24 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost_24', 'Cost 24:') !!}
    {!! Form::number('cost_24', null, ['class' => 'form-control']) !!}
</div>

<!-- Cost 48 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost_48', 'Cost 48:') !!}
    {!! Form::number('cost_48', null, ['class' => 'form-control']) !!}
</div>

<!-- Kg Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kg_price', 'Kg Price:') !!}
    {!! Form::number('kg_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.spotContainerCustomers.index') !!}" class="btn btn-light">Cancel</a>
</div>

@section('scripts')
<!-- Relational Form table -->
<script>
    $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;

        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             {!! Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    {!! Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->
@endsection
