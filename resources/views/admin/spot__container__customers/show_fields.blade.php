<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotContainerCustomer->id !!}</p>
</div>

<!-- Username Field -->
<div class="form-group">
    {!! Form::label('username', 'Username:') !!}
    <p>{!! $spotContainerCustomer->username !!}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{!! $spotContainerCustomer->password !!}</p>
</div>

<!-- Display Name Field -->
<div class="form-group">
    {!! Form::label('display_name', 'Display Name:') !!}
    <p>{!! $spotContainerCustomer->display_name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $spotContainerCustomer->email !!}</p>
</div>

<!-- Contact Email Field -->
<div class="form-group">
    {!! Form::label('contact_email', 'Contact Email:') !!}
    <p>{!! $spotContainerCustomer->contact_email !!}</p>
</div>

<!-- Vat Number Field -->
<div class="form-group">
    {!! Form::label('vat_number', 'Vat Number:') !!}
    <p>{!! $spotContainerCustomer->vat_number !!}</p>
</div>

<!-- Budget Field -->
<div class="form-group">
    {!! Form::label('budget', 'Budget:') !!}
    <p>{!! $spotContainerCustomer->budget !!}</p>
</div>

<!-- Street Field -->
<div class="form-group">
    {!! Form::label('street', 'Street:') !!}
    <p>{!! $spotContainerCustomer->street !!}</p>
</div>

<!-- Country Field -->
<div class="form-group">
    {!! Form::label('country', 'Country:') !!}
    <p>{!! $spotContainerCustomer->country !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $spotContainerCustomer->city !!}</p>
</div>

<!-- Postcode Field -->
<div class="form-group">
    {!! Form::label('postcode', 'Postcode:') !!}
    <p>{!! $spotContainerCustomer->postcode !!}</p>
</div>

<!-- Number Field -->
<div class="form-group">
    {!! Form::label('number', 'Number:') !!}
    <p>{!! $spotContainerCustomer->number !!}</p>
</div>

<!-- Box Field -->
<div class="form-group">
    {!! Form::label('box', 'Box:') !!}
    <p>{!! $spotContainerCustomer->box !!}</p>
</div>

<!-- Language Field -->
<div class="form-group">
    {!! Form::label('language', 'Language:') !!}
    <p>{!! $spotContainerCustomer->language !!}</p>
</div>

<!-- Clearance Field -->
<div class="form-group">
    {!! Form::label('clearance', 'Clearance:') !!}
    <p>{!! $spotContainerCustomer->clearance !!}</p>
</div>

<!-- Payment Term Field -->
<div class="form-group">
    {!! Form::label('payment_term', 'Payment Term:') !!}
    <p>{!! $spotContainerCustomer->payment_term !!}</p>
</div>

<!-- Fiscal Representation Field -->
<div class="form-group">
    {!! Form::label('fiscal_representation', 'Fiscal Representation:') !!}
    <p>{!! $spotContainerCustomer->fiscal_representation !!}</p>
</div>

<!-- Storage Fee Field -->
<div class="form-group">
    {!! Form::label('storage_fee', 'Storage Fee:') !!}
    <p>{!! $spotContainerCustomer->storage_fee !!}</p>
</div>

<!-- Cost 24 Field -->
<div class="form-group">
    {!! Form::label('cost_24', 'Cost 24:') !!}
    <p>{!! $spotContainerCustomer->cost_24 !!}</p>
</div>

<!-- Cost 48 Field -->
<div class="form-group">
    {!! Form::label('cost_48', 'Cost 48:') !!}
    <p>{!! $spotContainerCustomer->cost_48 !!}</p>
</div>

<!-- Kg Price Field -->
<div class="form-group">
    {!! Form::label('kg_price', 'Kg Price:') !!}
    <p>{!! $spotContainerCustomer->kg_price !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $spotContainerCustomer->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $spotContainerCustomer->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $spotContainerCustomer->deleted_at !!}</p>
</div>

