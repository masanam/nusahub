@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Shipment  Payment
        </h1>--}}

        {{--@include('admin.spot__shipment__payments.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Shipment  Payment</h4>

        <div data-label="Show" class="df-example demo-forms spot__shipment__payments-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__shipment__payments.show_fields')
                        <a href="{!! route('admin.spotShipmentPayments.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
