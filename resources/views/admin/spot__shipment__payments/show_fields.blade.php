<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotShipmentPayment->id !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $spotShipmentPayment->type !!}</p>
</div>

<!-- For Id Field -->
<div class="form-group">
    {!! Form::label('for_id', 'For Id:') !!}
    <p>{!! $spotShipmentPayment->for_id !!}</p>
</div>

<!-- Movement Field -->
<div class="form-group">
    {!! Form::label('movement', 'Movement:') !!}
    <p>{!! $spotShipmentPayment->movement !!}</p>
</div>

<!-- Other Field -->
<div class="form-group">
    {!! Form::label('other', 'Other:') !!}
    <p>{!! $spotShipmentPayment->other !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $spotShipmentPayment->description !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $spotShipmentPayment->amount !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $spotShipmentPayment->status !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $spotShipmentPayment->date !!}</p>
</div>

<!-- Item Id Field -->
<div class="form-group">
    {!! Form::label('item_id', 'Item Id:') !!}
    <p>{!! $spotShipmentPayment->item_id !!}</p>
</div>

<!-- Payment Type Field -->
<div class="form-group">
    {!! Form::label('payment_type', 'Payment Type:') !!}
    <p>{!! $spotShipmentPayment->payment_type !!}</p>
</div>

<!-- Payment With Field -->
<div class="form-group">
    {!! Form::label('payment_with', 'Payment With:') !!}
    <p>{!! $spotShipmentPayment->payment_with !!}</p>
</div>

<!-- Payment Method Field -->
<div class="form-group">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    <p>{!! $spotShipmentPayment->payment_method !!}</p>
</div>

<!-- Treasury Id Field -->
<div class="form-group">
    {!! Form::label('treasury_id', 'Treasury Id:') !!}
    <p>{!! $spotShipmentPayment->treasury_id !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $spotShipmentPayment->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $spotShipmentPayment->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $spotShipmentPayment->updated_at !!}</p>
</div>

