@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Spot  Container  Size
        </h1>--}}

        {{--@include('admin.spot__container__sizes.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Spot  Container  Size</h4>

        <div data-label="Show" class="df-example demo-forms spot__container__sizes-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__container__sizes.show_fields')
                        <a href="{!! route('admin.spotContainerSizes.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
