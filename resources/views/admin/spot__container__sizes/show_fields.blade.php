<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotContainerSize->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $spotContainerSize->name !!}</p>
</div>

<!-- Size Desc Field -->
<div class="form-group">
    {!! Form::label('size_desc', 'Size Desc:') !!}
    <p>{!! $spotContainerSize->size_desc !!}</p>
</div>

