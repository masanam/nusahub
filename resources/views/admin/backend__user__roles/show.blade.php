@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Backend  User  Roles
        </h1>--}}

        {{--@include('admin.backend__user__roles.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Backend  User  Roles</h4>

        <div data-label="Show" class="df-example demo-forms backend__user__roles-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.backend__user__roles.show_fields')
                        <a href="{!! route('admin.backendUserRoles.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
