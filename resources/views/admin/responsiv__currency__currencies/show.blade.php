@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Responsiv  Currency  Currencies
        </h1>--}}

        {{--@include('admin.responsiv__currency__currencies.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Responsiv  Currency  Currencies</h4>

        <div data-label="Show" class="df-example demo-forms responsiv__currency__currencies-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.responsiv__currency__currencies.show_fields')
                        <a href="{!! route('admin.responsivCurrencyCurrencies.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
