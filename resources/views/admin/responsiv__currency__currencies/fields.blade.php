<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:', ['class' => 'd-block']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Currency Code:', ['class' => 'd-block']) !!}
    {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Symbol Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_symbol', 'Currency Symbol:', ['class' => 'd-block']) !!}
    {!! Form::text('currency_symbol', null, ['class' => 'form-control']) !!}
</div>

<!-- Decimal Point Field -->
<div class="form-group col-sm-6">
    {!! Form::label('decimal_point', 'Decimal Point:', ['class' => 'd-block']) !!}
    {!! Form::text('decimal_point', null, ['class' => 'form-control']) !!}
</div>

<!-- Thousand Separator Field -->
<div class="form-group col-sm-6">
    {!! Form::label('thousand_separator', 'Thousand Separator:', ['class' => 'd-block']) !!}
    {!! Form::text('thousand_separator', null, ['class' => 'form-control']) !!}
</div>

<div class="custom-control custom-checkbox">
  {!! Form::checkbox('place_symbol_before', '1', null, ['class' => 'custom-control-input', 'id' => 'place_symbol_before']) !!}
  <label class="custom-control-label" for="place_symbol_before">{!! 1 !!}</label>
</div>

<div class="custom-control custom-checkbox">
  {!! Form::checkbox('with_space', '1', null, ['class' => 'custom-control-input', 'id' => 'with_space']) !!}
  <label class="custom-control-label" for="with_space">{!! 1 !!}</label>
</div>

<div class="custom-control custom-checkbox">
  {!! Form::checkbox('is_enabled', '1', null, ['class' => 'custom-control-input', 'id' => 'is_enabled']) !!}
  <label class="custom-control-label" for="is_enabled">{!! 1 !!}</label>
</div>

<div class="custom-control custom-checkbox">
  {!! Form::checkbox('is_primary', '1', null, ['class' => 'custom-control-input', 'id' => 'is_primary']) !!}
  <label class="custom-control-label" for="is_primary">{!! 1 !!}</label>
</div>

<!-- Initbiz Money Fraction Digits Field -->
<div class="form-group col-sm-6">
    {!! Form::label('initbiz_money_fraction_digits', 'Initbiz Money Fraction Digits:') !!}
    {!! Form::number('initbiz_money_fraction_digits', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.responsivCurrencyCurrencies.index') !!}" class="btn btn-light">Cancel</a>
</div>

@section('scripts')
<!-- Relational Form table -->
<script>
    $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;

        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             {!! Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    {!! Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->
@endsection
