<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $responsivCurrencyCurrencies->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $responsivCurrencyCurrencies->name !!}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{!! $responsivCurrencyCurrencies->currency_code !!}</p>
</div>

<!-- Currency Symbol Field -->
<div class="form-group">
    {!! Form::label('currency_symbol', 'Currency Symbol:') !!}
    <p>{!! $responsivCurrencyCurrencies->currency_symbol !!}</p>
</div>

<!-- Decimal Point Field -->
<div class="form-group">
    {!! Form::label('decimal_point', 'Decimal Point:') !!}
    <p>{!! $responsivCurrencyCurrencies->decimal_point !!}</p>
</div>

<!-- Thousand Separator Field -->
<div class="form-group">
    {!! Form::label('thousand_separator', 'Thousand Separator:') !!}
    <p>{!! $responsivCurrencyCurrencies->thousand_separator !!}</p>
</div>

<!-- Place Symbol Before Field -->
<div class="form-group">
    {!! Form::label('place_symbol_before', 'Place Symbol Before:') !!}
    <p>{!! $responsivCurrencyCurrencies->place_symbol_before !!}</p>
</div>

<!-- With Space Field -->
<div class="form-group">
    {!! Form::label('with_space', 'With Space:') !!}
    <p>{!! $responsivCurrencyCurrencies->with_space !!}</p>
</div>

<!-- Is Enabled Field -->
<div class="form-group">
    {!! Form::label('is_enabled', 'Is Enabled:') !!}
    <p>{!! $responsivCurrencyCurrencies->is_enabled !!}</p>
</div>

<!-- Is Primary Field -->
<div class="form-group">
    {!! Form::label('is_primary', 'Is Primary:') !!}
    <p>{!! $responsivCurrencyCurrencies->is_primary !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $responsivCurrencyCurrencies->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $responsivCurrencyCurrencies->updated_at !!}</p>
</div>

<!-- Initbiz Money Fraction Digits Field -->
<div class="form-group">
    {!! Form::label('initbiz_money_fraction_digits', 'Initbiz Money Fraction Digits:') !!}
    <p>{!! $responsivCurrencyCurrencies->initbiz_money_fraction_digits !!}</p>
</div>

