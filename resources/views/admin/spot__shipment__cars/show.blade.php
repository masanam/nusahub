@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Spot  Shipment  Car
        </h1>--}}

        {{--@include('admin.spot__shipment__cars.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Spot  Shipment  Car</h4>

        <div data-label="Show" class="df-example demo-forms spot__shipment__cars-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__shipment__cars.show_fields')
                        <a href="{!! route('admin.spotShipmentCars.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
