<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotShipmentCar->id !!}</p>
</div>

<!-- License Plate Field -->
<div class="form-group">
    {!! Form::label('license_plate', 'License Plate:') !!}
    <p>{!! $spotShipmentCar->license_plate !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $spotShipmentCar->name !!}</p>
</div>

<!-- Destination Id Field -->
<div class="form-group">
    {!! Form::label('destination_id', 'Destination Id:') !!}
    <p>{!! $spotShipmentCar->destination_id !!}</p>
</div>

<!-- Transport Id Field -->
<div class="form-group">
    {!! Form::label('transport_id', 'Transport Id:') !!}
    <p>{!! $spotShipmentCar->transport_id !!}</p>
</div>

<!-- Transport Date Field -->
<div class="form-group">
    {!! Form::label('transport_date', 'Transport Date:') !!}
    <p>{!! $spotShipmentCar->transport_date !!}</p>
</div>

<!-- Enable Field -->
<div class="form-group">
    {!! Form::label('enable', 'Enable:') !!}
    <p>{!! $spotShipmentCar->enable !!}</p>
</div>

<!-- Driver Id Field -->
<div class="form-group">
    {!! Form::label('driver_id', 'Driver Id:') !!}
    <p>{!! $spotShipmentCar->driver_id !!}</p>
</div>

<!-- Responsible Id Field -->
<div class="form-group">
    {!! Form::label('responsible_id', 'Responsible Id:') !!}
    <p>{!! $spotShipmentCar->responsible_id !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $spotShipmentCar->description !!}</p>
</div>

