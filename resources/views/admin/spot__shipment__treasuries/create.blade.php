@extends('layouts.app')

@section('contents')
    {{-- <div class="content content-components"> --}}
    <div class="content">
        <div class="container">
            @include('dashforge-templates::common.errors')

            <h4 id="section1" class="mg-b-10">Spot  Shipment  Treasury</h4>

            <p class="mg-b-30">Please, fill all required fields before click save button.</p>

            <div data-label="Create" class="df-example demo-forms spot__shipment__treasuries-forms">
                {!! Form::open(['route' => 'admin.spotShipmentTreasuries.store']) !!}
                    @include('admin.spot__shipment__treasuries.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /.content -->
@endsection
