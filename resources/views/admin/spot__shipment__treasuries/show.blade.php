@extends('layouts.app')

@section('contents')
    {{-- <section class="content-header">
        <h1>
            Spot  Shipment  Treasury
        </h1>--}}

        {{--@include('admin.spot__shipment__treasuries.version')--}}
    {{-- </section> --}}
    <div class="content">
        <h4>Spot  Shipment  Treasury</h4>

        <div data-label="Show" class="df-example demo-forms spot__shipment__treasuries-forms">
            <div class="box box-primary">
                <div class="box-body">
                    {{-- <div class="row" style="padding-left: 20px"> --}}
                        @include('admin.spot__shipment__treasuries.show_fields')
                        <a href="{!! route('admin.spotShipmentTreasuries.index') !!}" class="btn btn-light">Back</a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
