<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $spotShipmentTreasury->id !!}</p>
</div>

<!-- Default Field -->
<div class="form-group">
    {!! Form::label('default', 'Default:') !!}
    <p>{!! $spotShipmentTreasury->default !!}</p>
</div>

<!-- Office Id Field -->
<div class="form-group">
    {!! Form::label('office_id', 'Office Id:') !!}
    <p>{!! $spotShipmentTreasury->office_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $spotShipmentTreasury->name !!}</p>
</div>

<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('balance', 'Balance:') !!}
    <p>{!! $spotShipmentTreasury->balance !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $spotShipmentTreasury->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $spotShipmentTreasury->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $spotShipmentTreasury->deleted_at !!}</p>
</div>

