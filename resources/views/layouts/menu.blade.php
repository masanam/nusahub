<li class="nav-label">Dashboard</li>
<li class="{{ Request::is('stats*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! url('stats') !!}"><i data-feather="activity"></i><span>Stats</span></a>
</li>

          <li class="nav-item active"><a href="dashboard-one.html" class="nav-link"><i data-feather="shopping-bag"></i> <span>Monitoring</span></a></li>
          <li class="nav-item"><a href="dashboard-two.html" class="nav-link"><i data-feather="globe"></i> <span>Analytics</span></a></li>
          <li class="nav-item"><a href="dashboard-three.html" class="nav-link"><i data-feather="pie-chart"></i> <span>Latest Transaction</span></a></li>
          <li class="nav-item"><a href="dashboard-four.html" class="nav-link"><i data-feather="life-buoy"></i> <span>Management</span></a></li>
          
          <li class="nav-label mg-t-25">Shipments List</li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentOrders.index') !!}"><i data-feather="calendar"></i> <span>All Shipments</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentOrders.index') !!}"><i data-feather="box"></i><span>New Request</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentOrders.index') !!}"><i data-feather="layers"></i> <span>Approved</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentOrders.index') !!}"><i data-feather="file-text"></i> <span>Assigned</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentOrders.index') !!}"><i data-feather="mail"></i> <span>Delivered to Driver</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentOrders.index') !!}"><i data-feather="globe"></i> <span>In Stock</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentOrders.index') !!}"><i data-feather="pie-chart"></i> <span>Postponed</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentOrders.index') !!}"><i data-feather="life-buoy"></i> <span>Delivered Shipment</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentOrders.index') !!}"><i data-feather="layers"></i> <span>Supplied</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentOrders.index') !!}"><i data-feather="box"></i> <span>In Custom</span></a></li>


          <li class="nav-label mg-t-25">Container List</li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotContainerOrders.index') !!}"><i data-feather="layers"></i> <span>All Container</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotContainerOrders.index') !!}"><i data-feather="box"></i> <span>New Request</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotContainerOrders.index') !!}"><i data-feather="mail"></i> <span>Blocked Container</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotContainerOrders.index') !!}"><i data-feather="globe"></i> <span>Register Container</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotContainerOrders.index') !!}"><i data-feather="pie-chart"></i> <span>Loaded Container</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotContainerOrders.index') !!}"><i data-feather="life-buoy"></i> <span>Released Container</span></a></li>




          <li class="nav-label mg-t-25">Profile</li>
          <li class="nav-item with-sub">
            <a href="" class="nav-link"><i data-feather="user"></i> <span>Customer List</span></a>
            <ul>
              <li><a href="{!! route('admin.spotContainerCustomers.index') !!}">All Customer</a></li>
              <li><a href="{!! route('admin.spotContainerCustomers.index') !!}">Status</a></li>
            </ul>
          </li>
          <li class="nav-item with-sub">
            <a href="" class="nav-link"><i data-feather="file"></i> <span>Employee List</span></a>
            <ul>
                <li><a href="{!! route('admin.users.index') !!}"><span>All Employees</span></a></li>
                <li><a href="{!! route('admin.users.index') !!}">Status</a></li>
            </ul>
          </li>
          <li class="nav-label mg-t-25">Transaction</li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentPayments.index') !!}"><i data-feather="layers"></i> <span>Transactions</span></a></li>
          <li class="nav-item"><a class="nav-link" href="{!! route('admin.spotShipmentPayments.index') !!}"><i data-feather="box"></i> <span>Order</span></a></li>
        
          <li class="nav-label mg-t-25">Report</li>
          <li class="nav-item"><a href="../../components" class="nav-link"><i data-feather="layers"></i> <span>Delayed Shipment</span></a></li>
          <li class="nav-item"><a href="../../collections" class="nav-link"><i data-feather="box"></i> <span>Delivered Shipments</span></a></li>
          <li class="nav-item"><a href="app-mail.html" class="nav-link"><i data-feather="mail"></i> <span>Deposit Transaction</span></a></li>
          <li class="nav-item"><a href="dashboard-two.html" class="nav-link"><i data-feather="globe"></i> <span>Withdraw Transaction</span></a></li>

          <li class="nav-label mg-t-25">Setting</li>
          <li class="nav-item with-sub">
            <a href="" class="nav-link"><i data-feather="layers"></i> <span>Definition</span></a>
            <ul>
              <li><a href="{!! route('admin.spotShipmentOffices.index') !!}">Branch</a></li>
              <li><a href="{!! route('admin.spotShipmentTreasuries.index') !!}">Treasury</a></li>
              <li><a href="{!! route('admin.spotShipmentDeliveryTimes.index') !!}">Delivery Time</a></li>
              <li><a href="{!! route('admin.spotShipmentCouriers.index') !!}">Courier Company</a></li>
              <li><a href="{!! route('admin.spotShipmentModes.index') !!}">Shipping Mode</a></li>
              <li><a href="{!! route('admin.spotShipmentLabels.index') !!}">Label</a></li>
              <li><a href="{!! route('admin.spotShipmentHandlers.index') !!}">Ground Handler</a></li>
              <li><a href="{!! route('admin.spotShipmentBreaks.index') !!}">Breakdown</a></li>
              <li><a href="{!! route('admin.spotContainerDestinations.index') !!}">Destination</a></li>
              <li><a href="{!! route('admin.spotShipmentPackagings.index') !!}">Packaging Type</a></li>
              <li><a href="{!! route('admin.spotShipmentStatuses.index') !!}">Statuses</a></li>
              <li><a href="page-connections.html">InStock Status</a></li>
              <li><a href="page-profile-view.html">InCustom Status</a></li>
              <li><a href="{!! route('admin.spotShipmentCategories.index') !!}">Item Categories</a></li>
              <li><a href="{!! route('admin.spotShipmentCars.index') !!}">Cars</a></li>
            </ul>
          </li>
          <li class="nav-item with-sub">
            <a href="" class="nav-link"><i data-feather="box"></i> <span>Container</span></a>
            <ul>
              <li><a href="{!! route('admin.spotContainerLocations.index') !!}">Location Type</a></li>
              <li><a href="{!! route('admin.spotContainerStatuses.index') !!}">Status</a></li>
              <li><a href="{!! route('admin.spotContainerReasons.index') !!}">Reason of Arrival</a></li>
              <li><a href="{!! route('admin.spotContainerSizes.index') !!}">Size</a></li>
            </ul>
          </li>
          <li class="nav-item with-sub">
            <a href="" class="nav-link"><i data-feather="globe"></i> <span>Places</span></a>
            <ul>
              <li><a href="{!! route('admin.rainlabLocationCountries.index') !!}">Countries</a></li>
              <li><a href="{!! route('admin.rainlabLocationStates.index') !!}">States</a></li>
              <li><a href="{!! route('admin.spotShipmentCities.index') !!}">City</a></li>
              <li><a href="{!! route('admin.spotShipmentAreas.index') !!}">Area</a></li>
            </ul>
          </li>
          <li class="nav-item with-sub">
            <a href="" class="nav-link"><i data-feather="user"></i> <span>Employees</span></a>
            <ul>
              <li><a href="{!! route('admin.users.index') !!}">Employees</a></li>
              <li><a href="{!! route('admin.userGroups.index') !!}">Departments</a></li>
            </ul>
          </li>
          <li class="nav-item"><a href="{!! route('admin.responsivCurrencyCurrencies.index') !!}" class="nav-link"><i data-feather="message-square"></i> <span>Currency</span></a></li>
          <li class="nav-item"><a href="{!! route('admin.responsivPayInvoiceStatuses.index') !!}" class="nav-link"><i data-feather="file-text"></i> <span>Invoice Status</span></a></li>

            <li class="nav-label mg-t-25">User Management</li>

            <li class="{{ Request::is('backendUserRoles*') ? 'active' : '' }} nav-item">
                <a class="nav-link" href="{!! route('admin.backendUserRoles.index') !!}"><i data-feather="edit"></i><span>Backend  User  Roles</span></a>
            </li>

            <li class="{{ Request::is('backendUsers*') ? 'active' : '' }} nav-item">
                <a class="nav-link" href="{!! route('admin.backendUsers.index') !!}"><i data-feather="edit"></i><span>Backend  Users</span></a>
            </li>

            <li class="{{ Request::is('backendUserGroups*') ? 'active' : '' }} nav-item">
                <a class="nav-link" href="{!! route('admin.backendUserGroups.index') !!}"><i data-feather="edit"></i><span>Backend  User  Groups</span></a>
            </li>

<!-- 
<li class="nav-label mg-t-25">Management</li>

<li class="{{ Request::is('users*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.users.index') !!}"><i data-feather="edit"></i><span>Employees</span></a>
</li>

<li class="{{ Request::is('spotContainerCustomers*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotContainerCustomers.index') !!}"><i data-feather="edit"></i><span>Customers</span></a>
</li>

<li class="{{ Request::is('spotContainerOrders*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotContainerOrders.index') !!}"><i data-feather="edit"></i><span>Orders</span></a>
</li>

<li class="{{ Request::is('spotShipmentOrders*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentOrders.index') !!}"><i data-feather="edit"></i><span>Shipment</span></a>
</li>

<li class="{{ Request::is('spotShipmentPayments*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentPayments.index') !!}"><i data-feather="edit"></i><span>Payments</span></a>
</li>

<li class="{{ Request::is('spotContainerStatuses*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotContainerStatuses.index') !!}"><i data-feather="edit"></i><span>Spot  Container  Statuses</span></a>
</li>

<li class="{{ Request::is('spotContainerLocations*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotContainerLocations.index') !!}"><i data-feather="edit"></i><span>Spot  Container  Locations</span></a>
</li>

<li class="{{ Request::is('spotContainerReasons*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotContainerReasons.index') !!}"><i data-feather="edit"></i><span>Spot  Container  Reasons</span></a>
</li>

<li class="{{ Request::is('spotContainerSizes*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotContainerSizes.index') !!}"><i data-feather="edit"></i><span>Spot  Container  Sizes</span></a>
</li>

<li class="{{ Request::is('spotShipmentCars*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentCars.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Cars</span></a>
</li>

<li class="{{ Request::is('spotShipmentCategories*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentCategories.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Categories</span></a>
</li>

<li class="{{ Request::is('spotShipmentCouriers*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentCouriers.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Couriers</span></a>
</li>

<li class="{{ Request::is('spotShipmentLabels*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentLabels.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Labels</span></a>
</li>

<li class="{{ Request::is('spotShipmentDeliveryTimes*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentDeliveryTimes.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Delivery  Times</span></a>
</li>

<li class="{{ Request::is('spotShipmentHandlers*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentHandlers.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Handlers</span></a>
</li>

<li class="{{ Request::is('spotShipmentModes*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentModes.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Modes</span></a>
</li>

<li class="{{ Request::is('spotShipmentOffices*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentOffices.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Offices</span></a>
</li>

<li class="{{ Request::is('spotShipmentPackagings*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentPackagings.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Packagings</span></a>
</li>

<li class="{{ Request::is('spotShipmentStatuses*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentStatuses.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Statuses</span></a>
</li>

<li class="{{ Request::is('spotShipmentTreasuries*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentTreasuries.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Treasuries</span></a>
</li>

<li class="{{ Request::is('spotShipmentBreaks*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentBreaks.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Breaks</span></a>
</li>

<li class="{{ Request::is('spotContainerDestinations*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotContainerDestinations.index') !!}"><i data-feather="edit"></i><span>Spot  Container  Destinations</span></a>
</li>

<li class="{{ Request::is('rainlabLocationCountries*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.rainlabLocationCountries.index') !!}"><i data-feather="edit"></i><span>Rainlab  Location  Countries</span></a>
</li>

<li class="{{ Request::is('rainlabLocationStates*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.rainlabLocationStates.index') !!}"><i data-feather="edit"></i><span>Rainlab  Location  States</span></a>
</li>

<li class="{{ Request::is('spotShipmentCities*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentCities.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Cities</span></a>
</li>

<li class="{{ Request::is('spotShipmentAreas*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.spotShipmentAreas.index') !!}"><i data-feather="edit"></i><span>Spot  Shipment  Areas</span></a>
</li>

<li class="{{ Request::is('userGroups*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.userGroups.index') !!}"><i data-feather="edit"></i><span>User  Groups</span></a>
</li>

<li class="{{ Request::is('responsivCurrencyCurrencies*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.responsivCurrencyCurrencies.index') !!}"><i data-feather="edit"></i><span>Responsiv  Currency  Currencies</span></a>
</li>

<li class="{{ Request::is('responsivPayInvoiceStatuses*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('admin.responsivPayInvoiceStatuses.index') !!}"><i data-feather="edit"></i><span>Responsiv  Pay  Invoice  Statuses</span></a>
</li>
 -->
