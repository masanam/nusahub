<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Responsiv_Pay_Invoice_Statuses;

class CreateResponsiv_Pay_Invoice_StatusesRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Responsiv_Pay_Invoice_Statuses::$rules;
    }
}
