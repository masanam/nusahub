<?php

namespace App\Http\Requests\API;

use App\Models\Backend_User_Roles;
use Webcore\Generator\Request\APIRequest;

class UpdateBackend_User_RolesAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Backend_User_Roles::$rules;
    }
}
