<?php

namespace App\Http\Requests\API;

use App\Models\Rainlab_Location_Countries;
use Webcore\Generator\Request\APIRequest;

class CreateRainlab_Location_CountriesAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Rainlab_Location_Countries::$rules;
    }
}
