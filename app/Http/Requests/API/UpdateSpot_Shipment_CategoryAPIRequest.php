<?php

namespace App\Http\Requests\API;

use App\Models\Spot_Shipment_Category;
use Webcore\Generator\Request\APIRequest;

class UpdateSpot_Shipment_CategoryAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Spot_Shipment_Category::$rules;
    }
}
