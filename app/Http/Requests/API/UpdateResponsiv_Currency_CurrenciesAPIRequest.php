<?php

namespace App\Http\Requests\API;

use App\Models\Responsiv_Currency_Currencies;
use Webcore\Generator\Request\APIRequest;

class UpdateResponsiv_Currency_CurrenciesAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Responsiv_Currency_Currencies::$rules;
    }
}
