<?php

namespace App\Http\Requests\API;

use App\Models\Responsiv_Pay_Invoice_Statuses;
use Webcore\Generator\Request\APIRequest;

class UpdateResponsiv_Pay_Invoice_StatusesAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Responsiv_Pay_Invoice_Statuses::$rules;
    }
}
