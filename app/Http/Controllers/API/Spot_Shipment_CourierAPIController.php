<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_CourierAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_CourierAPIRequest;
use App\Models\Spot_Shipment_Courier;
use App\Repositories\Spot_Shipment_CourierRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_CourierController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_CourierAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_CourierRepository */
    private $spotShipmentCourierRepository;

    public function __construct(Spot_Shipment_CourierRepository $spotShipmentCourierRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentCourierRepository = $spotShipmentCourierRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentCouriers",
     *      summary="Get a listing of the Spot_Shipment_Couriers.",
     *      tags={"Spot_Shipment_Courier"},
     *      description="Get all Spot_Shipment_Couriers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Courier")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentCourierRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentCourierRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentCouriers = $this->spotShipmentCourierRepository->all();

        return $this->sendResponse($spotShipmentCouriers->toArray(), 'Spot  Shipment  Couriers retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_CourierAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentCouriers",
     *      summary="Store a newly created Spot_Shipment_Courier in storage",
     *      tags={"Spot_Shipment_Courier"},
     *      description="Store Spot_Shipment_Courier",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Courier that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Courier")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Courier"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_CourierAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentCouriers = $this->spotShipmentCourierRepository->create($input);

        return $this->sendResponse($spotShipmentCouriers->toArray(), 'Spot  Shipment  Courier saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentCouriers/{id}",
     *      summary="Display the specified Spot_Shipment_Courier",
     *      tags={"Spot_Shipment_Courier"},
     *      description="Get Spot_Shipment_Courier",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Courier",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Courier"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Courier $spotShipmentCourier */
        $spotShipmentCourier = $this->spotShipmentCourierRepository->findWithoutFail($id);

        if (empty($spotShipmentCourier)) {
            return $this->sendError('Spot  Shipment  Courier not found');
        }

        return $this->sendResponse($spotShipmentCourier->toArray(), 'Spot  Shipment  Courier retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_CourierAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentCouriers/{id}",
     *      summary="Update the specified Spot_Shipment_Courier in storage",
     *      tags={"Spot_Shipment_Courier"},
     *      description="Update Spot_Shipment_Courier",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Courier",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Courier that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Courier")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Courier"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_CourierAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Courier $spotShipmentCourier */
        $spotShipmentCourier = $this->spotShipmentCourierRepository->findWithoutFail($id);

        if (empty($spotShipmentCourier)) {
            return $this->sendError('Spot  Shipment  Courier not found');
        }

        $spotShipmentCourier = $this->spotShipmentCourierRepository->update($input, $id);

        return $this->sendResponse($spotShipmentCourier->toArray(), 'Spot_Shipment_Courier updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentCouriers/{id}",
     *      summary="Remove the specified Spot_Shipment_Courier from storage",
     *      tags={"Spot_Shipment_Courier"},
     *      description="Delete Spot_Shipment_Courier",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Courier",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Courier $spotShipmentCourier */
        $spotShipmentCourier = $this->spotShipmentCourierRepository->findWithoutFail($id);

        if (empty($spotShipmentCourier)) {
            return $this->sendError('Spot  Shipment  Courier not found');
        }

        $spotShipmentCourier->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Courier deleted successfully');
    }
}
