<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_CategoryAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_CategoryAPIRequest;
use App\Models\Spot_Shipment_Category;
use App\Repositories\Spot_Shipment_CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_CategoryController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_CategoryAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_CategoryRepository */
    private $spotShipmentCategoryRepository;

    public function __construct(Spot_Shipment_CategoryRepository $spotShipmentCategoryRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentCategoryRepository = $spotShipmentCategoryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentCategories",
     *      summary="Get a listing of the Spot_Shipment_Categories.",
     *      tags={"Spot_Shipment_Category"},
     *      description="Get all Spot_Shipment_Categories",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Category")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentCategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentCategoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentCategories = $this->spotShipmentCategoryRepository->all();

        return $this->sendResponse($spotShipmentCategories->toArray(), 'Spot  Shipment  Categories retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_CategoryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentCategories",
     *      summary="Store a newly created Spot_Shipment_Category in storage",
     *      tags={"Spot_Shipment_Category"},
     *      description="Store Spot_Shipment_Category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Category that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Category")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Category"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_CategoryAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentCategories = $this->spotShipmentCategoryRepository->create($input);

        return $this->sendResponse($spotShipmentCategories->toArray(), 'Spot  Shipment  Category saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentCategories/{id}",
     *      summary="Display the specified Spot_Shipment_Category",
     *      tags={"Spot_Shipment_Category"},
     *      description="Get Spot_Shipment_Category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Category",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Category"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Category $spotShipmentCategory */
        $spotShipmentCategory = $this->spotShipmentCategoryRepository->findWithoutFail($id);

        if (empty($spotShipmentCategory)) {
            return $this->sendError('Spot  Shipment  Category not found');
        }

        return $this->sendResponse($spotShipmentCategory->toArray(), 'Spot  Shipment  Category retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_CategoryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentCategories/{id}",
     *      summary="Update the specified Spot_Shipment_Category in storage",
     *      tags={"Spot_Shipment_Category"},
     *      description="Update Spot_Shipment_Category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Category",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Category that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Category")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Category"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_CategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Category $spotShipmentCategory */
        $spotShipmentCategory = $this->spotShipmentCategoryRepository->findWithoutFail($id);

        if (empty($spotShipmentCategory)) {
            return $this->sendError('Spot  Shipment  Category not found');
        }

        $spotShipmentCategory = $this->spotShipmentCategoryRepository->update($input, $id);

        return $this->sendResponse($spotShipmentCategory->toArray(), 'Spot_Shipment_Category updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentCategories/{id}",
     *      summary="Remove the specified Spot_Shipment_Category from storage",
     *      tags={"Spot_Shipment_Category"},
     *      description="Delete Spot_Shipment_Category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Category",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Category $spotShipmentCategory */
        $spotShipmentCategory = $this->spotShipmentCategoryRepository->findWithoutFail($id);

        if (empty($spotShipmentCategory)) {
            return $this->sendError('Spot  Shipment  Category not found');
        }

        $spotShipmentCategory->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Category deleted successfully');
    }
}
