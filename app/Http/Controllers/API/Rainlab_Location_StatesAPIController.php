<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRainlab_Location_StatesAPIRequest;
use App\Http\Requests\API\UpdateRainlab_Location_StatesAPIRequest;
use App\Models\Rainlab_Location_States;
use App\Repositories\Rainlab_Location_StatesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Rainlab_Location_StatesController
 * @package App\Http\Controllers\API
 */

class Rainlab_Location_StatesAPIController extends AppBaseController
{
    /** @var  Rainlab_Location_StatesRepository */
    private $rainlabLocationStatesRepository;

    public function __construct(Rainlab_Location_StatesRepository $rainlabLocationStatesRepo)
    {
        $this->middleware('auth:api');
        $this->rainlabLocationStatesRepository = $rainlabLocationStatesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/rainlabLocationStates",
     *      summary="Get a listing of the Rainlab_Location_States.",
     *      tags={"Rainlab_Location_States"},
     *      description="Get all Rainlab_Location_States",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Rainlab_Location_States")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->rainlabLocationStatesRepository->pushCriteria(new RequestCriteria($request));
        $this->rainlabLocationStatesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $rainlabLocationStates = $this->rainlabLocationStatesRepository->all();

        return $this->sendResponse($rainlabLocationStates->toArray(), 'Rainlab  Location  States retrieved successfully');
    }

    /**
     * @param CreateRainlab_Location_StatesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/rainlabLocationStates",
     *      summary="Store a newly created Rainlab_Location_States in storage",
     *      tags={"Rainlab_Location_States"},
     *      description="Store Rainlab_Location_States",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Rainlab_Location_States that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Rainlab_Location_States")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rainlab_Location_States"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRainlab_Location_StatesAPIRequest $request)
    {
        $input = $request->all();

        $rainlabLocationStates = $this->rainlabLocationStatesRepository->create($input);

        return $this->sendResponse($rainlabLocationStates->toArray(), 'Rainlab  Location  States saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/rainlabLocationStates/{id}",
     *      summary="Display the specified Rainlab_Location_States",
     *      tags={"Rainlab_Location_States"},
     *      description="Get Rainlab_Location_States",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rainlab_Location_States",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rainlab_Location_States"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Rainlab_Location_States $rainlabLocationStates */
        $rainlabLocationStates = $this->rainlabLocationStatesRepository->findWithoutFail($id);

        if (empty($rainlabLocationStates)) {
            return $this->sendError('Rainlab  Location  States not found');
        }

        return $this->sendResponse($rainlabLocationStates->toArray(), 'Rainlab  Location  States retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateRainlab_Location_StatesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/rainlabLocationStates/{id}",
     *      summary="Update the specified Rainlab_Location_States in storage",
     *      tags={"Rainlab_Location_States"},
     *      description="Update Rainlab_Location_States",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rainlab_Location_States",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Rainlab_Location_States that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Rainlab_Location_States")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rainlab_Location_States"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRainlab_Location_StatesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Rainlab_Location_States $rainlabLocationStates */
        $rainlabLocationStates = $this->rainlabLocationStatesRepository->findWithoutFail($id);

        if (empty($rainlabLocationStates)) {
            return $this->sendError('Rainlab  Location  States not found');
        }

        $rainlabLocationStates = $this->rainlabLocationStatesRepository->update($input, $id);

        return $this->sendResponse($rainlabLocationStates->toArray(), 'Rainlab_Location_States updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/rainlabLocationStates/{id}",
     *      summary="Remove the specified Rainlab_Location_States from storage",
     *      tags={"Rainlab_Location_States"},
     *      description="Delete Rainlab_Location_States",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rainlab_Location_States",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Rainlab_Location_States $rainlabLocationStates */
        $rainlabLocationStates = $this->rainlabLocationStatesRepository->findWithoutFail($id);

        if (empty($rainlabLocationStates)) {
            return $this->sendError('Rainlab  Location  States not found');
        }

        $rainlabLocationStates->delete();

        return $this->sendResponse($id, 'Rainlab  Location  States deleted successfully');
    }
}
