<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_HandlerAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_HandlerAPIRequest;
use App\Models\Spot_Shipment_Handler;
use App\Repositories\Spot_Shipment_HandlerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_HandlerController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_HandlerAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_HandlerRepository */
    private $spotShipmentHandlerRepository;

    public function __construct(Spot_Shipment_HandlerRepository $spotShipmentHandlerRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentHandlerRepository = $spotShipmentHandlerRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentHandlers",
     *      summary="Get a listing of the Spot_Shipment_Handlers.",
     *      tags={"Spot_Shipment_Handler"},
     *      description="Get all Spot_Shipment_Handlers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Handler")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentHandlerRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentHandlerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentHandlers = $this->spotShipmentHandlerRepository->all();

        return $this->sendResponse($spotShipmentHandlers->toArray(), 'Spot  Shipment  Handlers retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_HandlerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentHandlers",
     *      summary="Store a newly created Spot_Shipment_Handler in storage",
     *      tags={"Spot_Shipment_Handler"},
     *      description="Store Spot_Shipment_Handler",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Handler that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Handler")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Handler"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_HandlerAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentHandlers = $this->spotShipmentHandlerRepository->create($input);

        return $this->sendResponse($spotShipmentHandlers->toArray(), 'Spot  Shipment  Handler saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentHandlers/{id}",
     *      summary="Display the specified Spot_Shipment_Handler",
     *      tags={"Spot_Shipment_Handler"},
     *      description="Get Spot_Shipment_Handler",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Handler",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Handler"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Handler $spotShipmentHandler */
        $spotShipmentHandler = $this->spotShipmentHandlerRepository->findWithoutFail($id);

        if (empty($spotShipmentHandler)) {
            return $this->sendError('Spot  Shipment  Handler not found');
        }

        return $this->sendResponse($spotShipmentHandler->toArray(), 'Spot  Shipment  Handler retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_HandlerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentHandlers/{id}",
     *      summary="Update the specified Spot_Shipment_Handler in storage",
     *      tags={"Spot_Shipment_Handler"},
     *      description="Update Spot_Shipment_Handler",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Handler",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Handler that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Handler")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Handler"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_HandlerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Handler $spotShipmentHandler */
        $spotShipmentHandler = $this->spotShipmentHandlerRepository->findWithoutFail($id);

        if (empty($spotShipmentHandler)) {
            return $this->sendError('Spot  Shipment  Handler not found');
        }

        $spotShipmentHandler = $this->spotShipmentHandlerRepository->update($input, $id);

        return $this->sendResponse($spotShipmentHandler->toArray(), 'Spot_Shipment_Handler updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentHandlers/{id}",
     *      summary="Remove the specified Spot_Shipment_Handler from storage",
     *      tags={"Spot_Shipment_Handler"},
     *      description="Delete Spot_Shipment_Handler",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Handler",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Handler $spotShipmentHandler */
        $spotShipmentHandler = $this->spotShipmentHandlerRepository->findWithoutFail($id);

        if (empty($spotShipmentHandler)) {
            return $this->sendError('Spot  Shipment  Handler not found');
        }

        $spotShipmentHandler->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Handler deleted successfully');
    }
}
