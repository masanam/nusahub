<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Container_LocationAPIRequest;
use App\Http\Requests\API\UpdateSpot_Container_LocationAPIRequest;
use App\Models\Spot_Container_Location;
use App\Repositories\Spot_Container_LocationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Container_LocationController
 * @package App\Http\Controllers\API
 */

class Spot_Container_LocationAPIController extends AppBaseController
{
    /** @var  Spot_Container_LocationRepository */
    private $spotContainerLocationRepository;

    public function __construct(Spot_Container_LocationRepository $spotContainerLocationRepo)
    {
        $this->middleware('auth:api');
        $this->spotContainerLocationRepository = $spotContainerLocationRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerLocations",
     *      summary="Get a listing of the Spot_Container_Locations.",
     *      tags={"Spot_Container_Location"},
     *      description="Get all Spot_Container_Locations",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Container_Location")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotContainerLocationRepository->pushCriteria(new RequestCriteria($request));
        $this->spotContainerLocationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotContainerLocations = $this->spotContainerLocationRepository->all();

        return $this->sendResponse($spotContainerLocations->toArray(), 'Spot  Container  Locations retrieved successfully');
    }

    /**
     * @param CreateSpot_Container_LocationAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotContainerLocations",
     *      summary="Store a newly created Spot_Container_Location in storage",
     *      tags={"Spot_Container_Location"},
     *      description="Store Spot_Container_Location",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Location that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Location")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Location"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Container_LocationAPIRequest $request)
    {
        $input = $request->all();

        $spotContainerLocations = $this->spotContainerLocationRepository->create($input);

        return $this->sendResponse($spotContainerLocations->toArray(), 'Spot  Container  Location saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerLocations/{id}",
     *      summary="Display the specified Spot_Container_Location",
     *      tags={"Spot_Container_Location"},
     *      description="Get Spot_Container_Location",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Location",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Location"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Container_Location $spotContainerLocation */
        $spotContainerLocation = $this->spotContainerLocationRepository->findWithoutFail($id);

        if (empty($spotContainerLocation)) {
            return $this->sendError('Spot  Container  Location not found');
        }

        return $this->sendResponse($spotContainerLocation->toArray(), 'Spot  Container  Location retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Container_LocationAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotContainerLocations/{id}",
     *      summary="Update the specified Spot_Container_Location in storage",
     *      tags={"Spot_Container_Location"},
     *      description="Update Spot_Container_Location",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Location",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Location that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Location")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Location"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Container_LocationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Container_Location $spotContainerLocation */
        $spotContainerLocation = $this->spotContainerLocationRepository->findWithoutFail($id);

        if (empty($spotContainerLocation)) {
            return $this->sendError('Spot  Container  Location not found');
        }

        $spotContainerLocation = $this->spotContainerLocationRepository->update($input, $id);

        return $this->sendResponse($spotContainerLocation->toArray(), 'Spot_Container_Location updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotContainerLocations/{id}",
     *      summary="Remove the specified Spot_Container_Location from storage",
     *      tags={"Spot_Container_Location"},
     *      description="Delete Spot_Container_Location",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Location",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Container_Location $spotContainerLocation */
        $spotContainerLocation = $this->spotContainerLocationRepository->findWithoutFail($id);

        if (empty($spotContainerLocation)) {
            return $this->sendError('Spot  Container  Location not found');
        }

        $spotContainerLocation->delete();

        return $this->sendResponse($id, 'Spot  Container  Location deleted successfully');
    }
}
