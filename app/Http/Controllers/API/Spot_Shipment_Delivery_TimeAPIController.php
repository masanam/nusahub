<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_Delivery_TimeAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_Delivery_TimeAPIRequest;
use App\Models\Spot_Shipment_Delivery_Time;
use App\Repositories\Spot_Shipment_Delivery_TimeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_Delivery_TimeController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_Delivery_TimeAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_Delivery_TimeRepository */
    private $spotShipmentDeliveryTimeRepository;

    public function __construct(Spot_Shipment_Delivery_TimeRepository $spotShipmentDeliveryTimeRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentDeliveryTimeRepository = $spotShipmentDeliveryTimeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentDeliveryTimes",
     *      summary="Get a listing of the Spot_Shipment_Delivery_Times.",
     *      tags={"Spot_Shipment_Delivery_Time"},
     *      description="Get all Spot_Shipment_Delivery_Times",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Delivery_Time")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentDeliveryTimeRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentDeliveryTimeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentDeliveryTimes = $this->spotShipmentDeliveryTimeRepository->all();

        return $this->sendResponse($spotShipmentDeliveryTimes->toArray(), 'Spot  Shipment  Delivery  Times retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_Delivery_TimeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentDeliveryTimes",
     *      summary="Store a newly created Spot_Shipment_Delivery_Time in storage",
     *      tags={"Spot_Shipment_Delivery_Time"},
     *      description="Store Spot_Shipment_Delivery_Time",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Delivery_Time that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Delivery_Time")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Delivery_Time"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_Delivery_TimeAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentDeliveryTimes = $this->spotShipmentDeliveryTimeRepository->create($input);

        return $this->sendResponse($spotShipmentDeliveryTimes->toArray(), 'Spot  Shipment  Delivery  Time saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentDeliveryTimes/{id}",
     *      summary="Display the specified Spot_Shipment_Delivery_Time",
     *      tags={"Spot_Shipment_Delivery_Time"},
     *      description="Get Spot_Shipment_Delivery_Time",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Delivery_Time",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Delivery_Time"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Delivery_Time $spotShipmentDeliveryTime */
        $spotShipmentDeliveryTime = $this->spotShipmentDeliveryTimeRepository->findWithoutFail($id);

        if (empty($spotShipmentDeliveryTime)) {
            return $this->sendError('Spot  Shipment  Delivery  Time not found');
        }

        return $this->sendResponse($spotShipmentDeliveryTime->toArray(), 'Spot  Shipment  Delivery  Time retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_Delivery_TimeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentDeliveryTimes/{id}",
     *      summary="Update the specified Spot_Shipment_Delivery_Time in storage",
     *      tags={"Spot_Shipment_Delivery_Time"},
     *      description="Update Spot_Shipment_Delivery_Time",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Delivery_Time",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Delivery_Time that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Delivery_Time")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Delivery_Time"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_Delivery_TimeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Delivery_Time $spotShipmentDeliveryTime */
        $spotShipmentDeliveryTime = $this->spotShipmentDeliveryTimeRepository->findWithoutFail($id);

        if (empty($spotShipmentDeliveryTime)) {
            return $this->sendError('Spot  Shipment  Delivery  Time not found');
        }

        $spotShipmentDeliveryTime = $this->spotShipmentDeliveryTimeRepository->update($input, $id);

        return $this->sendResponse($spotShipmentDeliveryTime->toArray(), 'Spot_Shipment_Delivery_Time updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentDeliveryTimes/{id}",
     *      summary="Remove the specified Spot_Shipment_Delivery_Time from storage",
     *      tags={"Spot_Shipment_Delivery_Time"},
     *      description="Delete Spot_Shipment_Delivery_Time",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Delivery_Time",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Delivery_Time $spotShipmentDeliveryTime */
        $spotShipmentDeliveryTime = $this->spotShipmentDeliveryTimeRepository->findWithoutFail($id);

        if (empty($spotShipmentDeliveryTime)) {
            return $this->sendError('Spot  Shipment  Delivery  Time not found');
        }

        $spotShipmentDeliveryTime->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Delivery  Time deleted successfully');
    }
}
