<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_OfficeAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_OfficeAPIRequest;
use App\Models\Spot_Shipment_Office;
use App\Repositories\Spot_Shipment_OfficeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_OfficeController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_OfficeAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_OfficeRepository */
    private $spotShipmentOfficeRepository;

    public function __construct(Spot_Shipment_OfficeRepository $spotShipmentOfficeRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentOfficeRepository = $spotShipmentOfficeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentOffices",
     *      summary="Get a listing of the Spot_Shipment_Offices.",
     *      tags={"Spot_Shipment_Office"},
     *      description="Get all Spot_Shipment_Offices",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Office")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentOfficeRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentOfficeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentOffices = $this->spotShipmentOfficeRepository->all();

        return $this->sendResponse($spotShipmentOffices->toArray(), 'Spot  Shipment  Offices retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_OfficeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentOffices",
     *      summary="Store a newly created Spot_Shipment_Office in storage",
     *      tags={"Spot_Shipment_Office"},
     *      description="Store Spot_Shipment_Office",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Office that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Office")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Office"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_OfficeAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentOffices = $this->spotShipmentOfficeRepository->create($input);

        return $this->sendResponse($spotShipmentOffices->toArray(), 'Spot  Shipment  Office saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentOffices/{id}",
     *      summary="Display the specified Spot_Shipment_Office",
     *      tags={"Spot_Shipment_Office"},
     *      description="Get Spot_Shipment_Office",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Office",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Office"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Office $spotShipmentOffice */
        $spotShipmentOffice = $this->spotShipmentOfficeRepository->findWithoutFail($id);

        if (empty($spotShipmentOffice)) {
            return $this->sendError('Spot  Shipment  Office not found');
        }

        return $this->sendResponse($spotShipmentOffice->toArray(), 'Spot  Shipment  Office retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_OfficeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentOffices/{id}",
     *      summary="Update the specified Spot_Shipment_Office in storage",
     *      tags={"Spot_Shipment_Office"},
     *      description="Update Spot_Shipment_Office",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Office",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Office that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Office")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Office"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_OfficeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Office $spotShipmentOffice */
        $spotShipmentOffice = $this->spotShipmentOfficeRepository->findWithoutFail($id);

        if (empty($spotShipmentOffice)) {
            return $this->sendError('Spot  Shipment  Office not found');
        }

        $spotShipmentOffice = $this->spotShipmentOfficeRepository->update($input, $id);

        return $this->sendResponse($spotShipmentOffice->toArray(), 'Spot_Shipment_Office updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentOffices/{id}",
     *      summary="Remove the specified Spot_Shipment_Office from storage",
     *      tags={"Spot_Shipment_Office"},
     *      description="Delete Spot_Shipment_Office",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Office",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Office $spotShipmentOffice */
        $spotShipmentOffice = $this->spotShipmentOfficeRepository->findWithoutFail($id);

        if (empty($spotShipmentOffice)) {
            return $this->sendError('Spot  Shipment  Office not found');
        }

        $spotShipmentOffice->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Office deleted successfully');
    }
}
