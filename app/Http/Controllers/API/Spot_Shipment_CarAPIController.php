<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_CarAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_CarAPIRequest;
use App\Models\Spot_Shipment_Car;
use App\Repositories\Spot_Shipment_CarRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_CarController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_CarAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_CarRepository */
    private $spotShipmentCarRepository;

    public function __construct(Spot_Shipment_CarRepository $spotShipmentCarRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentCarRepository = $spotShipmentCarRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentCars",
     *      summary="Get a listing of the Spot_Shipment_Cars.",
     *      tags={"Spot_Shipment_Car"},
     *      description="Get all Spot_Shipment_Cars",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Car")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentCarRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentCarRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentCars = $this->spotShipmentCarRepository->all();

        return $this->sendResponse($spotShipmentCars->toArray(), 'Spot  Shipment  Cars retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_CarAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentCars",
     *      summary="Store a newly created Spot_Shipment_Car in storage",
     *      tags={"Spot_Shipment_Car"},
     *      description="Store Spot_Shipment_Car",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Car that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Car")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Car"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_CarAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentCars = $this->spotShipmentCarRepository->create($input);

        return $this->sendResponse($spotShipmentCars->toArray(), 'Spot  Shipment  Car saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentCars/{id}",
     *      summary="Display the specified Spot_Shipment_Car",
     *      tags={"Spot_Shipment_Car"},
     *      description="Get Spot_Shipment_Car",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Car",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Car"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Car $spotShipmentCar */
        $spotShipmentCar = $this->spotShipmentCarRepository->findWithoutFail($id);

        if (empty($spotShipmentCar)) {
            return $this->sendError('Spot  Shipment  Car not found');
        }

        return $this->sendResponse($spotShipmentCar->toArray(), 'Spot  Shipment  Car retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_CarAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentCars/{id}",
     *      summary="Update the specified Spot_Shipment_Car in storage",
     *      tags={"Spot_Shipment_Car"},
     *      description="Update Spot_Shipment_Car",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Car",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Car that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Car")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Car"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_CarAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Car $spotShipmentCar */
        $spotShipmentCar = $this->spotShipmentCarRepository->findWithoutFail($id);

        if (empty($spotShipmentCar)) {
            return $this->sendError('Spot  Shipment  Car not found');
        }

        $spotShipmentCar = $this->spotShipmentCarRepository->update($input, $id);

        return $this->sendResponse($spotShipmentCar->toArray(), 'Spot_Shipment_Car updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentCars/{id}",
     *      summary="Remove the specified Spot_Shipment_Car from storage",
     *      tags={"Spot_Shipment_Car"},
     *      description="Delete Spot_Shipment_Car",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Car",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Car $spotShipmentCar */
        $spotShipmentCar = $this->spotShipmentCarRepository->findWithoutFail($id);

        if (empty($spotShipmentCar)) {
            return $this->sendError('Spot  Shipment  Car not found');
        }

        $spotShipmentCar->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Car deleted successfully');
    }
}
