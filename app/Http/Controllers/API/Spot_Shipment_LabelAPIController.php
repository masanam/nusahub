<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_LabelAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_LabelAPIRequest;
use App\Models\Spot_Shipment_Label;
use App\Repositories\Spot_Shipment_LabelRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_LabelController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_LabelAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_LabelRepository */
    private $spotShipmentLabelRepository;

    public function __construct(Spot_Shipment_LabelRepository $spotShipmentLabelRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentLabelRepository = $spotShipmentLabelRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentLabels",
     *      summary="Get a listing of the Spot_Shipment_Labels.",
     *      tags={"Spot_Shipment_Label"},
     *      description="Get all Spot_Shipment_Labels",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Label")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentLabelRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentLabelRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentLabels = $this->spotShipmentLabelRepository->all();

        return $this->sendResponse($spotShipmentLabels->toArray(), 'Spot  Shipment  Labels retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_LabelAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentLabels",
     *      summary="Store a newly created Spot_Shipment_Label in storage",
     *      tags={"Spot_Shipment_Label"},
     *      description="Store Spot_Shipment_Label",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Label that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Label")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Label"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_LabelAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentLabels = $this->spotShipmentLabelRepository->create($input);

        return $this->sendResponse($spotShipmentLabels->toArray(), 'Spot  Shipment  Label saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentLabels/{id}",
     *      summary="Display the specified Spot_Shipment_Label",
     *      tags={"Spot_Shipment_Label"},
     *      description="Get Spot_Shipment_Label",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Label",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Label"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Label $spotShipmentLabel */
        $spotShipmentLabel = $this->spotShipmentLabelRepository->findWithoutFail($id);

        if (empty($spotShipmentLabel)) {
            return $this->sendError('Spot  Shipment  Label not found');
        }

        return $this->sendResponse($spotShipmentLabel->toArray(), 'Spot  Shipment  Label retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_LabelAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentLabels/{id}",
     *      summary="Update the specified Spot_Shipment_Label in storage",
     *      tags={"Spot_Shipment_Label"},
     *      description="Update Spot_Shipment_Label",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Label",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Label that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Label")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Label"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_LabelAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Label $spotShipmentLabel */
        $spotShipmentLabel = $this->spotShipmentLabelRepository->findWithoutFail($id);

        if (empty($spotShipmentLabel)) {
            return $this->sendError('Spot  Shipment  Label not found');
        }

        $spotShipmentLabel = $this->spotShipmentLabelRepository->update($input, $id);

        return $this->sendResponse($spotShipmentLabel->toArray(), 'Spot_Shipment_Label updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentLabels/{id}",
     *      summary="Remove the specified Spot_Shipment_Label from storage",
     *      tags={"Spot_Shipment_Label"},
     *      description="Delete Spot_Shipment_Label",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Label",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Label $spotShipmentLabel */
        $spotShipmentLabel = $this->spotShipmentLabelRepository->findWithoutFail($id);

        if (empty($spotShipmentLabel)) {
            return $this->sendError('Spot  Shipment  Label not found');
        }

        $spotShipmentLabel->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Label deleted successfully');
    }
}
