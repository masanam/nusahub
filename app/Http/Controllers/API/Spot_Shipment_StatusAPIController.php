<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_StatusAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_StatusAPIRequest;
use App\Models\Spot_Shipment_Status;
use App\Repositories\Spot_Shipment_StatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_StatusController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_StatusAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_StatusRepository */
    private $spotShipmentStatusRepository;

    public function __construct(Spot_Shipment_StatusRepository $spotShipmentStatusRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentStatusRepository = $spotShipmentStatusRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentStatuses",
     *      summary="Get a listing of the Spot_Shipment_Statuses.",
     *      tags={"Spot_Shipment_Status"},
     *      description="Get all Spot_Shipment_Statuses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Status")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentStatusRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentStatusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentStatuses = $this->spotShipmentStatusRepository->all();

        return $this->sendResponse($spotShipmentStatuses->toArray(), 'Spot  Shipment  Statuses retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_StatusAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentStatuses",
     *      summary="Store a newly created Spot_Shipment_Status in storage",
     *      tags={"Spot_Shipment_Status"},
     *      description="Store Spot_Shipment_Status",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Status that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Status")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Status"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_StatusAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentStatuses = $this->spotShipmentStatusRepository->create($input);

        return $this->sendResponse($spotShipmentStatuses->toArray(), 'Spot  Shipment  Status saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentStatuses/{id}",
     *      summary="Display the specified Spot_Shipment_Status",
     *      tags={"Spot_Shipment_Status"},
     *      description="Get Spot_Shipment_Status",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Status",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Status"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Status $spotShipmentStatus */
        $spotShipmentStatus = $this->spotShipmentStatusRepository->findWithoutFail($id);

        if (empty($spotShipmentStatus)) {
            return $this->sendError('Spot  Shipment  Status not found');
        }

        return $this->sendResponse($spotShipmentStatus->toArray(), 'Spot  Shipment  Status retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_StatusAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentStatuses/{id}",
     *      summary="Update the specified Spot_Shipment_Status in storage",
     *      tags={"Spot_Shipment_Status"},
     *      description="Update Spot_Shipment_Status",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Status",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Status that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Status")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Status"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_StatusAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Status $spotShipmentStatus */
        $spotShipmentStatus = $this->spotShipmentStatusRepository->findWithoutFail($id);

        if (empty($spotShipmentStatus)) {
            return $this->sendError('Spot  Shipment  Status not found');
        }

        $spotShipmentStatus = $this->spotShipmentStatusRepository->update($input, $id);

        return $this->sendResponse($spotShipmentStatus->toArray(), 'Spot_Shipment_Status updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentStatuses/{id}",
     *      summary="Remove the specified Spot_Shipment_Status from storage",
     *      tags={"Spot_Shipment_Status"},
     *      description="Delete Spot_Shipment_Status",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Status",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Status $spotShipmentStatus */
        $spotShipmentStatus = $this->spotShipmentStatusRepository->findWithoutFail($id);

        if (empty($spotShipmentStatus)) {
            return $this->sendError('Spot  Shipment  Status not found');
        }

        $spotShipmentStatus->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Status deleted successfully');
    }
}
