<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_PaymentAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_PaymentAPIRequest;
use App\Models\Spot_Shipment_Payment;
use App\Repositories\Spot_Shipment_PaymentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_PaymentController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_PaymentAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_PaymentRepository */
    private $spotShipmentPaymentRepository;

    public function __construct(Spot_Shipment_PaymentRepository $spotShipmentPaymentRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentPaymentRepository = $spotShipmentPaymentRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentPayments",
     *      summary="Get a listing of the Spot_Shipment_Payments.",
     *      tags={"Spot_Shipment_Payment"},
     *      description="Get all Spot_Shipment_Payments",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Payment")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentPaymentRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentPaymentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentPayments = $this->spotShipmentPaymentRepository->all();

        return $this->sendResponse($spotShipmentPayments->toArray(), 'Spot  Shipment  Payments retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_PaymentAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentPayments",
     *      summary="Store a newly created Spot_Shipment_Payment in storage",
     *      tags={"Spot_Shipment_Payment"},
     *      description="Store Spot_Shipment_Payment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Payment that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Payment")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Payment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_PaymentAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentPayments = $this->spotShipmentPaymentRepository->create($input);

        return $this->sendResponse($spotShipmentPayments->toArray(), 'Spot  Shipment  Payment saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentPayments/{id}",
     *      summary="Display the specified Spot_Shipment_Payment",
     *      tags={"Spot_Shipment_Payment"},
     *      description="Get Spot_Shipment_Payment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Payment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Payment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Payment $spotShipmentPayment */
        $spotShipmentPayment = $this->spotShipmentPaymentRepository->findWithoutFail($id);

        if (empty($spotShipmentPayment)) {
            return $this->sendError('Spot  Shipment  Payment not found');
        }

        return $this->sendResponse($spotShipmentPayment->toArray(), 'Spot  Shipment  Payment retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_PaymentAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentPayments/{id}",
     *      summary="Update the specified Spot_Shipment_Payment in storage",
     *      tags={"Spot_Shipment_Payment"},
     *      description="Update Spot_Shipment_Payment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Payment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Payment that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Payment")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Payment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_PaymentAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Payment $spotShipmentPayment */
        $spotShipmentPayment = $this->spotShipmentPaymentRepository->findWithoutFail($id);

        if (empty($spotShipmentPayment)) {
            return $this->sendError('Spot  Shipment  Payment not found');
        }

        $spotShipmentPayment = $this->spotShipmentPaymentRepository->update($input, $id);

        return $this->sendResponse($spotShipmentPayment->toArray(), 'Spot_Shipment_Payment updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentPayments/{id}",
     *      summary="Remove the specified Spot_Shipment_Payment from storage",
     *      tags={"Spot_Shipment_Payment"},
     *      description="Delete Spot_Shipment_Payment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Payment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Payment $spotShipmentPayment */
        $spotShipmentPayment = $this->spotShipmentPaymentRepository->findWithoutFail($id);

        if (empty($spotShipmentPayment)) {
            return $this->sendError('Spot  Shipment  Payment not found');
        }

        $spotShipmentPayment->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Payment deleted successfully');
    }
}
