<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_BreakAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_BreakAPIRequest;
use App\Models\Spot_Shipment_Break;
use App\Repositories\Spot_Shipment_BreakRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_BreakController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_BreakAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_BreakRepository */
    private $spotShipmentBreakRepository;

    public function __construct(Spot_Shipment_BreakRepository $spotShipmentBreakRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentBreakRepository = $spotShipmentBreakRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentBreaks",
     *      summary="Get a listing of the Spot_Shipment_Breaks.",
     *      tags={"Spot_Shipment_Break"},
     *      description="Get all Spot_Shipment_Breaks",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Break")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentBreakRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentBreakRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentBreaks = $this->spotShipmentBreakRepository->all();

        return $this->sendResponse($spotShipmentBreaks->toArray(), 'Spot  Shipment  Breaks retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_BreakAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentBreaks",
     *      summary="Store a newly created Spot_Shipment_Break in storage",
     *      tags={"Spot_Shipment_Break"},
     *      description="Store Spot_Shipment_Break",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Break that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Break")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Break"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_BreakAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentBreaks = $this->spotShipmentBreakRepository->create($input);

        return $this->sendResponse($spotShipmentBreaks->toArray(), 'Spot  Shipment  Break saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentBreaks/{id}",
     *      summary="Display the specified Spot_Shipment_Break",
     *      tags={"Spot_Shipment_Break"},
     *      description="Get Spot_Shipment_Break",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Break",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Break"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Break $spotShipmentBreak */
        $spotShipmentBreak = $this->spotShipmentBreakRepository->findWithoutFail($id);

        if (empty($spotShipmentBreak)) {
            return $this->sendError('Spot  Shipment  Break not found');
        }

        return $this->sendResponse($spotShipmentBreak->toArray(), 'Spot  Shipment  Break retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_BreakAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentBreaks/{id}",
     *      summary="Update the specified Spot_Shipment_Break in storage",
     *      tags={"Spot_Shipment_Break"},
     *      description="Update Spot_Shipment_Break",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Break",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Break that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Break")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Break"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_BreakAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Break $spotShipmentBreak */
        $spotShipmentBreak = $this->spotShipmentBreakRepository->findWithoutFail($id);

        if (empty($spotShipmentBreak)) {
            return $this->sendError('Spot  Shipment  Break not found');
        }

        $spotShipmentBreak = $this->spotShipmentBreakRepository->update($input, $id);

        return $this->sendResponse($spotShipmentBreak->toArray(), 'Spot_Shipment_Break updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentBreaks/{id}",
     *      summary="Remove the specified Spot_Shipment_Break from storage",
     *      tags={"Spot_Shipment_Break"},
     *      description="Delete Spot_Shipment_Break",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Break",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Break $spotShipmentBreak */
        $spotShipmentBreak = $this->spotShipmentBreakRepository->findWithoutFail($id);

        if (empty($spotShipmentBreak)) {
            return $this->sendError('Spot  Shipment  Break not found');
        }

        $spotShipmentBreak->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Break deleted successfully');
    }
}
