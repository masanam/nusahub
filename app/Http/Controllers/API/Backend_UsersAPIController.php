<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBackend_UsersAPIRequest;
use App\Http\Requests\API\UpdateBackend_UsersAPIRequest;
use App\Models\Backend_Users;
use App\Repositories\Backend_UsersRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Backend_UsersController
 * @package App\Http\Controllers\API
 */

class Backend_UsersAPIController extends AppBaseController
{
    /** @var  Backend_UsersRepository */
    private $backendUsersRepository;

    public function __construct(Backend_UsersRepository $backendUsersRepo)
    {
        $this->middleware('auth:api');
        $this->backendUsersRepository = $backendUsersRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/backendUsers",
     *      summary="Get a listing of the Backend_Users.",
     *      tags={"Backend_Users"},
     *      description="Get all Backend_Users",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Backend_Users")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->backendUsersRepository->pushCriteria(new RequestCriteria($request));
        $this->backendUsersRepository->pushCriteria(new LimitOffsetCriteria($request));
        $backendUsers = $this->backendUsersRepository->all();

        return $this->sendResponse($backendUsers->toArray(), 'Backend  Users retrieved successfully');
    }

    /**
     * @param CreateBackend_UsersAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/backendUsers",
     *      summary="Store a newly created Backend_Users in storage",
     *      tags={"Backend_Users"},
     *      description="Store Backend_Users",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Backend_Users that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Backend_Users")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Backend_Users"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBackend_UsersAPIRequest $request)
    {
        $input = $request->all();

        $backendUsers = $this->backendUsersRepository->create($input);

        return $this->sendResponse($backendUsers->toArray(), 'Backend  Users saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/backendUsers/{id}",
     *      summary="Display the specified Backend_Users",
     *      tags={"Backend_Users"},
     *      description="Get Backend_Users",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Backend_Users",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Backend_Users"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Backend_Users $backendUsers */
        $backendUsers = $this->backendUsersRepository->findWithoutFail($id);

        if (empty($backendUsers)) {
            return $this->sendError('Backend  Users not found');
        }

        return $this->sendResponse($backendUsers->toArray(), 'Backend  Users retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBackend_UsersAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/backendUsers/{id}",
     *      summary="Update the specified Backend_Users in storage",
     *      tags={"Backend_Users"},
     *      description="Update Backend_Users",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Backend_Users",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Backend_Users that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Backend_Users")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Backend_Users"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBackend_UsersAPIRequest $request)
    {
        $input = $request->all();

        /** @var Backend_Users $backendUsers */
        $backendUsers = $this->backendUsersRepository->findWithoutFail($id);

        if (empty($backendUsers)) {
            return $this->sendError('Backend  Users not found');
        }

        $backendUsers = $this->backendUsersRepository->update($input, $id);

        return $this->sendResponse($backendUsers->toArray(), 'Backend_Users updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/backendUsers/{id}",
     *      summary="Remove the specified Backend_Users from storage",
     *      tags={"Backend_Users"},
     *      description="Delete Backend_Users",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Backend_Users",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Backend_Users $backendUsers */
        $backendUsers = $this->backendUsersRepository->findWithoutFail($id);

        if (empty($backendUsers)) {
            return $this->sendError('Backend  Users not found');
        }

        $backendUsers->delete();

        return $this->sendResponse($id, 'Backend  Users deleted successfully');
    }
}
