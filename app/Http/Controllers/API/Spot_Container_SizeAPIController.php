<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Container_SizeAPIRequest;
use App\Http\Requests\API\UpdateSpot_Container_SizeAPIRequest;
use App\Models\Spot_Container_Size;
use App\Repositories\Spot_Container_SizeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Container_SizeController
 * @package App\Http\Controllers\API
 */

class Spot_Container_SizeAPIController extends AppBaseController
{
    /** @var  Spot_Container_SizeRepository */
    private $spotContainerSizeRepository;

    public function __construct(Spot_Container_SizeRepository $spotContainerSizeRepo)
    {
        $this->middleware('auth:api');
        $this->spotContainerSizeRepository = $spotContainerSizeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerSizes",
     *      summary="Get a listing of the Spot_Container_Sizes.",
     *      tags={"Spot_Container_Size"},
     *      description="Get all Spot_Container_Sizes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Container_Size")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotContainerSizeRepository->pushCriteria(new RequestCriteria($request));
        $this->spotContainerSizeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotContainerSizes = $this->spotContainerSizeRepository->all();

        return $this->sendResponse($spotContainerSizes->toArray(), 'Spot  Container  Sizes retrieved successfully');
    }

    /**
     * @param CreateSpot_Container_SizeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotContainerSizes",
     *      summary="Store a newly created Spot_Container_Size in storage",
     *      tags={"Spot_Container_Size"},
     *      description="Store Spot_Container_Size",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Size that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Size")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Size"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Container_SizeAPIRequest $request)
    {
        $input = $request->all();

        $spotContainerSizes = $this->spotContainerSizeRepository->create($input);

        return $this->sendResponse($spotContainerSizes->toArray(), 'Spot  Container  Size saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerSizes/{id}",
     *      summary="Display the specified Spot_Container_Size",
     *      tags={"Spot_Container_Size"},
     *      description="Get Spot_Container_Size",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Size",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Size"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Container_Size $spotContainerSize */
        $spotContainerSize = $this->spotContainerSizeRepository->findWithoutFail($id);

        if (empty($spotContainerSize)) {
            return $this->sendError('Spot  Container  Size not found');
        }

        return $this->sendResponse($spotContainerSize->toArray(), 'Spot  Container  Size retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Container_SizeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotContainerSizes/{id}",
     *      summary="Update the specified Spot_Container_Size in storage",
     *      tags={"Spot_Container_Size"},
     *      description="Update Spot_Container_Size",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Size",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Size that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Size")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Size"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Container_SizeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Container_Size $spotContainerSize */
        $spotContainerSize = $this->spotContainerSizeRepository->findWithoutFail($id);

        if (empty($spotContainerSize)) {
            return $this->sendError('Spot  Container  Size not found');
        }

        $spotContainerSize = $this->spotContainerSizeRepository->update($input, $id);

        return $this->sendResponse($spotContainerSize->toArray(), 'Spot_Container_Size updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotContainerSizes/{id}",
     *      summary="Remove the specified Spot_Container_Size from storage",
     *      tags={"Spot_Container_Size"},
     *      description="Delete Spot_Container_Size",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Size",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Container_Size $spotContainerSize */
        $spotContainerSize = $this->spotContainerSizeRepository->findWithoutFail($id);

        if (empty($spotContainerSize)) {
            return $this->sendError('Spot  Container  Size not found');
        }

        $spotContainerSize->delete();

        return $this->sendResponse($id, 'Spot  Container  Size deleted successfully');
    }
}
