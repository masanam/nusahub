<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Container_DestinationAPIRequest;
use App\Http\Requests\API\UpdateSpot_Container_DestinationAPIRequest;
use App\Models\Spot_Container_Destination;
use App\Repositories\Spot_Container_DestinationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Container_DestinationController
 * @package App\Http\Controllers\API
 */

class Spot_Container_DestinationAPIController extends AppBaseController
{
    /** @var  Spot_Container_DestinationRepository */
    private $spotContainerDestinationRepository;

    public function __construct(Spot_Container_DestinationRepository $spotContainerDestinationRepo)
    {
        $this->middleware('auth:api');
        $this->spotContainerDestinationRepository = $spotContainerDestinationRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerDestinations",
     *      summary="Get a listing of the Spot_Container_Destinations.",
     *      tags={"Spot_Container_Destination"},
     *      description="Get all Spot_Container_Destinations",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Container_Destination")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotContainerDestinationRepository->pushCriteria(new RequestCriteria($request));
        $this->spotContainerDestinationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotContainerDestinations = $this->spotContainerDestinationRepository->all();

        return $this->sendResponse($spotContainerDestinations->toArray(), 'Spot  Container  Destinations retrieved successfully');
    }

    /**
     * @param CreateSpot_Container_DestinationAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotContainerDestinations",
     *      summary="Store a newly created Spot_Container_Destination in storage",
     *      tags={"Spot_Container_Destination"},
     *      description="Store Spot_Container_Destination",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Destination that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Destination")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Destination"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Container_DestinationAPIRequest $request)
    {
        $input = $request->all();

        $spotContainerDestinations = $this->spotContainerDestinationRepository->create($input);

        return $this->sendResponse($spotContainerDestinations->toArray(), 'Spot  Container  Destination saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerDestinations/{id}",
     *      summary="Display the specified Spot_Container_Destination",
     *      tags={"Spot_Container_Destination"},
     *      description="Get Spot_Container_Destination",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Destination",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Destination"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Container_Destination $spotContainerDestination */
        $spotContainerDestination = $this->spotContainerDestinationRepository->findWithoutFail($id);

        if (empty($spotContainerDestination)) {
            return $this->sendError('Spot  Container  Destination not found');
        }

        return $this->sendResponse($spotContainerDestination->toArray(), 'Spot  Container  Destination retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Container_DestinationAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotContainerDestinations/{id}",
     *      summary="Update the specified Spot_Container_Destination in storage",
     *      tags={"Spot_Container_Destination"},
     *      description="Update Spot_Container_Destination",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Destination",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Destination that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Destination")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Destination"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Container_DestinationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Container_Destination $spotContainerDestination */
        $spotContainerDestination = $this->spotContainerDestinationRepository->findWithoutFail($id);

        if (empty($spotContainerDestination)) {
            return $this->sendError('Spot  Container  Destination not found');
        }

        $spotContainerDestination = $this->spotContainerDestinationRepository->update($input, $id);

        return $this->sendResponse($spotContainerDestination->toArray(), 'Spot_Container_Destination updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotContainerDestinations/{id}",
     *      summary="Remove the specified Spot_Container_Destination from storage",
     *      tags={"Spot_Container_Destination"},
     *      description="Delete Spot_Container_Destination",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Destination",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Container_Destination $spotContainerDestination */
        $spotContainerDestination = $this->spotContainerDestinationRepository->findWithoutFail($id);

        if (empty($spotContainerDestination)) {
            return $this->sendError('Spot  Container  Destination not found');
        }

        $spotContainerDestination->delete();

        return $this->sendResponse($id, 'Spot  Container  Destination deleted successfully');
    }
}
