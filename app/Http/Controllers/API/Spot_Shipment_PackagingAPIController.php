<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_PackagingAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_PackagingAPIRequest;
use App\Models\Spot_Shipment_Packaging;
use App\Repositories\Spot_Shipment_PackagingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_PackagingController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_PackagingAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_PackagingRepository */
    private $spotShipmentPackagingRepository;

    public function __construct(Spot_Shipment_PackagingRepository $spotShipmentPackagingRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentPackagingRepository = $spotShipmentPackagingRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentPackagings",
     *      summary="Get a listing of the Spot_Shipment_Packagings.",
     *      tags={"Spot_Shipment_Packaging"},
     *      description="Get all Spot_Shipment_Packagings",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Packaging")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentPackagingRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentPackagingRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentPackagings = $this->spotShipmentPackagingRepository->all();

        return $this->sendResponse($spotShipmentPackagings->toArray(), 'Spot  Shipment  Packagings retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_PackagingAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentPackagings",
     *      summary="Store a newly created Spot_Shipment_Packaging in storage",
     *      tags={"Spot_Shipment_Packaging"},
     *      description="Store Spot_Shipment_Packaging",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Packaging that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Packaging")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Packaging"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_PackagingAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentPackagings = $this->spotShipmentPackagingRepository->create($input);

        return $this->sendResponse($spotShipmentPackagings->toArray(), 'Spot  Shipment  Packaging saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentPackagings/{id}",
     *      summary="Display the specified Spot_Shipment_Packaging",
     *      tags={"Spot_Shipment_Packaging"},
     *      description="Get Spot_Shipment_Packaging",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Packaging",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Packaging"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Packaging $spotShipmentPackaging */
        $spotShipmentPackaging = $this->spotShipmentPackagingRepository->findWithoutFail($id);

        if (empty($spotShipmentPackaging)) {
            return $this->sendError('Spot  Shipment  Packaging not found');
        }

        return $this->sendResponse($spotShipmentPackaging->toArray(), 'Spot  Shipment  Packaging retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_PackagingAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentPackagings/{id}",
     *      summary="Update the specified Spot_Shipment_Packaging in storage",
     *      tags={"Spot_Shipment_Packaging"},
     *      description="Update Spot_Shipment_Packaging",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Packaging",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Packaging that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Packaging")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Packaging"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_PackagingAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Packaging $spotShipmentPackaging */
        $spotShipmentPackaging = $this->spotShipmentPackagingRepository->findWithoutFail($id);

        if (empty($spotShipmentPackaging)) {
            return $this->sendError('Spot  Shipment  Packaging not found');
        }

        $spotShipmentPackaging = $this->spotShipmentPackagingRepository->update($input, $id);

        return $this->sendResponse($spotShipmentPackaging->toArray(), 'Spot_Shipment_Packaging updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentPackagings/{id}",
     *      summary="Remove the specified Spot_Shipment_Packaging from storage",
     *      tags={"Spot_Shipment_Packaging"},
     *      description="Delete Spot_Shipment_Packaging",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Packaging",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Packaging $spotShipmentPackaging */
        $spotShipmentPackaging = $this->spotShipmentPackagingRepository->findWithoutFail($id);

        if (empty($spotShipmentPackaging)) {
            return $this->sendError('Spot  Shipment  Packaging not found');
        }

        $spotShipmentPackaging->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Packaging deleted successfully');
    }
}
