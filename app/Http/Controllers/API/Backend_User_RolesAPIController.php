<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBackend_User_RolesAPIRequest;
use App\Http\Requests\API\UpdateBackend_User_RolesAPIRequest;
use App\Models\Backend_User_Roles;
use App\Repositories\Backend_User_RolesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Backend_User_RolesController
 * @package App\Http\Controllers\API
 */

class Backend_User_RolesAPIController extends AppBaseController
{
    /** @var  Backend_User_RolesRepository */
    private $backendUserRolesRepository;

    public function __construct(Backend_User_RolesRepository $backendUserRolesRepo)
    {
        $this->middleware('auth:api');
        $this->backendUserRolesRepository = $backendUserRolesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/backendUserRoles",
     *      summary="Get a listing of the Backend_User_Roles.",
     *      tags={"Backend_User_Roles"},
     *      description="Get all Backend_User_Roles",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Backend_User_Roles")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->backendUserRolesRepository->pushCriteria(new RequestCriteria($request));
        $this->backendUserRolesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $backendUserRoles = $this->backendUserRolesRepository->all();

        return $this->sendResponse($backendUserRoles->toArray(), 'Backend  User  Roles retrieved successfully');
    }

    /**
     * @param CreateBackend_User_RolesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/backendUserRoles",
     *      summary="Store a newly created Backend_User_Roles in storage",
     *      tags={"Backend_User_Roles"},
     *      description="Store Backend_User_Roles",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Backend_User_Roles that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Backend_User_Roles")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Backend_User_Roles"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBackend_User_RolesAPIRequest $request)
    {
        $input = $request->all();

        $backendUserRoles = $this->backendUserRolesRepository->create($input);

        return $this->sendResponse($backendUserRoles->toArray(), 'Backend  User  Roles saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/backendUserRoles/{id}",
     *      summary="Display the specified Backend_User_Roles",
     *      tags={"Backend_User_Roles"},
     *      description="Get Backend_User_Roles",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Backend_User_Roles",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Backend_User_Roles"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Backend_User_Roles $backendUserRoles */
        $backendUserRoles = $this->backendUserRolesRepository->findWithoutFail($id);

        if (empty($backendUserRoles)) {
            return $this->sendError('Backend  User  Roles not found');
        }

        return $this->sendResponse($backendUserRoles->toArray(), 'Backend  User  Roles retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBackend_User_RolesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/backendUserRoles/{id}",
     *      summary="Update the specified Backend_User_Roles in storage",
     *      tags={"Backend_User_Roles"},
     *      description="Update Backend_User_Roles",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Backend_User_Roles",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Backend_User_Roles that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Backend_User_Roles")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Backend_User_Roles"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBackend_User_RolesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Backend_User_Roles $backendUserRoles */
        $backendUserRoles = $this->backendUserRolesRepository->findWithoutFail($id);

        if (empty($backendUserRoles)) {
            return $this->sendError('Backend  User  Roles not found');
        }

        $backendUserRoles = $this->backendUserRolesRepository->update($input, $id);

        return $this->sendResponse($backendUserRoles->toArray(), 'Backend_User_Roles updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/backendUserRoles/{id}",
     *      summary="Remove the specified Backend_User_Roles from storage",
     *      tags={"Backend_User_Roles"},
     *      description="Delete Backend_User_Roles",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Backend_User_Roles",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Backend_User_Roles $backendUserRoles */
        $backendUserRoles = $this->backendUserRolesRepository->findWithoutFail($id);

        if (empty($backendUserRoles)) {
            return $this->sendError('Backend  User  Roles not found');
        }

        $backendUserRoles->delete();

        return $this->sendResponse($id, 'Backend  User  Roles deleted successfully');
    }
}
