<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateResponsiv_Pay_Invoice_StatusesAPIRequest;
use App\Http\Requests\API\UpdateResponsiv_Pay_Invoice_StatusesAPIRequest;
use App\Models\Responsiv_Pay_Invoice_Statuses;
use App\Repositories\Responsiv_Pay_Invoice_StatusesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Responsiv_Pay_Invoice_StatusesController
 * @package App\Http\Controllers\API
 */

class Responsiv_Pay_Invoice_StatusesAPIController extends AppBaseController
{
    /** @var  Responsiv_Pay_Invoice_StatusesRepository */
    private $responsivPayInvoiceStatusesRepository;

    public function __construct(Responsiv_Pay_Invoice_StatusesRepository $responsivPayInvoiceStatusesRepo)
    {
        $this->middleware('auth:api');
        $this->responsivPayInvoiceStatusesRepository = $responsivPayInvoiceStatusesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/responsivPayInvoiceStatuses",
     *      summary="Get a listing of the Responsiv_Pay_Invoice_Statuses.",
     *      tags={"Responsiv_Pay_Invoice_Statuses"},
     *      description="Get all Responsiv_Pay_Invoice_Statuses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Responsiv_Pay_Invoice_Statuses")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->responsivPayInvoiceStatusesRepository->pushCriteria(new RequestCriteria($request));
        $this->responsivPayInvoiceStatusesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->all();

        return $this->sendResponse($responsivPayInvoiceStatuses->toArray(), 'Responsiv  Pay  Invoice  Statuses retrieved successfully');
    }

    /**
     * @param CreateResponsiv_Pay_Invoice_StatusesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/responsivPayInvoiceStatuses",
     *      summary="Store a newly created Responsiv_Pay_Invoice_Statuses in storage",
     *      tags={"Responsiv_Pay_Invoice_Statuses"},
     *      description="Store Responsiv_Pay_Invoice_Statuses",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Responsiv_Pay_Invoice_Statuses that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Responsiv_Pay_Invoice_Statuses")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Responsiv_Pay_Invoice_Statuses"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateResponsiv_Pay_Invoice_StatusesAPIRequest $request)
    {
        $input = $request->all();

        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->create($input);

        return $this->sendResponse($responsivPayInvoiceStatuses->toArray(), 'Responsiv  Pay  Invoice  Statuses saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/responsivPayInvoiceStatuses/{id}",
     *      summary="Display the specified Responsiv_Pay_Invoice_Statuses",
     *      tags={"Responsiv_Pay_Invoice_Statuses"},
     *      description="Get Responsiv_Pay_Invoice_Statuses",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Responsiv_Pay_Invoice_Statuses",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Responsiv_Pay_Invoice_Statuses"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Responsiv_Pay_Invoice_Statuses $responsivPayInvoiceStatuses */
        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->findWithoutFail($id);

        if (empty($responsivPayInvoiceStatuses)) {
            return $this->sendError('Responsiv  Pay  Invoice  Statuses not found');
        }

        return $this->sendResponse($responsivPayInvoiceStatuses->toArray(), 'Responsiv  Pay  Invoice  Statuses retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateResponsiv_Pay_Invoice_StatusesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/responsivPayInvoiceStatuses/{id}",
     *      summary="Update the specified Responsiv_Pay_Invoice_Statuses in storage",
     *      tags={"Responsiv_Pay_Invoice_Statuses"},
     *      description="Update Responsiv_Pay_Invoice_Statuses",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Responsiv_Pay_Invoice_Statuses",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Responsiv_Pay_Invoice_Statuses that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Responsiv_Pay_Invoice_Statuses")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Responsiv_Pay_Invoice_Statuses"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateResponsiv_Pay_Invoice_StatusesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Responsiv_Pay_Invoice_Statuses $responsivPayInvoiceStatuses */
        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->findWithoutFail($id);

        if (empty($responsivPayInvoiceStatuses)) {
            return $this->sendError('Responsiv  Pay  Invoice  Statuses not found');
        }

        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->update($input, $id);

        return $this->sendResponse($responsivPayInvoiceStatuses->toArray(), 'Responsiv_Pay_Invoice_Statuses updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/responsivPayInvoiceStatuses/{id}",
     *      summary="Remove the specified Responsiv_Pay_Invoice_Statuses from storage",
     *      tags={"Responsiv_Pay_Invoice_Statuses"},
     *      description="Delete Responsiv_Pay_Invoice_Statuses",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Responsiv_Pay_Invoice_Statuses",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Responsiv_Pay_Invoice_Statuses $responsivPayInvoiceStatuses */
        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->findWithoutFail($id);

        if (empty($responsivPayInvoiceStatuses)) {
            return $this->sendError('Responsiv  Pay  Invoice  Statuses not found');
        }

        $responsivPayInvoiceStatuses->delete();

        return $this->sendResponse($id, 'Responsiv  Pay  Invoice  Statuses deleted successfully');
    }
}
