<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateResponsiv_Currency_CurrenciesAPIRequest;
use App\Http\Requests\API\UpdateResponsiv_Currency_CurrenciesAPIRequest;
use App\Models\Responsiv_Currency_Currencies;
use App\Repositories\Responsiv_Currency_CurrenciesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Responsiv_Currency_CurrenciesController
 * @package App\Http\Controllers\API
 */

class Responsiv_Currency_CurrenciesAPIController extends AppBaseController
{
    /** @var  Responsiv_Currency_CurrenciesRepository */
    private $responsivCurrencyCurrenciesRepository;

    public function __construct(Responsiv_Currency_CurrenciesRepository $responsivCurrencyCurrenciesRepo)
    {
        $this->middleware('auth:api');
        $this->responsivCurrencyCurrenciesRepository = $responsivCurrencyCurrenciesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/responsivCurrencyCurrencies",
     *      summary="Get a listing of the Responsiv_Currency_Currencies.",
     *      tags={"Responsiv_Currency_Currencies"},
     *      description="Get all Responsiv_Currency_Currencies",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Responsiv_Currency_Currencies")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->responsivCurrencyCurrenciesRepository->pushCriteria(new RequestCriteria($request));
        $this->responsivCurrencyCurrenciesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->all();

        return $this->sendResponse($responsivCurrencyCurrencies->toArray(), 'Responsiv  Currency  Currencies retrieved successfully');
    }

    /**
     * @param CreateResponsiv_Currency_CurrenciesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/responsivCurrencyCurrencies",
     *      summary="Store a newly created Responsiv_Currency_Currencies in storage",
     *      tags={"Responsiv_Currency_Currencies"},
     *      description="Store Responsiv_Currency_Currencies",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Responsiv_Currency_Currencies that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Responsiv_Currency_Currencies")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Responsiv_Currency_Currencies"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateResponsiv_Currency_CurrenciesAPIRequest $request)
    {
        $input = $request->all();

        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->create($input);

        return $this->sendResponse($responsivCurrencyCurrencies->toArray(), 'Responsiv  Currency  Currencies saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/responsivCurrencyCurrencies/{id}",
     *      summary="Display the specified Responsiv_Currency_Currencies",
     *      tags={"Responsiv_Currency_Currencies"},
     *      description="Get Responsiv_Currency_Currencies",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Responsiv_Currency_Currencies",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Responsiv_Currency_Currencies"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Responsiv_Currency_Currencies $responsivCurrencyCurrencies */
        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->findWithoutFail($id);

        if (empty($responsivCurrencyCurrencies)) {
            return $this->sendError('Responsiv  Currency  Currencies not found');
        }

        return $this->sendResponse($responsivCurrencyCurrencies->toArray(), 'Responsiv  Currency  Currencies retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateResponsiv_Currency_CurrenciesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/responsivCurrencyCurrencies/{id}",
     *      summary="Update the specified Responsiv_Currency_Currencies in storage",
     *      tags={"Responsiv_Currency_Currencies"},
     *      description="Update Responsiv_Currency_Currencies",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Responsiv_Currency_Currencies",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Responsiv_Currency_Currencies that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Responsiv_Currency_Currencies")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Responsiv_Currency_Currencies"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateResponsiv_Currency_CurrenciesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Responsiv_Currency_Currencies $responsivCurrencyCurrencies */
        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->findWithoutFail($id);

        if (empty($responsivCurrencyCurrencies)) {
            return $this->sendError('Responsiv  Currency  Currencies not found');
        }

        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->update($input, $id);

        return $this->sendResponse($responsivCurrencyCurrencies->toArray(), 'Responsiv_Currency_Currencies updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/responsivCurrencyCurrencies/{id}",
     *      summary="Remove the specified Responsiv_Currency_Currencies from storage",
     *      tags={"Responsiv_Currency_Currencies"},
     *      description="Delete Responsiv_Currency_Currencies",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Responsiv_Currency_Currencies",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Responsiv_Currency_Currencies $responsivCurrencyCurrencies */
        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->findWithoutFail($id);

        if (empty($responsivCurrencyCurrencies)) {
            return $this->sendError('Responsiv  Currency  Currencies not found');
        }

        $responsivCurrencyCurrencies->delete();

        return $this->sendResponse($id, 'Responsiv  Currency  Currencies deleted successfully');
    }
}
