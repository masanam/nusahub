<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_OrderAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_OrderAPIRequest;
use App\Models\Spot_Shipment_Order;
use App\Repositories\Spot_Shipment_OrderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_OrderController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_OrderAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_OrderRepository */
    private $spotShipmentOrderRepository;

    public function __construct(Spot_Shipment_OrderRepository $spotShipmentOrderRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentOrderRepository = $spotShipmentOrderRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentOrders",
     *      summary="Get a listing of the Spot_Shipment_Orders.",
     *      tags={"Spot_Shipment_Order"},
     *      description="Get all Spot_Shipment_Orders",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Order")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentOrderRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentOrderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentOrders = $this->spotShipmentOrderRepository->all();

        return $this->sendResponse($spotShipmentOrders->toArray(), 'Spot  Shipment  Orders retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_OrderAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentOrders",
     *      summary="Store a newly created Spot_Shipment_Order in storage",
     *      tags={"Spot_Shipment_Order"},
     *      description="Store Spot_Shipment_Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Order that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Order")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_OrderAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentOrders = $this->spotShipmentOrderRepository->create($input);

        return $this->sendResponse($spotShipmentOrders->toArray(), 'Spot  Shipment  Order saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentOrders/{id}",
     *      summary="Display the specified Spot_Shipment_Order",
     *      tags={"Spot_Shipment_Order"},
     *      description="Get Spot_Shipment_Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Order $spotShipmentOrder */
        $spotShipmentOrder = $this->spotShipmentOrderRepository->findWithoutFail($id);

        if (empty($spotShipmentOrder)) {
            return $this->sendError('Spot  Shipment  Order not found');
        }

        return $this->sendResponse($spotShipmentOrder->toArray(), 'Spot  Shipment  Order retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_OrderAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentOrders/{id}",
     *      summary="Update the specified Spot_Shipment_Order in storage",
     *      tags={"Spot_Shipment_Order"},
     *      description="Update Spot_Shipment_Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Order that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Order")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_OrderAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Order $spotShipmentOrder */
        $spotShipmentOrder = $this->spotShipmentOrderRepository->findWithoutFail($id);

        if (empty($spotShipmentOrder)) {
            return $this->sendError('Spot  Shipment  Order not found');
        }

        $spotShipmentOrder = $this->spotShipmentOrderRepository->update($input, $id);

        return $this->sendResponse($spotShipmentOrder->toArray(), 'Spot_Shipment_Order updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentOrders/{id}",
     *      summary="Remove the specified Spot_Shipment_Order from storage",
     *      tags={"Spot_Shipment_Order"},
     *      description="Delete Spot_Shipment_Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Order $spotShipmentOrder */
        $spotShipmentOrder = $this->spotShipmentOrderRepository->findWithoutFail($id);

        if (empty($spotShipmentOrder)) {
            return $this->sendError('Spot  Shipment  Order not found');
        }

        $spotShipmentOrder->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Order deleted successfully');
    }
}
