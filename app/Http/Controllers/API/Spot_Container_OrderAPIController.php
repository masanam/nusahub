<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Container_OrderAPIRequest;
use App\Http\Requests\API\UpdateSpot_Container_OrderAPIRequest;
use App\Models\Spot_Container_Order;
use App\Repositories\Spot_Container_OrderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Container_OrderController
 * @package App\Http\Controllers\API
 */

class Spot_Container_OrderAPIController extends AppBaseController
{
    /** @var  Spot_Container_OrderRepository */
    private $spotContainerOrderRepository;

    public function __construct(Spot_Container_OrderRepository $spotContainerOrderRepo)
    {
        $this->middleware('auth:api');
        $this->spotContainerOrderRepository = $spotContainerOrderRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerOrders",
     *      summary="Get a listing of the Spot_Container_Orders.",
     *      tags={"Spot_Container_Order"},
     *      description="Get all Spot_Container_Orders",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Container_Order")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotContainerOrderRepository->pushCriteria(new RequestCriteria($request));
        $this->spotContainerOrderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotContainerOrders = $this->spotContainerOrderRepository->all();

        return $this->sendResponse($spotContainerOrders->toArray(), 'Spot  Container  Orders retrieved successfully');
    }

    /**
     * @param CreateSpot_Container_OrderAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotContainerOrders",
     *      summary="Store a newly created Spot_Container_Order in storage",
     *      tags={"Spot_Container_Order"},
     *      description="Store Spot_Container_Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Order that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Order")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Container_OrderAPIRequest $request)
    {
        $input = $request->all();

        $spotContainerOrders = $this->spotContainerOrderRepository->create($input);

        return $this->sendResponse($spotContainerOrders->toArray(), 'Spot  Container  Order saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerOrders/{id}",
     *      summary="Display the specified Spot_Container_Order",
     *      tags={"Spot_Container_Order"},
     *      description="Get Spot_Container_Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Container_Order $spotContainerOrder */
        $spotContainerOrder = $this->spotContainerOrderRepository->findWithoutFail($id);

        if (empty($spotContainerOrder)) {
            return $this->sendError('Spot  Container  Order not found');
        }

        return $this->sendResponse($spotContainerOrder->toArray(), 'Spot  Container  Order retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Container_OrderAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotContainerOrders/{id}",
     *      summary="Update the specified Spot_Container_Order in storage",
     *      tags={"Spot_Container_Order"},
     *      description="Update Spot_Container_Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Order that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Order")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Container_OrderAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Container_Order $spotContainerOrder */
        $spotContainerOrder = $this->spotContainerOrderRepository->findWithoutFail($id);

        if (empty($spotContainerOrder)) {
            return $this->sendError('Spot  Container  Order not found');
        }

        $spotContainerOrder = $this->spotContainerOrderRepository->update($input, $id);

        return $this->sendResponse($spotContainerOrder->toArray(), 'Spot_Container_Order updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotContainerOrders/{id}",
     *      summary="Remove the specified Spot_Container_Order from storage",
     *      tags={"Spot_Container_Order"},
     *      description="Delete Spot_Container_Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Container_Order $spotContainerOrder */
        $spotContainerOrder = $this->spotContainerOrderRepository->findWithoutFail($id);

        if (empty($spotContainerOrder)) {
            return $this->sendError('Spot  Container  Order not found');
        }

        $spotContainerOrder->delete();

        return $this->sendResponse($id, 'Spot  Container  Order deleted successfully');
    }
}
