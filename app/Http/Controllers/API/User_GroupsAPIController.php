<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUser_GroupsAPIRequest;
use App\Http\Requests\API\UpdateUser_GroupsAPIRequest;
use App\Models\User_Groups;
use App\Repositories\User_GroupsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class User_GroupsController
 * @package App\Http\Controllers\API
 */

class User_GroupsAPIController extends AppBaseController
{
    /** @var  User_GroupsRepository */
    private $userGroupsRepository;

    public function __construct(User_GroupsRepository $userGroupsRepo)
    {
        $this->middleware('auth:api');
        $this->userGroupsRepository = $userGroupsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/userGroups",
     *      summary="Get a listing of the User_Groups.",
     *      tags={"User_Groups"},
     *      description="Get all User_Groups",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/User_Groups")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->userGroupsRepository->pushCriteria(new RequestCriteria($request));
        $this->userGroupsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $userGroups = $this->userGroupsRepository->all();

        return $this->sendResponse($userGroups->toArray(), 'User  Groups retrieved successfully');
    }

    /**
     * @param CreateUser_GroupsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/userGroups",
     *      summary="Store a newly created User_Groups in storage",
     *      tags={"User_Groups"},
     *      description="Store User_Groups",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User_Groups that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/User_Groups")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/User_Groups"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateUser_GroupsAPIRequest $request)
    {
        $input = $request->all();

        $userGroups = $this->userGroupsRepository->create($input);

        return $this->sendResponse($userGroups->toArray(), 'User  Groups saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/userGroups/{id}",
     *      summary="Display the specified User_Groups",
     *      tags={"User_Groups"},
     *      description="Get User_Groups",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of User_Groups",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/User_Groups"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var User_Groups $userGroups */
        $userGroups = $this->userGroupsRepository->findWithoutFail($id);

        if (empty($userGroups)) {
            return $this->sendError('User  Groups not found');
        }

        return $this->sendResponse($userGroups->toArray(), 'User  Groups retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateUser_GroupsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/userGroups/{id}",
     *      summary="Update the specified User_Groups in storage",
     *      tags={"User_Groups"},
     *      description="Update User_Groups",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of User_Groups",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User_Groups that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/User_Groups")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/User_Groups"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateUser_GroupsAPIRequest $request)
    {
        $input = $request->all();

        /** @var User_Groups $userGroups */
        $userGroups = $this->userGroupsRepository->findWithoutFail($id);

        if (empty($userGroups)) {
            return $this->sendError('User  Groups not found');
        }

        $userGroups = $this->userGroupsRepository->update($input, $id);

        return $this->sendResponse($userGroups->toArray(), 'User_Groups updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/userGroups/{id}",
     *      summary="Remove the specified User_Groups from storage",
     *      tags={"User_Groups"},
     *      description="Delete User_Groups",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of User_Groups",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var User_Groups $userGroups */
        $userGroups = $this->userGroupsRepository->findWithoutFail($id);

        if (empty($userGroups)) {
            return $this->sendError('User  Groups not found');
        }

        $userGroups->delete();

        return $this->sendResponse($id, 'User  Groups deleted successfully');
    }
}
