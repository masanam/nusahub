<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_CityAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_CityAPIRequest;
use App\Models\Spot_Shipment_City;
use App\Repositories\Spot_Shipment_CityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_CityController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_CityAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_CityRepository */
    private $spotShipmentCityRepository;

    public function __construct(Spot_Shipment_CityRepository $spotShipmentCityRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentCityRepository = $spotShipmentCityRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentCities",
     *      summary="Get a listing of the Spot_Shipment_Cities.",
     *      tags={"Spot_Shipment_City"},
     *      description="Get all Spot_Shipment_Cities",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_City")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentCityRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentCityRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentCities = $this->spotShipmentCityRepository->all();

        return $this->sendResponse($spotShipmentCities->toArray(), 'Spot  Shipment  Cities retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_CityAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentCities",
     *      summary="Store a newly created Spot_Shipment_City in storage",
     *      tags={"Spot_Shipment_City"},
     *      description="Store Spot_Shipment_City",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_City that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_City")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_City"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_CityAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentCities = $this->spotShipmentCityRepository->create($input);

        return $this->sendResponse($spotShipmentCities->toArray(), 'Spot  Shipment  City saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentCities/{id}",
     *      summary="Display the specified Spot_Shipment_City",
     *      tags={"Spot_Shipment_City"},
     *      description="Get Spot_Shipment_City",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_City",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_City"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_City $spotShipmentCity */
        $spotShipmentCity = $this->spotShipmentCityRepository->findWithoutFail($id);

        if (empty($spotShipmentCity)) {
            return $this->sendError('Spot  Shipment  City not found');
        }

        return $this->sendResponse($spotShipmentCity->toArray(), 'Spot  Shipment  City retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_CityAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentCities/{id}",
     *      summary="Update the specified Spot_Shipment_City in storage",
     *      tags={"Spot_Shipment_City"},
     *      description="Update Spot_Shipment_City",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_City",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_City that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_City")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_City"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_CityAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_City $spotShipmentCity */
        $spotShipmentCity = $this->spotShipmentCityRepository->findWithoutFail($id);

        if (empty($spotShipmentCity)) {
            return $this->sendError('Spot  Shipment  City not found');
        }

        $spotShipmentCity = $this->spotShipmentCityRepository->update($input, $id);

        return $this->sendResponse($spotShipmentCity->toArray(), 'Spot_Shipment_City updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentCities/{id}",
     *      summary="Remove the specified Spot_Shipment_City from storage",
     *      tags={"Spot_Shipment_City"},
     *      description="Delete Spot_Shipment_City",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_City",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_City $spotShipmentCity */
        $spotShipmentCity = $this->spotShipmentCityRepository->findWithoutFail($id);

        if (empty($spotShipmentCity)) {
            return $this->sendError('Spot  Shipment  City not found');
        }

        $spotShipmentCity->delete();

        return $this->sendResponse($id, 'Spot  Shipment  City deleted successfully');
    }
}
