<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRainlab_Location_CountriesAPIRequest;
use App\Http\Requests\API\UpdateRainlab_Location_CountriesAPIRequest;
use App\Models\Rainlab_Location_Countries;
use App\Repositories\Rainlab_Location_CountriesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Rainlab_Location_CountriesController
 * @package App\Http\Controllers\API
 */

class Rainlab_Location_CountriesAPIController extends AppBaseController
{
    /** @var  Rainlab_Location_CountriesRepository */
    private $rainlabLocationCountriesRepository;

    public function __construct(Rainlab_Location_CountriesRepository $rainlabLocationCountriesRepo)
    {
        $this->middleware('auth:api');
        $this->rainlabLocationCountriesRepository = $rainlabLocationCountriesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/rainlabLocationCountries",
     *      summary="Get a listing of the Rainlab_Location_Countries.",
     *      tags={"Rainlab_Location_Countries"},
     *      description="Get all Rainlab_Location_Countries",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Rainlab_Location_Countries")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->rainlabLocationCountriesRepository->pushCriteria(new RequestCriteria($request));
        $this->rainlabLocationCountriesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->all();

        return $this->sendResponse($rainlabLocationCountries->toArray(), 'Rainlab  Location  Countries retrieved successfully');
    }

    /**
     * @param CreateRainlab_Location_CountriesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/rainlabLocationCountries",
     *      summary="Store a newly created Rainlab_Location_Countries in storage",
     *      tags={"Rainlab_Location_Countries"},
     *      description="Store Rainlab_Location_Countries",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Rainlab_Location_Countries that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Rainlab_Location_Countries")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rainlab_Location_Countries"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRainlab_Location_CountriesAPIRequest $request)
    {
        $input = $request->all();

        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->create($input);

        return $this->sendResponse($rainlabLocationCountries->toArray(), 'Rainlab  Location  Countries saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/rainlabLocationCountries/{id}",
     *      summary="Display the specified Rainlab_Location_Countries",
     *      tags={"Rainlab_Location_Countries"},
     *      description="Get Rainlab_Location_Countries",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rainlab_Location_Countries",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rainlab_Location_Countries"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Rainlab_Location_Countries $rainlabLocationCountries */
        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->findWithoutFail($id);

        if (empty($rainlabLocationCountries)) {
            return $this->sendError('Rainlab  Location  Countries not found');
        }

        return $this->sendResponse($rainlabLocationCountries->toArray(), 'Rainlab  Location  Countries retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateRainlab_Location_CountriesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/rainlabLocationCountries/{id}",
     *      summary="Update the specified Rainlab_Location_Countries in storage",
     *      tags={"Rainlab_Location_Countries"},
     *      description="Update Rainlab_Location_Countries",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rainlab_Location_Countries",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Rainlab_Location_Countries that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Rainlab_Location_Countries")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rainlab_Location_Countries"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRainlab_Location_CountriesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Rainlab_Location_Countries $rainlabLocationCountries */
        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->findWithoutFail($id);

        if (empty($rainlabLocationCountries)) {
            return $this->sendError('Rainlab  Location  Countries not found');
        }

        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->update($input, $id);

        return $this->sendResponse($rainlabLocationCountries->toArray(), 'Rainlab_Location_Countries updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/rainlabLocationCountries/{id}",
     *      summary="Remove the specified Rainlab_Location_Countries from storage",
     *      tags={"Rainlab_Location_Countries"},
     *      description="Delete Rainlab_Location_Countries",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rainlab_Location_Countries",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Rainlab_Location_Countries $rainlabLocationCountries */
        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->findWithoutFail($id);

        if (empty($rainlabLocationCountries)) {
            return $this->sendError('Rainlab  Location  Countries not found');
        }

        $rainlabLocationCountries->delete();

        return $this->sendResponse($id, 'Rainlab  Location  Countries deleted successfully');
    }
}
