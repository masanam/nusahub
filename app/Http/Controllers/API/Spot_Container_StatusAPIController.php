<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Container_StatusAPIRequest;
use App\Http\Requests\API\UpdateSpot_Container_StatusAPIRequest;
use App\Models\Spot_Container_Status;
use App\Repositories\Spot_Container_StatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Container_StatusController
 * @package App\Http\Controllers\API
 */

class Spot_Container_StatusAPIController extends AppBaseController
{
    /** @var  Spot_Container_StatusRepository */
    private $spotContainerStatusRepository;

    public function __construct(Spot_Container_StatusRepository $spotContainerStatusRepo)
    {
        $this->middleware('auth:api');
        $this->spotContainerStatusRepository = $spotContainerStatusRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerStatuses",
     *      summary="Get a listing of the Spot_Container_Statuses.",
     *      tags={"Spot_Container_Status"},
     *      description="Get all Spot_Container_Statuses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Container_Status")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotContainerStatusRepository->pushCriteria(new RequestCriteria($request));
        $this->spotContainerStatusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotContainerStatuses = $this->spotContainerStatusRepository->all();

        return $this->sendResponse($spotContainerStatuses->toArray(), 'Spot  Container  Statuses retrieved successfully');
    }

    /**
     * @param CreateSpot_Container_StatusAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotContainerStatuses",
     *      summary="Store a newly created Spot_Container_Status in storage",
     *      tags={"Spot_Container_Status"},
     *      description="Store Spot_Container_Status",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Status that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Status")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Status"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Container_StatusAPIRequest $request)
    {
        $input = $request->all();

        $spotContainerStatuses = $this->spotContainerStatusRepository->create($input);

        return $this->sendResponse($spotContainerStatuses->toArray(), 'Spot  Container  Status saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerStatuses/{id}",
     *      summary="Display the specified Spot_Container_Status",
     *      tags={"Spot_Container_Status"},
     *      description="Get Spot_Container_Status",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Status",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Status"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Container_Status $spotContainerStatus */
        $spotContainerStatus = $this->spotContainerStatusRepository->findWithoutFail($id);

        if (empty($spotContainerStatus)) {
            return $this->sendError('Spot  Container  Status not found');
        }

        return $this->sendResponse($spotContainerStatus->toArray(), 'Spot  Container  Status retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Container_StatusAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotContainerStatuses/{id}",
     *      summary="Update the specified Spot_Container_Status in storage",
     *      tags={"Spot_Container_Status"},
     *      description="Update Spot_Container_Status",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Status",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Status that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Status")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Status"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Container_StatusAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Container_Status $spotContainerStatus */
        $spotContainerStatus = $this->spotContainerStatusRepository->findWithoutFail($id);

        if (empty($spotContainerStatus)) {
            return $this->sendError('Spot  Container  Status not found');
        }

        $spotContainerStatus = $this->spotContainerStatusRepository->update($input, $id);

        return $this->sendResponse($spotContainerStatus->toArray(), 'Spot_Container_Status updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotContainerStatuses/{id}",
     *      summary="Remove the specified Spot_Container_Status from storage",
     *      tags={"Spot_Container_Status"},
     *      description="Delete Spot_Container_Status",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Status",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Container_Status $spotContainerStatus */
        $spotContainerStatus = $this->spotContainerStatusRepository->findWithoutFail($id);

        if (empty($spotContainerStatus)) {
            return $this->sendError('Spot  Container  Status not found');
        }

        $spotContainerStatus->delete();

        return $this->sendResponse($id, 'Spot  Container  Status deleted successfully');
    }
}
