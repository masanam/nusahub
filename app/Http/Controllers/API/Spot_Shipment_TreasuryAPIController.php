<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_TreasuryAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_TreasuryAPIRequest;
use App\Models\Spot_Shipment_Treasury;
use App\Repositories\Spot_Shipment_TreasuryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_TreasuryController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_TreasuryAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_TreasuryRepository */
    private $spotShipmentTreasuryRepository;

    public function __construct(Spot_Shipment_TreasuryRepository $spotShipmentTreasuryRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentTreasuryRepository = $spotShipmentTreasuryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentTreasuries",
     *      summary="Get a listing of the Spot_Shipment_Treasuries.",
     *      tags={"Spot_Shipment_Treasury"},
     *      description="Get all Spot_Shipment_Treasuries",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Treasury")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentTreasuryRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentTreasuryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentTreasuries = $this->spotShipmentTreasuryRepository->all();

        return $this->sendResponse($spotShipmentTreasuries->toArray(), 'Spot  Shipment  Treasuries retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_TreasuryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentTreasuries",
     *      summary="Store a newly created Spot_Shipment_Treasury in storage",
     *      tags={"Spot_Shipment_Treasury"},
     *      description="Store Spot_Shipment_Treasury",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Treasury that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Treasury")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Treasury"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_TreasuryAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentTreasuries = $this->spotShipmentTreasuryRepository->create($input);

        return $this->sendResponse($spotShipmentTreasuries->toArray(), 'Spot  Shipment  Treasury saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentTreasuries/{id}",
     *      summary="Display the specified Spot_Shipment_Treasury",
     *      tags={"Spot_Shipment_Treasury"},
     *      description="Get Spot_Shipment_Treasury",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Treasury",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Treasury"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Treasury $spotShipmentTreasury */
        $spotShipmentTreasury = $this->spotShipmentTreasuryRepository->findWithoutFail($id);

        if (empty($spotShipmentTreasury)) {
            return $this->sendError('Spot  Shipment  Treasury not found');
        }

        return $this->sendResponse($spotShipmentTreasury->toArray(), 'Spot  Shipment  Treasury retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_TreasuryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentTreasuries/{id}",
     *      summary="Update the specified Spot_Shipment_Treasury in storage",
     *      tags={"Spot_Shipment_Treasury"},
     *      description="Update Spot_Shipment_Treasury",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Treasury",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Treasury that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Treasury")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Treasury"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_TreasuryAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Treasury $spotShipmentTreasury */
        $spotShipmentTreasury = $this->spotShipmentTreasuryRepository->findWithoutFail($id);

        if (empty($spotShipmentTreasury)) {
            return $this->sendError('Spot  Shipment  Treasury not found');
        }

        $spotShipmentTreasury = $this->spotShipmentTreasuryRepository->update($input, $id);

        return $this->sendResponse($spotShipmentTreasury->toArray(), 'Spot_Shipment_Treasury updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentTreasuries/{id}",
     *      summary="Remove the specified Spot_Shipment_Treasury from storage",
     *      tags={"Spot_Shipment_Treasury"},
     *      description="Delete Spot_Shipment_Treasury",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Treasury",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Treasury $spotShipmentTreasury */
        $spotShipmentTreasury = $this->spotShipmentTreasuryRepository->findWithoutFail($id);

        if (empty($spotShipmentTreasury)) {
            return $this->sendError('Spot  Shipment  Treasury not found');
        }

        $spotShipmentTreasury->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Treasury deleted successfully');
    }
}
