<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Container_ReasonAPIRequest;
use App\Http\Requests\API\UpdateSpot_Container_ReasonAPIRequest;
use App\Models\Spot_Container_Reason;
use App\Repositories\Spot_Container_ReasonRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Container_ReasonController
 * @package App\Http\Controllers\API
 */

class Spot_Container_ReasonAPIController extends AppBaseController
{
    /** @var  Spot_Container_ReasonRepository */
    private $spotContainerReasonRepository;

    public function __construct(Spot_Container_ReasonRepository $spotContainerReasonRepo)
    {
        $this->middleware('auth:api');
        $this->spotContainerReasonRepository = $spotContainerReasonRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerReasons",
     *      summary="Get a listing of the Spot_Container_Reasons.",
     *      tags={"Spot_Container_Reason"},
     *      description="Get all Spot_Container_Reasons",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Container_Reason")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotContainerReasonRepository->pushCriteria(new RequestCriteria($request));
        $this->spotContainerReasonRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotContainerReasons = $this->spotContainerReasonRepository->all();

        return $this->sendResponse($spotContainerReasons->toArray(), 'Spot  Container  Reasons retrieved successfully');
    }

    /**
     * @param CreateSpot_Container_ReasonAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotContainerReasons",
     *      summary="Store a newly created Spot_Container_Reason in storage",
     *      tags={"Spot_Container_Reason"},
     *      description="Store Spot_Container_Reason",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Reason that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Reason")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Reason"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Container_ReasonAPIRequest $request)
    {
        $input = $request->all();

        $spotContainerReasons = $this->spotContainerReasonRepository->create($input);

        return $this->sendResponse($spotContainerReasons->toArray(), 'Spot  Container  Reason saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerReasons/{id}",
     *      summary="Display the specified Spot_Container_Reason",
     *      tags={"Spot_Container_Reason"},
     *      description="Get Spot_Container_Reason",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Reason",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Reason"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Container_Reason $spotContainerReason */
        $spotContainerReason = $this->spotContainerReasonRepository->findWithoutFail($id);

        if (empty($spotContainerReason)) {
            return $this->sendError('Spot  Container  Reason not found');
        }

        return $this->sendResponse($spotContainerReason->toArray(), 'Spot  Container  Reason retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Container_ReasonAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotContainerReasons/{id}",
     *      summary="Update the specified Spot_Container_Reason in storage",
     *      tags={"Spot_Container_Reason"},
     *      description="Update Spot_Container_Reason",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Reason",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Reason that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Reason")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Reason"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Container_ReasonAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Container_Reason $spotContainerReason */
        $spotContainerReason = $this->spotContainerReasonRepository->findWithoutFail($id);

        if (empty($spotContainerReason)) {
            return $this->sendError('Spot  Container  Reason not found');
        }

        $spotContainerReason = $this->spotContainerReasonRepository->update($input, $id);

        return $this->sendResponse($spotContainerReason->toArray(), 'Spot_Container_Reason updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotContainerReasons/{id}",
     *      summary="Remove the specified Spot_Container_Reason from storage",
     *      tags={"Spot_Container_Reason"},
     *      description="Delete Spot_Container_Reason",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Reason",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Container_Reason $spotContainerReason */
        $spotContainerReason = $this->spotContainerReasonRepository->findWithoutFail($id);

        if (empty($spotContainerReason)) {
            return $this->sendError('Spot  Container  Reason not found');
        }

        $spotContainerReason->delete();

        return $this->sendResponse($id, 'Spot  Container  Reason deleted successfully');
    }
}
