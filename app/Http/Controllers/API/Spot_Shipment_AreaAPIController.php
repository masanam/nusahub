<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_AreaAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_AreaAPIRequest;
use App\Models\Spot_Shipment_Area;
use App\Repositories\Spot_Shipment_AreaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_AreaController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_AreaAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_AreaRepository */
    private $spotShipmentAreaRepository;

    public function __construct(Spot_Shipment_AreaRepository $spotShipmentAreaRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentAreaRepository = $spotShipmentAreaRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentAreas",
     *      summary="Get a listing of the Spot_Shipment_Areas.",
     *      tags={"Spot_Shipment_Area"},
     *      description="Get all Spot_Shipment_Areas",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Area")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentAreaRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentAreaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentAreas = $this->spotShipmentAreaRepository->all();

        return $this->sendResponse($spotShipmentAreas->toArray(), 'Spot  Shipment  Areas retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_AreaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentAreas",
     *      summary="Store a newly created Spot_Shipment_Area in storage",
     *      tags={"Spot_Shipment_Area"},
     *      description="Store Spot_Shipment_Area",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Area that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Area")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Area"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_AreaAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentAreas = $this->spotShipmentAreaRepository->create($input);

        return $this->sendResponse($spotShipmentAreas->toArray(), 'Spot  Shipment  Area saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentAreas/{id}",
     *      summary="Display the specified Spot_Shipment_Area",
     *      tags={"Spot_Shipment_Area"},
     *      description="Get Spot_Shipment_Area",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Area",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Area"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Area $spotShipmentArea */
        $spotShipmentArea = $this->spotShipmentAreaRepository->findWithoutFail($id);

        if (empty($spotShipmentArea)) {
            return $this->sendError('Spot  Shipment  Area not found');
        }

        return $this->sendResponse($spotShipmentArea->toArray(), 'Spot  Shipment  Area retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_AreaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentAreas/{id}",
     *      summary="Update the specified Spot_Shipment_Area in storage",
     *      tags={"Spot_Shipment_Area"},
     *      description="Update Spot_Shipment_Area",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Area",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Area that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Area")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Area"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_AreaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Area $spotShipmentArea */
        $spotShipmentArea = $this->spotShipmentAreaRepository->findWithoutFail($id);

        if (empty($spotShipmentArea)) {
            return $this->sendError('Spot  Shipment  Area not found');
        }

        $spotShipmentArea = $this->spotShipmentAreaRepository->update($input, $id);

        return $this->sendResponse($spotShipmentArea->toArray(), 'Spot_Shipment_Area updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentAreas/{id}",
     *      summary="Remove the specified Spot_Shipment_Area from storage",
     *      tags={"Spot_Shipment_Area"},
     *      description="Delete Spot_Shipment_Area",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Area",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Area $spotShipmentArea */
        $spotShipmentArea = $this->spotShipmentAreaRepository->findWithoutFail($id);

        if (empty($spotShipmentArea)) {
            return $this->sendError('Spot  Shipment  Area not found');
        }

        $spotShipmentArea->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Area deleted successfully');
    }
}
