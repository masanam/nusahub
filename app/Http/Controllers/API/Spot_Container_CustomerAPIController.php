<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Container_CustomerAPIRequest;
use App\Http\Requests\API\UpdateSpot_Container_CustomerAPIRequest;
use App\Models\Spot_Container_Customer;
use App\Repositories\Spot_Container_CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Container_CustomerController
 * @package App\Http\Controllers\API
 */

class Spot_Container_CustomerAPIController extends AppBaseController
{
    /** @var  Spot_Container_CustomerRepository */
    private $spotContainerCustomerRepository;

    public function __construct(Spot_Container_CustomerRepository $spotContainerCustomerRepo)
    {
        $this->middleware('auth:api');
        $this->spotContainerCustomerRepository = $spotContainerCustomerRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerCustomers",
     *      summary="Get a listing of the Spot_Container_Customers.",
     *      tags={"Spot_Container_Customer"},
     *      description="Get all Spot_Container_Customers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Container_Customer")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotContainerCustomerRepository->pushCriteria(new RequestCriteria($request));
        $this->spotContainerCustomerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotContainerCustomers = $this->spotContainerCustomerRepository->all();

        return $this->sendResponse($spotContainerCustomers->toArray(), 'Spot  Container  Customers retrieved successfully');
    }

    /**
     * @param CreateSpot_Container_CustomerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotContainerCustomers",
     *      summary="Store a newly created Spot_Container_Customer in storage",
     *      tags={"Spot_Container_Customer"},
     *      description="Store Spot_Container_Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Customer that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Customer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Customer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Container_CustomerAPIRequest $request)
    {
        $input = $request->all();

        $spotContainerCustomers = $this->spotContainerCustomerRepository->create($input);

        return $this->sendResponse($spotContainerCustomers->toArray(), 'Spot  Container  Customer saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotContainerCustomers/{id}",
     *      summary="Display the specified Spot_Container_Customer",
     *      tags={"Spot_Container_Customer"},
     *      description="Get Spot_Container_Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Customer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Customer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Container_Customer $spotContainerCustomer */
        $spotContainerCustomer = $this->spotContainerCustomerRepository->findWithoutFail($id);

        if (empty($spotContainerCustomer)) {
            return $this->sendError('Spot  Container  Customer not found');
        }

        return $this->sendResponse($spotContainerCustomer->toArray(), 'Spot  Container  Customer retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Container_CustomerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotContainerCustomers/{id}",
     *      summary="Update the specified Spot_Container_Customer in storage",
     *      tags={"Spot_Container_Customer"},
     *      description="Update Spot_Container_Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Customer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Container_Customer that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Container_Customer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Container_Customer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Container_CustomerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Container_Customer $spotContainerCustomer */
        $spotContainerCustomer = $this->spotContainerCustomerRepository->findWithoutFail($id);

        if (empty($spotContainerCustomer)) {
            return $this->sendError('Spot  Container  Customer not found');
        }

        $spotContainerCustomer = $this->spotContainerCustomerRepository->update($input, $id);

        return $this->sendResponse($spotContainerCustomer->toArray(), 'Spot_Container_Customer updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotContainerCustomers/{id}",
     *      summary="Remove the specified Spot_Container_Customer from storage",
     *      tags={"Spot_Container_Customer"},
     *      description="Delete Spot_Container_Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Container_Customer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Container_Customer $spotContainerCustomer */
        $spotContainerCustomer = $this->spotContainerCustomerRepository->findWithoutFail($id);

        if (empty($spotContainerCustomer)) {
            return $this->sendError('Spot  Container  Customer not found');
        }

        $spotContainerCustomer->delete();

        return $this->sendResponse($id, 'Spot  Container  Customer deleted successfully');
    }
}
