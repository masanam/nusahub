<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBackend_User_GroupsAPIRequest;
use App\Http\Requests\API\UpdateBackend_User_GroupsAPIRequest;
use App\Models\Backend_User_Groups;
use App\Repositories\Backend_User_GroupsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Backend_User_GroupsController
 * @package App\Http\Controllers\API
 */

class Backend_User_GroupsAPIController extends AppBaseController
{
    /** @var  Backend_User_GroupsRepository */
    private $backendUserGroupsRepository;

    public function __construct(Backend_User_GroupsRepository $backendUserGroupsRepo)
    {
        $this->middleware('auth:api');
        $this->backendUserGroupsRepository = $backendUserGroupsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/backendUserGroups",
     *      summary="Get a listing of the Backend_User_Groups.",
     *      tags={"Backend_User_Groups"},
     *      description="Get all Backend_User_Groups",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Backend_User_Groups")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->backendUserGroupsRepository->pushCriteria(new RequestCriteria($request));
        $this->backendUserGroupsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $backendUserGroups = $this->backendUserGroupsRepository->all();

        return $this->sendResponse($backendUserGroups->toArray(), 'Backend  User  Groups retrieved successfully');
    }

    /**
     * @param CreateBackend_User_GroupsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/backendUserGroups",
     *      summary="Store a newly created Backend_User_Groups in storage",
     *      tags={"Backend_User_Groups"},
     *      description="Store Backend_User_Groups",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Backend_User_Groups that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Backend_User_Groups")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Backend_User_Groups"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBackend_User_GroupsAPIRequest $request)
    {
        $input = $request->all();

        $backendUserGroups = $this->backendUserGroupsRepository->create($input);

        return $this->sendResponse($backendUserGroups->toArray(), 'Backend  User  Groups saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/backendUserGroups/{id}",
     *      summary="Display the specified Backend_User_Groups",
     *      tags={"Backend_User_Groups"},
     *      description="Get Backend_User_Groups",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Backend_User_Groups",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Backend_User_Groups"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Backend_User_Groups $backendUserGroups */
        $backendUserGroups = $this->backendUserGroupsRepository->findWithoutFail($id);

        if (empty($backendUserGroups)) {
            return $this->sendError('Backend  User  Groups not found');
        }

        return $this->sendResponse($backendUserGroups->toArray(), 'Backend  User  Groups retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBackend_User_GroupsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/backendUserGroups/{id}",
     *      summary="Update the specified Backend_User_Groups in storage",
     *      tags={"Backend_User_Groups"},
     *      description="Update Backend_User_Groups",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Backend_User_Groups",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Backend_User_Groups that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Backend_User_Groups")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Backend_User_Groups"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBackend_User_GroupsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Backend_User_Groups $backendUserGroups */
        $backendUserGroups = $this->backendUserGroupsRepository->findWithoutFail($id);

        if (empty($backendUserGroups)) {
            return $this->sendError('Backend  User  Groups not found');
        }

        $backendUserGroups = $this->backendUserGroupsRepository->update($input, $id);

        return $this->sendResponse($backendUserGroups->toArray(), 'Backend_User_Groups updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/backendUserGroups/{id}",
     *      summary="Remove the specified Backend_User_Groups from storage",
     *      tags={"Backend_User_Groups"},
     *      description="Delete Backend_User_Groups",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Backend_User_Groups",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Backend_User_Groups $backendUserGroups */
        $backendUserGroups = $this->backendUserGroupsRepository->findWithoutFail($id);

        if (empty($backendUserGroups)) {
            return $this->sendError('Backend  User  Groups not found');
        }

        $backendUserGroups->delete();

        return $this->sendResponse($id, 'Backend  User  Groups deleted successfully');
    }
}
