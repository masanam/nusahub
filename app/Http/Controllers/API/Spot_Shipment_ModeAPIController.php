<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpot_Shipment_ModeAPIRequest;
use App\Http\Requests\API\UpdateSpot_Shipment_ModeAPIRequest;
use App\Models\Spot_Shipment_Mode;
use App\Repositories\Spot_Shipment_ModeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Webcore\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class Spot_Shipment_ModeController
 * @package App\Http\Controllers\API
 */

class Spot_Shipment_ModeAPIController extends AppBaseController
{
    /** @var  Spot_Shipment_ModeRepository */
    private $spotShipmentModeRepository;

    public function __construct(Spot_Shipment_ModeRepository $spotShipmentModeRepo)
    {
        $this->middleware('auth:api');
        $this->spotShipmentModeRepository = $spotShipmentModeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentModes",
     *      summary="Get a listing of the Spot_Shipment_Modes.",
     *      tags={"Spot_Shipment_Mode"},
     *      description="Get all Spot_Shipment_Modes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Spot_Shipment_Mode")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->spotShipmentModeRepository->pushCriteria(new RequestCriteria($request));
        $this->spotShipmentModeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $spotShipmentModes = $this->spotShipmentModeRepository->all();

        return $this->sendResponse($spotShipmentModes->toArray(), 'Spot  Shipment  Modes retrieved successfully');
    }

    /**
     * @param CreateSpot_Shipment_ModeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/spotShipmentModes",
     *      summary="Store a newly created Spot_Shipment_Mode in storage",
     *      tags={"Spot_Shipment_Mode"},
     *      description="Store Spot_Shipment_Mode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Mode that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Mode")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Mode"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpot_Shipment_ModeAPIRequest $request)
    {
        $input = $request->all();

        $spotShipmentModes = $this->spotShipmentModeRepository->create($input);

        return $this->sendResponse($spotShipmentModes->toArray(), 'Spot  Shipment  Mode saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/spotShipmentModes/{id}",
     *      summary="Display the specified Spot_Shipment_Mode",
     *      tags={"Spot_Shipment_Mode"},
     *      description="Get Spot_Shipment_Mode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Mode",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Mode"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Spot_Shipment_Mode $spotShipmentMode */
        $spotShipmentMode = $this->spotShipmentModeRepository->findWithoutFail($id);

        if (empty($spotShipmentMode)) {
            return $this->sendError('Spot  Shipment  Mode not found');
        }

        return $this->sendResponse($spotShipmentMode->toArray(), 'Spot  Shipment  Mode retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpot_Shipment_ModeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/spotShipmentModes/{id}",
     *      summary="Update the specified Spot_Shipment_Mode in storage",
     *      tags={"Spot_Shipment_Mode"},
     *      description="Update Spot_Shipment_Mode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Mode",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Spot_Shipment_Mode that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Spot_Shipment_Mode")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Spot_Shipment_Mode"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpot_Shipment_ModeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Spot_Shipment_Mode $spotShipmentMode */
        $spotShipmentMode = $this->spotShipmentModeRepository->findWithoutFail($id);

        if (empty($spotShipmentMode)) {
            return $this->sendError('Spot  Shipment  Mode not found');
        }

        $spotShipmentMode = $this->spotShipmentModeRepository->update($input, $id);

        return $this->sendResponse($spotShipmentMode->toArray(), 'Spot_Shipment_Mode updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/spotShipmentModes/{id}",
     *      summary="Remove the specified Spot_Shipment_Mode from storage",
     *      tags={"Spot_Shipment_Mode"},
     *      description="Delete Spot_Shipment_Mode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Spot_Shipment_Mode",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Spot_Shipment_Mode $spotShipmentMode */
        $spotShipmentMode = $this->spotShipmentModeRepository->findWithoutFail($id);

        if (empty($spotShipmentMode)) {
            return $this->sendError('Spot  Shipment  Mode not found');
        }

        $spotShipmentMode->delete();

        return $this->sendResponse($id, 'Spot  Shipment  Mode deleted successfully');
    }
}
