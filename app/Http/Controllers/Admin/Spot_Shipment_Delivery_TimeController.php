<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_Delivery_TimeDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_Delivery_TimeRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_Delivery_TimeRequest;
use App\Repositories\Spot_Shipment_Delivery_TimeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_Delivery_TimeController extends AppBaseController
{
    /** @var  Spot_Shipment_Delivery_TimeRepository */
    private $spotShipmentDeliveryTimeRepository;

    public function __construct(Spot_Shipment_Delivery_TimeRepository $spotShipmentDeliveryTimeRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentDeliveryTimeRepository = $spotShipmentDeliveryTimeRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Delivery_Time.
     *
     * @param Spot_Shipment_Delivery_TimeDataTable $spotShipmentDeliveryTimeDataTable
     * @return Response
     */
    public function index(Spot_Shipment_Delivery_TimeDataTable $spotShipmentDeliveryTimeDataTable)
    {
        return $spotShipmentDeliveryTimeDataTable->render('admin.spot__shipment__delivery__times.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Delivery_Time.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__delivery__times.create');
        return view('admin.spot__shipment__delivery__times.create');
    }

    /**
     * Store a newly created Spot_Shipment_Delivery_Time in storage.
     *
     * @param CreateSpot_Shipment_Delivery_TimeRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_Delivery_TimeRequest $request)
    {
        $input = $request->all();

        $spotShipmentDeliveryTime = $this->spotShipmentDeliveryTimeRepository->create($input);

        Flash::success('Spot  Shipment  Delivery  Time saved successfully.');

        return redirect(route('admin.spotShipmentDeliveryTimes.index'));
    }

    /**
     * Display the specified Spot_Shipment_Delivery_Time.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentDeliveryTime = $this->spotShipmentDeliveryTimeRepository->findWithoutFail($id);

        if (empty($spotShipmentDeliveryTime)) {
            Flash::error('Spot  Shipment  Delivery  Time not found');

            return redirect(route('admin.spotShipmentDeliveryTimes.index'));
        }

        return view('admin.spot__shipment__delivery__times.show')->with('spotShipmentDeliveryTime', $spotShipmentDeliveryTime);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Delivery_Time.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentDeliveryTime = $this->spotShipmentDeliveryTimeRepository->findWithoutFail($id);

        if (empty($spotShipmentDeliveryTime)) {
            Flash::error('Spot  Shipment  Delivery  Time not found');

            return redirect(route('admin.spotShipmentDeliveryTimes.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__delivery__times.edit')->with('spotShipmentDeliveryTime', $spotShipmentDeliveryTime);
        return view('admin.spot__shipment__delivery__times.edit')
            ->with('spotShipmentDeliveryTime', $spotShipmentDeliveryTime);        
    }

    /**
     * Update the specified Spot_Shipment_Delivery_Time in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_Delivery_TimeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_Delivery_TimeRequest $request)
    {
        $spotShipmentDeliveryTime = $this->spotShipmentDeliveryTimeRepository->findWithoutFail($id);

        if (empty($spotShipmentDeliveryTime)) {
            Flash::error('Spot  Shipment  Delivery  Time not found');

            return redirect(route('admin.spotShipmentDeliveryTimes.index'));
        }

        $spotShipmentDeliveryTime = $this->spotShipmentDeliveryTimeRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Delivery  Time updated successfully.');

        return redirect(route('admin.spotShipmentDeliveryTimes.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Delivery_Time from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentDeliveryTime = $this->spotShipmentDeliveryTimeRepository->findWithoutFail($id);

        if (empty($spotShipmentDeliveryTime)) {
            Flash::error('Spot  Shipment  Delivery  Time not found');

            return redirect(route('admin.spotShipmentDeliveryTimes.index'));
        }

        $this->spotShipmentDeliveryTimeRepository->delete($id);

        Flash::success('Spot  Shipment  Delivery  Time deleted successfully.');

        return redirect(route('admin.spotShipmentDeliveryTimes.index'));
    }

    /**
     * Store data Spot_Shipment_Delivery_Time from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentDeliveryTime = $this->spotShipmentDeliveryTimeRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Delivery  Time saved successfully.');

        return redirect(route('admin.spotShipmentDeliveryTimes.index'));
    }
}
