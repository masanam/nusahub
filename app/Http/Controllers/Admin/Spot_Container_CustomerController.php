<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Container_CustomerDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Container_CustomerRequest;
use App\Http\Requests\Admin\UpdateSpot_Container_CustomerRequest;
use App\Repositories\Spot_Container_CustomerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Container_CustomerController extends AppBaseController
{
    /** @var  Spot_Container_CustomerRepository */
    private $spotContainerCustomerRepository;

    public function __construct(Spot_Container_CustomerRepository $spotContainerCustomerRepo)
    {
        $this->middleware('auth');
        $this->spotContainerCustomerRepository = $spotContainerCustomerRepo;
    }

    /**
     * Display a listing of the Spot_Container_Customer.
     *
     * @param Spot_Container_CustomerDataTable $spotContainerCustomerDataTable
     * @return Response
     */
    public function index(Spot_Container_CustomerDataTable $spotContainerCustomerDataTable)
    {
        return $spotContainerCustomerDataTable->render('admin.spot__container__customers.index');
    }

    /**
     * Show the form for creating a new Spot_Container_Customer.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__container__customers.create');
        return view('admin.spot__container__customers.create');
    }

    /**
     * Store a newly created Spot_Container_Customer in storage.
     *
     * @param CreateSpot_Container_CustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Container_CustomerRequest $request)
    {
        $input = $request->all();

        $spotContainerCustomer = $this->spotContainerCustomerRepository->create($input);

        Flash::success('Spot  Container  Customer saved successfully.');

        return redirect(route('admin.spotContainerCustomers.index'));
    }

    /**
     * Display the specified Spot_Container_Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotContainerCustomer = $this->spotContainerCustomerRepository->findWithoutFail($id);

        if (empty($spotContainerCustomer)) {
            Flash::error('Spot  Container  Customer not found');

            return redirect(route('admin.spotContainerCustomers.index'));
        }

        return view('admin.spot__container__customers.show')->with('spotContainerCustomer', $spotContainerCustomer);
    }

    /**
     * Show the form for editing the specified Spot_Container_Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotContainerCustomer = $this->spotContainerCustomerRepository->findWithoutFail($id);

        if (empty($spotContainerCustomer)) {
            Flash::error('Spot  Container  Customer not found');

            return redirect(route('admin.spotContainerCustomers.index'));
        }

        // edited by dandisy
        // return view('admin.spot__container__customers.edit')->with('spotContainerCustomer', $spotContainerCustomer);
        return view('admin.spot__container__customers.edit')
            ->with('spotContainerCustomer', $spotContainerCustomer);        
    }

    /**
     * Update the specified Spot_Container_Customer in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Container_CustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Container_CustomerRequest $request)
    {
        $spotContainerCustomer = $this->spotContainerCustomerRepository->findWithoutFail($id);

        if (empty($spotContainerCustomer)) {
            Flash::error('Spot  Container  Customer not found');

            return redirect(route('admin.spotContainerCustomers.index'));
        }

        $spotContainerCustomer = $this->spotContainerCustomerRepository->update($request->all(), $id);

        Flash::success('Spot  Container  Customer updated successfully.');

        return redirect(route('admin.spotContainerCustomers.index'));
    }

    /**
     * Remove the specified Spot_Container_Customer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotContainerCustomer = $this->spotContainerCustomerRepository->findWithoutFail($id);

        if (empty($spotContainerCustomer)) {
            Flash::error('Spot  Container  Customer not found');

            return redirect(route('admin.spotContainerCustomers.index'));
        }

        $this->spotContainerCustomerRepository->delete($id);

        Flash::success('Spot  Container  Customer deleted successfully.');

        return redirect(route('admin.spotContainerCustomers.index'));
    }

    /**
     * Store data Spot_Container_Customer from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotContainerCustomer = $this->spotContainerCustomerRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Container  Customer saved successfully.');

        return redirect(route('admin.spotContainerCustomers.index'));
    }
}
