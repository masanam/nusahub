<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_AreaDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_AreaRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_AreaRequest;
use App\Repositories\Spot_Shipment_AreaRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_AreaController extends AppBaseController
{
    /** @var  Spot_Shipment_AreaRepository */
    private $spotShipmentAreaRepository;

    public function __construct(Spot_Shipment_AreaRepository $spotShipmentAreaRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentAreaRepository = $spotShipmentAreaRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Area.
     *
     * @param Spot_Shipment_AreaDataTable $spotShipmentAreaDataTable
     * @return Response
     */
    public function index(Spot_Shipment_AreaDataTable $spotShipmentAreaDataTable)
    {
        return $spotShipmentAreaDataTable->render('admin.spot__shipment__areas.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Area.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__areas.create');
        return view('admin.spot__shipment__areas.create');
    }

    /**
     * Store a newly created Spot_Shipment_Area in storage.
     *
     * @param CreateSpot_Shipment_AreaRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_AreaRequest $request)
    {
        $input = $request->all();

        $spotShipmentArea = $this->spotShipmentAreaRepository->create($input);

        Flash::success('Spot  Shipment  Area saved successfully.');

        return redirect(route('admin.spotShipmentAreas.index'));
    }

    /**
     * Display the specified Spot_Shipment_Area.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentArea = $this->spotShipmentAreaRepository->findWithoutFail($id);

        if (empty($spotShipmentArea)) {
            Flash::error('Spot  Shipment  Area not found');

            return redirect(route('admin.spotShipmentAreas.index'));
        }

        return view('admin.spot__shipment__areas.show')->with('spotShipmentArea', $spotShipmentArea);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Area.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentArea = $this->spotShipmentAreaRepository->findWithoutFail($id);

        if (empty($spotShipmentArea)) {
            Flash::error('Spot  Shipment  Area not found');

            return redirect(route('admin.spotShipmentAreas.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__areas.edit')->with('spotShipmentArea', $spotShipmentArea);
        return view('admin.spot__shipment__areas.edit')
            ->with('spotShipmentArea', $spotShipmentArea);        
    }

    /**
     * Update the specified Spot_Shipment_Area in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_AreaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_AreaRequest $request)
    {
        $spotShipmentArea = $this->spotShipmentAreaRepository->findWithoutFail($id);

        if (empty($spotShipmentArea)) {
            Flash::error('Spot  Shipment  Area not found');

            return redirect(route('admin.spotShipmentAreas.index'));
        }

        $spotShipmentArea = $this->spotShipmentAreaRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Area updated successfully.');

        return redirect(route('admin.spotShipmentAreas.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Area from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentArea = $this->spotShipmentAreaRepository->findWithoutFail($id);

        if (empty($spotShipmentArea)) {
            Flash::error('Spot  Shipment  Area not found');

            return redirect(route('admin.spotShipmentAreas.index'));
        }

        $this->spotShipmentAreaRepository->delete($id);

        Flash::success('Spot  Shipment  Area deleted successfully.');

        return redirect(route('admin.spotShipmentAreas.index'));
    }

    /**
     * Store data Spot_Shipment_Area from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentArea = $this->spotShipmentAreaRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Area saved successfully.');

        return redirect(route('admin.spotShipmentAreas.index'));
    }
}
