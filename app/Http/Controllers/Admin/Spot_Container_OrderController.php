<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Container_OrderDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Container_OrderRequest;
use App\Http\Requests\Admin\UpdateSpot_Container_OrderRequest;
use App\Repositories\Spot_Container_OrderRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Container_OrderController extends AppBaseController
{
    /** @var  Spot_Container_OrderRepository */
    private $spotContainerOrderRepository;

    public function __construct(Spot_Container_OrderRepository $spotContainerOrderRepo)
    {
        $this->middleware('auth');
        $this->spotContainerOrderRepository = $spotContainerOrderRepo;
    }

    /**
     * Display a listing of the Spot_Container_Order.
     *
     * @param Spot_Container_OrderDataTable $spotContainerOrderDataTable
     * @return Response
     */
    public function index(Spot_Container_OrderDataTable $spotContainerOrderDataTable)
    {
        return $spotContainerOrderDataTable->render('admin.spot__container__orders.index');
    }

    /**
     * Show the form for creating a new Spot_Container_Order.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__container__orders.create');
        return view('admin.spot__container__orders.create');
    }

    /**
     * Store a newly created Spot_Container_Order in storage.
     *
     * @param CreateSpot_Container_OrderRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Container_OrderRequest $request)
    {
        $input = $request->all();

        $spotContainerOrder = $this->spotContainerOrderRepository->create($input);

        Flash::success('Spot  Container  Order saved successfully.');

        return redirect(route('admin.spotContainerOrders.index'));
    }

    /**
     * Display the specified Spot_Container_Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotContainerOrder = $this->spotContainerOrderRepository->findWithoutFail($id);

        if (empty($spotContainerOrder)) {
            Flash::error('Spot  Container  Order not found');

            return redirect(route('admin.spotContainerOrders.index'));
        }

        return view('admin.spot__container__orders.show')->with('spotContainerOrder', $spotContainerOrder);
    }

    /**
     * Show the form for editing the specified Spot_Container_Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotContainerOrder = $this->spotContainerOrderRepository->findWithoutFail($id);

        if (empty($spotContainerOrder)) {
            Flash::error('Spot  Container  Order not found');

            return redirect(route('admin.spotContainerOrders.index'));
        }

        // edited by dandisy
        // return view('admin.spot__container__orders.edit')->with('spotContainerOrder', $spotContainerOrder);
        return view('admin.spot__container__orders.edit')
            ->with('spotContainerOrder', $spotContainerOrder);        
    }

    /**
     * Update the specified Spot_Container_Order in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Container_OrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Container_OrderRequest $request)
    {
        $spotContainerOrder = $this->spotContainerOrderRepository->findWithoutFail($id);

        if (empty($spotContainerOrder)) {
            Flash::error('Spot  Container  Order not found');

            return redirect(route('admin.spotContainerOrders.index'));
        }

        $spotContainerOrder = $this->spotContainerOrderRepository->update($request->all(), $id);

        Flash::success('Spot  Container  Order updated successfully.');

        return redirect(route('admin.spotContainerOrders.index'));
    }

    /**
     * Remove the specified Spot_Container_Order from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotContainerOrder = $this->spotContainerOrderRepository->findWithoutFail($id);

        if (empty($spotContainerOrder)) {
            Flash::error('Spot  Container  Order not found');

            return redirect(route('admin.spotContainerOrders.index'));
        }

        $this->spotContainerOrderRepository->delete($id);

        Flash::success('Spot  Container  Order deleted successfully.');

        return redirect(route('admin.spotContainerOrders.index'));
    }

    /**
     * Store data Spot_Container_Order from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotContainerOrder = $this->spotContainerOrderRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Container  Order saved successfully.');

        return redirect(route('admin.spotContainerOrders.index'));
    }
}
