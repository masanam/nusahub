<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_TreasuryDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_TreasuryRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_TreasuryRequest;
use App\Repositories\Spot_Shipment_TreasuryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_TreasuryController extends AppBaseController
{
    /** @var  Spot_Shipment_TreasuryRepository */
    private $spotShipmentTreasuryRepository;

    public function __construct(Spot_Shipment_TreasuryRepository $spotShipmentTreasuryRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentTreasuryRepository = $spotShipmentTreasuryRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Treasury.
     *
     * @param Spot_Shipment_TreasuryDataTable $spotShipmentTreasuryDataTable
     * @return Response
     */
    public function index(Spot_Shipment_TreasuryDataTable $spotShipmentTreasuryDataTable)
    {
        return $spotShipmentTreasuryDataTable->render('admin.spot__shipment__treasuries.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Treasury.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__treasuries.create');
        return view('admin.spot__shipment__treasuries.create');
    }

    /**
     * Store a newly created Spot_Shipment_Treasury in storage.
     *
     * @param CreateSpot_Shipment_TreasuryRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_TreasuryRequest $request)
    {
        $input = $request->all();

        $spotShipmentTreasury = $this->spotShipmentTreasuryRepository->create($input);

        Flash::success('Spot  Shipment  Treasury saved successfully.');

        return redirect(route('admin.spotShipmentTreasuries.index'));
    }

    /**
     * Display the specified Spot_Shipment_Treasury.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentTreasury = $this->spotShipmentTreasuryRepository->findWithoutFail($id);

        if (empty($spotShipmentTreasury)) {
            Flash::error('Spot  Shipment  Treasury not found');

            return redirect(route('admin.spotShipmentTreasuries.index'));
        }

        return view('admin.spot__shipment__treasuries.show')->with('spotShipmentTreasury', $spotShipmentTreasury);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Treasury.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentTreasury = $this->spotShipmentTreasuryRepository->findWithoutFail($id);

        if (empty($spotShipmentTreasury)) {
            Flash::error('Spot  Shipment  Treasury not found');

            return redirect(route('admin.spotShipmentTreasuries.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__treasuries.edit')->with('spotShipmentTreasury', $spotShipmentTreasury);
        return view('admin.spot__shipment__treasuries.edit')
            ->with('spotShipmentTreasury', $spotShipmentTreasury);        
    }

    /**
     * Update the specified Spot_Shipment_Treasury in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_TreasuryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_TreasuryRequest $request)
    {
        $spotShipmentTreasury = $this->spotShipmentTreasuryRepository->findWithoutFail($id);

        if (empty($spotShipmentTreasury)) {
            Flash::error('Spot  Shipment  Treasury not found');

            return redirect(route('admin.spotShipmentTreasuries.index'));
        }

        $spotShipmentTreasury = $this->spotShipmentTreasuryRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Treasury updated successfully.');

        return redirect(route('admin.spotShipmentTreasuries.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Treasury from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentTreasury = $this->spotShipmentTreasuryRepository->findWithoutFail($id);

        if (empty($spotShipmentTreasury)) {
            Flash::error('Spot  Shipment  Treasury not found');

            return redirect(route('admin.spotShipmentTreasuries.index'));
        }

        $this->spotShipmentTreasuryRepository->delete($id);

        Flash::success('Spot  Shipment  Treasury deleted successfully.');

        return redirect(route('admin.spotShipmentTreasuries.index'));
    }

    /**
     * Store data Spot_Shipment_Treasury from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentTreasury = $this->spotShipmentTreasuryRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Treasury saved successfully.');

        return redirect(route('admin.spotShipmentTreasuries.index'));
    }
}
