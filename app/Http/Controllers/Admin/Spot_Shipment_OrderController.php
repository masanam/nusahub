<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_OrderDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_OrderRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_OrderRequest;
use App\Repositories\Spot_Shipment_OrderRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_OrderController extends AppBaseController
{
    /** @var  Spot_Shipment_OrderRepository */
    private $spotShipmentOrderRepository;

    public function __construct(Spot_Shipment_OrderRepository $spotShipmentOrderRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentOrderRepository = $spotShipmentOrderRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Order.
     *
     * @param Spot_Shipment_OrderDataTable $spotShipmentOrderDataTable
     * @return Response
     */
    public function index(Spot_Shipment_OrderDataTable $spotShipmentOrderDataTable)
    {
        return $spotShipmentOrderDataTable->render('admin.spot__shipment__orders.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Order.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__orders.create');
        return view('admin.spot__shipment__orders.create');
    }

    /**
     * Store a newly created Spot_Shipment_Order in storage.
     *
     * @param CreateSpot_Shipment_OrderRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_OrderRequest $request)
    {
        $input = $request->all();

        $spotShipmentOrder = $this->spotShipmentOrderRepository->create($input);

        Flash::success('Spot  Shipment  Order saved successfully.');

        return redirect(route('admin.spotShipmentOrders.index'));
    }

    /**
     * Display the specified Spot_Shipment_Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentOrder = $this->spotShipmentOrderRepository->findWithoutFail($id);

        if (empty($spotShipmentOrder)) {
            Flash::error('Spot  Shipment  Order not found');

            return redirect(route('admin.spotShipmentOrders.index'));
        }

        return view('admin.spot__shipment__orders.show')->with('spotShipmentOrder', $spotShipmentOrder);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentOrder = $this->spotShipmentOrderRepository->findWithoutFail($id);

        if (empty($spotShipmentOrder)) {
            Flash::error('Spot  Shipment  Order not found');

            return redirect(route('admin.spotShipmentOrders.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__orders.edit')->with('spotShipmentOrder', $spotShipmentOrder);
        return view('admin.spot__shipment__orders.edit')
            ->with('spotShipmentOrder', $spotShipmentOrder);        
    }

    /**
     * Update the specified Spot_Shipment_Order in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_OrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_OrderRequest $request)
    {
        $spotShipmentOrder = $this->spotShipmentOrderRepository->findWithoutFail($id);

        if (empty($spotShipmentOrder)) {
            Flash::error('Spot  Shipment  Order not found');

            return redirect(route('admin.spotShipmentOrders.index'));
        }

        $spotShipmentOrder = $this->spotShipmentOrderRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Order updated successfully.');

        return redirect(route('admin.spotShipmentOrders.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Order from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentOrder = $this->spotShipmentOrderRepository->findWithoutFail($id);

        if (empty($spotShipmentOrder)) {
            Flash::error('Spot  Shipment  Order not found');

            return redirect(route('admin.spotShipmentOrders.index'));
        }

        $this->spotShipmentOrderRepository->delete($id);

        Flash::success('Spot  Shipment  Order deleted successfully.');

        return redirect(route('admin.spotShipmentOrders.index'));
    }

    /**
     * Store data Spot_Shipment_Order from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentOrder = $this->spotShipmentOrderRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Order saved successfully.');

        return redirect(route('admin.spotShipmentOrders.index'));
    }
}
