<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_StatusDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_StatusRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_StatusRequest;
use App\Repositories\Spot_Shipment_StatusRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_StatusController extends AppBaseController
{
    /** @var  Spot_Shipment_StatusRepository */
    private $spotShipmentStatusRepository;

    public function __construct(Spot_Shipment_StatusRepository $spotShipmentStatusRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentStatusRepository = $spotShipmentStatusRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Status.
     *
     * @param Spot_Shipment_StatusDataTable $spotShipmentStatusDataTable
     * @return Response
     */
    public function index(Spot_Shipment_StatusDataTable $spotShipmentStatusDataTable)
    {
        return $spotShipmentStatusDataTable->render('admin.spot__shipment__statuses.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Status.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__statuses.create');
        return view('admin.spot__shipment__statuses.create');
    }

    /**
     * Store a newly created Spot_Shipment_Status in storage.
     *
     * @param CreateSpot_Shipment_StatusRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_StatusRequest $request)
    {
        $input = $request->all();

        $spotShipmentStatus = $this->spotShipmentStatusRepository->create($input);

        Flash::success('Spot  Shipment  Status saved successfully.');

        return redirect(route('admin.spotShipmentStatuses.index'));
    }

    /**
     * Display the specified Spot_Shipment_Status.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentStatus = $this->spotShipmentStatusRepository->findWithoutFail($id);

        if (empty($spotShipmentStatus)) {
            Flash::error('Spot  Shipment  Status not found');

            return redirect(route('admin.spotShipmentStatuses.index'));
        }

        return view('admin.spot__shipment__statuses.show')->with('spotShipmentStatus', $spotShipmentStatus);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Status.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentStatus = $this->spotShipmentStatusRepository->findWithoutFail($id);

        if (empty($spotShipmentStatus)) {
            Flash::error('Spot  Shipment  Status not found');

            return redirect(route('admin.spotShipmentStatuses.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__statuses.edit')->with('spotShipmentStatus', $spotShipmentStatus);
        return view('admin.spot__shipment__statuses.edit')
            ->with('spotShipmentStatus', $spotShipmentStatus);        
    }

    /**
     * Update the specified Spot_Shipment_Status in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_StatusRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_StatusRequest $request)
    {
        $spotShipmentStatus = $this->spotShipmentStatusRepository->findWithoutFail($id);

        if (empty($spotShipmentStatus)) {
            Flash::error('Spot  Shipment  Status not found');

            return redirect(route('admin.spotShipmentStatuses.index'));
        }

        $spotShipmentStatus = $this->spotShipmentStatusRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Status updated successfully.');

        return redirect(route('admin.spotShipmentStatuses.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Status from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentStatus = $this->spotShipmentStatusRepository->findWithoutFail($id);

        if (empty($spotShipmentStatus)) {
            Flash::error('Spot  Shipment  Status not found');

            return redirect(route('admin.spotShipmentStatuses.index'));
        }

        $this->spotShipmentStatusRepository->delete($id);

        Flash::success('Spot  Shipment  Status deleted successfully.');

        return redirect(route('admin.spotShipmentStatuses.index'));
    }

    /**
     * Store data Spot_Shipment_Status from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentStatus = $this->spotShipmentStatusRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Status saved successfully.');

        return redirect(route('admin.spotShipmentStatuses.index'));
    }
}
