<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Container_LocationDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Container_LocationRequest;
use App\Http\Requests\Admin\UpdateSpot_Container_LocationRequest;
use App\Repositories\Spot_Container_LocationRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Container_LocationController extends AppBaseController
{
    /** @var  Spot_Container_LocationRepository */
    private $spotContainerLocationRepository;

    public function __construct(Spot_Container_LocationRepository $spotContainerLocationRepo)
    {
        $this->middleware('auth');
        $this->spotContainerLocationRepository = $spotContainerLocationRepo;
    }

    /**
     * Display a listing of the Spot_Container_Location.
     *
     * @param Spot_Container_LocationDataTable $spotContainerLocationDataTable
     * @return Response
     */
    public function index(Spot_Container_LocationDataTable $spotContainerLocationDataTable)
    {
        return $spotContainerLocationDataTable->render('admin.spot__container__locations.index');
    }

    /**
     * Show the form for creating a new Spot_Container_Location.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__container__locations.create');
        return view('admin.spot__container__locations.create');
    }

    /**
     * Store a newly created Spot_Container_Location in storage.
     *
     * @param CreateSpot_Container_LocationRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Container_LocationRequest $request)
    {
        $input = $request->all();

        $spotContainerLocation = $this->spotContainerLocationRepository->create($input);

        Flash::success('Spot  Container  Location saved successfully.');

        return redirect(route('admin.spotContainerLocations.index'));
    }

    /**
     * Display the specified Spot_Container_Location.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotContainerLocation = $this->spotContainerLocationRepository->findWithoutFail($id);

        if (empty($spotContainerLocation)) {
            Flash::error('Spot  Container  Location not found');

            return redirect(route('admin.spotContainerLocations.index'));
        }

        return view('admin.spot__container__locations.show')->with('spotContainerLocation', $spotContainerLocation);
    }

    /**
     * Show the form for editing the specified Spot_Container_Location.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotContainerLocation = $this->spotContainerLocationRepository->findWithoutFail($id);

        if (empty($spotContainerLocation)) {
            Flash::error('Spot  Container  Location not found');

            return redirect(route('admin.spotContainerLocations.index'));
        }

        // edited by dandisy
        // return view('admin.spot__container__locations.edit')->with('spotContainerLocation', $spotContainerLocation);
        return view('admin.spot__container__locations.edit')
            ->with('spotContainerLocation', $spotContainerLocation);        
    }

    /**
     * Update the specified Spot_Container_Location in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Container_LocationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Container_LocationRequest $request)
    {
        $spotContainerLocation = $this->spotContainerLocationRepository->findWithoutFail($id);

        if (empty($spotContainerLocation)) {
            Flash::error('Spot  Container  Location not found');

            return redirect(route('admin.spotContainerLocations.index'));
        }

        $spotContainerLocation = $this->spotContainerLocationRepository->update($request->all(), $id);

        Flash::success('Spot  Container  Location updated successfully.');

        return redirect(route('admin.spotContainerLocations.index'));
    }

    /**
     * Remove the specified Spot_Container_Location from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotContainerLocation = $this->spotContainerLocationRepository->findWithoutFail($id);

        if (empty($spotContainerLocation)) {
            Flash::error('Spot  Container  Location not found');

            return redirect(route('admin.spotContainerLocations.index'));
        }

        $this->spotContainerLocationRepository->delete($id);

        Flash::success('Spot  Container  Location deleted successfully.');

        return redirect(route('admin.spotContainerLocations.index'));
    }

    /**
     * Store data Spot_Container_Location from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotContainerLocation = $this->spotContainerLocationRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Container  Location saved successfully.');

        return redirect(route('admin.spotContainerLocations.index'));
    }
}
