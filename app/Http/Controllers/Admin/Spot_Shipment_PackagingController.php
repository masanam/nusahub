<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_PackagingDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_PackagingRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_PackagingRequest;
use App\Repositories\Spot_Shipment_PackagingRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_PackagingController extends AppBaseController
{
    /** @var  Spot_Shipment_PackagingRepository */
    private $spotShipmentPackagingRepository;

    public function __construct(Spot_Shipment_PackagingRepository $spotShipmentPackagingRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentPackagingRepository = $spotShipmentPackagingRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Packaging.
     *
     * @param Spot_Shipment_PackagingDataTable $spotShipmentPackagingDataTable
     * @return Response
     */
    public function index(Spot_Shipment_PackagingDataTable $spotShipmentPackagingDataTable)
    {
        return $spotShipmentPackagingDataTable->render('admin.spot__shipment__packagings.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Packaging.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__packagings.create');
        return view('admin.spot__shipment__packagings.create');
    }

    /**
     * Store a newly created Spot_Shipment_Packaging in storage.
     *
     * @param CreateSpot_Shipment_PackagingRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_PackagingRequest $request)
    {
        $input = $request->all();

        $spotShipmentPackaging = $this->spotShipmentPackagingRepository->create($input);

        Flash::success('Spot  Shipment  Packaging saved successfully.');

        return redirect(route('admin.spotShipmentPackagings.index'));
    }

    /**
     * Display the specified Spot_Shipment_Packaging.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentPackaging = $this->spotShipmentPackagingRepository->findWithoutFail($id);

        if (empty($spotShipmentPackaging)) {
            Flash::error('Spot  Shipment  Packaging not found');

            return redirect(route('admin.spotShipmentPackagings.index'));
        }

        return view('admin.spot__shipment__packagings.show')->with('spotShipmentPackaging', $spotShipmentPackaging);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Packaging.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentPackaging = $this->spotShipmentPackagingRepository->findWithoutFail($id);

        if (empty($spotShipmentPackaging)) {
            Flash::error('Spot  Shipment  Packaging not found');

            return redirect(route('admin.spotShipmentPackagings.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__packagings.edit')->with('spotShipmentPackaging', $spotShipmentPackaging);
        return view('admin.spot__shipment__packagings.edit')
            ->with('spotShipmentPackaging', $spotShipmentPackaging);        
    }

    /**
     * Update the specified Spot_Shipment_Packaging in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_PackagingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_PackagingRequest $request)
    {
        $spotShipmentPackaging = $this->spotShipmentPackagingRepository->findWithoutFail($id);

        if (empty($spotShipmentPackaging)) {
            Flash::error('Spot  Shipment  Packaging not found');

            return redirect(route('admin.spotShipmentPackagings.index'));
        }

        $spotShipmentPackaging = $this->spotShipmentPackagingRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Packaging updated successfully.');

        return redirect(route('admin.spotShipmentPackagings.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Packaging from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentPackaging = $this->spotShipmentPackagingRepository->findWithoutFail($id);

        if (empty($spotShipmentPackaging)) {
            Flash::error('Spot  Shipment  Packaging not found');

            return redirect(route('admin.spotShipmentPackagings.index'));
        }

        $this->spotShipmentPackagingRepository->delete($id);

        Flash::success('Spot  Shipment  Packaging deleted successfully.');

        return redirect(route('admin.spotShipmentPackagings.index'));
    }

    /**
     * Store data Spot_Shipment_Packaging from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentPackaging = $this->spotShipmentPackagingRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Packaging saved successfully.');

        return redirect(route('admin.spotShipmentPackagings.index'));
    }
}
