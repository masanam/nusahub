<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Container_DestinationDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Container_DestinationRequest;
use App\Http\Requests\Admin\UpdateSpot_Container_DestinationRequest;
use App\Repositories\Spot_Container_DestinationRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Container_DestinationController extends AppBaseController
{
    /** @var  Spot_Container_DestinationRepository */
    private $spotContainerDestinationRepository;

    public function __construct(Spot_Container_DestinationRepository $spotContainerDestinationRepo)
    {
        $this->middleware('auth');
        $this->spotContainerDestinationRepository = $spotContainerDestinationRepo;
    }

    /**
     * Display a listing of the Spot_Container_Destination.
     *
     * @param Spot_Container_DestinationDataTable $spotContainerDestinationDataTable
     * @return Response
     */
    public function index(Spot_Container_DestinationDataTable $spotContainerDestinationDataTable)
    {
        return $spotContainerDestinationDataTable->render('admin.spot__container__destinations.index');
    }

    /**
     * Show the form for creating a new Spot_Container_Destination.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__container__destinations.create');
        return view('admin.spot__container__destinations.create');
    }

    /**
     * Store a newly created Spot_Container_Destination in storage.
     *
     * @param CreateSpot_Container_DestinationRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Container_DestinationRequest $request)
    {
        $input = $request->all();

        $spotContainerDestination = $this->spotContainerDestinationRepository->create($input);

        Flash::success('Spot  Container  Destination saved successfully.');

        return redirect(route('admin.spotContainerDestinations.index'));
    }

    /**
     * Display the specified Spot_Container_Destination.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotContainerDestination = $this->spotContainerDestinationRepository->findWithoutFail($id);

        if (empty($spotContainerDestination)) {
            Flash::error('Spot  Container  Destination not found');

            return redirect(route('admin.spotContainerDestinations.index'));
        }

        return view('admin.spot__container__destinations.show')->with('spotContainerDestination', $spotContainerDestination);
    }

    /**
     * Show the form for editing the specified Spot_Container_Destination.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotContainerDestination = $this->spotContainerDestinationRepository->findWithoutFail($id);

        if (empty($spotContainerDestination)) {
            Flash::error('Spot  Container  Destination not found');

            return redirect(route('admin.spotContainerDestinations.index'));
        }

        // edited by dandisy
        // return view('admin.spot__container__destinations.edit')->with('spotContainerDestination', $spotContainerDestination);
        return view('admin.spot__container__destinations.edit')
            ->with('spotContainerDestination', $spotContainerDestination);        
    }

    /**
     * Update the specified Spot_Container_Destination in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Container_DestinationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Container_DestinationRequest $request)
    {
        $spotContainerDestination = $this->spotContainerDestinationRepository->findWithoutFail($id);

        if (empty($spotContainerDestination)) {
            Flash::error('Spot  Container  Destination not found');

            return redirect(route('admin.spotContainerDestinations.index'));
        }

        $spotContainerDestination = $this->spotContainerDestinationRepository->update($request->all(), $id);

        Flash::success('Spot  Container  Destination updated successfully.');

        return redirect(route('admin.spotContainerDestinations.index'));
    }

    /**
     * Remove the specified Spot_Container_Destination from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotContainerDestination = $this->spotContainerDestinationRepository->findWithoutFail($id);

        if (empty($spotContainerDestination)) {
            Flash::error('Spot  Container  Destination not found');

            return redirect(route('admin.spotContainerDestinations.index'));
        }

        $this->spotContainerDestinationRepository->delete($id);

        Flash::success('Spot  Container  Destination deleted successfully.');

        return redirect(route('admin.spotContainerDestinations.index'));
    }

    /**
     * Store data Spot_Container_Destination from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotContainerDestination = $this->spotContainerDestinationRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Container  Destination saved successfully.');

        return redirect(route('admin.spotContainerDestinations.index'));
    }
}
