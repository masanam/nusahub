<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_OfficeDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_OfficeRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_OfficeRequest;
use App\Repositories\Spot_Shipment_OfficeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_OfficeController extends AppBaseController
{
    /** @var  Spot_Shipment_OfficeRepository */
    private $spotShipmentOfficeRepository;

    public function __construct(Spot_Shipment_OfficeRepository $spotShipmentOfficeRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentOfficeRepository = $spotShipmentOfficeRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Office.
     *
     * @param Spot_Shipment_OfficeDataTable $spotShipmentOfficeDataTable
     * @return Response
     */
    public function index(Spot_Shipment_OfficeDataTable $spotShipmentOfficeDataTable)
    {
        return $spotShipmentOfficeDataTable->render('admin.spot__shipment__offices.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Office.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__offices.create');
        return view('admin.spot__shipment__offices.create');
    }

    /**
     * Store a newly created Spot_Shipment_Office in storage.
     *
     * @param CreateSpot_Shipment_OfficeRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_OfficeRequest $request)
    {
        $input = $request->all();

        $spotShipmentOffice = $this->spotShipmentOfficeRepository->create($input);

        Flash::success('Spot  Shipment  Office saved successfully.');

        return redirect(route('admin.spotShipmentOffices.index'));
    }

    /**
     * Display the specified Spot_Shipment_Office.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentOffice = $this->spotShipmentOfficeRepository->findWithoutFail($id);

        if (empty($spotShipmentOffice)) {
            Flash::error('Spot  Shipment  Office not found');

            return redirect(route('admin.spotShipmentOffices.index'));
        }

        return view('admin.spot__shipment__offices.show')->with('spotShipmentOffice', $spotShipmentOffice);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Office.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentOffice = $this->spotShipmentOfficeRepository->findWithoutFail($id);

        if (empty($spotShipmentOffice)) {
            Flash::error('Spot  Shipment  Office not found');

            return redirect(route('admin.spotShipmentOffices.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__offices.edit')->with('spotShipmentOffice', $spotShipmentOffice);
        return view('admin.spot__shipment__offices.edit')
            ->with('spotShipmentOffice', $spotShipmentOffice);        
    }

    /**
     * Update the specified Spot_Shipment_Office in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_OfficeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_OfficeRequest $request)
    {
        $spotShipmentOffice = $this->spotShipmentOfficeRepository->findWithoutFail($id);

        if (empty($spotShipmentOffice)) {
            Flash::error('Spot  Shipment  Office not found');

            return redirect(route('admin.spotShipmentOffices.index'));
        }

        $spotShipmentOffice = $this->spotShipmentOfficeRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Office updated successfully.');

        return redirect(route('admin.spotShipmentOffices.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Office from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentOffice = $this->spotShipmentOfficeRepository->findWithoutFail($id);

        if (empty($spotShipmentOffice)) {
            Flash::error('Spot  Shipment  Office not found');

            return redirect(route('admin.spotShipmentOffices.index'));
        }

        $this->spotShipmentOfficeRepository->delete($id);

        Flash::success('Spot  Shipment  Office deleted successfully.');

        return redirect(route('admin.spotShipmentOffices.index'));
    }

    /**
     * Store data Spot_Shipment_Office from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentOffice = $this->spotShipmentOfficeRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Office saved successfully.');

        return redirect(route('admin.spotShipmentOffices.index'));
    }
}
