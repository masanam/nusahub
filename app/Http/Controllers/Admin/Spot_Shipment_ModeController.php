<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_ModeDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_ModeRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_ModeRequest;
use App\Repositories\Spot_Shipment_ModeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_ModeController extends AppBaseController
{
    /** @var  Spot_Shipment_ModeRepository */
    private $spotShipmentModeRepository;

    public function __construct(Spot_Shipment_ModeRepository $spotShipmentModeRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentModeRepository = $spotShipmentModeRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Mode.
     *
     * @param Spot_Shipment_ModeDataTable $spotShipmentModeDataTable
     * @return Response
     */
    public function index(Spot_Shipment_ModeDataTable $spotShipmentModeDataTable)
    {
        return $spotShipmentModeDataTable->render('admin.spot__shipment__modes.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Mode.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__modes.create');
        return view('admin.spot__shipment__modes.create');
    }

    /**
     * Store a newly created Spot_Shipment_Mode in storage.
     *
     * @param CreateSpot_Shipment_ModeRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_ModeRequest $request)
    {
        $input = $request->all();

        $spotShipmentMode = $this->spotShipmentModeRepository->create($input);

        Flash::success('Spot  Shipment  Mode saved successfully.');

        return redirect(route('admin.spotShipmentModes.index'));
    }

    /**
     * Display the specified Spot_Shipment_Mode.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentMode = $this->spotShipmentModeRepository->findWithoutFail($id);

        if (empty($spotShipmentMode)) {
            Flash::error('Spot  Shipment  Mode not found');

            return redirect(route('admin.spotShipmentModes.index'));
        }

        return view('admin.spot__shipment__modes.show')->with('spotShipmentMode', $spotShipmentMode);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Mode.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentMode = $this->spotShipmentModeRepository->findWithoutFail($id);

        if (empty($spotShipmentMode)) {
            Flash::error('Spot  Shipment  Mode not found');

            return redirect(route('admin.spotShipmentModes.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__modes.edit')->with('spotShipmentMode', $spotShipmentMode);
        return view('admin.spot__shipment__modes.edit')
            ->with('spotShipmentMode', $spotShipmentMode);        
    }

    /**
     * Update the specified Spot_Shipment_Mode in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_ModeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_ModeRequest $request)
    {
        $spotShipmentMode = $this->spotShipmentModeRepository->findWithoutFail($id);

        if (empty($spotShipmentMode)) {
            Flash::error('Spot  Shipment  Mode not found');

            return redirect(route('admin.spotShipmentModes.index'));
        }

        $spotShipmentMode = $this->spotShipmentModeRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Mode updated successfully.');

        return redirect(route('admin.spotShipmentModes.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Mode from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentMode = $this->spotShipmentModeRepository->findWithoutFail($id);

        if (empty($spotShipmentMode)) {
            Flash::error('Spot  Shipment  Mode not found');

            return redirect(route('admin.spotShipmentModes.index'));
        }

        $this->spotShipmentModeRepository->delete($id);

        Flash::success('Spot  Shipment  Mode deleted successfully.');

        return redirect(route('admin.spotShipmentModes.index'));
    }

    /**
     * Store data Spot_Shipment_Mode from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentMode = $this->spotShipmentModeRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Mode saved successfully.');

        return redirect(route('admin.spotShipmentModes.index'));
    }
}
