<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_CategoryDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_CategoryRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_CategoryRequest;
use App\Repositories\Spot_Shipment_CategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_CategoryController extends AppBaseController
{
    /** @var  Spot_Shipment_CategoryRepository */
    private $spotShipmentCategoryRepository;

    public function __construct(Spot_Shipment_CategoryRepository $spotShipmentCategoryRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentCategoryRepository = $spotShipmentCategoryRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Category.
     *
     * @param Spot_Shipment_CategoryDataTable $spotShipmentCategoryDataTable
     * @return Response
     */
    public function index(Spot_Shipment_CategoryDataTable $spotShipmentCategoryDataTable)
    {
        return $spotShipmentCategoryDataTable->render('admin.spot__shipment__categories.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Category.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__categories.create');
        return view('admin.spot__shipment__categories.create');
    }

    /**
     * Store a newly created Spot_Shipment_Category in storage.
     *
     * @param CreateSpot_Shipment_CategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_CategoryRequest $request)
    {
        $input = $request->all();

        $spotShipmentCategory = $this->spotShipmentCategoryRepository->create($input);

        Flash::success('Spot  Shipment  Category saved successfully.');

        return redirect(route('admin.spotShipmentCategories.index'));
    }

    /**
     * Display the specified Spot_Shipment_Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentCategory = $this->spotShipmentCategoryRepository->findWithoutFail($id);

        if (empty($spotShipmentCategory)) {
            Flash::error('Spot  Shipment  Category not found');

            return redirect(route('admin.spotShipmentCategories.index'));
        }

        return view('admin.spot__shipment__categories.show')->with('spotShipmentCategory', $spotShipmentCategory);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentCategory = $this->spotShipmentCategoryRepository->findWithoutFail($id);

        if (empty($spotShipmentCategory)) {
            Flash::error('Spot  Shipment  Category not found');

            return redirect(route('admin.spotShipmentCategories.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__categories.edit')->with('spotShipmentCategory', $spotShipmentCategory);
        return view('admin.spot__shipment__categories.edit')
            ->with('spotShipmentCategory', $spotShipmentCategory);        
    }

    /**
     * Update the specified Spot_Shipment_Category in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_CategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_CategoryRequest $request)
    {
        $spotShipmentCategory = $this->spotShipmentCategoryRepository->findWithoutFail($id);

        if (empty($spotShipmentCategory)) {
            Flash::error('Spot  Shipment  Category not found');

            return redirect(route('admin.spotShipmentCategories.index'));
        }

        $spotShipmentCategory = $this->spotShipmentCategoryRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Category updated successfully.');

        return redirect(route('admin.spotShipmentCategories.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Category from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentCategory = $this->spotShipmentCategoryRepository->findWithoutFail($id);

        if (empty($spotShipmentCategory)) {
            Flash::error('Spot  Shipment  Category not found');

            return redirect(route('admin.spotShipmentCategories.index'));
        }

        $this->spotShipmentCategoryRepository->delete($id);

        Flash::success('Spot  Shipment  Category deleted successfully.');

        return redirect(route('admin.spotShipmentCategories.index'));
    }

    /**
     * Store data Spot_Shipment_Category from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentCategory = $this->spotShipmentCategoryRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Category saved successfully.');

        return redirect(route('admin.spotShipmentCategories.index'));
    }
}
