<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Responsiv_Pay_Invoice_StatusesDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateResponsiv_Pay_Invoice_StatusesRequest;
use App\Http\Requests\Admin\UpdateResponsiv_Pay_Invoice_StatusesRequest;
use App\Repositories\Responsiv_Pay_Invoice_StatusesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Responsiv_Pay_Invoice_StatusesController extends AppBaseController
{
    /** @var  Responsiv_Pay_Invoice_StatusesRepository */
    private $responsivPayInvoiceStatusesRepository;

    public function __construct(Responsiv_Pay_Invoice_StatusesRepository $responsivPayInvoiceStatusesRepo)
    {
        $this->middleware('auth');
        $this->responsivPayInvoiceStatusesRepository = $responsivPayInvoiceStatusesRepo;
    }

    /**
     * Display a listing of the Responsiv_Pay_Invoice_Statuses.
     *
     * @param Responsiv_Pay_Invoice_StatusesDataTable $responsivPayInvoiceStatusesDataTable
     * @return Response
     */
    public function index(Responsiv_Pay_Invoice_StatusesDataTable $responsivPayInvoiceStatusesDataTable)
    {
        return $responsivPayInvoiceStatusesDataTable->render('admin.responsiv__pay__invoice__statuses.index');
    }

    /**
     * Show the form for creating a new Responsiv_Pay_Invoice_Statuses.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.responsiv__pay__invoice__statuses.create');
        return view('admin.responsiv__pay__invoice__statuses.create');
    }

    /**
     * Store a newly created Responsiv_Pay_Invoice_Statuses in storage.
     *
     * @param CreateResponsiv_Pay_Invoice_StatusesRequest $request
     *
     * @return Response
     */
    public function store(CreateResponsiv_Pay_Invoice_StatusesRequest $request)
    {
        $input = $request->all();

        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->create($input);

        Flash::success('Responsiv  Pay  Invoice  Statuses saved successfully.');

        return redirect(route('admin.responsivPayInvoiceStatuses.index'));
    }

    /**
     * Display the specified Responsiv_Pay_Invoice_Statuses.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->findWithoutFail($id);

        if (empty($responsivPayInvoiceStatuses)) {
            Flash::error('Responsiv  Pay  Invoice  Statuses not found');

            return redirect(route('admin.responsivPayInvoiceStatuses.index'));
        }

        return view('admin.responsiv__pay__invoice__statuses.show')->with('responsivPayInvoiceStatuses', $responsivPayInvoiceStatuses);
    }

    /**
     * Show the form for editing the specified Responsiv_Pay_Invoice_Statuses.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->findWithoutFail($id);

        if (empty($responsivPayInvoiceStatuses)) {
            Flash::error('Responsiv  Pay  Invoice  Statuses not found');

            return redirect(route('admin.responsivPayInvoiceStatuses.index'));
        }

        // edited by dandisy
        // return view('admin.responsiv__pay__invoice__statuses.edit')->with('responsivPayInvoiceStatuses', $responsivPayInvoiceStatuses);
        return view('admin.responsiv__pay__invoice__statuses.edit')
            ->with('responsivPayInvoiceStatuses', $responsivPayInvoiceStatuses);        
    }

    /**
     * Update the specified Responsiv_Pay_Invoice_Statuses in storage.
     *
     * @param  int              $id
     * @param UpdateResponsiv_Pay_Invoice_StatusesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResponsiv_Pay_Invoice_StatusesRequest $request)
    {
        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->findWithoutFail($id);

        if (empty($responsivPayInvoiceStatuses)) {
            Flash::error('Responsiv  Pay  Invoice  Statuses not found');

            return redirect(route('admin.responsivPayInvoiceStatuses.index'));
        }

        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->update($request->all(), $id);

        Flash::success('Responsiv  Pay  Invoice  Statuses updated successfully.');

        return redirect(route('admin.responsivPayInvoiceStatuses.index'));
    }

    /**
     * Remove the specified Responsiv_Pay_Invoice_Statuses from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->findWithoutFail($id);

        if (empty($responsivPayInvoiceStatuses)) {
            Flash::error('Responsiv  Pay  Invoice  Statuses not found');

            return redirect(route('admin.responsivPayInvoiceStatuses.index'));
        }

        $this->responsivPayInvoiceStatusesRepository->delete($id);

        Flash::success('Responsiv  Pay  Invoice  Statuses deleted successfully.');

        return redirect(route('admin.responsivPayInvoiceStatuses.index'));
    }

    /**
     * Store data Responsiv_Pay_Invoice_Statuses from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $responsivPayInvoiceStatuses = $this->responsivPayInvoiceStatusesRepository->create($item->toArray());
            });
        });

        Flash::success('Responsiv  Pay  Invoice  Statuses saved successfully.');

        return redirect(route('admin.responsivPayInvoiceStatuses.index'));
    }
}
