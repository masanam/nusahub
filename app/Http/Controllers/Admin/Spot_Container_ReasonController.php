<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Container_ReasonDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Container_ReasonRequest;
use App\Http\Requests\Admin\UpdateSpot_Container_ReasonRequest;
use App\Repositories\Spot_Container_ReasonRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Container_ReasonController extends AppBaseController
{
    /** @var  Spot_Container_ReasonRepository */
    private $spotContainerReasonRepository;

    public function __construct(Spot_Container_ReasonRepository $spotContainerReasonRepo)
    {
        $this->middleware('auth');
        $this->spotContainerReasonRepository = $spotContainerReasonRepo;
    }

    /**
     * Display a listing of the Spot_Container_Reason.
     *
     * @param Spot_Container_ReasonDataTable $spotContainerReasonDataTable
     * @return Response
     */
    public function index(Spot_Container_ReasonDataTable $spotContainerReasonDataTable)
    {
        return $spotContainerReasonDataTable->render('admin.spot__container__reasons.index');
    }

    /**
     * Show the form for creating a new Spot_Container_Reason.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__container__reasons.create');
        return view('admin.spot__container__reasons.create');
    }

    /**
     * Store a newly created Spot_Container_Reason in storage.
     *
     * @param CreateSpot_Container_ReasonRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Container_ReasonRequest $request)
    {
        $input = $request->all();

        $spotContainerReason = $this->spotContainerReasonRepository->create($input);

        Flash::success('Spot  Container  Reason saved successfully.');

        return redirect(route('admin.spotContainerReasons.index'));
    }

    /**
     * Display the specified Spot_Container_Reason.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotContainerReason = $this->spotContainerReasonRepository->findWithoutFail($id);

        if (empty($spotContainerReason)) {
            Flash::error('Spot  Container  Reason not found');

            return redirect(route('admin.spotContainerReasons.index'));
        }

        return view('admin.spot__container__reasons.show')->with('spotContainerReason', $spotContainerReason);
    }

    /**
     * Show the form for editing the specified Spot_Container_Reason.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotContainerReason = $this->spotContainerReasonRepository->findWithoutFail($id);

        if (empty($spotContainerReason)) {
            Flash::error('Spot  Container  Reason not found');

            return redirect(route('admin.spotContainerReasons.index'));
        }

        // edited by dandisy
        // return view('admin.spot__container__reasons.edit')->with('spotContainerReason', $spotContainerReason);
        return view('admin.spot__container__reasons.edit')
            ->with('spotContainerReason', $spotContainerReason);        
    }

    /**
     * Update the specified Spot_Container_Reason in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Container_ReasonRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Container_ReasonRequest $request)
    {
        $spotContainerReason = $this->spotContainerReasonRepository->findWithoutFail($id);

        if (empty($spotContainerReason)) {
            Flash::error('Spot  Container  Reason not found');

            return redirect(route('admin.spotContainerReasons.index'));
        }

        $spotContainerReason = $this->spotContainerReasonRepository->update($request->all(), $id);

        Flash::success('Spot  Container  Reason updated successfully.');

        return redirect(route('admin.spotContainerReasons.index'));
    }

    /**
     * Remove the specified Spot_Container_Reason from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotContainerReason = $this->spotContainerReasonRepository->findWithoutFail($id);

        if (empty($spotContainerReason)) {
            Flash::error('Spot  Container  Reason not found');

            return redirect(route('admin.spotContainerReasons.index'));
        }

        $this->spotContainerReasonRepository->delete($id);

        Flash::success('Spot  Container  Reason deleted successfully.');

        return redirect(route('admin.spotContainerReasons.index'));
    }

    /**
     * Store data Spot_Container_Reason from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotContainerReason = $this->spotContainerReasonRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Container  Reason saved successfully.');

        return redirect(route('admin.spotContainerReasons.index'));
    }
}
