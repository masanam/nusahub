<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Backend_UsersDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateBackend_UsersRequest;
use App\Http\Requests\Admin\UpdateBackend_UsersRequest;
use App\Repositories\Backend_UsersRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Backend_UsersController extends AppBaseController
{
    /** @var  Backend_UsersRepository */
    private $backendUsersRepository;

    public function __construct(Backend_UsersRepository $backendUsersRepo)
    {
        $this->middleware('auth');
        $this->backendUsersRepository = $backendUsersRepo;
    }

    /**
     * Display a listing of the Backend_Users.
     *
     * @param Backend_UsersDataTable $backendUsersDataTable
     * @return Response
     */
    public function index(Backend_UsersDataTable $backendUsersDataTable)
    {
        return $backendUsersDataTable->render('admin.backend__users.index');
    }

    /**
     * Show the form for creating a new Backend_Users.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.backend__users.create');
        return view('admin.backend__users.create');
    }

    /**
     * Store a newly created Backend_Users in storage.
     *
     * @param CreateBackend_UsersRequest $request
     *
     * @return Response
     */
    public function store(CreateBackend_UsersRequest $request)
    {
        $input = $request->all();

        $backendUsers = $this->backendUsersRepository->create($input);

        Flash::success('Backend  Users saved successfully.');

        return redirect(route('admin.backendUsers.index'));
    }

    /**
     * Display the specified Backend_Users.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $backendUsers = $this->backendUsersRepository->findWithoutFail($id);

        if (empty($backendUsers)) {
            Flash::error('Backend  Users not found');

            return redirect(route('admin.backendUsers.index'));
        }

        return view('admin.backend__users.show')->with('backendUsers', $backendUsers);
    }

    /**
     * Show the form for editing the specified Backend_Users.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $backendUsers = $this->backendUsersRepository->findWithoutFail($id);

        if (empty($backendUsers)) {
            Flash::error('Backend  Users not found');

            return redirect(route('admin.backendUsers.index'));
        }

        // edited by dandisy
        // return view('admin.backend__users.edit')->with('backendUsers', $backendUsers);
        return view('admin.backend__users.edit')
            ->with('backendUsers', $backendUsers);        
    }

    /**
     * Update the specified Backend_Users in storage.
     *
     * @param  int              $id
     * @param UpdateBackend_UsersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBackend_UsersRequest $request)
    {
        $backendUsers = $this->backendUsersRepository->findWithoutFail($id);

        if (empty($backendUsers)) {
            Flash::error('Backend  Users not found');

            return redirect(route('admin.backendUsers.index'));
        }

        $backendUsers = $this->backendUsersRepository->update($request->all(), $id);

        Flash::success('Backend  Users updated successfully.');

        return redirect(route('admin.backendUsers.index'));
    }

    /**
     * Remove the specified Backend_Users from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $backendUsers = $this->backendUsersRepository->findWithoutFail($id);

        if (empty($backendUsers)) {
            Flash::error('Backend  Users not found');

            return redirect(route('admin.backendUsers.index'));
        }

        $this->backendUsersRepository->delete($id);

        Flash::success('Backend  Users deleted successfully.');

        return redirect(route('admin.backendUsers.index'));
    }

    /**
     * Store data Backend_Users from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $backendUsers = $this->backendUsersRepository->create($item->toArray());
            });
        });

        Flash::success('Backend  Users saved successfully.');

        return redirect(route('admin.backendUsers.index'));
    }
}
