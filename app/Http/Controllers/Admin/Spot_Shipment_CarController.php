<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_CarDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_CarRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_CarRequest;
use App\Repositories\Spot_Shipment_CarRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_CarController extends AppBaseController
{
    /** @var  Spot_Shipment_CarRepository */
    private $spotShipmentCarRepository;

    public function __construct(Spot_Shipment_CarRepository $spotShipmentCarRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentCarRepository = $spotShipmentCarRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Car.
     *
     * @param Spot_Shipment_CarDataTable $spotShipmentCarDataTable
     * @return Response
     */
    public function index(Spot_Shipment_CarDataTable $spotShipmentCarDataTable)
    {
        return $spotShipmentCarDataTable->render('admin.spot__shipment__cars.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Car.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__cars.create');
        return view('admin.spot__shipment__cars.create');
    }

    /**
     * Store a newly created Spot_Shipment_Car in storage.
     *
     * @param CreateSpot_Shipment_CarRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_CarRequest $request)
    {
        $input = $request->all();

        $spotShipmentCar = $this->spotShipmentCarRepository->create($input);

        Flash::success('Spot  Shipment  Car saved successfully.');

        return redirect(route('admin.spotShipmentCars.index'));
    }

    /**
     * Display the specified Spot_Shipment_Car.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentCar = $this->spotShipmentCarRepository->findWithoutFail($id);

        if (empty($spotShipmentCar)) {
            Flash::error('Spot  Shipment  Car not found');

            return redirect(route('admin.spotShipmentCars.index'));
        }

        return view('admin.spot__shipment__cars.show')->with('spotShipmentCar', $spotShipmentCar);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Car.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentCar = $this->spotShipmentCarRepository->findWithoutFail($id);

        if (empty($spotShipmentCar)) {
            Flash::error('Spot  Shipment  Car not found');

            return redirect(route('admin.spotShipmentCars.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__cars.edit')->with('spotShipmentCar', $spotShipmentCar);
        return view('admin.spot__shipment__cars.edit')
            ->with('spotShipmentCar', $spotShipmentCar);        
    }

    /**
     * Update the specified Spot_Shipment_Car in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_CarRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_CarRequest $request)
    {
        $spotShipmentCar = $this->spotShipmentCarRepository->findWithoutFail($id);

        if (empty($spotShipmentCar)) {
            Flash::error('Spot  Shipment  Car not found');

            return redirect(route('admin.spotShipmentCars.index'));
        }

        $spotShipmentCar = $this->spotShipmentCarRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Car updated successfully.');

        return redirect(route('admin.spotShipmentCars.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Car from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentCar = $this->spotShipmentCarRepository->findWithoutFail($id);

        if (empty($spotShipmentCar)) {
            Flash::error('Spot  Shipment  Car not found');

            return redirect(route('admin.spotShipmentCars.index'));
        }

        $this->spotShipmentCarRepository->delete($id);

        Flash::success('Spot  Shipment  Car deleted successfully.');

        return redirect(route('admin.spotShipmentCars.index'));
    }

    /**
     * Store data Spot_Shipment_Car from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentCar = $this->spotShipmentCarRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Car saved successfully.');

        return redirect(route('admin.spotShipmentCars.index'));
    }
}
