<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Backend_User_GroupsDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateBackend_User_GroupsRequest;
use App\Http\Requests\Admin\UpdateBackend_User_GroupsRequest;
use App\Repositories\Backend_User_GroupsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Backend_User_GroupsController extends AppBaseController
{
    /** @var  Backend_User_GroupsRepository */
    private $backendUserGroupsRepository;

    public function __construct(Backend_User_GroupsRepository $backendUserGroupsRepo)
    {
        $this->middleware('auth');
        $this->backendUserGroupsRepository = $backendUserGroupsRepo;
    }

    /**
     * Display a listing of the Backend_User_Groups.
     *
     * @param Backend_User_GroupsDataTable $backendUserGroupsDataTable
     * @return Response
     */
    public function index(Backend_User_GroupsDataTable $backendUserGroupsDataTable)
    {
        return $backendUserGroupsDataTable->render('admin.backend__user__groups.index');
    }

    /**
     * Show the form for creating a new Backend_User_Groups.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.backend__user__groups.create');
        return view('admin.backend__user__groups.create');
    }

    /**
     * Store a newly created Backend_User_Groups in storage.
     *
     * @param CreateBackend_User_GroupsRequest $request
     *
     * @return Response
     */
    public function store(CreateBackend_User_GroupsRequest $request)
    {
        $input = $request->all();

        $backendUserGroups = $this->backendUserGroupsRepository->create($input);

        Flash::success('Backend  User  Groups saved successfully.');

        return redirect(route('admin.backendUserGroups.index'));
    }

    /**
     * Display the specified Backend_User_Groups.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $backendUserGroups = $this->backendUserGroupsRepository->findWithoutFail($id);

        if (empty($backendUserGroups)) {
            Flash::error('Backend  User  Groups not found');

            return redirect(route('admin.backendUserGroups.index'));
        }

        return view('admin.backend__user__groups.show')->with('backendUserGroups', $backendUserGroups);
    }

    /**
     * Show the form for editing the specified Backend_User_Groups.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $backendUserGroups = $this->backendUserGroupsRepository->findWithoutFail($id);

        if (empty($backendUserGroups)) {
            Flash::error('Backend  User  Groups not found');

            return redirect(route('admin.backendUserGroups.index'));
        }

        // edited by dandisy
        // return view('admin.backend__user__groups.edit')->with('backendUserGroups', $backendUserGroups);
        return view('admin.backend__user__groups.edit')
            ->with('backendUserGroups', $backendUserGroups);        
    }

    /**
     * Update the specified Backend_User_Groups in storage.
     *
     * @param  int              $id
     * @param UpdateBackend_User_GroupsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBackend_User_GroupsRequest $request)
    {
        $backendUserGroups = $this->backendUserGroupsRepository->findWithoutFail($id);

        if (empty($backendUserGroups)) {
            Flash::error('Backend  User  Groups not found');

            return redirect(route('admin.backendUserGroups.index'));
        }

        $backendUserGroups = $this->backendUserGroupsRepository->update($request->all(), $id);

        Flash::success('Backend  User  Groups updated successfully.');

        return redirect(route('admin.backendUserGroups.index'));
    }

    /**
     * Remove the specified Backend_User_Groups from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $backendUserGroups = $this->backendUserGroupsRepository->findWithoutFail($id);

        if (empty($backendUserGroups)) {
            Flash::error('Backend  User  Groups not found');

            return redirect(route('admin.backendUserGroups.index'));
        }

        $this->backendUserGroupsRepository->delete($id);

        Flash::success('Backend  User  Groups deleted successfully.');

        return redirect(route('admin.backendUserGroups.index'));
    }

    /**
     * Store data Backend_User_Groups from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $backendUserGroups = $this->backendUserGroupsRepository->create($item->toArray());
            });
        });

        Flash::success('Backend  User  Groups saved successfully.');

        return redirect(route('admin.backendUserGroups.index'));
    }
}
