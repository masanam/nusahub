<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Responsiv_Currency_CurrenciesDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateResponsiv_Currency_CurrenciesRequest;
use App\Http\Requests\Admin\UpdateResponsiv_Currency_CurrenciesRequest;
use App\Repositories\Responsiv_Currency_CurrenciesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Responsiv_Currency_CurrenciesController extends AppBaseController
{
    /** @var  Responsiv_Currency_CurrenciesRepository */
    private $responsivCurrencyCurrenciesRepository;

    public function __construct(Responsiv_Currency_CurrenciesRepository $responsivCurrencyCurrenciesRepo)
    {
        $this->middleware('auth');
        $this->responsivCurrencyCurrenciesRepository = $responsivCurrencyCurrenciesRepo;
    }

    /**
     * Display a listing of the Responsiv_Currency_Currencies.
     *
     * @param Responsiv_Currency_CurrenciesDataTable $responsivCurrencyCurrenciesDataTable
     * @return Response
     */
    public function index(Responsiv_Currency_CurrenciesDataTable $responsivCurrencyCurrenciesDataTable)
    {
        return $responsivCurrencyCurrenciesDataTable->render('admin.responsiv__currency__currencies.index');
    }

    /**
     * Show the form for creating a new Responsiv_Currency_Currencies.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.responsiv__currency__currencies.create');
        return view('admin.responsiv__currency__currencies.create');
    }

    /**
     * Store a newly created Responsiv_Currency_Currencies in storage.
     *
     * @param CreateResponsiv_Currency_CurrenciesRequest $request
     *
     * @return Response
     */
    public function store(CreateResponsiv_Currency_CurrenciesRequest $request)
    {
        $input = $request->all();

        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->create($input);

        Flash::success('Responsiv  Currency  Currencies saved successfully.');

        return redirect(route('admin.responsivCurrencyCurrencies.index'));
    }

    /**
     * Display the specified Responsiv_Currency_Currencies.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->findWithoutFail($id);

        if (empty($responsivCurrencyCurrencies)) {
            Flash::error('Responsiv  Currency  Currencies not found');

            return redirect(route('admin.responsivCurrencyCurrencies.index'));
        }

        return view('admin.responsiv__currency__currencies.show')->with('responsivCurrencyCurrencies', $responsivCurrencyCurrencies);
    }

    /**
     * Show the form for editing the specified Responsiv_Currency_Currencies.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->findWithoutFail($id);

        if (empty($responsivCurrencyCurrencies)) {
            Flash::error('Responsiv  Currency  Currencies not found');

            return redirect(route('admin.responsivCurrencyCurrencies.index'));
        }

        // edited by dandisy
        // return view('admin.responsiv__currency__currencies.edit')->with('responsivCurrencyCurrencies', $responsivCurrencyCurrencies);
        return view('admin.responsiv__currency__currencies.edit')
            ->with('responsivCurrencyCurrencies', $responsivCurrencyCurrencies);        
    }

    /**
     * Update the specified Responsiv_Currency_Currencies in storage.
     *
     * @param  int              $id
     * @param UpdateResponsiv_Currency_CurrenciesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResponsiv_Currency_CurrenciesRequest $request)
    {
        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->findWithoutFail($id);

        if (empty($responsivCurrencyCurrencies)) {
            Flash::error('Responsiv  Currency  Currencies not found');

            return redirect(route('admin.responsivCurrencyCurrencies.index'));
        }

        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->update($request->all(), $id);

        Flash::success('Responsiv  Currency  Currencies updated successfully.');

        return redirect(route('admin.responsivCurrencyCurrencies.index'));
    }

    /**
     * Remove the specified Responsiv_Currency_Currencies from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->findWithoutFail($id);

        if (empty($responsivCurrencyCurrencies)) {
            Flash::error('Responsiv  Currency  Currencies not found');

            return redirect(route('admin.responsivCurrencyCurrencies.index'));
        }

        $this->responsivCurrencyCurrenciesRepository->delete($id);

        Flash::success('Responsiv  Currency  Currencies deleted successfully.');

        return redirect(route('admin.responsivCurrencyCurrencies.index'));
    }

    /**
     * Store data Responsiv_Currency_Currencies from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $responsivCurrencyCurrencies = $this->responsivCurrencyCurrenciesRepository->create($item->toArray());
            });
        });

        Flash::success('Responsiv  Currency  Currencies saved successfully.');

        return redirect(route('admin.responsivCurrencyCurrencies.index'));
    }
}
