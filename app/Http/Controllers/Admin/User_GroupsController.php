<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\User_GroupsDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateUser_GroupsRequest;
use App\Http\Requests\Admin\UpdateUser_GroupsRequest;
use App\Repositories\User_GroupsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class User_GroupsController extends AppBaseController
{
    /** @var  User_GroupsRepository */
    private $userGroupsRepository;

    public function __construct(User_GroupsRepository $userGroupsRepo)
    {
        $this->middleware('auth');
        $this->userGroupsRepository = $userGroupsRepo;
    }

    /**
     * Display a listing of the User_Groups.
     *
     * @param User_GroupsDataTable $userGroupsDataTable
     * @return Response
     */
    public function index(User_GroupsDataTable $userGroupsDataTable)
    {
        return $userGroupsDataTable->render('admin.user__groups.index');
    }

    /**
     * Show the form for creating a new User_Groups.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.user__groups.create');
        return view('admin.user__groups.create');
    }

    /**
     * Store a newly created User_Groups in storage.
     *
     * @param CreateUser_GroupsRequest $request
     *
     * @return Response
     */
    public function store(CreateUser_GroupsRequest $request)
    {
        $input = $request->all();

        $userGroups = $this->userGroupsRepository->create($input);

        Flash::success('User  Groups saved successfully.');

        return redirect(route('admin.userGroups.index'));
    }

    /**
     * Display the specified User_Groups.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userGroups = $this->userGroupsRepository->findWithoutFail($id);

        if (empty($userGroups)) {
            Flash::error('User  Groups not found');

            return redirect(route('admin.userGroups.index'));
        }

        return view('admin.user__groups.show')->with('userGroups', $userGroups);
    }

    /**
     * Show the form for editing the specified User_Groups.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $userGroups = $this->userGroupsRepository->findWithoutFail($id);

        if (empty($userGroups)) {
            Flash::error('User  Groups not found');

            return redirect(route('admin.userGroups.index'));
        }

        // edited by dandisy
        // return view('admin.user__groups.edit')->with('userGroups', $userGroups);
        return view('admin.user__groups.edit')
            ->with('userGroups', $userGroups);        
    }

    /**
     * Update the specified User_Groups in storage.
     *
     * @param  int              $id
     * @param UpdateUser_GroupsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUser_GroupsRequest $request)
    {
        $userGroups = $this->userGroupsRepository->findWithoutFail($id);

        if (empty($userGroups)) {
            Flash::error('User  Groups not found');

            return redirect(route('admin.userGroups.index'));
        }

        $userGroups = $this->userGroupsRepository->update($request->all(), $id);

        Flash::success('User  Groups updated successfully.');

        return redirect(route('admin.userGroups.index'));
    }

    /**
     * Remove the specified User_Groups from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userGroups = $this->userGroupsRepository->findWithoutFail($id);

        if (empty($userGroups)) {
            Flash::error('User  Groups not found');

            return redirect(route('admin.userGroups.index'));
        }

        $this->userGroupsRepository->delete($id);

        Flash::success('User  Groups deleted successfully.');

        return redirect(route('admin.userGroups.index'));
    }

    /**
     * Store data User_Groups from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $userGroups = $this->userGroupsRepository->create($item->toArray());
            });
        });

        Flash::success('User  Groups saved successfully.');

        return redirect(route('admin.userGroups.index'));
    }
}
