<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_HandlerDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_HandlerRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_HandlerRequest;
use App\Repositories\Spot_Shipment_HandlerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_HandlerController extends AppBaseController
{
    /** @var  Spot_Shipment_HandlerRepository */
    private $spotShipmentHandlerRepository;

    public function __construct(Spot_Shipment_HandlerRepository $spotShipmentHandlerRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentHandlerRepository = $spotShipmentHandlerRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Handler.
     *
     * @param Spot_Shipment_HandlerDataTable $spotShipmentHandlerDataTable
     * @return Response
     */
    public function index(Spot_Shipment_HandlerDataTable $spotShipmentHandlerDataTable)
    {
        return $spotShipmentHandlerDataTable->render('admin.spot__shipment__handlers.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Handler.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__handlers.create');
        return view('admin.spot__shipment__handlers.create');
    }

    /**
     * Store a newly created Spot_Shipment_Handler in storage.
     *
     * @param CreateSpot_Shipment_HandlerRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_HandlerRequest $request)
    {
        $input = $request->all();

        $spotShipmentHandler = $this->spotShipmentHandlerRepository->create($input);

        Flash::success('Spot  Shipment  Handler saved successfully.');

        return redirect(route('admin.spotShipmentHandlers.index'));
    }

    /**
     * Display the specified Spot_Shipment_Handler.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentHandler = $this->spotShipmentHandlerRepository->findWithoutFail($id);

        if (empty($spotShipmentHandler)) {
            Flash::error('Spot  Shipment  Handler not found');

            return redirect(route('admin.spotShipmentHandlers.index'));
        }

        return view('admin.spot__shipment__handlers.show')->with('spotShipmentHandler', $spotShipmentHandler);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Handler.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentHandler = $this->spotShipmentHandlerRepository->findWithoutFail($id);

        if (empty($spotShipmentHandler)) {
            Flash::error('Spot  Shipment  Handler not found');

            return redirect(route('admin.spotShipmentHandlers.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__handlers.edit')->with('spotShipmentHandler', $spotShipmentHandler);
        return view('admin.spot__shipment__handlers.edit')
            ->with('spotShipmentHandler', $spotShipmentHandler);        
    }

    /**
     * Update the specified Spot_Shipment_Handler in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_HandlerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_HandlerRequest $request)
    {
        $spotShipmentHandler = $this->spotShipmentHandlerRepository->findWithoutFail($id);

        if (empty($spotShipmentHandler)) {
            Flash::error('Spot  Shipment  Handler not found');

            return redirect(route('admin.spotShipmentHandlers.index'));
        }

        $spotShipmentHandler = $this->spotShipmentHandlerRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Handler updated successfully.');

        return redirect(route('admin.spotShipmentHandlers.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Handler from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentHandler = $this->spotShipmentHandlerRepository->findWithoutFail($id);

        if (empty($spotShipmentHandler)) {
            Flash::error('Spot  Shipment  Handler not found');

            return redirect(route('admin.spotShipmentHandlers.index'));
        }

        $this->spotShipmentHandlerRepository->delete($id);

        Flash::success('Spot  Shipment  Handler deleted successfully.');

        return redirect(route('admin.spotShipmentHandlers.index'));
    }

    /**
     * Store data Spot_Shipment_Handler from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentHandler = $this->spotShipmentHandlerRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Handler saved successfully.');

        return redirect(route('admin.spotShipmentHandlers.index'));
    }
}
