<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Container_SizeDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Container_SizeRequest;
use App\Http\Requests\Admin\UpdateSpot_Container_SizeRequest;
use App\Repositories\Spot_Container_SizeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Container_SizeController extends AppBaseController
{
    /** @var  Spot_Container_SizeRepository */
    private $spotContainerSizeRepository;

    public function __construct(Spot_Container_SizeRepository $spotContainerSizeRepo)
    {
        $this->middleware('auth');
        $this->spotContainerSizeRepository = $spotContainerSizeRepo;
    }

    /**
     * Display a listing of the Spot_Container_Size.
     *
     * @param Spot_Container_SizeDataTable $spotContainerSizeDataTable
     * @return Response
     */
    public function index(Spot_Container_SizeDataTable $spotContainerSizeDataTable)
    {
        return $spotContainerSizeDataTable->render('admin.spot__container__sizes.index');
    }

    /**
     * Show the form for creating a new Spot_Container_Size.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__container__sizes.create');
        return view('admin.spot__container__sizes.create');
    }

    /**
     * Store a newly created Spot_Container_Size in storage.
     *
     * @param CreateSpot_Container_SizeRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Container_SizeRequest $request)
    {
        $input = $request->all();

        $spotContainerSize = $this->spotContainerSizeRepository->create($input);

        Flash::success('Spot  Container  Size saved successfully.');

        return redirect(route('admin.spotContainerSizes.index'));
    }

    /**
     * Display the specified Spot_Container_Size.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotContainerSize = $this->spotContainerSizeRepository->findWithoutFail($id);

        if (empty($spotContainerSize)) {
            Flash::error('Spot  Container  Size not found');

            return redirect(route('admin.spotContainerSizes.index'));
        }

        return view('admin.spot__container__sizes.show')->with('spotContainerSize', $spotContainerSize);
    }

    /**
     * Show the form for editing the specified Spot_Container_Size.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotContainerSize = $this->spotContainerSizeRepository->findWithoutFail($id);

        if (empty($spotContainerSize)) {
            Flash::error('Spot  Container  Size not found');

            return redirect(route('admin.spotContainerSizes.index'));
        }

        // edited by dandisy
        // return view('admin.spot__container__sizes.edit')->with('spotContainerSize', $spotContainerSize);
        return view('admin.spot__container__sizes.edit')
            ->with('spotContainerSize', $spotContainerSize);        
    }

    /**
     * Update the specified Spot_Container_Size in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Container_SizeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Container_SizeRequest $request)
    {
        $spotContainerSize = $this->spotContainerSizeRepository->findWithoutFail($id);

        if (empty($spotContainerSize)) {
            Flash::error('Spot  Container  Size not found');

            return redirect(route('admin.spotContainerSizes.index'));
        }

        $spotContainerSize = $this->spotContainerSizeRepository->update($request->all(), $id);

        Flash::success('Spot  Container  Size updated successfully.');

        return redirect(route('admin.spotContainerSizes.index'));
    }

    /**
     * Remove the specified Spot_Container_Size from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotContainerSize = $this->spotContainerSizeRepository->findWithoutFail($id);

        if (empty($spotContainerSize)) {
            Flash::error('Spot  Container  Size not found');

            return redirect(route('admin.spotContainerSizes.index'));
        }

        $this->spotContainerSizeRepository->delete($id);

        Flash::success('Spot  Container  Size deleted successfully.');

        return redirect(route('admin.spotContainerSizes.index'));
    }

    /**
     * Store data Spot_Container_Size from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotContainerSize = $this->spotContainerSizeRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Container  Size saved successfully.');

        return redirect(route('admin.spotContainerSizes.index'));
    }
}
