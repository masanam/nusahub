<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_BreakDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_BreakRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_BreakRequest;
use App\Repositories\Spot_Shipment_BreakRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_BreakController extends AppBaseController
{
    /** @var  Spot_Shipment_BreakRepository */
    private $spotShipmentBreakRepository;

    public function __construct(Spot_Shipment_BreakRepository $spotShipmentBreakRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentBreakRepository = $spotShipmentBreakRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Break.
     *
     * @param Spot_Shipment_BreakDataTable $spotShipmentBreakDataTable
     * @return Response
     */
    public function index(Spot_Shipment_BreakDataTable $spotShipmentBreakDataTable)
    {
        return $spotShipmentBreakDataTable->render('admin.spot__shipment__breaks.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Break.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__breaks.create');
        return view('admin.spot__shipment__breaks.create');
    }

    /**
     * Store a newly created Spot_Shipment_Break in storage.
     *
     * @param CreateSpot_Shipment_BreakRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_BreakRequest $request)
    {
        $input = $request->all();

        $spotShipmentBreak = $this->spotShipmentBreakRepository->create($input);

        Flash::success('Spot  Shipment  Break saved successfully.');

        return redirect(route('admin.spotShipmentBreaks.index'));
    }

    /**
     * Display the specified Spot_Shipment_Break.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentBreak = $this->spotShipmentBreakRepository->findWithoutFail($id);

        if (empty($spotShipmentBreak)) {
            Flash::error('Spot  Shipment  Break not found');

            return redirect(route('admin.spotShipmentBreaks.index'));
        }

        return view('admin.spot__shipment__breaks.show')->with('spotShipmentBreak', $spotShipmentBreak);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Break.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentBreak = $this->spotShipmentBreakRepository->findWithoutFail($id);

        if (empty($spotShipmentBreak)) {
            Flash::error('Spot  Shipment  Break not found');

            return redirect(route('admin.spotShipmentBreaks.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__breaks.edit')->with('spotShipmentBreak', $spotShipmentBreak);
        return view('admin.spot__shipment__breaks.edit')
            ->with('spotShipmentBreak', $spotShipmentBreak);        
    }

    /**
     * Update the specified Spot_Shipment_Break in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_BreakRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_BreakRequest $request)
    {
        $spotShipmentBreak = $this->spotShipmentBreakRepository->findWithoutFail($id);

        if (empty($spotShipmentBreak)) {
            Flash::error('Spot  Shipment  Break not found');

            return redirect(route('admin.spotShipmentBreaks.index'));
        }

        $spotShipmentBreak = $this->spotShipmentBreakRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Break updated successfully.');

        return redirect(route('admin.spotShipmentBreaks.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Break from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentBreak = $this->spotShipmentBreakRepository->findWithoutFail($id);

        if (empty($spotShipmentBreak)) {
            Flash::error('Spot  Shipment  Break not found');

            return redirect(route('admin.spotShipmentBreaks.index'));
        }

        $this->spotShipmentBreakRepository->delete($id);

        Flash::success('Spot  Shipment  Break deleted successfully.');

        return redirect(route('admin.spotShipmentBreaks.index'));
    }

    /**
     * Store data Spot_Shipment_Break from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentBreak = $this->spotShipmentBreakRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Break saved successfully.');

        return redirect(route('admin.spotShipmentBreaks.index'));
    }
}
