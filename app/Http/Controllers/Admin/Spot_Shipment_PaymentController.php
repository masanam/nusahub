<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_PaymentDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_PaymentRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_PaymentRequest;
use App\Repositories\Spot_Shipment_PaymentRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_PaymentController extends AppBaseController
{
    /** @var  Spot_Shipment_PaymentRepository */
    private $spotShipmentPaymentRepository;

    public function __construct(Spot_Shipment_PaymentRepository $spotShipmentPaymentRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentPaymentRepository = $spotShipmentPaymentRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Payment.
     *
     * @param Spot_Shipment_PaymentDataTable $spotShipmentPaymentDataTable
     * @return Response
     */
    public function index(Spot_Shipment_PaymentDataTable $spotShipmentPaymentDataTable)
    {
        return $spotShipmentPaymentDataTable->render('admin.spot__shipment__payments.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Payment.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__payments.create');
        return view('admin.spot__shipment__payments.create');
    }

    /**
     * Store a newly created Spot_Shipment_Payment in storage.
     *
     * @param CreateSpot_Shipment_PaymentRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_PaymentRequest $request)
    {
        $input = $request->all();

        $spotShipmentPayment = $this->spotShipmentPaymentRepository->create($input);

        Flash::success('Spot  Shipment  Payment saved successfully.');

        return redirect(route('admin.spotShipmentPayments.index'));
    }

    /**
     * Display the specified Spot_Shipment_Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentPayment = $this->spotShipmentPaymentRepository->findWithoutFail($id);

        if (empty($spotShipmentPayment)) {
            Flash::error('Spot  Shipment  Payment not found');

            return redirect(route('admin.spotShipmentPayments.index'));
        }

        return view('admin.spot__shipment__payments.show')->with('spotShipmentPayment', $spotShipmentPayment);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentPayment = $this->spotShipmentPaymentRepository->findWithoutFail($id);

        if (empty($spotShipmentPayment)) {
            Flash::error('Spot  Shipment  Payment not found');

            return redirect(route('admin.spotShipmentPayments.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__payments.edit')->with('spotShipmentPayment', $spotShipmentPayment);
        return view('admin.spot__shipment__payments.edit')
            ->with('spotShipmentPayment', $spotShipmentPayment);        
    }

    /**
     * Update the specified Spot_Shipment_Payment in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_PaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_PaymentRequest $request)
    {
        $spotShipmentPayment = $this->spotShipmentPaymentRepository->findWithoutFail($id);

        if (empty($spotShipmentPayment)) {
            Flash::error('Spot  Shipment  Payment not found');

            return redirect(route('admin.spotShipmentPayments.index'));
        }

        $spotShipmentPayment = $this->spotShipmentPaymentRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Payment updated successfully.');

        return redirect(route('admin.spotShipmentPayments.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Payment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentPayment = $this->spotShipmentPaymentRepository->findWithoutFail($id);

        if (empty($spotShipmentPayment)) {
            Flash::error('Spot  Shipment  Payment not found');

            return redirect(route('admin.spotShipmentPayments.index'));
        }

        $this->spotShipmentPaymentRepository->delete($id);

        Flash::success('Spot  Shipment  Payment deleted successfully.');

        return redirect(route('admin.spotShipmentPayments.index'));
    }

    /**
     * Store data Spot_Shipment_Payment from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentPayment = $this->spotShipmentPaymentRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Payment saved successfully.');

        return redirect(route('admin.spotShipmentPayments.index'));
    }
}
