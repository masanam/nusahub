<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Rainlab_Location_StatesDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateRainlab_Location_StatesRequest;
use App\Http\Requests\Admin\UpdateRainlab_Location_StatesRequest;
use App\Repositories\Rainlab_Location_StatesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Rainlab_Location_StatesController extends AppBaseController
{
    /** @var  Rainlab_Location_StatesRepository */
    private $rainlabLocationStatesRepository;

    public function __construct(Rainlab_Location_StatesRepository $rainlabLocationStatesRepo)
    {
        $this->middleware('auth');
        $this->rainlabLocationStatesRepository = $rainlabLocationStatesRepo;
    }

    /**
     * Display a listing of the Rainlab_Location_States.
     *
     * @param Rainlab_Location_StatesDataTable $rainlabLocationStatesDataTable
     * @return Response
     */
    public function index(Rainlab_Location_StatesDataTable $rainlabLocationStatesDataTable)
    {
        return $rainlabLocationStatesDataTable->render('admin.rainlab__location__states.index');
    }

    /**
     * Show the form for creating a new Rainlab_Location_States.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.rainlab__location__states.create');
        return view('admin.rainlab__location__states.create');
    }

    /**
     * Store a newly created Rainlab_Location_States in storage.
     *
     * @param CreateRainlab_Location_StatesRequest $request
     *
     * @return Response
     */
    public function store(CreateRainlab_Location_StatesRequest $request)
    {
        $input = $request->all();

        $rainlabLocationStates = $this->rainlabLocationStatesRepository->create($input);

        Flash::success('Rainlab  Location  States saved successfully.');

        return redirect(route('admin.rainlabLocationStates.index'));
    }

    /**
     * Display the specified Rainlab_Location_States.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $rainlabLocationStates = $this->rainlabLocationStatesRepository->findWithoutFail($id);

        if (empty($rainlabLocationStates)) {
            Flash::error('Rainlab  Location  States not found');

            return redirect(route('admin.rainlabLocationStates.index'));
        }

        return view('admin.rainlab__location__states.show')->with('rainlabLocationStates', $rainlabLocationStates);
    }

    /**
     * Show the form for editing the specified Rainlab_Location_States.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $rainlabLocationStates = $this->rainlabLocationStatesRepository->findWithoutFail($id);

        if (empty($rainlabLocationStates)) {
            Flash::error('Rainlab  Location  States not found');

            return redirect(route('admin.rainlabLocationStates.index'));
        }

        // edited by dandisy
        // return view('admin.rainlab__location__states.edit')->with('rainlabLocationStates', $rainlabLocationStates);
        return view('admin.rainlab__location__states.edit')
            ->with('rainlabLocationStates', $rainlabLocationStates);        
    }

    /**
     * Update the specified Rainlab_Location_States in storage.
     *
     * @param  int              $id
     * @param UpdateRainlab_Location_StatesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRainlab_Location_StatesRequest $request)
    {
        $rainlabLocationStates = $this->rainlabLocationStatesRepository->findWithoutFail($id);

        if (empty($rainlabLocationStates)) {
            Flash::error('Rainlab  Location  States not found');

            return redirect(route('admin.rainlabLocationStates.index'));
        }

        $rainlabLocationStates = $this->rainlabLocationStatesRepository->update($request->all(), $id);

        Flash::success('Rainlab  Location  States updated successfully.');

        return redirect(route('admin.rainlabLocationStates.index'));
    }

    /**
     * Remove the specified Rainlab_Location_States from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rainlabLocationStates = $this->rainlabLocationStatesRepository->findWithoutFail($id);

        if (empty($rainlabLocationStates)) {
            Flash::error('Rainlab  Location  States not found');

            return redirect(route('admin.rainlabLocationStates.index'));
        }

        $this->rainlabLocationStatesRepository->delete($id);

        Flash::success('Rainlab  Location  States deleted successfully.');

        return redirect(route('admin.rainlabLocationStates.index'));
    }

    /**
     * Store data Rainlab_Location_States from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $rainlabLocationStates = $this->rainlabLocationStatesRepository->create($item->toArray());
            });
        });

        Flash::success('Rainlab  Location  States saved successfully.');

        return redirect(route('admin.rainlabLocationStates.index'));
    }
}
