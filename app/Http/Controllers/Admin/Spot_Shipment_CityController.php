<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_CityDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_CityRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_CityRequest;
use App\Repositories\Spot_Shipment_CityRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_CityController extends AppBaseController
{
    /** @var  Spot_Shipment_CityRepository */
    private $spotShipmentCityRepository;

    public function __construct(Spot_Shipment_CityRepository $spotShipmentCityRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentCityRepository = $spotShipmentCityRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_City.
     *
     * @param Spot_Shipment_CityDataTable $spotShipmentCityDataTable
     * @return Response
     */
    public function index(Spot_Shipment_CityDataTable $spotShipmentCityDataTable)
    {
        return $spotShipmentCityDataTable->render('admin.spot__shipment__cities.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_City.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__cities.create');
        return view('admin.spot__shipment__cities.create');
    }

    /**
     * Store a newly created Spot_Shipment_City in storage.
     *
     * @param CreateSpot_Shipment_CityRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_CityRequest $request)
    {
        $input = $request->all();

        $spotShipmentCity = $this->spotShipmentCityRepository->create($input);

        Flash::success('Spot  Shipment  City saved successfully.');

        return redirect(route('admin.spotShipmentCities.index'));
    }

    /**
     * Display the specified Spot_Shipment_City.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentCity = $this->spotShipmentCityRepository->findWithoutFail($id);

        if (empty($spotShipmentCity)) {
            Flash::error('Spot  Shipment  City not found');

            return redirect(route('admin.spotShipmentCities.index'));
        }

        return view('admin.spot__shipment__cities.show')->with('spotShipmentCity', $spotShipmentCity);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_City.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentCity = $this->spotShipmentCityRepository->findWithoutFail($id);

        if (empty($spotShipmentCity)) {
            Flash::error('Spot  Shipment  City not found');

            return redirect(route('admin.spotShipmentCities.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__cities.edit')->with('spotShipmentCity', $spotShipmentCity);
        return view('admin.spot__shipment__cities.edit')
            ->with('spotShipmentCity', $spotShipmentCity);        
    }

    /**
     * Update the specified Spot_Shipment_City in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_CityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_CityRequest $request)
    {
        $spotShipmentCity = $this->spotShipmentCityRepository->findWithoutFail($id);

        if (empty($spotShipmentCity)) {
            Flash::error('Spot  Shipment  City not found');

            return redirect(route('admin.spotShipmentCities.index'));
        }

        $spotShipmentCity = $this->spotShipmentCityRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  City updated successfully.');

        return redirect(route('admin.spotShipmentCities.index'));
    }

    /**
     * Remove the specified Spot_Shipment_City from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentCity = $this->spotShipmentCityRepository->findWithoutFail($id);

        if (empty($spotShipmentCity)) {
            Flash::error('Spot  Shipment  City not found');

            return redirect(route('admin.spotShipmentCities.index'));
        }

        $this->spotShipmentCityRepository->delete($id);

        Flash::success('Spot  Shipment  City deleted successfully.');

        return redirect(route('admin.spotShipmentCities.index'));
    }

    /**
     * Store data Spot_Shipment_City from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentCity = $this->spotShipmentCityRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  City saved successfully.');

        return redirect(route('admin.spotShipmentCities.index'));
    }
}
