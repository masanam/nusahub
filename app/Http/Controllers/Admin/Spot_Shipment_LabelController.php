<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_LabelDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_LabelRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_LabelRequest;
use App\Repositories\Spot_Shipment_LabelRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_LabelController extends AppBaseController
{
    /** @var  Spot_Shipment_LabelRepository */
    private $spotShipmentLabelRepository;

    public function __construct(Spot_Shipment_LabelRepository $spotShipmentLabelRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentLabelRepository = $spotShipmentLabelRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Label.
     *
     * @param Spot_Shipment_LabelDataTable $spotShipmentLabelDataTable
     * @return Response
     */
    public function index(Spot_Shipment_LabelDataTable $spotShipmentLabelDataTable)
    {
        return $spotShipmentLabelDataTable->render('admin.spot__shipment__labels.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Label.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__labels.create');
        return view('admin.spot__shipment__labels.create');
    }

    /**
     * Store a newly created Spot_Shipment_Label in storage.
     *
     * @param CreateSpot_Shipment_LabelRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_LabelRequest $request)
    {
        $input = $request->all();

        $spotShipmentLabel = $this->spotShipmentLabelRepository->create($input);

        Flash::success('Spot  Shipment  Label saved successfully.');

        return redirect(route('admin.spotShipmentLabels.index'));
    }

    /**
     * Display the specified Spot_Shipment_Label.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentLabel = $this->spotShipmentLabelRepository->findWithoutFail($id);

        if (empty($spotShipmentLabel)) {
            Flash::error('Spot  Shipment  Label not found');

            return redirect(route('admin.spotShipmentLabels.index'));
        }

        return view('admin.spot__shipment__labels.show')->with('spotShipmentLabel', $spotShipmentLabel);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Label.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentLabel = $this->spotShipmentLabelRepository->findWithoutFail($id);

        if (empty($spotShipmentLabel)) {
            Flash::error('Spot  Shipment  Label not found');

            return redirect(route('admin.spotShipmentLabels.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__labels.edit')->with('spotShipmentLabel', $spotShipmentLabel);
        return view('admin.spot__shipment__labels.edit')
            ->with('spotShipmentLabel', $spotShipmentLabel);        
    }

    /**
     * Update the specified Spot_Shipment_Label in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_LabelRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_LabelRequest $request)
    {
        $spotShipmentLabel = $this->spotShipmentLabelRepository->findWithoutFail($id);

        if (empty($spotShipmentLabel)) {
            Flash::error('Spot  Shipment  Label not found');

            return redirect(route('admin.spotShipmentLabels.index'));
        }

        $spotShipmentLabel = $this->spotShipmentLabelRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Label updated successfully.');

        return redirect(route('admin.spotShipmentLabels.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Label from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentLabel = $this->spotShipmentLabelRepository->findWithoutFail($id);

        if (empty($spotShipmentLabel)) {
            Flash::error('Spot  Shipment  Label not found');

            return redirect(route('admin.spotShipmentLabels.index'));
        }

        $this->spotShipmentLabelRepository->delete($id);

        Flash::success('Spot  Shipment  Label deleted successfully.');

        return redirect(route('admin.spotShipmentLabels.index'));
    }

    /**
     * Store data Spot_Shipment_Label from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentLabel = $this->spotShipmentLabelRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Label saved successfully.');

        return redirect(route('admin.spotShipmentLabels.index'));
    }
}
