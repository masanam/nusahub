<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Container_StatusDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Container_StatusRequest;
use App\Http\Requests\Admin\UpdateSpot_Container_StatusRequest;
use App\Repositories\Spot_Container_StatusRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Container_StatusController extends AppBaseController
{
    /** @var  Spot_Container_StatusRepository */
    private $spotContainerStatusRepository;

    public function __construct(Spot_Container_StatusRepository $spotContainerStatusRepo)
    {
        $this->middleware('auth');
        $this->spotContainerStatusRepository = $spotContainerStatusRepo;
    }

    /**
     * Display a listing of the Spot_Container_Status.
     *
     * @param Spot_Container_StatusDataTable $spotContainerStatusDataTable
     * @return Response
     */
    public function index(Spot_Container_StatusDataTable $spotContainerStatusDataTable)
    {
        return $spotContainerStatusDataTable->render('admin.spot__container__statuses.index');
    }

    /**
     * Show the form for creating a new Spot_Container_Status.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__container__statuses.create');
        return view('admin.spot__container__statuses.create');
    }

    /**
     * Store a newly created Spot_Container_Status in storage.
     *
     * @param CreateSpot_Container_StatusRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Container_StatusRequest $request)
    {
        $input = $request->all();

        $spotContainerStatus = $this->spotContainerStatusRepository->create($input);

        Flash::success('Spot  Container  Status saved successfully.');

        return redirect(route('admin.spotContainerStatuses.index'));
    }

    /**
     * Display the specified Spot_Container_Status.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotContainerStatus = $this->spotContainerStatusRepository->findWithoutFail($id);

        if (empty($spotContainerStatus)) {
            Flash::error('Spot  Container  Status not found');

            return redirect(route('admin.spotContainerStatuses.index'));
        }

        return view('admin.spot__container__statuses.show')->with('spotContainerStatus', $spotContainerStatus);
    }

    /**
     * Show the form for editing the specified Spot_Container_Status.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotContainerStatus = $this->spotContainerStatusRepository->findWithoutFail($id);

        if (empty($spotContainerStatus)) {
            Flash::error('Spot  Container  Status not found');

            return redirect(route('admin.spotContainerStatuses.index'));
        }

        // edited by dandisy
        // return view('admin.spot__container__statuses.edit')->with('spotContainerStatus', $spotContainerStatus);
        return view('admin.spot__container__statuses.edit')
            ->with('spotContainerStatus', $spotContainerStatus);        
    }

    /**
     * Update the specified Spot_Container_Status in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Container_StatusRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Container_StatusRequest $request)
    {
        $spotContainerStatus = $this->spotContainerStatusRepository->findWithoutFail($id);

        if (empty($spotContainerStatus)) {
            Flash::error('Spot  Container  Status not found');

            return redirect(route('admin.spotContainerStatuses.index'));
        }

        $spotContainerStatus = $this->spotContainerStatusRepository->update($request->all(), $id);

        Flash::success('Spot  Container  Status updated successfully.');

        return redirect(route('admin.spotContainerStatuses.index'));
    }

    /**
     * Remove the specified Spot_Container_Status from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotContainerStatus = $this->spotContainerStatusRepository->findWithoutFail($id);

        if (empty($spotContainerStatus)) {
            Flash::error('Spot  Container  Status not found');

            return redirect(route('admin.spotContainerStatuses.index'));
        }

        $this->spotContainerStatusRepository->delete($id);

        Flash::success('Spot  Container  Status deleted successfully.');

        return redirect(route('admin.spotContainerStatuses.index'));
    }

    /**
     * Store data Spot_Container_Status from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotContainerStatus = $this->spotContainerStatusRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Container  Status saved successfully.');

        return redirect(route('admin.spotContainerStatuses.index'));
    }
}
