<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Backend_User_RolesDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateBackend_User_RolesRequest;
use App\Http\Requests\Admin\UpdateBackend_User_RolesRequest;
use App\Repositories\Backend_User_RolesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Backend_User_RolesController extends AppBaseController
{
    /** @var  Backend_User_RolesRepository */
    private $backendUserRolesRepository;

    public function __construct(Backend_User_RolesRepository $backendUserRolesRepo)
    {
        $this->middleware('auth');
        $this->backendUserRolesRepository = $backendUserRolesRepo;
    }

    /**
     * Display a listing of the Backend_User_Roles.
     *
     * @param Backend_User_RolesDataTable $backendUserRolesDataTable
     * @return Response
     */
    public function index(Backend_User_RolesDataTable $backendUserRolesDataTable)
    {
        return $backendUserRolesDataTable->render('admin.backend__user__roles.index');
    }

    /**
     * Show the form for creating a new Backend_User_Roles.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.backend__user__roles.create');
        return view('admin.backend__user__roles.create');
    }

    /**
     * Store a newly created Backend_User_Roles in storage.
     *
     * @param CreateBackend_User_RolesRequest $request
     *
     * @return Response
     */
    public function store(CreateBackend_User_RolesRequest $request)
    {
        $input = $request->all();

        $backendUserRoles = $this->backendUserRolesRepository->create($input);

        Flash::success('Backend  User  Roles saved successfully.');

        return redirect(route('admin.backendUserRoles.index'));
    }

    /**
     * Display the specified Backend_User_Roles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $backendUserRoles = $this->backendUserRolesRepository->findWithoutFail($id);

        if (empty($backendUserRoles)) {
            Flash::error('Backend  User  Roles not found');

            return redirect(route('admin.backendUserRoles.index'));
        }

        return view('admin.backend__user__roles.show')->with('backendUserRoles', $backendUserRoles);
    }

    /**
     * Show the form for editing the specified Backend_User_Roles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $backendUserRoles = $this->backendUserRolesRepository->findWithoutFail($id);

        if (empty($backendUserRoles)) {
            Flash::error('Backend  User  Roles not found');

            return redirect(route('admin.backendUserRoles.index'));
        }

        // edited by dandisy
        // return view('admin.backend__user__roles.edit')->with('backendUserRoles', $backendUserRoles);
        return view('admin.backend__user__roles.edit')
            ->with('backendUserRoles', $backendUserRoles);        
    }

    /**
     * Update the specified Backend_User_Roles in storage.
     *
     * @param  int              $id
     * @param UpdateBackend_User_RolesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBackend_User_RolesRequest $request)
    {
        $backendUserRoles = $this->backendUserRolesRepository->findWithoutFail($id);

        if (empty($backendUserRoles)) {
            Flash::error('Backend  User  Roles not found');

            return redirect(route('admin.backendUserRoles.index'));
        }

        $backendUserRoles = $this->backendUserRolesRepository->update($request->all(), $id);

        Flash::success('Backend  User  Roles updated successfully.');

        return redirect(route('admin.backendUserRoles.index'));
    }

    /**
     * Remove the specified Backend_User_Roles from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $backendUserRoles = $this->backendUserRolesRepository->findWithoutFail($id);

        if (empty($backendUserRoles)) {
            Flash::error('Backend  User  Roles not found');

            return redirect(route('admin.backendUserRoles.index'));
        }

        $this->backendUserRolesRepository->delete($id);

        Flash::success('Backend  User  Roles deleted successfully.');

        return redirect(route('admin.backendUserRoles.index'));
    }

    /**
     * Store data Backend_User_Roles from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $backendUserRoles = $this->backendUserRolesRepository->create($item->toArray());
            });
        });

        Flash::success('Backend  User  Roles saved successfully.');

        return redirect(route('admin.backendUserRoles.index'));
    }
}
