<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Spot_Shipment_CourierDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpot_Shipment_CourierRequest;
use App\Http\Requests\Admin\UpdateSpot_Shipment_CourierRequest;
use App\Repositories\Spot_Shipment_CourierRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Spot_Shipment_CourierController extends AppBaseController
{
    /** @var  Spot_Shipment_CourierRepository */
    private $spotShipmentCourierRepository;

    public function __construct(Spot_Shipment_CourierRepository $spotShipmentCourierRepo)
    {
        $this->middleware('auth');
        $this->spotShipmentCourierRepository = $spotShipmentCourierRepo;
    }

    /**
     * Display a listing of the Spot_Shipment_Courier.
     *
     * @param Spot_Shipment_CourierDataTable $spotShipmentCourierDataTable
     * @return Response
     */
    public function index(Spot_Shipment_CourierDataTable $spotShipmentCourierDataTable)
    {
        return $spotShipmentCourierDataTable->render('admin.spot__shipment__couriers.index');
    }

    /**
     * Show the form for creating a new Spot_Shipment_Courier.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.spot__shipment__couriers.create');
        return view('admin.spot__shipment__couriers.create');
    }

    /**
     * Store a newly created Spot_Shipment_Courier in storage.
     *
     * @param CreateSpot_Shipment_CourierRequest $request
     *
     * @return Response
     */
    public function store(CreateSpot_Shipment_CourierRequest $request)
    {
        $input = $request->all();

        $spotShipmentCourier = $this->spotShipmentCourierRepository->create($input);

        Flash::success('Spot  Shipment  Courier saved successfully.');

        return redirect(route('admin.spotShipmentCouriers.index'));
    }

    /**
     * Display the specified Spot_Shipment_Courier.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spotShipmentCourier = $this->spotShipmentCourierRepository->findWithoutFail($id);

        if (empty($spotShipmentCourier)) {
            Flash::error('Spot  Shipment  Courier not found');

            return redirect(route('admin.spotShipmentCouriers.index'));
        }

        return view('admin.spot__shipment__couriers.show')->with('spotShipmentCourier', $spotShipmentCourier);
    }

    /**
     * Show the form for editing the specified Spot_Shipment_Courier.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $spotShipmentCourier = $this->spotShipmentCourierRepository->findWithoutFail($id);

        if (empty($spotShipmentCourier)) {
            Flash::error('Spot  Shipment  Courier not found');

            return redirect(route('admin.spotShipmentCouriers.index'));
        }

        // edited by dandisy
        // return view('admin.spot__shipment__couriers.edit')->with('spotShipmentCourier', $spotShipmentCourier);
        return view('admin.spot__shipment__couriers.edit')
            ->with('spotShipmentCourier', $spotShipmentCourier);        
    }

    /**
     * Update the specified Spot_Shipment_Courier in storage.
     *
     * @param  int              $id
     * @param UpdateSpot_Shipment_CourierRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpot_Shipment_CourierRequest $request)
    {
        $spotShipmentCourier = $this->spotShipmentCourierRepository->findWithoutFail($id);

        if (empty($spotShipmentCourier)) {
            Flash::error('Spot  Shipment  Courier not found');

            return redirect(route('admin.spotShipmentCouriers.index'));
        }

        $spotShipmentCourier = $this->spotShipmentCourierRepository->update($request->all(), $id);

        Flash::success('Spot  Shipment  Courier updated successfully.');

        return redirect(route('admin.spotShipmentCouriers.index'));
    }

    /**
     * Remove the specified Spot_Shipment_Courier from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spotShipmentCourier = $this->spotShipmentCourierRepository->findWithoutFail($id);

        if (empty($spotShipmentCourier)) {
            Flash::error('Spot  Shipment  Courier not found');

            return redirect(route('admin.spotShipmentCouriers.index'));
        }

        $this->spotShipmentCourierRepository->delete($id);

        Flash::success('Spot  Shipment  Courier deleted successfully.');

        return redirect(route('admin.spotShipmentCouriers.index'));
    }

    /**
     * Store data Spot_Shipment_Courier from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $spotShipmentCourier = $this->spotShipmentCourierRepository->create($item->toArray());
            });
        });

        Flash::success('Spot  Shipment  Courier saved successfully.');

        return redirect(route('admin.spotShipmentCouriers.index'));
    }
}
