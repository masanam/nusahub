<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\Rainlab_Location_CountriesDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateRainlab_Location_CountriesRequest;
use App\Http\Requests\Admin\UpdateRainlab_Location_CountriesRequest;
use App\Repositories\Rainlab_Location_CountriesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request; // added by dandisy
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class Rainlab_Location_CountriesController extends AppBaseController
{
    /** @var  Rainlab_Location_CountriesRepository */
    private $rainlabLocationCountriesRepository;

    public function __construct(Rainlab_Location_CountriesRepository $rainlabLocationCountriesRepo)
    {
        $this->middleware('auth');
        $this->rainlabLocationCountriesRepository = $rainlabLocationCountriesRepo;
    }

    /**
     * Display a listing of the Rainlab_Location_Countries.
     *
     * @param Rainlab_Location_CountriesDataTable $rainlabLocationCountriesDataTable
     * @return Response
     */
    public function index(Rainlab_Location_CountriesDataTable $rainlabLocationCountriesDataTable)
    {
        return $rainlabLocationCountriesDataTable->render('admin.rainlab__location__countries.index');
    }

    /**
     * Show the form for creating a new Rainlab_Location_Countries.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('admin.rainlab__location__countries.create');
        return view('admin.rainlab__location__countries.create');
    }

    /**
     * Store a newly created Rainlab_Location_Countries in storage.
     *
     * @param CreateRainlab_Location_CountriesRequest $request
     *
     * @return Response
     */
    public function store(CreateRainlab_Location_CountriesRequest $request)
    {
        $input = $request->all();

        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->create($input);

        Flash::success('Rainlab  Location  Countries saved successfully.');

        return redirect(route('admin.rainlabLocationCountries.index'));
    }

    /**
     * Display the specified Rainlab_Location_Countries.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->findWithoutFail($id);

        if (empty($rainlabLocationCountries)) {
            Flash::error('Rainlab  Location  Countries not found');

            return redirect(route('admin.rainlabLocationCountries.index'));
        }

        return view('admin.rainlab__location__countries.show')->with('rainlabLocationCountries', $rainlabLocationCountries);
    }

    /**
     * Show the form for editing the specified Rainlab_Location_Countries.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->findWithoutFail($id);

        if (empty($rainlabLocationCountries)) {
            Flash::error('Rainlab  Location  Countries not found');

            return redirect(route('admin.rainlabLocationCountries.index'));
        }

        // edited by dandisy
        // return view('admin.rainlab__location__countries.edit')->with('rainlabLocationCountries', $rainlabLocationCountries);
        return view('admin.rainlab__location__countries.edit')
            ->with('rainlabLocationCountries', $rainlabLocationCountries);        
    }

    /**
     * Update the specified Rainlab_Location_Countries in storage.
     *
     * @param  int              $id
     * @param UpdateRainlab_Location_CountriesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRainlab_Location_CountriesRequest $request)
    {
        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->findWithoutFail($id);

        if (empty($rainlabLocationCountries)) {
            Flash::error('Rainlab  Location  Countries not found');

            return redirect(route('admin.rainlabLocationCountries.index'));
        }

        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->update($request->all(), $id);

        Flash::success('Rainlab  Location  Countries updated successfully.');

        return redirect(route('admin.rainlabLocationCountries.index'));
    }

    /**
     * Remove the specified Rainlab_Location_Countries from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->findWithoutFail($id);

        if (empty($rainlabLocationCountries)) {
            Flash::error('Rainlab  Location  Countries not found');

            return redirect(route('admin.rainlabLocationCountries.index'));
        }

        $this->rainlabLocationCountriesRepository->delete($id);

        Flash::success('Rainlab  Location  Countries deleted successfully.');

        return redirect(route('admin.rainlabLocationCountries.index'));
    }

    /**
     * Store data Rainlab_Location_Countries from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $rainlabLocationCountries = $this->rainlabLocationCountriesRepository->create($item->toArray());
            });
        });

        Flash::success('Rainlab  Location  Countries saved successfully.');

        return redirect(route('admin.rainlabLocationCountries.index'));
    }
}
