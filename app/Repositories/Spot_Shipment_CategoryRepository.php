<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Category;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_CategoryRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:18 pm WIB
 *
 * @method Spot_Shipment_Category findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Category find($id, $columns = ['*'])
 * @method Spot_Shipment_Category first($columns = ['*'])
*/
class Spot_Shipment_CategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Category::class;
    }
}
