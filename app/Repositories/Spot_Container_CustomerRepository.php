<?php

namespace App\Repositories;

use App\Models\Spot_Container_Customer;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Container_CustomerRepository
 * @package App\Repositories
 * @version February 24, 2021, 1:02 am WIB
 *
 * @method Spot_Container_Customer findWithoutFail($id, $columns = ['*'])
 * @method Spot_Container_Customer find($id, $columns = ['*'])
 * @method Spot_Container_Customer first($columns = ['*'])
*/
class Spot_Container_CustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'username',
        'password',
        'display_name',
        'email',
        'contact_email',
        'vat_number',
        'budget',
        'street',
        'country',
        'city',
        'postcode',
        'number',
        'box',
        'language',
        'clearance',
        'payment_term',
        'fiscal_representation',
        'storage_fee',
        'cost_24',
        'cost_48',
        'kg_price'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Container_Customer::class;
    }
}
