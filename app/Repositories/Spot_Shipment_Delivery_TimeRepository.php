<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Delivery_Time;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_Delivery_TimeRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:20 pm WIB
 *
 * @method Spot_Shipment_Delivery_Time findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Delivery_Time find($id, $columns = ['*'])
 * @method Spot_Shipment_Delivery_Time first($columns = ['*'])
*/
class Spot_Shipment_Delivery_TimeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'count'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Delivery_Time::class;
    }
}
