<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Handler;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_HandlerRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:21 pm WIB
 *
 * @method Spot_Shipment_Handler findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Handler find($id, $columns = ['*'])
 * @method Spot_Shipment_Handler first($columns = ['*'])
*/
class Spot_Shipment_HandlerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'handler_desc'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Handler::class;
    }
}
