<?php

namespace App\Repositories;

use App\Models\Spot_Container_Size;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Container_SizeRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:13 pm WIB
 *
 * @method Spot_Container_Size findWithoutFail($id, $columns = ['*'])
 * @method Spot_Container_Size find($id, $columns = ['*'])
 * @method Spot_Container_Size first($columns = ['*'])
*/
class Spot_Container_SizeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'size_desc'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Container_Size::class;
    }
}
