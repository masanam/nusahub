<?php

namespace App\Repositories;

use App\Models\Backend_User_Roles;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Backend_User_RolesRepository
 * @package App\Repositories
 * @version February 24, 2021, 1:44 am WIB
 *
 * @method Backend_User_Roles findWithoutFail($id, $columns = ['*'])
 * @method Backend_User_Roles find($id, $columns = ['*'])
 * @method Backend_User_Roles first($columns = ['*'])
*/
class Backend_User_RolesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'description',
        'permissions',
        'is_system'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Backend_User_Roles::class;
    }
}
