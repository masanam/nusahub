<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Break;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_BreakRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:43 pm WIB
 *
 * @method Spot_Shipment_Break findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Break find($id, $columns = ['*'])
 * @method Spot_Shipment_Break first($columns = ['*'])
*/
class Spot_Shipment_BreakRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'break_desc'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Break::class;
    }
}
