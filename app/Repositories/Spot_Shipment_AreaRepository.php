<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Area;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_AreaRepository
 * @package App\Repositories
 * @version February 25, 2021, 1:06 am WIB
 *
 * @method Spot_Shipment_Area findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Area find($id, $columns = ['*'])
 * @method Spot_Shipment_Area first($columns = ['*'])
*/
class Spot_Shipment_AreaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'city_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Area::class;
    }
}
