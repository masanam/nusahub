<?php

namespace App\Repositories;

use App\Models\Spot_Container_Location;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Container_LocationRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:11 pm WIB
 *
 * @method Spot_Container_Location findWithoutFail($id, $columns = ['*'])
 * @method Spot_Container_Location find($id, $columns = ['*'])
 * @method Spot_Container_Location first($columns = ['*'])
*/
class Spot_Container_LocationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'location_desc'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Container_Location::class;
    }
}
