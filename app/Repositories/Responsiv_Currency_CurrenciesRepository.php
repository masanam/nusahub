<?php

namespace App\Repositories;

use App\Models\Responsiv_Currency_Currencies;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Responsiv_Currency_CurrenciesRepository
 * @package App\Repositories
 * @version February 25, 2021, 1:23 am WIB
 *
 * @method Responsiv_Currency_Currencies findWithoutFail($id, $columns = ['*'])
 * @method Responsiv_Currency_Currencies find($id, $columns = ['*'])
 * @method Responsiv_Currency_Currencies first($columns = ['*'])
*/
class Responsiv_Currency_CurrenciesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'currency_code',
        'currency_symbol',
        'decimal_point',
        'thousand_separator',
        'place_symbol_before',
        'with_space',
        'is_enabled',
        'is_primary',
        'initbiz_money_fraction_digits'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Responsiv_Currency_Currencies::class;
    }
}
