<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Courier;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_CourierRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:18 pm WIB
 *
 * @method Spot_Shipment_Courier findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Courier find($id, $columns = ['*'])
 * @method Spot_Shipment_Courier first($columns = ['*'])
*/
class Spot_Shipment_CourierRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'address',
        'phone'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Courier::class;
    }
}
