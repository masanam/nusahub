<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Payment;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_PaymentRepository
 * @package App\Repositories
 * @version February 24, 2021, 1:17 am WIB
 *
 * @method Spot_Shipment_Payment findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Payment find($id, $columns = ['*'])
 * @method Spot_Shipment_Payment first($columns = ['*'])
*/
class Spot_Shipment_PaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'for_id',
        'movement',
        'other',
        'description',
        'amount',
        'status',
        'date',
        'item_id',
        'payment_type',
        'payment_with',
        'payment_method',
        'treasury_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Payment::class;
    }
}
