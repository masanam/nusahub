<?php

namespace App\Repositories;

use App\Models\Spot_Container_Order;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Container_OrderRepository
 * @package App\Repositories
 * @version February 24, 2021, 1:08 am WIB
 *
 * @method Spot_Container_Order findWithoutFail($id, $columns = ['*'])
 * @method Spot_Container_Order find($id, $columns = ['*'])
 * @method Spot_Container_Order first($columns = ['*'])
*/
class Spot_Container_OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'customer_address_id',
        'number',
        'bol',
        'location_type',
        'company_id',
        'destination',
        'reason_for_arrive',
        't1',
        'status_id',
        'cc',
        'eta_port',
        'eta_lgg',
        'loading_date',
        'inspected_port',
        'dp',
        'requested',
        'barcode',
        'custom_clearance',
        'fiscal_representation',
        'payment_term',
        'price_kg',
        'storage_fee',
        'cost_24',
        'cost_48'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Container_Order::class;
    }
}
