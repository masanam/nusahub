<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Car;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_CarRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:16 pm WIB
 *
 * @method Spot_Shipment_Car findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Car find($id, $columns = ['*'])
 * @method Spot_Shipment_Car first($columns = ['*'])
*/
class Spot_Shipment_CarRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'license_plate',
        'name',
        'destination_id',
        'transport_id',
        'transport_date',
        'enable',
        'driver_id',
        'responsible_id',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Car::class;
    }
}
