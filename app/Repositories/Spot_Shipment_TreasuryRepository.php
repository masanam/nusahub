<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Treasury;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_TreasuryRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:41 pm WIB
 *
 * @method Spot_Shipment_Treasury findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Treasury find($id, $columns = ['*'])
 * @method Spot_Shipment_Treasury first($columns = ['*'])
*/
class Spot_Shipment_TreasuryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'default',
        'office_id',
        'name',
        'balance'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Treasury::class;
    }
}
