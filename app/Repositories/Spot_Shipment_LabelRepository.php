<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Label;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_LabelRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:19 pm WIB
 *
 * @method Spot_Shipment_Label findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Label find($id, $columns = ['*'])
 * @method Spot_Shipment_Label first($columns = ['*'])
*/
class Spot_Shipment_LabelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'label_desc'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Label::class;
    }
}
