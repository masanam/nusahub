<?php

namespace App\Repositories;

use App\Models\Rainlab_Location_States;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Rainlab_Location_StatesRepository
 * @package App\Repositories
 * @version February 25, 2021, 12:53 am WIB
 *
 * @method Rainlab_Location_States findWithoutFail($id, $columns = ['*'])
 * @method Rainlab_Location_States find($id, $columns = ['*'])
 * @method Rainlab_Location_States first($columns = ['*'])
*/
class Rainlab_Location_StatesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'country_id',
        'name',
        'code'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rainlab_Location_States::class;
    }
}
