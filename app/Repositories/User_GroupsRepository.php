<?php

namespace App\Repositories;

use App\Models\User_Groups;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class User_GroupsRepository
 * @package App\Repositories
 * @version February 25, 2021, 1:16 am WIB
 *
 * @method User_Groups findWithoutFail($id, $columns = ['*'])
 * @method User_Groups find($id, $columns = ['*'])
 * @method User_Groups first($columns = ['*'])
*/
class User_GroupsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User_Groups::class;
    }
}
