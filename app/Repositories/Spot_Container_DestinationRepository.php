<?php

namespace App\Repositories;

use App\Models\Spot_Container_Destination;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Container_DestinationRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:47 pm WIB
 *
 * @method Spot_Container_Destination findWithoutFail($id, $columns = ['*'])
 * @method Spot_Container_Destination find($id, $columns = ['*'])
 * @method Spot_Container_Destination first($columns = ['*'])
*/
class Spot_Container_DestinationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'desc'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Container_Destination::class;
    }
}
