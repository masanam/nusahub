<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Mode;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_ModeRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:38 pm WIB
 *
 * @method Spot_Shipment_Mode findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Mode find($id, $columns = ['*'])
 * @method Spot_Shipment_Mode first($columns = ['*'])
*/
class Spot_Shipment_ModeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Mode::class;
    }
}
