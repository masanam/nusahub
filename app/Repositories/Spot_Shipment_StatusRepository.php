<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Status;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_StatusRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:40 pm WIB
 *
 * @method Spot_Shipment_Status findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Status find($id, $columns = ['*'])
 * @method Spot_Shipment_Status first($columns = ['*'])
*/
class Spot_Shipment_StatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'color',
        'equal'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Status::class;
    }
}
