<?php

namespace App\Repositories;

use App\Models\Responsiv_Pay_Invoice_Statuses;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Responsiv_Pay_Invoice_StatusesRepository
 * @package App\Repositories
 * @version February 25, 2021, 1:25 am WIB
 *
 * @method Responsiv_Pay_Invoice_Statuses findWithoutFail($id, $columns = ['*'])
 * @method Responsiv_Pay_Invoice_Statuses find($id, $columns = ['*'])
 * @method Responsiv_Pay_Invoice_Statuses first($columns = ['*'])
*/
class Responsiv_Pay_Invoice_StatusesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'is_enabled',
        'notify_user',
        'notify_template'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Responsiv_Pay_Invoice_Statuses::class;
    }
}
