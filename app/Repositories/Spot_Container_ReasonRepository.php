<?php

namespace App\Repositories;

use App\Models\Spot_Container_Reason;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Container_ReasonRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:11 pm WIB
 *
 * @method Spot_Container_Reason findWithoutFail($id, $columns = ['*'])
 * @method Spot_Container_Reason find($id, $columns = ['*'])
 * @method Spot_Container_Reason first($columns = ['*'])
*/
class Spot_Container_ReasonRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'reason_desc'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Container_Reason::class;
    }
}
