<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Order;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_OrderRepository
 * @package App\Repositories
 * @version February 24, 2021, 10:50 pm WIB
 *
 * @method Spot_Shipment_Order findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Order find($id, $columns = ['*'])
 * @method Spot_Shipment_Order first($columns = ['*'])
*/
class Spot_Shipment_OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'office_id',
        'number',
        'type',
        'sender_id',
        'sender_address_id',
        'receiver_id',
        'receiver_address_id',
        'sender_name',
        'sender_mobile',
        'sender_city',
        'sender_sector',
        'sender_addr',
        'receiver_name',
        'receiver_mobile',
        'receiver_city',
        'receiver_sector',
        'receiver_addr',
        'packaging_id',
        'ship_date',
        'receive_date',
        'courier_id',
        'delivery_time_id',
        'delivery_date',
        'mode_id',
        'status_id',
        'tax',
        'insurance',
        'currency_id',
        'payment_type',
        'customs_value',
        'courier_fee',
        'package_fee',
        'return_package_fee',
        'return_courier_fee',
        'return_defray',
        'manifest_id',
        'channel',
        'assigned_id',
        'requested',
        'barcode',
        'esign',
        'postponed',
        'delivered_by',
        'delivered_responsiable',
        'received_by',
        'airWayBill',
        'location',
        'cc',
        'transfer_jost',
        'label_id',
        'handler_id',
        'breakdown_id',
        'preAlert_received',
        'releasedNote_received',
        'remarks',
        'released_note',
        'custom_clearance',
        'fiscal_representation',
        'payment_term',
        'price_kg',
        'storage_fee',
        'cost_24',
        'cost_48'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Order::class;
    }
}
