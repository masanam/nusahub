<?php

namespace App\Repositories;

use App\Models\Spot_Container_Status;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Container_StatusRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:10 pm WIB
 *
 * @method Spot_Container_Status findWithoutFail($id, $columns = ['*'])
 * @method Spot_Container_Status find($id, $columns = ['*'])
 * @method Spot_Container_Status first($columns = ['*'])
*/
class Spot_Container_StatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'color',
        'description',
        'equal'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Container_Status::class;
    }
}
