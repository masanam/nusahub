<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_City;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_CityRepository
 * @package App\Repositories
 * @version February 25, 2021, 1:05 am WIB
 *
 * @method Spot_Shipment_City findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_City find($id, $columns = ['*'])
 * @method Spot_Shipment_City first($columns = ['*'])
*/
class Spot_Shipment_CityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'country_id',
        'state_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_City::class;
    }
}
