<?php

namespace App\Repositories;

use App\Models\Backend_User_Groups;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Backend_User_GroupsRepository
 * @package App\Repositories
 * @version February 24, 2021, 1:46 am WIB
 *
 * @method Backend_User_Groups findWithoutFail($id, $columns = ['*'])
 * @method Backend_User_Groups find($id, $columns = ['*'])
 * @method Backend_User_Groups first($columns = ['*'])
*/
class Backend_User_GroupsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'description',
        'is_new_user_default'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Backend_User_Groups::class;
    }
}
