<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Packaging;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_PackagingRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:39 pm WIB
 *
 * @method Spot_Shipment_Packaging findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Packaging find($id, $columns = ['*'])
 * @method Spot_Shipment_Packaging first($columns = ['*'])
*/
class Spot_Shipment_PackagingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Packaging::class;
    }
}
