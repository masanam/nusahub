<?php

namespace App\Repositories;

use App\Models\Spot_Shipment_Office;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Spot_Shipment_OfficeRepository
 * @package App\Repositories
 * @version February 24, 2021, 11:39 pm WIB
 *
 * @method Spot_Shipment_Office findWithoutFail($id, $columns = ['*'])
 * @method Spot_Shipment_Office find($id, $columns = ['*'])
 * @method Spot_Shipment_Office first($columns = ['*'])
*/
class Spot_Shipment_OfficeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'street',
        'county',
        'city',
        'state',
        'zipcode',
        'country',
        'lat',
        'lng',
        'url',
        'phone',
        'mobile',
        'mobile_2',
        'mobile_3',
        'manger_id',
        'responsible_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Spot_Shipment_Office::class;
    }
}
