<?php

namespace App\Repositories;

use App\Models\Rainlab_Location_Countries;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Rainlab_Location_CountriesRepository
 * @package App\Repositories
 * @version February 25, 2021, 12:52 am WIB
 *
 * @method Rainlab_Location_Countries findWithoutFail($id, $columns = ['*'])
 * @method Rainlab_Location_Countries find($id, $columns = ['*'])
 * @method Rainlab_Location_Countries first($columns = ['*'])
*/
class Rainlab_Location_CountriesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'is_enabled',
        'name',
        'code',
        'is_pinned'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rainlab_Location_Countries::class;
    }
}
