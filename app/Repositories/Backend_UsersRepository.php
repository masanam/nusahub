<?php

namespace App\Repositories;

use App\Models\Backend_Users;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class Backend_UsersRepository
 * @package App\Repositories
 * @version February 24, 2021, 1:45 am WIB
 *
 * @method Backend_Users findWithoutFail($id, $columns = ['*'])
 * @method Backend_Users find($id, $columns = ['*'])
 * @method Backend_Users first($columns = ['*'])
*/
class Backend_UsersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'login',
        'email',
        'password',
        'activation_code',
        'persist_code',
        'reset_password_code',
        'permissions',
        'is_activated',
        'role_id',
        'activated_at',
        'last_login',
        'is_superuser'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Backend_Users::class;
    }
}
