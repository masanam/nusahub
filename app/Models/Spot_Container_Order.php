<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Spot_Container_Order",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="customer_address_id",
 *          description="customer_address_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="number",
 *          description="number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="bol",
 *          description="bol",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_type",
 *          description="location_type",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="company_id",
 *          description="company_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="destination",
 *          description="destination",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="reason_for_arrive",
 *          description="reason_for_arrive",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="t1",
 *          description="t1",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status_id",
 *          description="status_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cc",
 *          description="cc",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="inspected_port",
 *          description="inspected_port",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="dp",
 *          description="dp",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="requested",
 *          description="requested",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="barcode",
 *          description="barcode",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="custom_clearance",
 *          description="custom_clearance",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="fiscal_representation",
 *          description="fiscal_representation",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_term",
 *          description="payment_term",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="price_kg",
 *          description="price_kg",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="storage_fee",
 *          description="storage_fee",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="cost_24",
 *          description="cost_24",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cost_48",
 *          description="cost_48",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Spot_Container_Order extends Model
{
    use SoftDeletes;

    public $table = 'spot_container_order';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer_id',
        'customer_address_id',
        'number',
        'bol',
        'location_type',
        'company_id',
        'destination',
        'reason_for_arrive',
        't1',
        'status_id',
        'cc',
        'eta_port',
        'eta_lgg',
        'loading_date',
        'inspected_port',
        'dp',
        'requested',
        'barcode',
        'custom_clearance',
        'fiscal_representation',
        'payment_term',
        'price_kg',
        'storage_fee',
        'cost_24',
        'cost_48'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'customer_address_id' => 'integer',
        'number' => 'string',
        'bol' => 'string',
        'location_type' => 'integer',
        'company_id' => 'integer',
        'destination' => 'integer',
        'reason_for_arrive' => 'integer',
        't1' => 'string',
        'status_id' => 'integer',
        'cc' => 'integer',
        'inspected_port' => 'integer',
        'dp' => 'string',
        'requested' => 'integer',
        'barcode' => 'integer',
        'custom_clearance' => 'integer',
        'fiscal_representation' => 'integer',
        'payment_term' => 'integer',
        'price_kg' => 'integer',
        'storage_fee' => 'boolean',
        'cost_24' => 'integer',
        'cost_48' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    
}
