<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Responsiv_Pay_Invoice_Statuses",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_enabled",
 *          description="is_enabled",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="notify_user",
 *          description="notify_user",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="notify_template",
 *          description="notify_template",
 *          type="string"
 *      )
 * )
 */
class Responsiv_Pay_Invoice_Statuses extends Model
{
    use SoftDeletes;

    public $table = 'responsiv_pay_invoice_statuses';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'code',
        'is_enabled',
        'notify_user',
        'notify_template'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'is_enabled' => 'boolean',
        'notify_user' => 'boolean',
        'notify_template' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    
}
