<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Spot_Shipment_Payment",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="for_id",
 *          description="for_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="movement",
 *          description="movement",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="other",
 *          description="other",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="amount",
 *          description="amount",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="date",
 *          description="date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="item_id",
 *          description="item_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_type",
 *          description="payment_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_with",
 *          description="payment_with",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_method",
 *          description="payment_method",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="treasury_id",
 *          description="treasury_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Spot_Shipment_Payment extends Model
{
    use SoftDeletes;

    public $table = 'spot_shipment_payment';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'type',
        'for_id',
        'movement',
        'other',
        'description',
        'amount',
        'status',
        'date',
        'item_id',
        'payment_type',
        'payment_with',
        'payment_method',
        'treasury_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'for_id' => 'integer',
        'movement' => 'integer',
        'other' => 'string',
        'description' => 'string',
        'status' => 'integer',
        'date' => 'date',
        'item_id' => 'integer',
        'payment_type' => 'string',
        'payment_with' => 'integer',
        'payment_method' => 'string',
        'treasury_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    
}
