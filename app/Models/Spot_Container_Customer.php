<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Spot_Container_Customer",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="username",
 *          description="username",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="display_name",
 *          description="display_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="contact_email",
 *          description="contact_email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="vat_number",
 *          description="vat_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="budget",
 *          description="budget",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="street",
 *          description="street",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="country",
 *          description="country",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="city",
 *          description="city",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="postcode",
 *          description="postcode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="number",
 *          description="number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="box",
 *          description="box",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="language",
 *          description="language",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="clearance",
 *          description="clearance",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_term",
 *          description="payment_term",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="fiscal_representation",
 *          description="fiscal_representation",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="storage_fee",
 *          description="storage_fee",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cost_24",
 *          description="cost_24",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cost_48",
 *          description="cost_48",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="kg_price",
 *          description="kg_price",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Spot_Container_Customer extends Model
{
    use SoftDeletes;

    public $table = 'spot_container_customer';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'username',
        'password',
        'display_name',
        'email',
        'contact_email',
        'vat_number',
        'budget',
        'street',
        'country',
        'city',
        'postcode',
        'number',
        'box',
        'language',
        'clearance',
        'payment_term',
        'fiscal_representation',
        'storage_fee',
        'cost_24',
        'cost_48',
        'kg_price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'username' => 'string',
        'password' => 'string',
        'display_name' => 'string',
        'email' => 'string',
        'contact_email' => 'string',
        'vat_number' => 'string',
        'budget' => 'integer',
        'street' => 'string',
        'country' => 'integer',
        'city' => 'integer',
        'postcode' => 'string',
        'number' => 'string',
        'box' => 'string',
        'language' => 'string',
        'clearance' => 'integer',
        'payment_term' => 'integer',
        'fiscal_representation' => 'integer',
        'storage_fee' => 'integer',
        'cost_24' => 'integer',
        'cost_48' => 'integer',
        'kg_price' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    
}
