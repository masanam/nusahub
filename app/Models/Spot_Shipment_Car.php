<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Spot_Shipment_Car",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="license_plate",
 *          description="license_plate",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="destination_id",
 *          description="destination_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="transport_id",
 *          description="transport_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="transport_date",
 *          description="transport_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="enable",
 *          description="enable",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="driver_id",
 *          description="driver_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="responsible_id",
 *          description="responsible_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      )
 * )
 */
class Spot_Shipment_Car extends Model
{
    use SoftDeletes;

    public $table = 'spot_shipment_car';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'license_plate',
        'name',
        'destination_id',
        'transport_id',
        'transport_date',
        'enable',
        'driver_id',
        'responsible_id',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'license_plate' => 'string',
        'name' => 'string',
        'destination_id' => 'integer',
        'transport_id' => 'integer',
        'transport_date' => 'date',
        'driver_id' => 'integer',
        'responsible_id' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    
}
