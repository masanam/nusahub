<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Backend_Users",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="login",
 *          description="login",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="activation_code",
 *          description="activation_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="persist_code",
 *          description="persist_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="reset_password_code",
 *          description="reset_password_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="permissions",
 *          description="permissions",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_activated",
 *          description="is_activated",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="role_id",
 *          description="role_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="is_superuser",
 *          description="is_superuser",
 *          type="boolean"
 *      )
 * )
 */
class Backend_Users extends Model
{
    use SoftDeletes;

    public $table = 'backend_users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'first_name',
        'last_name',
        'login',
        'email',
        'password',
        'activation_code',
        'persist_code',
        'reset_password_code',
        'permissions',
        'is_activated',
        'role_id',
        'activated_at',
        'last_login',
        'is_superuser'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'login' => 'string',
        'email' => 'string',
        'password' => 'string',
        'activation_code' => 'string',
        'persist_code' => 'string',
        'reset_password_code' => 'string',
        'permissions' => 'string',
        'is_activated' => 'boolean',
        'role_id' => 'integer',
        'is_superuser' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    
}
