<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Spot_Shipment_Order",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="office_id",
 *          description="office_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="number",
 *          description="number",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="sender_id",
 *          description="sender_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="sender_address_id",
 *          description="sender_address_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="receiver_id",
 *          description="receiver_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="receiver_address_id",
 *          description="receiver_address_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="sender_name",
 *          description="sender_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sender_mobile",
 *          description="sender_mobile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sender_city",
 *          description="sender_city",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="sender_sector",
 *          description="sender_sector",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="sender_addr",
 *          description="sender_addr",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="receiver_name",
 *          description="receiver_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="receiver_mobile",
 *          description="receiver_mobile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="receiver_city",
 *          description="receiver_city",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="receiver_sector",
 *          description="receiver_sector",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="receiver_addr",
 *          description="receiver_addr",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="packaging_id",
 *          description="packaging_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="ship_date",
 *          description="ship_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="receive_date",
 *          description="receive_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="courier_id",
 *          description="courier_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="delivery_time_id",
 *          description="delivery_time_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="delivery_date",
 *          description="delivery_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="mode_id",
 *          description="mode_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status_id",
 *          description="status_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="tax",
 *          description="tax",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="insurance",
 *          description="insurance",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="currency_id",
 *          description="currency_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_type",
 *          description="payment_type",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="customs_value",
 *          description="customs_value",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="courier_fee",
 *          description="courier_fee",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="package_fee",
 *          description="package_fee",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="return_package_fee",
 *          description="return_package_fee",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="return_courier_fee",
 *          description="return_courier_fee",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="return_defray",
 *          description="return_defray",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="manifest_id",
 *          description="manifest_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="channel",
 *          description="channel",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="assigned_id",
 *          description="assigned_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="requested",
 *          description="requested",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="barcode",
 *          description="barcode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="esign",
 *          description="esign",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="postponed",
 *          description="postponed",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="delivered_by",
 *          description="delivered_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="delivered_responsiable",
 *          description="delivered_responsiable",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="received_by",
 *          description="received_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="airWayBill",
 *          description="airWayBill",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location",
 *          description="location",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cc",
 *          description="cc",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="transfer_jost",
 *          description="transfer_jost",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="label_id",
 *          description="label_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="handler_id",
 *          description="handler_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="breakdown_id",
 *          description="breakdown_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="remarks",
 *          description="remarks",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="released_note",
 *          description="released_note",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="custom_clearance",
 *          description="custom_clearance",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="fiscal_representation",
 *          description="fiscal_representation",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_term",
 *          description="payment_term",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="price_kg",
 *          description="price_kg",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="storage_fee",
 *          description="storage_fee",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="cost_24",
 *          description="cost_24",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cost_48",
 *          description="cost_48",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Spot_Shipment_Order extends Model
{
    use SoftDeletes;

    public $table = 'spot_shipment_order';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'office_id',
        'number',
        'type',
        'sender_id',
        'sender_address_id',
        'receiver_id',
        'receiver_address_id',
        'sender_name',
        'sender_mobile',
        'sender_city',
        'sender_sector',
        'sender_addr',
        'receiver_name',
        'receiver_mobile',
        'receiver_city',
        'receiver_sector',
        'receiver_addr',
        'packaging_id',
        'ship_date',
        'receive_date',
        'courier_id',
        'delivery_time_id',
        'delivery_date',
        'mode_id',
        'status_id',
        'tax',
        'insurance',
        'currency_id',
        'payment_type',
        'customs_value',
        'courier_fee',
        'package_fee',
        'return_package_fee',
        'return_courier_fee',
        'return_defray',
        'manifest_id',
        'channel',
        'assigned_id',
        'requested',
        'barcode',
        'esign',
        'postponed',
        'delivered_by',
        'delivered_responsiable',
        'received_by',
        'airWayBill',
        'location',
        'cc',
        'transfer_jost',
        'label_id',
        'handler_id',
        'breakdown_id',
        'preAlert_received',
        'releasedNote_received',
        'remarks',
        'released_note',
        'custom_clearance',
        'fiscal_representation',
        'payment_term',
        'price_kg',
        'storage_fee',
        'cost_24',
        'cost_48'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'office_id' => 'integer',
        'number' => 'integer',
        'sender_id' => 'integer',
        'sender_address_id' => 'integer',
        'receiver_id' => 'integer',
        'receiver_address_id' => 'integer',
        'sender_name' => 'string',
        'sender_mobile' => 'string',
        'sender_city' => 'integer',
        'sender_sector' => 'integer',
        'sender_addr' => 'string',
        'receiver_name' => 'string',
        'receiver_mobile' => 'string',
        'receiver_city' => 'integer',
        'receiver_sector' => 'integer',
        'receiver_addr' => 'string',
        'packaging_id' => 'integer',
        'ship_date' => 'date',
        'receive_date' => 'date',
        'courier_id' => 'integer',
        'delivery_time_id' => 'integer',
        'delivery_date' => 'date',
        'mode_id' => 'integer',
        'status_id' => 'integer',
        'tax' => 'integer',
        'insurance' => 'integer',
        'currency_id' => 'integer',
        'manifest_id' => 'integer',
        'channel' => 'string',
        'assigned_id' => 'integer',
        'requested' => 'integer',
        'barcode' => 'string',
        'esign' => 'string',
        'postponed' => 'boolean',
        'delivered_by' => 'integer',
        'delivered_responsiable' => 'integer',
        'received_by' => 'string',
        'airWayBill' => 'string',
        'location' => 'string',
        'cc' => 'boolean',
        'transfer_jost' => 'boolean',
        'label_id' => 'integer',
        'handler_id' => 'integer',
        'breakdown_id' => 'integer',
        'remarks' => 'string',
        'released_note' => 'string',
        'custom_clearance' => 'integer',
        'fiscal_representation' => 'integer',
        'payment_term' => 'integer',
        'price_kg' => 'integer',
        'storage_fee' => 'boolean',
        'cost_24' => 'integer',
        'cost_48' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    
}
