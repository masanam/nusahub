<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Responsiv_Currency_Currencies",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="currency_code",
 *          description="currency_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="currency_symbol",
 *          description="currency_symbol",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="decimal_point",
 *          description="decimal_point",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="thousand_separator",
 *          description="thousand_separator",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="place_symbol_before",
 *          description="place_symbol_before",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="with_space",
 *          description="with_space",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_enabled",
 *          description="is_enabled",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_primary",
 *          description="is_primary",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="initbiz_money_fraction_digits",
 *          description="initbiz_money_fraction_digits",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Responsiv_Currency_Currencies extends Model
{
    use SoftDeletes;

    public $table = 'responsiv_currency_currencies';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'currency_code',
        'currency_symbol',
        'decimal_point',
        'thousand_separator',
        'place_symbol_before',
        'with_space',
        'is_enabled',
        'is_primary',
        'initbiz_money_fraction_digits'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'currency_code' => 'string',
        'currency_symbol' => 'string',
        'decimal_point' => 'string',
        'thousand_separator' => 'string',
        'place_symbol_before' => 'boolean',
        'with_space' => 'boolean',
        'is_enabled' => 'boolean',
        'is_primary' => 'boolean',
        'initbiz_money_fraction_digits' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    
}
